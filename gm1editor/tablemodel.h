#pragma once

#include <memory>

#include <QAbstractItemModel>

#include "abstractrecord.h"
#include "abstracttable.h"

namespace gmtool {

class TableModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit TableModel(std::unique_ptr<AbstractTable> table, QObject *parent = 0);

    ~TableModel() override = default;

    /// Reset current table in the model causing all views to reset aswell.
    void reset(std::unique_ptr<AbstractTable> table);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    bool moveRows(const QModelIndex &sourceParent,
                  int sourceRow,
                  int count,
                  const QModelIndex &destinationParent,
                  int destinationChild) override;

    RecordId getIdOf(int row) const;

    bool assignRow(int row, const AbstractRecord &record);

    bool assignRecord(const RecordId &id, const AbstractRecord &record);

    const AbstractRecord *getRecord(const RecordId &id) const;

    template<typename RecordT>
    const RecordT* getRecordAs(const RecordId &id) const
    {
        return dynamic_cast<const RecordT *>(getRecord(id));
    }

    template<typename RecordT>
    const RecordT* getRecordAt(int row) const
    {
        return dynamic_cast<const RecordT *>(getRecord(getIdOf(row)));
    }

    int getPositionOf(const RecordId &id) const;

private:
    std::unique_ptr<AbstractTable> mTable;

    TableModel(const TableModel &) = delete;
    TableModel &operator=(const TableModel &) = delete;
};

} // namespace gmtool
