#pragma once

#include <QColor>
#include <QImage>

#include <boost/filesystem/path.hpp>

#include <gm1.h>

#include "gm1filedata.h"

namespace gmtool {

class Gm1Writer : public QObject
{
    Q_OBJECT
public:
    explicit Gm1Writer(QObject *parent = nullptr);

    bool isNull() const;

    bool saveToFile(const boost::filesystem::path &filepath);

    void setFileData(const Gm1FileData &fileData);

signals:
    void progress(int current, int total);
    void diagnosticsError(const QString &message);
    void diagnosticsWarning(const QString &message);

private:
    QImage entryImage(size_t index) const;

    void emitError(const char *format, ...);
    void emitWarning(const char *format, ...);

private:
    Gm1FileData mFileData;
};

} // namespace gmtool
