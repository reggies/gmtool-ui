#!/usr/bin/env python
# -*- coding: utf-8-auto -*-

letters=[u"🅰", u"🅱", u"🅲", u"🅳", u"🅴", u"🅵", u"🅶", u"🅷", u"🅸", u"🅹", u"🅺", u"🅻", u"🅼", u"🅽", u"🅾", u"🅿", u"🆀", u"🆁", u"🆂", u"🆃", u"🆄", u"🆅", u"🆆", u"🆇", u"🆈", u"🆉"]

for letter in letters:
    filename = "letter_" + chr(ord(letter) - ord(u"🅰") + ord('a')) + ".svg"
    with open(filename, "w") as f:
        f.write(u'<svg width="24" height="24" viewBox="0 10 11 11" xmlns="http://www.w3.org/2000/svg">')
        f.write(u'  <style>')
        f.write(u'    .heavy { font: 24px sans-serif; fill: black; background: none; }')
        f.write(u'</style>')
        f.write(u'<text x="0" y="20" class="heavy">')
        f.write(letter.encode('utf-8', 'ignore'))
        f.write(u'</text>')
        f.write(u'</svg>')
