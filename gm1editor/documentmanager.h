#pragma once

#include <QObject>

#include <set>

namespace gmtool {

class Document;

class DocumentManager : public QObject
{
    Q_OBJECT

public:
    explicit DocumentManager(QObject *parent = 0);
    ~DocumentManager();

    Document *createDocument();
    void removeDocument(Document *document);

private:
    std::set<Document *> mDocuments;
    ///< owned by this

    QByteArray mAppPath;
    /// \todo store this to properly initialize python path with a "constant"

    int mNextDocumentNumber = 0;

    bool mInitialized = false;

    DocumentManager &operator=(const DocumentManager &) = delete;
    DocumentManager(const DocumentManager &) = delete;
};

} // namespace gmtool
