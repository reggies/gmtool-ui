# Overview #

User interface for viewing and manipulating Stronghold's GM1 data archives.

# Dependencies #

    - Qt5
    - Boost
      - filesystem
      - program_options
      - optional
      - ranges
      - format
    - libgm1 (https://bitbucket.org/reggies/libgm1)

# Build #

    - Build on Windows has been tested with MSVC 2015
    - Build on Linux has been tested with GCC 4.9

# Screenshots

![Main Window](/media/mainwindow.png)
