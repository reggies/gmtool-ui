#pragma once

#include <set>

#include "documenteditor.h"
#include "fontrecord.h"
#include "gm1filedata.h"
#include "recordset.h"
#include "spriterecord.h"

namespace gmtool {

class FontEditor : public DocumentEditor
{
public:
    FontEditor();
    ~FontEditor() override;

    PropertiesModel *getPropertiesModel() override;
    TableModel *getModel(TableId table) override;
    std::unique_ptr<AbstractRecord> createBlankEntry() override;
    std::unique_ptr<NodeFactory> createLayoutFactory() override;
    std::unique_ptr<NodeFactory> createSpriteFactory() override;
    std::unique_ptr<SpriteLoader> createSpriteLoader() override;
    bool load(const Gm1FileData &fileData, LogModel *log) override;
    bool save(Gm1FileData *fileData, LogModel *log) override;
    bool renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result) override;
    std::unique_ptr<AnimationFactory> createAnimationFactory() override;

private:
    void setupEntriesTableModel();
    void setupSpritesTableModel();

private:
    RecordSet<FontRecord> mEntries;
    RecordSet<SpriteRecord> mSprites;

    std::unique_ptr<TableModel> mEntriesTableModel;
    std::unique_ptr<TableModel> mSpritesTableModel;
    std::unique_ptr<PropertiesModel> mPropertiesModel;

    Gm1FileData mGm1;

private:
    FontEditor(const FontEditor &) = delete;
    FontEditor &operator=(const FontEditor &) = delete;
};

} // namespace gmtool
