#include "repeaternode.h"

#include <set>
#include <boost/range/adaptor/reversed.hpp>

#include "removecommand.h"

namespace gmtool {

RepeaterNode::RepeaterNode(TableModel *table,
                           std::unique_ptr<NodeFactory> nodeFactory,
                           QGraphicsObject *parent)
    : QGraphicsObject(parent)
    , mTable(table)
    , mNodeFactory(std::move(nodeFactory))
{
    setFlag(QGraphicsItem::ItemHasNoContents, true);
    for (int i = 0; i < mTable->rowCount(); ++i) {
        auto item = mNodeFactory->createNode(mTable->getIdOf(i));
        if (item != nullptr) {
            item->setParentItem(this);
            item->setZValue(i);
        }
    }

    connect(mTable, &QAbstractItemModel::rowsInserted, this, &RepeaterNode::itemsInserted);
    connect(mTable,
            &QAbstractItemModel::rowsAboutToBeRemoved,
            this,
            &RepeaterNode::itemsAboutToBeRemoved);
    connect(mTable, &QAbstractItemModel::modelReset, this, &RepeaterNode::itemsReset);
    connect(mTable, &QAbstractItemModel::rowsMoved, this, &RepeaterNode::itemsMoved);
}

void RepeaterNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
}

void RepeaterNode::allowSelection(bool allow)
{
    for (QGraphicsItem *item : childItems()) {
        item->setFlag(QGraphicsItem::ItemIsSelectable, allow);
    }
}

void RepeaterNode::selectAll()
{
    for (QGraphicsItem *item : childItems()) {
        item->setSelected(true);
    }
}

void RepeaterNode::unselectAll()
{
    for (QGraphicsItem *item : childItems()) {
        item->setSelected(false);
    }
}

void RepeaterNode::setViewState(const ViewState &state)
{
    mViewState.reset(new ViewState(state));
    for (QGraphicsItem *item : childItems()) {
        static_cast<SceneNode *>(item)->setViewState(state);
    }
}

void RepeaterNode::setPropogatePaletteChanges(bool propogatePaletteChanges)
{
    mPropogatePaletteChanges = propogatePaletteChanges;
}

void RepeaterNode::invalidatePalette()
{
    if (mPropogatePaletteChanges) {
        for (QGraphicsItem *item : childItems()) {
            static_cast<SceneNode *>(item)->invalidatePalette();
        }
    }
}

void RepeaterNode::removeNodes(int first, int last)
{
    for (int i = first; i <= last; ++i) {
        auto removedId = mTable->getIdOf(i);
        for (auto child : childItems()) {
            auto childId = child->data(NodeAttribute::RecordId).value<RecordId>();
            if (childId == removedId) {
                delete child;
            }
        }
    }
}

void RepeaterNode::insertNodes(int first, int last)
{
    for (int i = first; i <= last; ++i) {
        auto item = mNodeFactory->createNode(mTable->getIdOf(i));
        if (item == nullptr) {
            qFatal("impossible");
        }
        item->setZValue(0);
        item->setParentItem(this);
        if (mViewState) {
            item->setViewState(*mViewState);
        }
    }

    for (auto child : childItems()) {
        auto id = child->data(NodeAttribute::RecordId).value<RecordId>();
        child->setZValue(mTable->getPositionOf(id));
    }
}

void RepeaterNode::moveNodes(int start, int end, int row)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    Q_UNUSED(row);

    for (auto child : childItems()) {
        auto id = child->data(NodeAttribute::RecordId).value<RecordId>();
        child->setZValue(mTable->getPositionOf(id));
    }
}

std::vector<RecordId> RepeaterNode::selectedIds() const
{
    auto nodes = selectedNodes();
    std::vector<RecordId> ids;
    ids.reserve(nodes.size());
    for (auto node : nodes) {
        auto id = node->data(NodeAttribute::RecordId).value<RecordId>();
        if (id.isValid()) {
            auto table = node->data(NodeAttribute::TableId).value<TableId>();
            if (table == TableId::Spritesheet) {
                ids.push_back(id);
            }
        }
    }
    return ids;
}

std::vector<SceneNode *> RepeaterNode::selectedNodes() const
{
    std::vector<SceneNode *> nodes;
    auto items = childItems();
    nodes.reserve(items.size());
    for (auto item : items) {
        if (item->isSelected()) {
            if (auto node = qgraphicsitem_cast<SceneNode *>(item)) {
                if (!node->data(NodeAttribute::RecordId).isNull()
                    && !node->data(NodeAttribute::TableId).isNull()) {
                    nodes.push_back(node);
                }
            }
        }
    }
    return nodes;
}

std::vector<int> RepeaterNode::selectedIndices() const
{
    auto nodes = selectedNodes();
    std::vector<int> indices;
    indices.reserve(nodes.size());
    for (auto node : nodes) {
        auto id = node->data(NodeAttribute::RecordId).value<RecordId>();
        if (id.isValid()) {
            indices.push_back(mTable->getPositionOf(id));
        }
    }
    return indices;
}

int RepeaterNode::selectedCount() const
{
    return selectedNodes().size();
}

void RepeaterNode::removeSelectedNodes(undo::Macro *macro)
{
    auto unsorted = selectedIndices();
    std::set<int> sorted(unsorted.begin(), unsorted.end());
    for (int row : sorted | boost::adaptors::reversed) {
        macro->add(std::make_unique<RemoveCommand>(row, mTable));
    }
}

void RepeaterNode::itemsInserted(const QModelIndex &parent, int first, int last)
{
    insertNodes(first, last);
}

void RepeaterNode::itemsAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    removeNodes(first, last);
}

void RepeaterNode::itemsReset()
{
    for(auto child : childItems()) {
        delete child;
    }
    if (mTable->rowCount() > 0) {
        insertNodes(0, mTable->rowCount() - 1);
    }
}

void RepeaterNode::itemsMoved(
    const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row)
{
    moveNodes(start, end, row);
}

}
