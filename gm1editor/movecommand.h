#pragma once

#include "undo.h"

#include <QAbstractItemModel>

namespace gmtool {

class MoveCommand : public undo::Command
{
public:
    MoveCommand(QAbstractItemModel *model, int row, int count, int dest);

    ~MoveCommand() override;

    bool undo() override;
    bool redo() override;

    QString name() const override;

private:
    QAbstractItemModel *mModel;
    int mRow;
    int mCount;
    int mDest;
};

} // namespace gmtool
