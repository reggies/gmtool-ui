#pragma once

#include <QVariant>
#include <QtGlobal>

#include "abstractrecord.h"
#include "recordid.h"

namespace gmtool {

/// Should be used only by the TableModel.
struct AbstractTable
{
    virtual ~AbstractTable() {}
    virtual bool setData(int row, int col, const QVariant &value) = 0;
    virtual QVariant displayData(int row, int col) const = 0;
    virtual QVariant editData(int row, int col) const = 0;
    virtual int rowCount() const = 0;
    virtual int columnCount() const = 0;
    virtual Qt::ItemFlags flags(int column) const = 0;
    virtual QVariant headerData(int column) const = 0;
    virtual bool insertRows(int row, int count) = 0;
    virtual bool removeRows(int row, int count) = 0;
    virtual RecordId getRecordIdByRow(int row) const = 0;
    virtual void moveRows(int source, int count, int dest) = 0;
    virtual void setRecord(int row, const AbstractRecord &record) = 0;
    virtual const AbstractRecord *getRecord(const RecordId &id) const = 0;
    virtual AbstractRecord *getRecord(const RecordId &id) = 0;
};

} // namespace gmtool
