#pragma once

#include <cmath>

#include <QColor>
#include <QPoint>
#include <QRect>

inline static QPoint floorPoint(const QPointF &point)
{
    return QPoint(std::floor(point.x()), std::floor(point.y()));
}

inline static QPointF clipPoint(const QPointF &point, const QRectF &rect)
{
    if (!rect.contains(point)) {
        QPointF newPoint;
        newPoint.setX(qMin(rect.right(), qMax(point.x(), rect.left())));
        newPoint.setY(qMin(rect.bottom(), qMax(point.y(), rect.top())));
        return newPoint;
    }
    return point;
}

inline const QColor operator-(const QColor &lhs, const QColor &rhs)
{
    return QColor(lhs.red() - rhs.red(), lhs.green() - rhs.green(),
                  lhs.blue() - rhs.blue());
}

inline const QColor operator+(const QColor &lhs, const QColor &rhs)
{
    return QColor(lhs.red() + rhs.red(), lhs.green() + rhs.green(),
                  lhs.blue() + rhs.blue());
}
