#pragma once

#include "abstractrecord.h"
#include "recordid.h"
#include "animationframegroup.h"
#include "tablemodel.h"

#include <memory>

#include <QString>
#include <QPoint>

namespace gmtool {
class FramesModel;

struct FramesRecordData
{
    void createImagesAndMasks(AnimationGroupModel   *groupModel,
                              SpriteModel           *spriteModel,
                              AnimationModel        *animationModel);

    bool clean = false;
    QPoint pivot;
    AnimationFrameGroupRecordData group;
    std::vector<QImage> images;
    std::vector<QBitmap> masks;
};

class FramesRecord : public QObject
{
    Q_OBJECT

public:
    explicit FramesRecord(FramesModel *model, const RecordId &frameGroupId, QObject *parent = nullptr)
        : QObject(parent)
        , mModel(model)
        , mFrameGroupId(frameGroupId)
    {}

    RecordId frameGroupId() const { return mFrameGroupId; }

    void createImagesAndMasks() const;

    void markDirty();

    QString toString() const;

    AnimationFrameGroupRecordData frameGroup() const;

    std::vector<QImage> images() const;
    std::vector<QBitmap> masks() const;
    QPoint pivot() const;

signals:
    void changed();

private:
    FramesModel *mModel;
    RecordId mFrameGroupId;

    /// Is this a Cache?
    mutable FramesRecordData mData;
};

} // namespace gmtool
