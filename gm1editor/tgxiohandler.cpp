#include "tgxiohandler.h"

#include <QImage>

#include <gm1.h>

namespace gmtool {

static gm1_size_t readFromGm1(void *userdata, gm1_u8_t *buffer, gm1_size_t count)
{
    QIODevice *dev = static_cast<QIODevice *>(userdata);
    return dev->read(reinterpret_cast<char *>(buffer), count);
}

static gm1_size_t writeToGm1(void *userdata, const gm1_u8_t *buffer, gm1_size_t count)
{
    QIODevice *dev = static_cast<QIODevice *>(userdata);
    return dev->write(reinterpret_cast<const char *>(buffer), count);
}

TgxIoHandler::TgxIoHandler() {}

TgxIoHandler::~TgxIoHandler() {}

bool TgxIoHandler::canRead() const
{
    return device() && device()->bytesAvailable() >= 8;
}

bool TgxIoHandler::read(QImage *image)
{
    if (device() == nullptr) {
        return false;
    }

    gm1_istream_t inputStream = {};
    inputStream.UserData = device();
    inputStream.Read = readFromGm1;

    gm1_status_t status;

    gm1_gfx_header_t header = {};
    status = gm1_read_gfx_header(&inputStream, &header);
    if (gm1_error(status)) {
        return false;
    }

    if (header.Width == 0 || header.Height == 0) {
        return false;
    }

    QImage buffer(header.Width, header.Height, QImage::Format_RGB555);
    if (buffer.isNull()) {
        return false;
    }

    buffer.fill(0);

    gm1_image_props_t props = {};
    gm1_get_gfx_image_props(&props);
    gm1_image_t desc = {};
    desc.Width = buffer.width();
    desc.Height = buffer.height();
    desc.BytesPerPixel = buffer.depth() / 8;
    desc.BytesPerRow = buffer.bytesPerLine();
    desc.Pixels = buffer.bits();
    desc.ColorKey = reinterpret_cast<gm1_u8_t *>(&props.ColorKey);
    status = gm1_read_gfx_image(&inputStream, &desc);
    if (gm1_error(status)) {
        return false;
    }

    *image = buffer;
    return true;
}

bool TgxIoHandler::write(const QImage &image)
{
    if (device() == nullptr || image.isNull() || image.width() == 0 || image.height() == 0) {
        return false;
    }

    gm1_ostream_t outputStream = {};
    outputStream.UserData = device();
    outputStream.Write = writeToGm1;

    gm1_status_t status;

    gm1_gfx_header_t header = {};
    header.Width = image.width();
    header.Height = image.height();
    status = gm1_write_gfx_header(&outputStream, &header);
    if (gm1_error(status)) {
        return false;
    }

    QImage buffer = image.convertToFormat(QImage::Format_RGB555);
    if (buffer.isNull()) {
        return false;
    }

    gm1_image_props_t props = {};
    gm1_get_gfx_image_props(&props);
    gm1_image_t desc = {};
    desc.Width = buffer.width();
    desc.Height = buffer.height();
    desc.BytesPerPixel = buffer.depth() / 8;
    desc.BytesPerRow = buffer.bytesPerLine();
    desc.Pixels = const_cast<gm1_u8_t *>(buffer.constBits());
    desc.ColorKey = reinterpret_cast<gm1_u8_t *>(&props.ColorKey);
    status = gm1_write_gfx_image(&outputStream, &desc);
    if (gm1_error(status)) {
        return false;
    }

    return true;
}

} // namespace gmtool
