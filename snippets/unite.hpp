#pragma once

#include <QAbstractItemModel>
#include <QAbstractListModel>
#include <QAbstractTableModel>
#include <QAction>
#include <QBitmap>
#include <QCheckBox>
#include <QCloseEvent>
#include <QColor>
#include <QComboBox>
#include <QDateTime>
#include <QDebug>
#include <QDialog>
#include <QGraphicsItem>
#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGroupBox>
#include <QImage>
#include <QImageIOHandler>
#include <QImageIOPlugin>
#include <QLabel>
#include <QLine>
#include <QLineEdit>
#include <QList>
#include <QListView>
#include <QMainWindow>
#include <QMetaType>
#include <QMouseEvent>
#include <QObject>
#include <QPaintEvent>
#include <QPlainTextEdit>
#include <QPoint>
#include <QPointer>
#include <QProgressBar>
#include <QPushButton>
#include <QRect>
#include <QRegion>
#include <QSortFilterProxyModel>
#include <QString>
#include <QStringList>
#include <QStyledItemDelegate>
#include <QSyntaxHighlighter>
#include <QTabWidget>
#include <QTableView>
#include <QToolBar>
#include <QTreeView>
#include <QVariant>
#include <QWidget>
#include <QtGlobal>

#include <cstdint>

#include <algorithm>
#include <array>
#include <functional>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/filesystem/path.hpp>
#include <boost/optional.hpp>
#include <boost/python.hpp>
#include <boost/python/exec.hpp>

#include <frameobject.h>

#include <gm1.h>

namespace gmtool {

class AbstractRecord;
class AnimationEditor;
class AnimationFrameGroup;
class AnimationGraphicsItem;
class AnimationLoadDialog;
class AnimationNode;
class AnimationPivotGraphicsItem;
class AnimationPreviewItem;
class AnimationRecord;
class AnimationRecordData;
class AnimationSpriteLoader;
class AssignCommand;
class BasicSpriteLoader;
class BitmapEditor;
class BitmapGraphicsItem;
class BitmapPivotGraphicsItem;
class BitmapRecord;
class BitmapRecordData;
class ColorEdit;
class ColorTableWindow;
class ColorsArrayWidget;
class Command;
class CommandItemDelegate;
class ConstFilePath;
class ConstHeight;
class ConstName;
class ConstWidth;
class CreateDocumentDialog;
class Document;
class DocumentEditor;
class DocumentLoader;
class DocumentManager;
class DocumentOperation;
class DocumentTypesModel;
class DocumentWriter;
class Editor;
class EditorFactory;
class EmptyNode;
class Engine;
class EntryTableWindow;
class FontEditor;
class FontGraphicsItem;
class FontRecord;
class FontRecordData;
class Frame;
class FrameManager;
class FramesModel;
class FramesRecord;
class Gm1Animation;
class Gm1FileData;
class Gm1Loader;
class Gm1Writer;
class GridGraphicsView;
class GridScene;
class ISpriteSheet;
class InsertCommand;
class LogMessage;
class LogModel;
class LogViewer;
class Macro;
class ModelTracker;
class ModifyCommand;
class MoveCommand;
class MutableBoxHeight;
class MutableFontBearing;
class MutableHeight;
class MutableLeft;
class MutableTileSize;
class MutableTop;
class MutableWidth;
class MyColumn;
class Navigator;
class NavigatorPopup;
class Operation;
class Palette;
class PaletteComboBox;
class PaletteDialog;
class PaletteItemDelegate;
class PalettePreview;
class PaletteRecord;
class PaletteRecordData;
class PivotRecord;
class PlotWindow;
class PreviewWindow;
class PropertiesModel;
class PropertiesWindow;
class PropertyItem;
class Ratio;
class RecordId;
class RecordModelHelper;
class RemoveCommand;
class RepeaterNode;
class Repl;
class ReplHighlighter;
class ReplWindow;
class SceneBuilder;
class SceneNode;
class SeverityFilterModel;
class SimpleSpriteSheet;
class Sprite;
class SpriteGraphicsItem;
class SpriteLoader;
class SpritePlotter;
class SpriteRecord;
class SpriteRecordData;
class SpritesWindow;
class Stack;
class TableAdapter;
class TableColumn;
class TableModel;
class TgxIoHandler;
class TgxPlugin;
class TileEditor;
class TileGraphicsItem;
class TileRecord;
class TileRecordData;
class TransformCommand;
class TransformController;
class ViewState;
class Window;
class World;
class ZoomComboBox;
class ZoomModel;
struct AbstractTable;
struct AddEntry;
struct AllowSelectSprites;
struct AnimationFrameGroupRecordData;
struct BringForward;
struct ChangeHue;
struct ExportSprites;
struct FindSpriteOrEntry;
struct FramesRecordData;
struct Gm1FileData;
struct Gm1FileEntry;
struct IAnimationControl;
struct IAnimationSequence;
struct IAssignRecord;
struct IAssignRow;
struct IEntryFactory;
struct IGm1Loader;
struct IGm1Writer;
struct IPropertyGet;
struct IPropertyName;
struct IPropertySet;
struct ISceneNode;
struct ISpriteLoaderFactory;
struct ISpriteRenderer;
struct ImportSprites;
struct Item;
struct LoadPalette;
struct MoveBackward;
struct MoveForward;
struct NodeAttribute;
struct RemoveSpriteOrEntry;
struct SavePalette;
struct ScrollMode;
struct SelectAll;
struct SelectClear;
struct SendBack;
struct ShowGrid;
struct ShowOverlay;
struct SnapToGrid;
struct SourceImage;
struct SpriteDescriptor;
struct SubTile;
struct TileBox;
struct TransformMode;

enum class DocumentType { Font, Anim, Static, Tile, Bitmap, Invalid };
enum class Gm1Editor { Anim, Tiles, Bitmaps, Font };
enum class InteractionMode {Scroll, Transform, Pick,};
enum class Severity { Error = 1, Warning = 2, Information = 3 };
enum class TableId {Animation, Font, Tile, Bitmap};
enum class TileAlignment {Left, Right, Center, None};
enum class TransparencyMode {None, Color, Alpha};

enum {
    SpriteGraphicsItemType = QGraphicsItem::UserType + 1,
};


class AbstractRecord : public QObject
{
    Q_OBJECT
public:
    explicit AbstractRecord(QObject *parent = nullptr)
        : QObject(parent)
    {}
    virtual ~AbstractRecord() {}
    virtual std::unique_ptr<AbstractRecord> clone() const = 0;

    /// - Used by AssignCommand for item movement and palette importing
    /// - Used by InsertCommand
    /// - Used by RemoveCommand
    /// - Used by RecordSet to insert records without copying QObject
    virtual void assign(const AbstractRecord &that) = 0;
};

/// Should be used only by the TableModel.
struct AbstractTable
{
    virtual ~AbstractTable() {}
    virtual bool setData(int row, int col, const QVariant &value) = 0;
    virtual QVariant displayData(int row, int col) const = 0;
    virtual QVariant editData(int row, int col) const = 0;
    virtual int rowCount() const = 0;
    virtual int columnCount() const = 0;
    virtual Qt::ItemFlags flags(int column) const = 0;
    virtual QVariant headerData(int column) const = 0;
    virtual bool insertRows(int row, int count) = 0;
    virtual bool removeRows(int row, int count) = 0;
    virtual RecordId getRecordIdByRow(int row) const = 0;
    virtual void moveRows(int source, int count, int dest) = 0;
    virtual const AbstractRecord *getRecord(const RecordId &id) const = 0;
    virtual AbstractRecord *getRecord(const RecordId &id) = 0;
};

void makeArcherAnimationGroups(RecordSet<AnimationFrameGroup> &groups);

class AnimationEditor : public DocumentEditor
{
public:
    AnimationEditor();
    ~AnimationEditor() override;

    std::unique_ptr<AbstractRecord> createBlankEntry() override;

    bool load(const Gm1FileData &fileData, LogModel *log) override;
    bool save(Gm1FileData *fileData, LogModel *log) override;

    std::unique_ptr<SpriteLoader> createSpriteLoader() override;

    bool renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result) override;

    QPoint pivot() const;

    TableId getEntryTableId() const override { return TableId::Animation; }

private:
    AnimationEditor(const AnimationEditor &) = delete;
    AnimationEditor &operator=(const AnimationEditor &) = delete;
};

struct AnimationFrameGroupRecordData
{
    AnimationFrameGroupRecordData() = default;
    AnimationFrameGroupRecordData(
        const QString &name, int frameStart, int frameStep, int frameCount, bool backAndForth)
        : name(name)
        , frameStart(frameStart)
        , frameStep(frameStep)
        , frameCount(frameCount)
        , backAndForth(backAndForth)
    {}

    AnimationFrameGroupRecordData(const AnimationFrameGroupRecordData &) = default;
    AnimationFrameGroupRecordData &operator=(const AnimationFrameGroupRecordData &) = default;

    bool indexInGroup(int group) const {
        if (frameCount == 0 || frameStep == 0) {
            return false;
        } else {
            Q_ASSERT(frameStep > 0);
            return
                (group - frameStart) % frameStep == 0 &&
                (group - frameStart) / frameStep < frameCount;
        }
    }

    QString name;
    int frameStart = 0;
    int frameStep = 0;
    int frameCount = 1;
    bool backAndForth = false;

    QString toString() const {
        return name;
    }
};

class AnimationFrameGroup : public AbstractRecord, public AnimationFrameGroupRecordData
{
    Q_OBJECT

public:
    explicit AnimationFrameGroup(const AnimationFrameGroupRecordData &data = {},
                                 QObject *parent = nullptr)
        : AbstractRecord(parent)
        , AnimationFrameGroupRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new AnimationFrameGroup(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        AnimationFrameGroupRecordData::operator=(dynamic_cast<const AnimationFrameGroupRecordData &>(other));
    }
};

class AnimationGraphicsItem : public SceneNode
{
public:
    explicit AnimationGraphicsItem(AnimationRecord *record, SceneNode *parent = nullptr);

    void setViewState(const ViewState &state) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    void moveTo(undo::Macro *macro, const QPoint &toPoint) override;

private:
    void positionChanged();

private:
    AnimationRecord *mRecord;
};


class AnimationLoadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AnimationLoadDialog(AnimationEditor *editor,
                                 const std::vector<boost::filesystem::path> &loadPaths,
                                 QWidget *parent = nullptr);
    virtual ~AnimationLoadDialog();

    std::vector<SpriteRecordData> sprites() const;

private:
    QVector<QRgb> activeColorTable() const;

    void rebuild();
    void prepareOriginal();

    void pickColor();

private:
    AnimationEditor *mEditor;

    GridScene *mScene;
    QCheckBox *mCheckDither;
    QGroupBox *mCheckMatchPalette;
    QComboBox *mBoxPalettes;

    QGroupBox *mCheckZeroIndexTransparency;
    QGroupBox *mCheckOverrideTransparency;

    GridGraphicsView *mView;
    ColorEdit *mTransparentColor;
    EmptyNode *mLayer;

    GridGraphicsView *mOriginalView;
    GridScene *mOriginalScene;

    struct SourceImage
    {
        SourceImage(const boost::filesystem::path &path, const QImage &image, const QPoint &topLeft)
            : path(path)
            , image(image)
            , topLeft(topLeft)
        {}
        boost::filesystem::path path;
        QImage image;
        QPoint topLeft;
    };

    std::vector<SourceImage> mImages;

    std::unique_ptr<SimpleSpriteSheet> mSpriteSheet;
};

struct IAnimationControl {
    virtual ~IAnimationControl() {}
    virtual void advanceTime(float dtime) = 0;
    virtual float totalDuration() = 0;
    virtual int totalFrames() = 0;
    virtual void rewind(float pos) = 0;
};

class AnimationNode : public QGraphicsObject,
                      public IAnimationControl,
                      public ISceneNode
{
    Q_OBJECT
public:
    using QGraphicsObject::QGraphicsObject;

    virtual void movedRecord(undo::Macro *macro, const QPoint &toPoint) {}

    virtual void rewind(float pos) override {}
    virtual void invalidatePalette() override {}
    virtual void setViewState(const ViewState &view) override {}
};


class AnimationPivotGraphicsItem : public SceneNode
{
public:
    explicit AnimationPivotGraphicsItem(const AnimationRecord *record,
                                        const PivotRecord *pivot,
                                        SceneNode *parent = nullptr);

    QRectF boundingRect() const override;

    void reloadModelData() override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void setViewState(const ViewState &state) override;

private:
    const AnimationRecord *mRecord;
    const PivotRecord *mPivot;
    QSize mSize;
};

class AnimationPreviewItem : public AnimationNode
{
    Q_OBJECT
public:
    explicit AnimationPreviewItem(
        FramesRecord *record,
        PaletteModel *palette,
        QGraphicsItem *parent = nullptr);

    void reloadModelData() override;

    void setViewState(const ViewState &view) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void advanceTime(float dtime) override;

    float totalDuration() override;
    int totalFrames() override;

private:
    std::vector<QPixmap> getFrames() const;

    void setupFrames();

    void recordChanged();

private:
    const FramesRecord *mRecord;
    PaletteModel *mPalette;
    QSize mSize;
    float mAccumulatedTime;
    int mFrameIndex;
    QPoint mPivot;
    RecordId mPaletteId;
    bool mFramesDirty;
};





class AnimationRecordData
{
public:
    QRect mRect;
};

class AnimationRecord : public AbstractRecord, public AnimationRecordData
{
    Q_OBJECT

public:
    explicit AnimationRecord(const AnimationRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , AnimationRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new AnimationRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        AnimationRecordData::operator=(dynamic_cast<const AnimationRecordData &>(other));
        emit positionChanged();
    }

    QRect rect() const { return mRect; }
    void setRect(const QRect &rect)
    {
        mRect = rect;
        emit positionChanged();
    }

    int left() const { return mRect.x(); }
    int width() const { return mRect.width(); }
    int top() const { return mRect.y(); }
    int height() const { return mRect.height(); }

    void setLeft(int value)
    {
        mRect.moveLeft(value);
        emit positionChanged();
    }
    void setWidth(int value)
    {
        mRect.setWidth(value);
        emit positionChanged();
    }
    void setTop(int value)
    {
        mRect.moveTop(value);
        emit positionChanged();
    }
    void setHeight(int value)
    {
        mRect.setHeight(value);
        emit positionChanged();
    }

signals:
    void positionChanged();
};

class AnimationSpriteLoader : public SpriteLoader
{
public:
    explicit AnimationSpriteLoader(AnimationEditor *editor);

    void setPosition(const QPoint &point) override;

    std::vector<std::unique_ptr<AbstractRecord>> loadSprites(
        const std::vector<boost::filesystem::path> &loadPaths, QWidget *parent) override;

private:
    AnimationEditor *mEditor;
    QPoint mPos;
};



class AssignCommand : public undo::Command
{
public:
    AssignCommand(int row, IAssignRow *model, const AbstractRecord &record);

    ~AssignCommand() override;

    bool undo() override;
    bool redo() override;

    QString name() const override;

private:
    int mRow;
    IAssignRow *mModel;
    std::unique_ptr<AbstractRecord> mRecord;
    std::unique_ptr<AbstractRecord> mOldRecord;
};

class BasicSpriteLoader : public SpriteLoader
{
public:
    explicit BasicSpriteLoader(DocumentEditor *editor,
                               TransparencyMode mode,
                               QRgb defaultTransparentPixel = 0,
                               QImage::Format format = QImage::Format_RGB555);

    void setPosition(const QPoint &position) override;

    std::vector<std::unique_ptr<AbstractRecord>> loadSprites(
        const std::vector<boost::filesystem::path> &paths, QWidget *parent) override;

    void setLogModel(LogModel *logModel) override;

private:
    QImage::Format mFormat;
    DocumentEditor *mEditor;
    QPoint mPosition;
    QRgb mTransparentPixel;
    TransparencyMode mTransparencyMode;
    LogModel *mLog;
};





class BitmapEditor : public DocumentEditor
{
public:
    explicit BitmapEditor(int flags = BitmapEditor::NoFlags);
    ~BitmapEditor() override;

    std::unique_ptr<AbstractRecord> createBlankEntry() override;
    std::unique_ptr<SpriteLoader> createSpriteLoader() override;
    bool load(const Gm1FileData &fileData, LogModel *log) override;
    bool save(Gm1FileData *fileData, LogModel *log) override;

    bool renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result) override;

    TableId getEntryTableId() const override { return TableId::Bitmap; }

private:
    auto getGm1EntryClass() const;

    QRgb getDefaultTransparentPixel() const;

private:
    BitmapEditor(const BitmapEditor &) = delete;
    BitmapEditor &operator=(const BitmapEditor &) = delete;
};

class BitmapGraphicsItem : public SceneNode
{
public:
    explicit BitmapGraphicsItem(BitmapRecord *record, SceneNode *parent = nullptr);

    void setViewState(const ViewState &state) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    void moveTo(undo::Macro *macro, const QPoint &toPoint) override;

private:
    BitmapRecord *mRecord;
};





class BitmapRecordData
{
public:
    BitmapRecordData() = default;

    QRect rect() const { return mRect; }
    void setRect(const QRect &rect) { mRect = rect; }

    int left() const { return mRect.x(); }
    int width() const { return mRect.width(); }
    int top() const { return mRect.y(); }
    int height() const { return mRect.height(); }

    void setLeft(int value) { mRect.moveLeft(value); }
    void setWidth(int value) { mRect.setWidth(value); }
    void setTop(int value) { mRect.moveTop(value); }
    void setHeight(int value) { mRect.setHeight(value); }

private:
    QRect mRect;
};

class BitmapRecord : public AbstractRecord, public BitmapRecordData
{
    Q_OBJECT

public:
    explicit BitmapRecord(const BitmapRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , BitmapRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new BitmapRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        BitmapRecordData::operator=(dynamic_cast<const BitmapRecordData &>(other));
    }
};

class ColorEdit : public QWidget
{
    Q_OBJECT

public:
    explicit ColorEdit(QWidget *parent = nullptr);

    QColor currentColor() const;

public slots:
    void setCurrentColor(const QColor &color);

signals:
    void currentColorChanged();

private slots:
    void buttonPressed();
    void editingFinished();

private:
    QLineEdit *mLineEdit;
    QColor mCurrentColor;
};




QBitmap createColorMask(QRgb color, const QImage &image);




class ColorsArrayWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ColorsArrayWidget(QWidget *parent = nullptr);

    void setColors(const QVector<QColor> &colors);

protected:
    void paintEvent(QPaintEvent *paintEvent) override;

    QSize sizeHint() const override;

private:
    void createPixmap();

private:
    QVector<QColor> mColors;
    QPixmap mPixmap;
};




class ColorTableWindow : public Window
{
    Q_OBJECT

public:
    explicit ColorTableWindow(Document *document, QWidget *parent = nullptr);

    void route(ChangeHue *action) override;
    void unroute(ChangeHue *action) override;

    void route(LoadPalette *action) override;
    void unroute(LoadPalette *action) override;

    void route(SavePalette *action) override;
    void unroute(SavePalette *action) override;

signals:
    void gotAnySelection(bool any);
    void gotSingleSelectedRow(bool single);

private slots:
    void loadPalette();
    void savePalette();
    void changeHue();

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    Document *mDoc;
    QTableView *mTableView;
    QAbstractItemDelegate *mDefaultDelegate;
    QAbstractItemDelegate *mTextDelegate;
};




namespace columns {

Q_DECL_CONSTEXPR static QRect kSceneBounds = QRect(0,
                                                   0,
                                                   std::numeric_limits<std::uint16_t>::max(),
                                                   std::numeric_limits<std::uint16_t>::max());

template<class RecordT>
class MutableLeft : public MyColumn<int, RecordT>
{
public:
    MutableLeft()
        : MyColumn<int, RecordT>(QObject::tr("Left"), &RecordT::left, &RecordT::setLeft)
    {}

    bool check(const RecordT &record, const int &left) const override
    {
        QRect bbox = record.rect();
        bbox.moveLeft(left);
        return (kSceneBounds & bbox) == bbox;
    }
};

template<class RecordT>
class MutableTop : public MyColumn<int, RecordT>
{
public:
    MutableTop()
        : MyColumn<int, RecordT>(QObject::tr("Top"), &RecordT::top, &RecordT::setTop)
    {}

    bool check(const RecordT &record, const int &top) const override
    {
        QRect bbox = record.rect();
        bbox.moveTop(top);
        return (kSceneBounds & bbox) == bbox;
    }
};

template<class RecordT>
class ConstWidth : public MyColumn<int, RecordT>
{
public:
    ConstWidth()
        : MyColumn<int, RecordT>(QObject::tr("Width"), &RecordT::width)
    {}
};

template<class RecordT>
class ConstHeight : public MyColumn<int, RecordT>
{
public:
    ConstHeight()
        : MyColumn<int, RecordT>(QObject::tr("Height"), &RecordT::height)
    {}
};

template<class RecordT>
class ConstFilePath : public MyColumn<QString, RecordT>
{
public:
    ConstFilePath()
        : MyColumn<QString, RecordT>(QObject::tr("File"), &RecordT::filePath)
    {}
};

template<class RecordT>
class MutableWidth : public MyColumn<int, RecordT>
{
public:
    MutableWidth()
        : MyColumn<int, RecordT>(QObject::tr("Width"), &RecordT::width, &RecordT::setWidth)
    {}

    bool check(const RecordT &record, const int &width) const override
    {
        if (width <= 0)
            return false;
        QRect bbox = record.rect();
        bbox.setWidth(width);
        return (kSceneBounds & bbox) == bbox;
    }
};

template<class RecordT>
class MutableHeight : public MyColumn<int, RecordT>
{
public:
    MutableHeight()
        : MyColumn<int, RecordT>(QObject::tr("Height"), &RecordT::height, &RecordT::setHeight)
    {}

    bool check(const RecordT &record, const int &height) const override
    {
        if (height <= 0)
            return false;
        QRect bbox = record.rect();
        bbox.setHeight(height);
        return (kSceneBounds & bbox) == bbox;
    }
};

template<class RecordT>
class Color : public MyColumn<QColor, RecordT>
{
public:
    explicit Color(int index)
        : MyColumn<QColor, RecordT>(QString("#%1").arg(index),
                                    std::bind(&RecordT::colorAt, std::placeholders::_1, index),
                                    std::bind(&RecordT::setColorAt,
                                              std::placeholders::_1,
                                              index,
                                              std::placeholders::_2))
    {}
};

class MutableBoxHeight : public MyColumn<int, TileRecord>
{
public:
    MutableBoxHeight()
        : MyColumn<int, TileRecord>(QObject::tr("Box"), &TileRecord::boxHeight, &TileRecord::setBoxHeight)
    {}

    bool check(const TileRecord &entry, const int &box) const override
    {
        if (box < 0) {
            return false;
        }
        TileRecordData temp = entry;
        temp.setBoxHeight(box);
        return (kSceneBounds & temp.rect()) == temp.rect();
    }
};

class MutableTileSize : public MyColumn<int, TileRecord>
{
public:
    MutableTileSize()
        : MyColumn<int, TileRecord>(QObject::tr("Tile Size"), &TileRecord::size, &TileRecord::setSize)
    {}

    bool check(const TileRecord &entry, const int &value) const override
    {
        if (value < 1 || value > 16) {
            return false;
        }
        TileRecordData temp = entry;
        temp.setSize(value);
        return (kSceneBounds & temp.rect()) == temp.rect();
    }
};

template<class RecordT>
class MutableFontBearing : public MyColumn<int, RecordT>
{
public:
    MutableFontBearing()
        : MyColumn<int, RecordT>(QObject::tr("Bearing"), &RecordT::bearing, &RecordT::setBearing)
    {}
};

class PalettePreview : public TableColumn<PaletteRecord>
{
public:
    PalettePreview()
        : TableColumn(QObject::tr("Name"))
    {}

    QVariant displayData(const PaletteRecord &record) const override { return record.name(); }
};

class ConstName : public TableColumn<AnimationFrameGroup>
{
public:
    ConstName()
        : TableColumn(QObject::tr("Name"))
    {}

    QVariant displayData(const AnimationFrameGroup &record) const override { return record.name; }
};

} // namespace columns



namespace undo {
class Stack;
}

class CommandItemDelegate : public QStyledItemDelegate
{
public:
    explicit CommandItemDelegate(QAbstractItemModel *model, undo::Stack *stack, QObject *parent);

    QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    void paint(QPainter *painter,
               const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;

    void setModelData(QWidget *editor,
                      QAbstractItemModel *model,
                      const QModelIndex &index) const override;

private:
    QAbstractItemModel *mModel;
    undo::Stack *mUndoStack;
};

class CreateDocumentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateDocumentDialog(QWidget *parent = nullptr);

    DocumentType selectedDocumentType() const;

    static std::unique_ptr<EditorFactory> createEditorFactory(QWidget *parent);

private:
    DocumentTypesModel *mModel;
    QListView *mView;
};

struct IEntryFactory
{
    virtual std::unique_ptr<AbstractRecord> createBlankEntry() = 0;
};

struct ISpriteLoaderFactory
{
    virtual std::unique_ptr<SpriteLoader> createSpriteLoader() = 0;
};

struct IGm1Loader
{
    virtual bool load(const Gm1FileData &fileData, LogModel *log) = 0;
};

struct IGm1Writer
{
    virtual bool save(Gm1FileData *fileData, LogModel *log) = 0;
};

struct ISpriteRenderer
{
    virtual bool renderSprites(const std::set<RecordId> &records, LogModel *log, QImage *result) = 0;
};

class DocumentEditor : public ISpriteSheet,              // view
                       public IEntryFactory,             // plot
                       public IGm1Loader,
                       public IGm1Writer,
                       //! I need a strong reason to keep this function in this interface
                       public ISpriteRenderer,
                       public ISpriteLoaderFactory
{
public:
    DocumentEditor(int flags = 0);
    virtual ~DocumentEditor() = default;

protected:
    void setupColorTable();

    void setupTileEntriesTableModel();
    void setupBitmapEntriesTableModel(int flags);
    void setupFontEntriesTableModel();
    void setupAnimationEntriesTableModel();

    void setupAnimationsTableModel();
    void setupColorTableModel();
    void setupPropertiesModel();
    void setupSpritesTableModel();

    void setupFramesModel();

public:
    SpriteModel *getSpritesheet() final override { return mSpritesTableModel.get(); }
    PaletteModel *getPalette() final override { return mColorTableModel.get(); }
    AnimationModel *getAnimation() { return mAnimationModel.get();}
    AnimationGroupModel *getAnimationGroup() { return mAnimationsTableModel.get();}
    FontModel *getFont() { return mFontModel.get();}
    BitmapModel *getBitmap() { return mBitmapModel.get();}
    TileModel *getTile() { return mTileModel.get();}
    PropertiesModel *getPropertiesModel() { return mPropertiesModel.get(); }
    FramesModel *getFramesModel() { return mFramesModel.get(); };

    virtual TableId getEntryTableId() const = 0;

    TableModel *getModel(TableId tableId) {
        switch (tableId) {
        case TableId::Animation:
            return mAnimationModel.get();
        case TableId::Font:
            return mFontModel.get();
        case TableId::Tile:
            return mTileModel.get();
        case TableId::Bitmap:
            return mBitmapModel.get();
        }
    }

    PivotRecord* getPivot() {
        return mPivotRecord;
    }

    enum Flags {
        NoFlags = 0,
        ///< for GM1_ENTRY_CLASS_BITMAP

        UseTransparency = 1,
        ///< for GM1_ENTRY_CLASS_TGX16

        LayoutIsConstSize = 2,
        ///< for GM1_ENTRY_CLASS_TGX16CONSTSIZE

        MagicBitmap = 4
        ///< for GM1_ENTRY_CLASS_ANOTHERBITMAP
    };

protected:
    RecordSet<AnimationRecord> mAnimations;
    RecordSet<FontRecord> mGlyphs;
    RecordSet<BitmapRecord> mBitmaps;
    RecordSet<PaletteRecord> mColorTable;
    RecordSet<SpriteRecord> mSprites;
    RecordSet<AnimationFrameGroup> mAnimationGroups;
    RecordSet<TileRecord> mTiles;

    std::unique_ptr<PaletteModel> mColorTableModel;
    std::unique_ptr<SpriteModel> mSpritesTableModel;
    std::unique_ptr<PropertiesModel> mPropertiesModel;
    std::unique_ptr<AnimationGroupModel> mAnimationsTableModel;
    std::unique_ptr<AnimationModel> mAnimationModel;
    std::unique_ptr<FontModel> mFontModel;
    std::unique_ptr<TileModel> mTileModel;
    std::unique_ptr<BitmapModel> mBitmapModel;
    std::unique_ptr<FramesModel> mFramesModel;

    Gm1FileData mGm1;

    PropertyItem *mXPivotProp;
    PropertyItem *mYPivotProp;

    PivotRecord *mPivotRecord;

    int mFlags;

};

class Document : public QObject
{
    Q_OBJECT

public:
    explicit Document(QObject *parent = 0);
    virtual ~Document();

    undo::Stack *undoStack();

    DocumentEditor *editor();

    LogModel *logModel();

    DocumentOperation *operation();

    boost::filesystem::path filePath() const;
    void setFilePath(const boost::filesystem::path &path);

    QString name() const;
    void setName(const QString &name);

    bool isEmpty() const;

    void setEditorFactory(const EditorFactory &factory);

    void loadFromFile(const boost::filesystem::path &path);

    void saveToFile(const boost::filesystem::path &path);

    //! This is probably not the best class for this method.
    void moveForward(const std::set<int> &spriteIndices);

    //! This is probably not the best class for this method.
    void moveBackward(const std::set<int> &spriteIndices);

    //! This is probably not the best class for this method.
    void bringForward(const std::set<int> &spriteIndices);

    //! This is probably not the best class for this method.
    void sendBack(const std::set<int> &spriteIndices);

    //! This is probably not the best class for this method.
    void importSprites(QWidget *parent, const QPoint &advisedPosition = QPoint());

    //! This is probably not the best class for this method.
    void exportSprites(QWidget *parent, const std::set<RecordId> &ids);

    //! This is probably not the best class for this method.
    void addEntry(QWidget *parent, const QPoint &advisedPosition = QPoint(), int beforeRow = -1);

signals:
    void filePathChanged();
    void editorChanged();
    void operationChanged();

private slots:
    //! the loader ownership is transferred to this function
    void loaderFinished(DocumentLoader *loader);

    //! the loader ownership is transferred to this function
    void writerFinished(DocumentWriter *writer);

private:
    void setEditor(std::unique_ptr<DocumentEditor> editor);

private:
    undo::Stack mUndoStack;
    std::unique_ptr<DocumentEditor> mEditor;
    DocumentOperation *mOperation;

    boost::filesystem::path mPath;

    QString mDocumentName;

    LogModel *mLogModel;

private:
    Document &operator=(const Document &) = delete;
    Document(const Document &) = delete;
};

class DocumentLoader : public Operation
{
    Q_OBJECT

public:
    explicit DocumentLoader(QObject *parent = 0);

    void setFilePath(const boost::filesystem::path &filepath);

    bool success() const;

    LogModel *logs() const;

    boost::filesystem::path filePath() const;

    std::unique_ptr<EditorFactory> createEditorFactory() const;

    QString status() const override;

    void start() override;

private:
    Gm1FileData mGm1;
    LogModel *mLogModel;
    boost::filesystem::path mFilePath;
    bool mSuccess = false;

    DocumentLoader(const DocumentLoader &) = delete;
    DocumentLoader &operator=(const DocumentLoader &) = delete;
};

class DocumentManager : public QObject
{
    Q_OBJECT

public:
    explicit DocumentManager(QObject *parent = 0);

    Document *createDocument();
    void removeDocument(Document *document);

private:
    std::set<Document *> mDocuments;
    ///< owned by this

    int mNextDocumentNumber;

    DocumentManager &operator=(const DocumentManager &) = delete;
    DocumentManager(const DocumentManager &) = delete;
};





class DocumentOperation : public QObject
{
    Q_OBJECT

public:
    explicit DocumentOperation(QObject *parent = 0);

    bool isRunning() const;

    int currentProgress() const;

    int totalProgress() const;

    QString statusString() const;

    //! Ownership is not transferred
    bool pushOperation(Operation *operation);

    void setStatusString(const QString &statusString);

signals:
    void stateChanged();

private:
    void setProgress(int current, int total);
    void setFinished();
    void setStarted();

private:
    bool mIsRunning = false;
    int mCurrentProgress = 0;
    int mTotalProgress = 0;
    QString mStatusString;
};






class DocumentTypesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit DocumentTypesModel(QObject *parent = 0);

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    void setRecentIndex(int index);
    int recentIndex() const;

    DocumentType getValue(int index) const;

    // returns -1 if nothing found
    int indexOf(const DocumentType &type) const;
    int defaultIndex() const;

private:
    QString getDocumentTypeName(const DocumentType &type) const;

private:
    std::vector<DocumentType> mTypes;
    int mRecentIndex;

    DocumentTypesModel(DocumentTypesModel const &) = delete;
    DocumentTypesModel &operator=(DocumentTypesModel const &) = delete;
};





class DocumentWriter : public Operation
{
    Q_OBJECT
public:
    explicit DocumentWriter(QObject *parent = 0);

    void setFilePath(const boost::filesystem::path &filePath);

    LogModel *logs() const;

    bool success() const;

    boost::filesystem::path filePath() const;

    void setFileData(const Gm1FileData &fileData);

    QString status() const override;

public slots:
    void start() override;

private:
    Gm1FileData mGm1;
    boost::filesystem::path mFilePath;
    LogModel *mLogModel;
    bool mSuccess = false;

    DocumentWriter(const DocumentWriter &) = delete;
    DocumentWriter &operator=(const DocumentWriter &) = delete;
};

class EditorFactory
{
public:
    EditorFactory();

    static bool isTypeSupported(const DocumentType &type);

    std::unique_ptr<DocumentEditor> createEditor(LogModel *log) const;
    std::unique_ptr<DocumentEditor> createEmptyEditor() const;

    void setDocumentType(const DocumentType &type);
    void setFileData(const Gm1FileData &fileData);
    void setConstSize(bool constSize);
    void setMagicBitmap(bool magicBitmap);

private:
    DocumentType mType = DocumentType::Invalid;
    boost::optional<Gm1FileData> mFileData;
    bool mConstSize;
    ///< documents of Static type can have constant sized layout entries.
    ///< ignored for other types

    bool mMagicBitmap;
    ///< applied to bitmap archives of unusual type
};





class EmptyNode : public QGraphicsItem
{
public:
    explicit EmptyNode(QGraphicsItem *parent)
        : QGraphicsItem(parent)
    {}

    QRectF boundingRect() const override { return QRectF(); }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override
    {}

    void setPropogatePaletteChanges(bool propogatePaletteChanges)
    {
        mPropogatePaletteChanges = propogatePaletteChanges;
    }

    void invalidatePalette()
    {
        if (mPropogatePaletteChanges) {
            for (QGraphicsItem *item : childItems()) {
                qgraphicsitem_cast<SceneNode *>(item)->invalidatePalette();
            }
        }
    }

    void setViewState(const ViewState &state)
    {
        for (QGraphicsItem *item : childItems()) {
            qgraphicsitem_cast<SceneNode *>(item)->setViewState(state);
        }
    }

private:
    bool mPropogatePaletteChanges = false;
};





class Engine : public QObject
{
    Q_OBJECT

public:
    Engine();

    void createDocument(const EditorFactory &factory);
    int exec();

private:
    void newDocumentRequest(Frame *frame);
    void openDocumentRequest(Frame *sender, const boost::filesystem::path &path);
    void closeDocumentRequest(Document *document);

private:
    DocumentManager *mDocMgr;
    FrameManager *mFrameMgr;

    Engine(const Engine &) = delete;
    Engine &operator=(const Engine &) = delete;
};


class EntryTableWindow : public Window
{
    Q_OBJECT

public:
    explicit EntryTableWindow(TableId tableId, Document *document, QWidget *parent = nullptr);

    bool focusEntry(const RecordId &id) override;

    void route(RemoveSpriteOrEntry *action) override;
    void unroute(RemoveSpriteOrEntry *action) override;

    void route(AddEntry *action) override;
    void unroute(AddEntry *action) override;

signals:
    void gotAnyRowSelected(bool any);

private:
    void add();
    void remove();

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    Document *mDoc;
    QTableView *mTableView;
    TableId mTableId;
};





class FontEditor : public DocumentEditor
{
public:
    FontEditor();
    ~FontEditor() override;

    std::unique_ptr<AbstractRecord> createBlankEntry() override;
    std::unique_ptr<SpriteLoader> createSpriteLoader() override;
    bool load(const Gm1FileData &fileData, LogModel *log) override;
    bool save(Gm1FileData *fileData, LogModel *log) override;
    bool renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result) override;

    TableId getEntryTableId() const override { return TableId::Font; }

private:
    FontEditor(const FontEditor &) = delete;
    FontEditor &operator=(const FontEditor &) = delete;
};



class FontGraphicsItem : public SceneNode
{
public:
    explicit FontGraphicsItem(FontRecord *record, SceneNode *parent = 0);

    void setViewState(const ViewState &state) override;
    QRectF boundingRect() const override;
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    void moveTo(undo::Macro *macro, const QPoint &toPoint) override;

private:
    FontRecord *mRecord;
    bool mShowOverlay;
};




class FontRecordData
{
public:
    FontRecordData()
        : mBearing(0)
    {}

    QRect rect() const { return mRect; }

    void setRect(const QRect &rect) { mRect = rect; }

    int width() const { return mRect.width(); }

    void setWidth(int width) { mRect.setWidth(width); }

    int height() const { return mRect.height(); }

    void setHeight(int height) { mRect.setHeight(height); }

    int top() const { return mRect.top(); }

    void setTop(int top) { mRect.moveTop(top); }

    int left() const { return mRect.left(); }

    void setLeft(int left) { mRect.moveLeft(left); }

    int bearing() const { return mBearing; }

    void setBearing(int bearing) { mBearing = bearing; }

    QLine baseLine() const { return QLine(0, mBearing, mRect.width(), mBearing); }

private:
    QRect mRect;
    int mBearing;
};

class FontRecord : public AbstractRecord, public FontRecordData
{
    Q_OBJECT

public:
    explicit FontRecord(const FontRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , FontRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new FontRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        FontRecordData::operator=(dynamic_cast<const FontRecordData &>(other));
    }
};




class Frame : public QMainWindow
{
    Q_OBJECT

public:
    explicit Frame(Document *document, QWidget *parent = 0);
    ~Frame() override;

    const Document *document() const;
    Document *document();

    Window *focusWindow();

    /// returns nullptr if none
    const Window *focusWindow() const;

    bool isWindowFocused(Window *window) const;
    void closeAllWindows();
    void setCloseAllowed(bool allowed);

    void updateCursorInfo(const QPoint &pos);

signals:
    void newDocumentRequested(Frame *frame);
    void openDocumentRequested(Frame *frame, const boost::filesystem::path &path);
    void closeDocumentRequested(Document *document);
    void cloneFrameRequested(Frame *frame);
    void closeFrameRequested(Frame *frame);

protected:
    void closeEvent(QCloseEvent *event) override;
    bool eventFilter(QObject *receiver, QEvent *event) override;

private:
    void newDocument();
    void openDocument();
    void saveDocument();
    void saveAsDocument();
    void closeDocument();
    void revertDocument();

    void cloneFrame();

    void showEntryTable();
    void showPlot();
    void showColorTable();
    void showSprites();
    void showProperties();
    void showPreview();
    void showRepl();

    void updateOperation();

    void updateTitle();

    /// called per Document::editorChanged
    void updateEditor();
    void updateMenus();

    void setupFileMenu();
    void setupEditMenu();
    void setupViewMenu();
    void setupWindowMenu();
    void setupToolsMenu();
    void setupSpritesMenu();
    void setupColorTableMenu();

    /// returned object is owned by this Frame
    QDockWidget *addWindow(Window *window);

    void findRequested(const RecordId &id);

    QString documentContainingDir() const;

    void linkActivated(const QString &link);

    void updateMessages();
    void showMessageLog();

    void applicationFocusChanged(QWidget *old, QWidget *now);

    void windowClosed(Window *window);

    void routeAllActions(Window *window);
    void unrouteAllActions(Window *window);

private:
    Document *mDocument;
    ///< many views can share one document

    std::set<Window *> mWindows;
    ///< owned by this object

    std::set<QDockWidget *> mDocks;
    ///< owner by this

    Window *mLastFocusedWindow = nullptr;
    ///< not owned; can be invalidated at any time

    QProgressBar *mOperationProgress;
    QLabel *mOperationStatus;

    QLabel *mCursorPosSection;

    bool mCloseAllowed;

    QWidget *mWarningsPanel;
    QLabel *mWarnings;
    QWidget *mErrorsPanel;
    QLabel *mErrors;

    QDockWidget *mViewerDock;
    QPointer<LogViewer> mCurrentViewer;

    QToolBar *mFileBar;
    QToolBar *mEditBar;
    QToolBar *mViewBar;
    QToolBar *mWindowBar;
    QToolBar *mToolsBar;

    QToolBar *mColorTableBar;
    QToolBar *mSpritesBar;

    //
    // SpritesBar
    //
    ImportSprites *mImportSprites; //    +SpritesWindow +PlotWindow
    ExportSprites *mExportSprites; //   +SpritesWindow +PlotWindow

    //
    // ColorTable
    //
    LoadPalette *mLoadPalette; //   +ColorTable
    SavePalette *mSavePalette; //    +ColorTable
    ChangeHue *mChangeHue;     //    +ColorTable

    //
    // Global data
    //
    FindSpriteOrEntry *mFindSpriteOrEntry;     //         +SpritesWindow +PlotWindow +EntryWindow
    RemoveSpriteOrEntry *mRemoveSpriteOrEntry; //         +SpritesWindow +PlotWindow +EntryWindow
    AddEntry *mAddEntry;                       //        +PlotWindow +EntryWindow
    MoveForward *mMoveForward;                 //  +SpritesWindow +PlotWindow
    MoveBackward *mMoveBackward;               //   +SpritesWindow +PlotWindow
    BringForward *mBringForward;               //   +SpritesWindow +PlotWindow
    SendBack *mSendBack;                       //  +SpritesWindow +PlotWindow
    SelectAll *mSelectAll;     //   +SpritesWindow +PlotWindow +EntryWindow +ColorTable
    SelectClear *mSelectClear; //    +SpritesWindow +PlotWindow +EntryWindow +ColorTable

    //
    // Frame tools
    //
    ScrollMode *mScrollMode;
    TransformMode *mTransformMode;
    AllowSelectSprites *mAllowSelectSprites;
    AllowSelectEntries *mAllowSelectEntries;

    //
    // Frame view
    //
    SnapToGrid *mSnapToGrid;
    ShowGrid *mShowGrid;
    ShowOverlay *mShowOverlay;

private:
    Frame(const Frame &) = delete;
    Frame &operator=(const Frame &) = delete;
};


class FrameManager : public QObject
{
    Q_OBJECT

public:
    explicit FrameManager(QObject *parent = 0);
    virtual ~FrameManager();

    Frame *createFrame(Document *document);
    void removeFrame(Frame *frame);

    int getFramesCount(const Document *document) const;
    void removeFrames(const Document *document);

signals:
    void newDocumentRequested(Frame *frame);
    void openDocumentRequested(Frame *frame, const boost::filesystem::path &path);
    void closeDocumentRequested(Document *document);

private slots:
    void cloneFrameRequested(Frame *frame);
    void closeFrameRequested(Frame *frame);

private:
    std::set<Frame *> mFrames;
    ///< owner by this object exclusively

    FrameManager(const FrameManager &) = delete;
    FrameManager &operator=(const FrameManager &) = delete;
};






class FramesModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit FramesModel(QObject *parent = 0);
    ~FramesModel() override = default;

    void setLayoutModel(AnimationModel *layoutModel);
    void setFrameGroupModel(AnimationGroupModel *frameGroupModel);
    void setSpriteModel(SpriteModel *spriteModel);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    /// returns -1 on error
    int findIndexByFrameGroupId(const RecordId &id) const;

    FramesRecord* getRecordByIndex(int row);
    const FramesRecord* getRecordByIndex(int row) const;

private:
    void entryInserted(const QModelIndex &parent, int first, int last);
    void entryAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void entryReset();
    void entryChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void entryMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    void spriteInserted(const QModelIndex &parent, int first, int last);
    void spriteAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void spriteReset();
    void spriteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void spriteMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    void groupInserted(const QModelIndex &parent, int first, int last);
    void groupAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void groupReset();
    void groupChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void groupMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    void createImagesAndMasks(FramesRecord *record) const;

    void markRecordAsDirty(FramesRecord *record);
    void invalidateAllRecords();

    void rebuildRecordList();
    
private:
    AnimationModel *mLayoutModel = nullptr;
    AnimationGroupModel *mFrameGroupModel = nullptr;
    SpriteModel *mSpriteModel = nullptr;

    std::vector<FramesRecord *> mRecords;

    friend class FramesRecord;

private:
    FramesModel(const FramesModel &) = delete;
    FramesModel &operator=(const FramesModel &) = delete;
};


struct FramesRecordData
{
    void createImagesAndMasks(AnimationGroupModel   *groupModel,
                              SpriteModel           *spriteModel,
                              AnimationModel        *animationModel);

    bool clean = false;
    QPoint pivot;
    AnimationFrameGroupRecordData group;
    std::vector<QImage> images;
    std::vector<QBitmap> masks;
};

class FramesRecord : public QObject
{
    Q_OBJECT

public:
    explicit FramesRecord(FramesModel *model, const RecordId &frameGroupId, QObject *parent = nullptr)
        : QObject(parent)
        , mModel(model)
        , mFrameGroupId(frameGroupId)
    {}

    RecordId frameGroupId() const { return mFrameGroupId; }

    void createImagesAndMasks() const;

    void markDirty();

    QString toString() const;

    AnimationFrameGroupRecordData frameGroup() const;

    std::vector<QImage> images() const;
    std::vector<QBitmap> masks() const;
    QPoint pivot() const;

signals:
    void changed();

private:
    FramesModel *mModel;
    RecordId mFrameGroupId;

    /// Is this a Cache?
    mutable FramesRecordData mData;
};

struct Gm1FileEntry
{
    gm1_entry_t Gm1 = {};
    QRect Rect;

    struct TileBox
    {
        int left = 0;
        int height = 0;
        int width = 0;
    };

    void setEntryRect(const QRect &rect);
    void setPartId(int partId);
    void setPartSize(int partSize);
    void setBox(const Gm1FileEntry::TileBox &box);
    void setAlignment(TileAlignment alignment);
    void setFontBearing(int bearing);

    QRect entryRect() const;
    int partId() const;
    int partSize() const;
    TileAlignment alignment() const;
    int fontBearing() const;
};

struct Gm1FileData
{
    constexpr static size_t MaxPaletteCount = GM1_NUM_PALETTES;
    constexpr static size_t MaxPaletteSize = GM1_NUM_COLORS;

    typedef std::array<QColor, MaxPaletteSize> palette_t;

    gm1_header_t Header;
    gm1_image_props_t Properties;
    std::array<palette_t, MaxPaletteCount> Palettes;
    std::vector<Gm1FileEntry> Entries;
    QImage Plot;
    QPoint Origin;

    void setPaletteAt(size_t index, const palette_t &palette);
    void setPivot(const QPoint &point);
    void setPlotImage(const QImage &image);
    void setPlotOrigin(const QPoint &plotOrigin);
    void setEntryCount(size_t num_entries);
    void setEntryClass(gm1_entry_class_t entry_class);

    bool hasPivot() const;
    bool hasPalette() const;
    size_t paletteCount() const;
    bool hasTransparency() const;
    gm1_u16_t transparentPixel() const;
    palette_t paletteAt(size_t index) const;
    QPoint pivot() const;
    QImage plotImage() const;
    QPoint plotOrigin() const;
    size_t entryCount() const;
    gm1_entry_class_t entryClass() const;
};

class Gm1Loader : public QObject
{
    Q_OBJECT
public:
    explicit Gm1Loader(QObject *parent = nullptr);

    bool isNull() const;

    bool loadFromFile(const boost::filesystem::path &filepath);

    Gm1FileData fileData() const;

signals:
    void progress(int current, int total);
    void diagnosticsError(const QString &message);
    void diagnosticsWarning(const QString &message);

private:
    QImage entryImage(size_t index) const;

    void emitError(const char *format, ...);
    void emitWarning(const char *format, ...);

    bool readHeader(gm1_istream_t *is);
    bool readEntries(gm1_istream_t *is);
    bool readEntry(gm1_istream_t *is, size_t index);
    bool initPlot();

private:
    Gm1FileData mFileData;
};

class Gm1Writer : public QObject
{
    Q_OBJECT
public:
    explicit Gm1Writer(QObject *parent = nullptr);

    bool isNull() const;

    bool saveToFile(const boost::filesystem::path &filepath);

    void setFileData(const Gm1FileData &fileData);

signals:
    void progress(int current, int total);
    void diagnosticsError(const QString &message);
    void diagnosticsWarning(const QString &message);

private:
    QImage entryImage(size_t index) const;

    void emitError(const char *format, ...);
    void emitWarning(const char *format, ...);

private:
    Gm1FileData mFileData;
};

class GridGraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit GridGraphicsView(QWidget *parent = nullptr);

    void setShowGrid(bool on);
    bool showGrid() const;

    void recalculateSceneRect();

    void center(qreal sceneX, qreal sceneY);

signals:
    void showGridChanged(bool value);
    void scaleChanged(float delta);

protected:
    void drawForeground(QPainter *painter, const QRectF &rect) override;
    void wheelEvent(QWheelEvent *event) override;

    void scrollContentsBy(int dx, int dy) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

    void resizeEvent(QResizeEvent *event) override;

private:
    void drawGrid(QPainter *painter,
                  const QRectF &rect,
                  const QTransform &xform,
                  float strokeLength,
                  const QPen &pen) const;

private:
    int mScrollDelta;
    bool mShowGrid;

    bool mScrolling;

    QPointF mLastPos;
};

class GridScene : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit GridScene(QObject *parent = nullptr);
    virtual ~GridScene();

    QPoint mouseScenePos() const;
    void setSnapToGrid(bool enable);
    bool snapToGrid() const;
    void setInteractionMode(InteractionMode mode);
    InteractionMode interactionMode() const;

    int gridSize() const;

    bool isTransforming() const;
    QList<QGraphicsItem *> transformingItems() const;

    void setTileBackground(bool tileBackground);
    void setSolidBackground(bool solidBackground);

    //! Color picking should be banned from this API because
    //! GridScene is going to be reused with non SpriteGraphicsItems
    //! in PreviewWindow.
    boost::optional<QColor> pickedColor() const;

    bool isPicking() const;
    boost::optional<QColor> pixelAt(const QPoint &point) const;

    QPoint transformingTargetPos() const;

signals:
    void interactionModeChanged();

    void cursorMoved(const QPoint &pos);
    void snapToGridChanged(bool value);
    void itemsMovementStarted();
    void itemsMovementFinished();
    void itemsMovementReset();

    void pickingChanged();
    void pickingStarted();
    void pickingFinished();
    void pickingReset();

protected:
    bool event(QEvent *event) override;

    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

    void drawBackground(QPainter *painter, const QRectF &rect) override;

    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    void prepareTransparentGrid();

    bool startTransforming(const QPoint &anchorScenePos);
    void stopTransforming(bool reset);
    void updateItemsPos(bool ignoreSnapToGrid);

    void stopPicking(bool reset);

    void updatePicking(const QPoint &point);

    QPointF snapItem(const QPointF &point, const QPoint &transformOrigin);

    int keyboardMovementMagnitude(Qt::KeyboardModifiers modifiers) const;
    QPoint keyboardMovementVector(int key, Qt::KeyboardModifiers modifiers) const;

private:
    bool mSnapToGrid;
    InteractionMode mMode;
    QPoint mMousePos;
    ///< in scene coordinates

    bool mDrawTransparentGrid;

    bool mIsTransforming;
    QList<QGraphicsItem *> mTransformingItems;

    bool mIsPicking;
    boost::optional<QColor> mPickedColor;

    bool mTileBackground;

    QWidget *mTransformingInputGrabber;
    QPoint mTransformingTargetPos;

    bool mIsTransformingWithKeyboard;
};

class InsertCommand : public undo::Command
{
public:
    InsertCommand(std::unique_ptr<AbstractRecord> record,
                  int position,
                  TableModel *model,
                  const QString &name = QString());
    ~InsertCommand() override;

    bool undo() override;
    bool redo() override;
    QString name() const override;

private:
    QString mName;
    int mPos;
    TableModel *mModel;
    std::unique_ptr<AbstractRecord> mRecord;
};

class LogMessage
{
public:
    QDateTime timestamp;
    QString text;
    Severity severity = Severity::Error;
    bool viewed = false;
};

inline static bool operator<(const LogMessage &lhs, const LogMessage &rhs)
{
    return lhs.timestamp < rhs.timestamp;
}

class LogModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit LogModel(QObject *parent = 0);
    ~LogModel() override;

    void clear();

    void merge(const LogModel *other);

    void addMessage(const QString &text, Severity severity = Severity::Error);

    void addError(const QString &text);
    void addWarning(const QString &text);

    void error(const char *format, ...);
    void warning(const char *format, ...);
    void info(const char *format, ...);

    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int errorCount() const;
    int warningCount() const;

    void markAsViewed();

    enum Roles {
        SeverityRole = Qt::UserRole,
    };

signals:
    void messagesChanged();

private:
    std::vector<LogMessage> mMessages;
};

class LogViewer : public Window
{
    Q_OBJECT

public:
    explicit LogViewer(Document *document, QWidget *parent);
    ~LogViewer() override;

    void showErrors();
    void showWarnings();

    void clearAll();

private:
    void createGeneralTab();
    void createErrorsTab();
    void createWarningsTab();

private:
    Document *mDoc;

    QWidget *mGeneralTab;
    QWidget *mErrors;
    QWidget *mWarnings;

    QTableView *mGeneralView;
    QTableView *mErrorsView;
    QTableView *mWarningsView;

    QTabWidget *mTabWidget;
};

class ModifyCommand : public undo::Command
{
public:
    ModifyCommand(const QModelIndex &index,
                  const QVariant &newValue,
                  QAbstractItemModel *model,
                  const QString &name = QString());

    ~ModifyCommand() override;

    bool undo() override;
    bool redo() override;

    QString name() const override;

private:
    QModelIndex mIndex;
    QString mName;
    QVariant mNewValue;
    QVariant mOld;
    QAbstractItemModel *mModel;
};

class MoveCommand : public undo::Command
{
public:
    MoveCommand(QAbstractItemModel *model, int row, int count, int dest);

    ~MoveCommand() override;

    bool undo() override;
    bool redo() override;

    QString name() const override;

private:
    QAbstractItemModel *mModel;
    int mRow;
    int mCount;
    int mDest;
};

class Navigator : public QWidget
{
    Q_OBJECT

public:
    explicit Navigator(QGraphicsScene *scene, QWidget *parent = nullptr);
    ~Navigator();

    QSize minimumSizeHint() const override;

signals:
    void navigated(const QPoint &pos, const QSize &size);

protected:
    void paintEvent(QPaintEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    void onPopupReleased();

private:
    QGraphicsScene *mScene;
    NavigatorPopup *mPopup;
};

class NavigatorPopup : public QGraphicsView
{
    Q_OBJECT

public:
    explicit NavigatorPopup(QWidget *parent = nullptr);
    ~NavigatorPopup();

signals:
    void mouseMoved(const QPoint &localPos, const QSize &size);

protected:
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    bool event(QEvent *e) override;
};


struct NodeAttribute
{
    enum {
        RecordId,
        ///< RecordId QVariant. Must be present for all model items.
        TransformOffset,
        ///< If present, the items movement is biased by this QPoint.
        TransformTopLeft,
        ///< QPoint
        TransformBottomRight,
        ///< QPoint
    };
};

template<class T>
class Factory final
{
public:
    using creator_fn_t = std::function<T *(RecordId)>;
    using self_type = Factory<T>;

public:
    Factory() {}
    ~Factory() {}

    T *createNode(const RecordId &id) const
    {
        return mCreator(id);
    }

    void addNodeCreator(creator_fn_t creator)
    {
        mCreator = creator;
    }

private:
    creator_fn_t mCreator;
};

using NodeFactory = Factory<SceneNode>;
using AnimationFactory = Factory<AnimationNode>;


class Operation : public QObject
{
    Q_OBJECT

public:
    explicit Operation(QObject *parent = 0);

    virtual void start() = 0;

    virtual QString status() const;

signals:
    void progress(int, int);
    void finished();

protected:
    void setProgress(int current, int total);
};

class PaletteComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit PaletteComboBox(DocumentEditor *editor, QWidget *parent = nullptr);

private:
    DocumentEditor *mEditor;
};

class PaletteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PaletteDialog(DocumentEditor *editor, const QString &text, QWidget *parent = nullptr);

    static RecordId getPaletteId(DocumentEditor *editor, QWidget *parent);

    RecordId selectedId() const;

private:
    DocumentEditor *mEditor;
    QComboBox *mPaletteBox;
};

class PaletteItemDelegate : public QStyledItemDelegate
{
public:
    explicit PaletteItemDelegate(PaletteModel *model, QWidget *parent = nullptr);

    ~PaletteItemDelegate() override;

    virtual void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const override;

    virtual QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const override;

private:
    PalettePreview *mPreview;
    PaletteModel *mModel;
};


class PalettePreview : public QWidget
{
    Q_OBJECT

public:
    explicit PalettePreview(const PaletteRecord &record, QWidget *parent = nullptr);

    void reset(const PaletteRecord &record);

private:
    QLabel *mLabel;
    ColorsArrayWidget *mColorsArray;
};





class PaletteRecordData
{
public:
    Gm1FileData::palette_t colors() const { return mColors; }

    void setName(const QString &name) { mName = name; }

    QString name() const { return mName; }

    void setColors(const Gm1FileData::palette_t &colors) { mColors = colors; }

    void setColorAt(int index, const QColor &color) { mColors.at(index) = color; }

    size_t colorCount() const { return mColors.size(); }

    QColor colorAt(int index) const { return mColors.at(index); }

private:
    Gm1FileData::palette_t mColors;
    QString mName;
};

class PaletteRecord : public AbstractRecord, public PaletteRecordData
{
    Q_OBJECT

public:
    explicit PaletteRecord(const PaletteRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , PaletteRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new PaletteRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        PaletteRecordData::operator=(dynamic_cast<const PaletteRecordData &>(other));
    }
};




class PivotRecord : public QObject {
    Q_OBJECT
public:
    PivotRecord(PropertyItem *x, PropertyItem *y, QObject *parent = 0)
        : QObject(parent)
        , mXPivotProp(x)
        , mYPivotProp(y)
    {}

    QPoint pivot() const {
        return QPoint(mXPivotProp->value().toInt(), mYPivotProp->value().toInt());
    }

private:
    PropertyItem *mXPivotProp;
    PropertyItem *mYPivotProp;
};

}




class PlotWindow : public Window
{
    Q_OBJECT

public:
    explicit PlotWindow(TableId tableId, Document *document, QWidget *parent = nullptr);
    ~PlotWindow() override;

    void route(ImportSprites *action) override;
    void unroute(ImportSprites *action) override;

    void route(ExportSprites *action) override;
    void unroute(ExportSprites *action) override;

    void route(FindSpriteOrEntry *action) override;
    void unroute(FindSpriteOrEntry *action) override;

    void route(RemoveSpriteOrEntry *action) override;
    void unroute(RemoveSpriteOrEntry *action) override;

    void route(AddEntry *action) override;
    void unroute(AddEntry *action) override;

    void route(MoveForward *action) override;
    void unroute(MoveForward *action) override;

    void route(MoveBackward *action) override;
    void unroute(MoveBackward *action) override;

    void route(BringForward *action) override;
    void unroute(BringForward *action) override;

    void route(SendBack *action) override;
    void unroute(SendBack *action) override;

    void route(SelectAll *action) override;
    void unroute(SelectAll *action) override;

    void route(SelectClear *action) override;
    void unroute(SelectClear *action) override;

    void route(ScrollMode *action) override;
    void route(TransformMode *action) override;
    void route(AllowSelectSprites *action) override;
    void route(AllowSelectEntries *action) override;
    void route(SnapToGrid *action) override;
    void route(ShowGrid *action) override;
    void route(ShowOverlay *action) override;

    void setShowOverlay(bool showOverlay);

    void setScrollMode();
    void setTransformMode();

signals:
    void findRequested(const RecordId &id);
    void cursorScenePosChanged(const QPoint &pos);

    void gotSpriteOrEntrySelected(bool any);
    void gotSpriteButNoEntrySelected(bool any);
    void gotSingleItemSelected(bool any);

private:
    void paletteIndexChanged(int index);
    void addImage();
    void saveImage();
    void findSelected();
    void addEntry();
    void remove();

    void populateToolbar(QToolBar *toolbar);

    void updateScene();

    void prepareScene();
    void prepareView();

    void createActions();

    void onSceneRectChanged(const QRectF &rect);

    void onSceneChanged(const QList<QRectF> &region);

    std::vector<SceneNode *> selectedNodes() const;

    std::vector<int> selectedSpriteIndices() const;

    std::vector<RecordId> selectedSpriteIds() const;

    void setViewState(const ViewState &state);

    void rebuildScene();

    void spritesInserted(const QModelIndex &parent, int first, int last);
    void spritesAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void spritesReset();

    void spritesMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    void entriesInserted(const QModelIndex &parent, int first, int last);
    void entriesAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void entriesReset();

    void paletteReset();
    void paletteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

    void propertiesReset();
    void propertiesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

    void updateInteractionMode();

    void setAllowSelectSprites(bool allow);
    void setAllowSelectEntries(bool allow);

    void itemsMovementFinished();
    void itemsMovementReset();

    void moveForward();
    void moveBackward();
    void bringForward();
    void sendBack();

    void selectAll();
    void selectClear();

    void selectedNodesChanged();

    std::unique_ptr<NodeFactory> createSpriteFactory();
    std::unique_ptr<NodeFactory> createAnimationFactory();
    std::unique_ptr<NodeFactory> createFontFactory();
    std::unique_ptr<NodeFactory> createBitmapFactory();
    std::unique_ptr<NodeFactory> createTileFactory();

private:
    Document *mDoc;

    GridGraphicsView *mPlotView;
    GridScene *mScene;
    RecordId mPaletteId;
    QGraphicsItem *mProps;

    RepeaterNode *mBgLayer;
    RepeaterNode *mLayoutLayer;

    bool mAllowSpritesSelection;
    bool mAllowEntriesSelection;

    bool mShowOverlay = true;

    QComboBox *mPaletteBox = nullptr;

    TableId mTableId;

    std::unique_ptr<ViewState> mViewState;
};




class PreviewWindow : public Window
{
    Q_OBJECT
public:
    explicit PreviewWindow(Document *document, QWidget *parent = nullptr);
    ~PreviewWindow() override;

private:
    void prepareScene();
    void rebuildScene();

    void paletteReset();
    void paletteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

    void propertiesReset();
    void propertiesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

    void updateAnimation();

    void currentGroupChanged(int group);
    void paletteIndexChanged(int index);

    void populateToolbar(QToolBar *toolBar) const;

private:
    Document *mDoc;
    GridScene *mScene;
    GridGraphicsView *mView;
    AnimationNode *mSpriteItem;
    QTimer *mTimer;
    RecordId mFrameGroupId;
    RecordId mPaletteId;
};






class PropertiesModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit PropertiesModel(QObject *parent = 0);
    ~PropertiesModel() override;

    void reset();

    template<class... U>
    PropertyItem *addPropertyItem(U &&... args)
    {
        auto prop = new PropertyItem(std::forward<U>(args)...);
        mItems.push_back(prop);
        return prop;
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

private:
    // We want an array of pointers since we want some sort of
    // persistency of these objects outside of the model.
    std::vector<PropertyItem *> mItems;
};





class PropertiesWindow : public Window
{
    Q_OBJECT

public:
    explicit PropertiesWindow(Document *doc, QWidget *parent = nullptr);

private:
    QTreeView *mView;

private:
    PropertiesWindow(const PropertiesWindow &) = delete;
    PropertiesWindow &operator=(const PropertiesWindow &) = delete;
};




class PropertyItem
{
public:
    PropertyItem(const QString &name, QVariant::Type type)
        : mIsEditable(false)
        , mName(name)
        , mValue(type)
    {}

    QString name() const { return mName; }
    QVariant value() const { return mValue; }

    void setName(const QString &name) { mName = name; }
    bool setValue(const QVariant &value)
    {
        if (value.canConvert(mValue.type()))
            mValue = value;
        return value.canConvert(mValue.type());
    }

    void setEditable(bool editable) { mIsEditable = editable; }
    bool isEditable() const { return mIsEditable; }

private:
    bool mIsEditable;
    QString mName;
    QVariant mValue;
};





class RecordId final
{
public:
    RecordId()
        : RecordId(-1)
    {}
    explicit RecordId(int id)
        : mId(id)
    {}

    auto isValid() const { return mId != -1; }

    auto toString() const { return QString::number(mId); }

    bool operator<(const RecordId &that) const { return mId < that.mId; }

    bool operator==(const RecordId &that) const { return mId == that.mId; }

    bool operator!=(const RecordId &that) const { return mId != that.mId; }

private:
    int mId;
};





template<class RecordT>
class RecordSet
{
private:
    using storage_type = std::map<RecordId, RecordT>;

public:
    using const_iterator = typename storage_type::const_iterator;
    using iterator = typename storage_type::iterator;
    using value_type = typename storage_type::value_type;

public:
    RecordSet() = default;

    std::set<RecordId> keys() const { return {mIds.begin(), mIds.end()}; }

    std::size_t size() const { return mIds.size(); }

    template<typename SizeT>
    RecordId getIdAt(SizeT position) const
    {
        return mIds.at(position);
    }

    const RecordT &get(const RecordId &id) const { return mEntries.at(id); }

    RecordT &get(const RecordId &id) { return mEntries.at(id); }

    template<typename SizeT>
    RecordId insert(const RecordT &record, SizeT position)
    {
        const RecordId id(mIdCounter);
        ++mIdCounter;

        mEntries[id].assign(record);
        mIds.insert(mIds.begin() + position, id);

        return id;
    }

    RecordId append(const RecordT &record) { return insert(record, size()); }

    void remove(const RecordId &id)
    {
        auto it = mEntries.find(id);
        if (it != mEntries.end()) {
            mIds.erase(std::remove(mIds.begin(), mIds.end(), id), mIds.end());
            mEntries.erase(it);
        }
    }

    bool contains(const RecordId &id) const { return mEntries.find(id) != mEntries.end(); }

    void clear()
    {
        mEntries.clear();
        mIds.clear();
    }

    const_iterator begin() const { return mEntries.begin(); }

    const_iterator end() const { return mEntries.end(); }

    iterator begin() { return mEntries.begin(); }

    iterator end() { return mEntries.end(); }

    using id_storage_type = typename std::vector<RecordId>;
    using id_iterator = typename id_storage_type::iterator;
    using id_value_type = typename id_storage_type::value_type;

    id_iterator idBegin() { return mIds.begin(); }

    id_iterator idEnd() { return mIds.end(); }

private:
    int mIdCounter = 0;

    std::vector<RecordId> mIds;
    std::map<RecordId, RecordT> mEntries;

private:
    RecordSet(const RecordSet &) = delete;
    RecordSet &operator=(const RecordSet &) = delete;
};





class RemoveCommand : public undo::Command
{
public:
    RemoveCommand(int position, TableModel *model, const QString &name = QString());

    ~RemoveCommand() override;

    bool undo() override;
    bool redo() override;

    QString name() const override;

private:
    QString mName;
    std::unique_ptr<AbstractRecord> mRecord;
    int mPos;
    TableModel *mModel;
};






class RepeaterNode : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit RepeaterNode(TableModel *table,
                          std::unique_ptr<NodeFactory> nodeFactory,
                          QGraphicsObject *parent)
        : QGraphicsObject(parent)
        , mTable(table)
        , mNodeFactory(std::move(nodeFactory))
    {
        setFlag(QGraphicsItem::ItemHasNoContents, true);
        for (int i = 0; i < mTable->rowCount(); ++i) {
            auto item = mNodeFactory->createNode(mTable->getIdOf(i));
            if (item != nullptr) {
                item->setParentItem(this);
                item->setZValue(i);
            }
        }
    }

    QRectF boundingRect() const override { return QRectF(); }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override
    {}

    void allowSelection(bool allow)
    {
        for (QGraphicsItem *item : childItems()) {
            item->setFlag(QGraphicsItem::ItemIsSelectable, allow);
        }
    }

    void selectAll()
    {
        for (QGraphicsItem *item : childItems()) {
            item->setSelected(true);
        }
    }

    void unselectAll()
    {
        for (QGraphicsItem *item : childItems()) {
            item->setSelected(false);
        }
    }

    void setViewState(const ViewState &state)
    {
        mViewState.reset(new ViewState(state));
        for (QGraphicsItem *item : childItems()) {
            static_cast<SceneNode *>(item)->setViewState(state);
        }
    }

    void setPropogatePaletteChanges(bool propogatePaletteChanges)
    {
        mPropogatePaletteChanges = propogatePaletteChanges;
    }

    void invalidatePalette()
    {
        if (mPropogatePaletteChanges) {
            for (QGraphicsItem *item : childItems()) {
                static_cast<SceneNode *>(item)->invalidatePalette();
            }
        }
    }

    void removeNodes(int first, int last)
    {
        for (int i = first; i <= last; ++i) {
            auto removedId = mTable->getIdOf(i);
            for (auto child : childItems()) {
                auto childId = child->data(NodeAttribute::RecordId).value<RecordId>();
                if (childId == removedId) {
                    delete child;
                }
            }
        }
    }

    void insertNodes(int first, int last)
    {
        for (int i = first; i <= last; ++i) {
            auto item = mNodeFactory->createNode(mTable->getIdOf(i));
            if (item == nullptr) {
                qFatal("impossible");
            }
            item->setZValue(0);
            item->setParentItem(this);
            if (mViewState) {
                item->setViewState(*mViewState);
            }
        }

        for (auto child : childItems()) {
            auto id = child->data(NodeAttribute::RecordId).value<RecordId>();
            child->setZValue(mTable->getPositionOf(id));
        }
    }

    void moveNodes(int start, int end, int row)
    {
        Q_UNUSED(start);
        Q_UNUSED(end);
        Q_UNUSED(row);

        for (auto child : childItems()) {
            auto id = child->data(NodeAttribute::RecordId).value<RecordId>();
            child->setZValue(mTable->getPositionOf(id));
        }
    }

signals:
    void selectionChanged();

private:
    TableModel *mTable;
    std::unique_ptr<NodeFactory> mNodeFactory;
    bool mPropogatePaletteChanges = false;
    std::unique_ptr<ViewState> mViewState;
};




namespace bp = boost::python;

class Repl : public QObject {
    Q_OBJECT
public:
    Repl();
    ~Repl() override;

    static Repl* instance()
    {
        static Repl repl;
        return &repl;
    }

    void redirectStdHandles();
    bool grabStdout(std::string &output);
    bool grabStderr(std::string &error);
    bool createWorld(Document *doc);
    void destroyWorld();
    bool execute(const std::string &commands);
    bool evaluate(const std::string &commands);

signals:
    void outputStderr(const std::string &output);
    void outputStdout(const std::string &output);

private:
    bool mInitialized = false;
    bp::object mMain;
    bp::dict mGlobals;
    bp::object mWorldMod;
    World mWorld;
    Document *mDoc = nullptr;
};


}




class ReplHighlighter : public QSyntaxHighlighter {
    Q_OBJECT

public:
    ReplHighlighter(QTextDocument *parent);

    void highlightUsingFormat(const QString &text,
                              const QRegularExpression &expr,
                              const QTextCharFormat &format);

protected:
    void highlightBlock(const QString &text) override;
};

}



class ReplWindow : public Window {
    Q_OBJECT
public:
    explicit ReplWindow(Document *doc, QWidget *parent = nullptr);
    ~ReplWindow() override;

    void processStdin(const std::string &input);
    void processStderr(const std::string &output);
    void processStdout(const std::string &output);

    void runCommand(const std::string &commands);

protected:
    bool eventFilter(QObject *obj, QEvent *event) override;

private:
    Document *mDoc;
    Repl *mRepl;

    QPlainTextEdit *mInput;
    QPlainTextEdit *mHistory;
};

}




struct ISceneNode {
    virtual ~ISceneNode() {}
    virtual void reloadModelData() = 0;
    virtual void invalidatePalette() = 0;
    virtual void setViewState(const ViewState &view) = 0;
};

class SceneNode : public QGraphicsObject,
                  public ISceneNode
{
public:
    using QGraphicsObject::QGraphicsObject;

    virtual void invalidatePalette() override {}
    virtual void setViewState(const ViewState &view) override {}

    virtual void moveTo(undo::Macro *macro, const QPoint &toPoint) {}
};



class SeverityFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    explicit SeverityFilterModel(const QFlags<Severity> &severity, QObject *parent = 0);
    ~SeverityFilterModel() override;

    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
    QFlags<Severity> mSeverity;
};




inline QDebug operator<<(QDebug dbg, const Severity &s)
{
    switch (s) {
    case Severity::Error:
        dbg << QObject::tr("Severity Error");
        break;

    case Severity::Warning:
        dbg << QObject::tr("Severity Warning");
        break;

    case Severity::Information:
        dbg << QObject::tr("Severity Information");
        break;

    default:
        dbg << QObject::tr("Severity unknown");
        break;
    }

    return dbg;
}




class TableModel;

class SimpleSpriteSheet : public ISpriteSheet
{
public:
    explicit SimpleSpriteSheet(PaletteModel *palette);
    ~SimpleSpriteSheet() override;

    SpriteModel *getSpritesheet() override { return mSpriteTable.get(); }
    PaletteModel *getPalette() override { return mPalette; }

    void append(const SpriteRecord &record);

private:
    RecordSet<SpriteRecord> mSprites;
    std::unique_ptr<SpriteModel> mSpriteTable;
    PaletteModel *mPalette;
};



class ISpriteSheet;

class SpriteGraphicsItem : public SceneNode
{
public:
    enum { Type = SpriteGraphicsItemType };

    explicit SpriteGraphicsItem(ISpriteSheet *spritesheet,
                                const RecordId &id,
                                SceneNode *parent = nullptr);

    void setViewState(const ViewState &state) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    void invalidatePalette() override;

    QColor pixelAt(const QPoint &point);

    void moveTo(undo::Macro *macro, const QPoint &toPoint) override;

    int type() const override;

private:
    void createPixmap(bool force = false);

    void spriteChanged();
    void positionChanged();

private:
    ISpriteSheet *mSpriteSheet;
    SpriteRecord *mRecord;
    RecordId mRecordId;
    RecordId mPaletteId;
    QPixmap mCachedPixmap;
    QSize mRecordSize;
};






class LogModel;

class SpriteLoader
{
public:
    virtual ~SpriteLoader() {}

    virtual void setPosition(const QPoint &point){};

    virtual std::vector<std::unique_ptr<AbstractRecord>>
    loadSprites(const std::vector<boost::filesystem::path> &loadPaths, QWidget *parent) = 0;

    virtual void setLogModel(LogModel *model) {}
};






class LogModel;

class SpritePlotter
{
public:
    SpritePlotter();

    void addImage(const QImage &image, const QRect &rect, const QBitmap &mask);
    void setTransparentPixel(uint pixel);
    void setTransparentIndex(int index);
    bool render(QImage::Format format, const QRect &bounds, LogModel *log);
    QImage plot() const;
    QPoint origin() const;

private:
    struct Item
    {
        QImage image;
        QRect rect;
        QBitmap mask;
    };

    std::vector<Item> mItems;
    QPoint mOrigin;
    QImage mPlot;
    boost::optional<uint> mTransparentPixel;
    boost::optional<int> mTransparentIndex;
};







class SpriteRecordData
{
public:
    QImage mImg;
    mutable QBitmap mMask;
    QPoint mPos;
    QRgb mTransparentPixel = 0;
    TransparencyMode mTransparencyMode = TransparencyMode::None;
    boost::filesystem::path mPath;
};

class SpriteRecord : public AbstractRecord, public SpriteRecordData
{
    Q_OBJECT

public:
    explicit SpriteRecord(const SpriteRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , SpriteRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new SpriteRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        SpriteRecordData::operator=(dynamic_cast<const SpriteRecordData &>(other));
        emit imageChanged();
        emit positionChanged();
    }

    auto isNull() const { return mImg.isNull() && mPos.isNull(); }

    QBitmap mask() const
    {
        if (mMask.isNull() && (mTransparencyMode == TransparencyMode::Color)) {
            mMask = createColorMask(mTransparentPixel, mImg);
        }
        return mMask;
    }

    auto image() const { return mImg; }
    void setImage(const QImage &image)
    {
        mImg = image;
        emit imageChanged();
    }
    auto position() const { return mPos; }
    void setPosition(const QPoint &position)
    {
        mPos = position;
        emit positionChanged();
    }
    auto left() const { return mPos.x(); }
    void setLeft(int left)
    {
        mPos.setX(left);
        emit positionChanged();
    }
    auto top() const { return mPos.y(); }
    void setTop(int top)
    {
        mPos.setY(top);
        emit positionChanged();
    }
    auto width() const { return mImg.width(); }
    auto height() const { return mImg.height(); }
    auto rect() const { return QRect(mPos, mImg.size()); }

    void setTransparentPixel(const QRgb &color)
    {
        mTransparentPixel = color;
        mMask = QBitmap();
        emit imageChanged();
    }

    void setMask(const QBitmap &mask)
    {
        mMask = mask;
        emit imageChanged();
    }

    void setTransparencyMode(TransparencyMode mode)
    {
        mTransparencyMode = mode;
        emit imageChanged();
    }

    auto filePath() const { return mPath; }
    void setFilePath(const boost::filesystem::path &path) { mPath = path; }

signals:
    void positionChanged();
    void imageChanged();
};




class ISpriteSheet
{
public:
    virtual ~ISpriteSheet(){};
    virtual SpriteModel *getSpritesheet() = 0;
    virtual PaletteModel *getPalette() = 0;
};




class Document;

class SpritesWindow : public Window
{
    Q_OBJECT

public:
    explicit SpritesWindow(Document *doc, QWidget *parent = nullptr);
    ~SpritesWindow() override;

    bool focusEntry(const RecordId &id) override;

    void route(ImportSprites *action) override;
    void unroute(ImportSprites *action) override;

    void route(ExportSprites *action) override;
    void unroute(ExportSprites *action) override;

    void route(RemoveSpriteOrEntry *action) override;
    void unroute(RemoveSpriteOrEntry *action) override;

    void route(MoveForward *action) override;
    void unroute(MoveForward *action) override;

    void route(MoveBackward *action) override;
    void unroute(MoveBackward *action) override;

    void route(BringForward *action) override;
    void unroute(BringForward *action) override;

    void route(SendBack *action) override;
    void unroute(SendBack *action) override;

signals:
    void gotAnyRowSelected(bool any);

private slots:
    void add();
    void save();
    void remove();
    void bringForward();
    void sendBack();
    void moveForward();
    void moveBackward();

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    Document *mDoc;
    QTableView *mView;
};





struct SubTile
{
    int order;
    int xSkewed;
    int ySkewed;
    int xAligned;
    int yAligned;
    TileAlignment alignment;
    QRect tileBbox;
    QRect entryBbox;
    int boxHeight;
};

void walkSubTiles(const TileRecord &record, const std::function<void(SubTile)> &visitor);





/// Templated adapter of RecordSet for Qt's object model.
template<class RecordT>
class TableAdapter : public AbstractTable
{
public:
    TableAdapter(RecordSet<RecordT> &records)
        : mRecords(records)
    {}

    template<class Column, class... Args>
    void addColumn(Args &&... args)
    {
        mColumns.emplace_back();
        mColumns.back().reset(new Column(std::forward<Args>(args)...));
    }

    virtual bool setData(int row, int col, const QVariant &value) override
    {
        return mColumns.at(col)->setData(mRecords.get(mRecords.getIdAt(row)), value);
    }

    virtual QVariant displayData(int row, int col) const override
    {
        return mColumns.at(col)->displayData(mRecords.get(mRecords.getIdAt(row)));
    }

    virtual QVariant editData(int row, int col) const override
    {
        return mColumns.at(col)->editData(mRecords.get(mRecords.getIdAt(row)));
    }

    virtual int rowCount() const override { return static_cast<int>(mRecords.size()); }

    virtual int columnCount() const override { return static_cast<int>(mColumns.size()); }

    virtual Qt::ItemFlags flags(int column) const override { return mColumns.at(column)->flags(); }

    virtual QVariant headerData(int column) const override
    {
        return mColumns.at(column)->headerData();
    }

    virtual bool insertRows(int row, int count) override
    {
        for (int i = 0; i < count; ++i) {
            mRecords.insert(RecordT(), row + i);
        }
        return true;
    }

    virtual bool removeRows(int row, int count) override
    {
        for (int i = 0; i < count; ++i) {
            mRecords.remove(mRecords.getIdAt(row));
        }
        return true;
    }

    virtual RecordId getRecordIdByRow(int row) const override { return mRecords.getIdAt(row); }

    virtual const AbstractRecord *getRecord(const RecordId &id) const override
    {
        return &mRecords.get(id);
    }

    virtual AbstractRecord *getRecord(const RecordId &id) override { return &mRecords.get(id); }

    virtual void moveRows(int source, int count, int dest) override
    {
        if (source < dest) {
            std::rotate(mRecords.idBegin() + source,
                        mRecords.idBegin() + source + count,
                        mRecords.idBegin() + dest);
        } else {
            std::rotate(mRecords.idBegin() + dest,
                        mRecords.idBegin() + source,
                        mRecords.idBegin() + source + count);
        }
    }

private:
    RecordSet<RecordT> &mRecords;

    std::vector<std::unique_ptr<TableColumn<RecordT>>> mColumns;

    TableAdapter(const TableAdapter &) = delete;
    TableAdapter &operator=(const TableAdapter &) = delete;
};





template<class RecordT>
class TableColumn
{
public:
    explicit TableColumn(const QString &title)
        : mHeaderData(title)
    {}

    virtual ~TableColumn() {}

    virtual QVariant displayData(const RecordT &row) const = 0;

    virtual QVariant editData(const RecordT &row) const { return QVariant(); }

    virtual bool setData(RecordT & /* row */, const QVariant & /* data */) const { return false; }

    virtual QVariant headerData() const { return mHeaderData; }

    virtual bool isEditable() const { return false; }

    virtual Qt::ItemFlags flags() const
    {
        Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        if (isEditable()) {
            flags |= Qt::ItemIsEditable;
        }
        return flags;
    }

private:
    QVariant mHeaderData;
};

template<class T, class RecordT>
class MyColumn : public TableColumn<RecordT>
{
    using get_t = std::function<T(const RecordT *)>;
    using set_t = std::function<void(RecordT *, const T &)>;

public:
    explicit MyColumn(const QString &name, get_t get, set_t set = set_t())
        : TableColumn<RecordT>(name)
        , mGet(get)
        , mSet(set)
    {}

    virtual bool check(const RecordT & /* record */, const T & /* value */) const { return true; }

    QVariant displayData(const RecordT &record) const override
    {
        return QVariant::fromValue<T>(mGet(&record));
    }

    QVariant editData(const RecordT &record) const override
    {
        return QVariant::fromValue<T>(mGet(&record));
    }

    bool setData(RecordT &record, const QVariant &data) const override
    {
        if (!data.canConvert<T>()) {
            return false;
        }
        if (!check(record, data.value<T>())) {
            return false;
        }
        mSet(&record, data.value<T>());
        return true;
    }

    bool isEditable() const override { return mSet.operator bool(); }

private:
    get_t mGet;
    set_t mSet;
};




inline QString getTableElementName(TableId tableId)
{
    return QString();
}







struct IAssignRow {
    virtual const AbstractRecord *getRecord(int row) const = 0;
    virtual AbstractRecord *getRecord(int row) = 0;
};

struct IAssignRecord {
    virtual const AbstractRecord* getRecordById(const RecordId &id) const = 0;
    virtual AbstractRecord* getRecordById(const RecordId &id) = 0;
};

class TableModel : public QAbstractItemModel,
                   public IAssignRow,
                   public IAssignRecord
{
public:
    explicit TableModel(std::unique_ptr<AbstractTable> table, QObject *parent = 0);

    ~TableModel() override = default;

    /// Reset current table in the model causing all views to reset aswell.
    void reset(std::unique_ptr<AbstractTable> table);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    bool moveRows(const QModelIndex &sourceParent,
                  int sourceRow,
                  int count,
                  const QModelIndex &destinationParent,
                  int destinationChild) override;

    RecordId getIdOf(int row) const;

    int getPositionOf(const RecordId &id) const;

protected:
    std::unique_ptr<AbstractTable> mTable;

    TableModel(const TableModel &) = delete;
    TableModel &operator=(const TableModel &) = delete;
};

template<class RecordT>
class RecordModelHelper : public TableModel {
public:
    using TableModel::TableModel;
    const RecordT *getRecordById(const RecordId &id) const override {
        return dynamic_cast<const RecordT*>(mTable->getRecord(id));
    }
    RecordT *getRecordById(const RecordId &id) override {
        return const_cast<RecordT*>(const_cast<const RecordModelHelper<RecordT>*>(this)->getRecordById(id));
    }
    const RecordT *getRecord(int row) const override {
        return dynamic_cast<const RecordT*>(mTable->getRecord(getIdOf(row)));
    }
    RecordT *getRecord(int row) override {
        return const_cast<RecordT*>(const_cast<const RecordModelHelper<RecordT>*>(this)->getRecord(row));
    }
};

using AnimationModel = RecordModelHelper<AnimationRecord>;
using PaletteModel = RecordModelHelper<PaletteRecord>;
using SpriteModel = RecordModelHelper<SpriteRecord>;
using TileModel = RecordModelHelper<TileRecord>;
using BitmapModel = RecordModelHelper<BitmapRecord>;
using FontModel = RecordModelHelper<FontRecord>;
using AnimationGroupModel = RecordModelHelper<AnimationFrameGroup>;

inline TableModel::TableModel(std::unique_ptr<AbstractTable> table, QObject *parent)
    : QAbstractItemModel(parent)
    , mTable(std::move(table))
{}

inline void TableModel::reset(std::unique_ptr<AbstractTable> table)
{
    beginResetModel();
    mTable = std::move(table);
    endResetModel();
}

inline int TableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return mTable->rowCount();
}

inline QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if ((index.row() >= rowCount()) || (index.column() >= columnCount())) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return mTable->displayData(index.row(), index.column());
    } else if (role == Qt::EditRole) {
        return mTable->editData(index.row(), index.column());
    }

    return QVariant();
}

inline QModelIndex TableModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid())
        return QModelIndex();

    if (row < 0 || row >= rowCount())
        return QModelIndex();

    if (column < 0 || column >= columnCount())
        return QModelIndex();

    return QAbstractItemModel::createIndex(row, column);
}

inline QModelIndex TableModel::parent(const QModelIndex & /* index */) const
{
    return QModelIndex();
}

inline int TableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return mTable->columnCount();
}

inline QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    if (orientation == Qt::Horizontal) {
        return mTable->headerData(section);
    } else {
        return QVariant();
    }
}

inline Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    if (index.column() >= columnCount()) {
        return 0;
    }

    return mTable->flags(index.column());
}

inline bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole) {
        return false;
    }

    if (index.row() >= rowCount() || index.column() >= columnCount()) {
        return false;
    }

    if (mTable->setData(index.row(), index.column(), value)) {
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

inline bool TableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if (parent.isValid()) {
        return false;
    }

    if (count <= 0) {
        return false;
    }

    beginInsertRows(parent, row, row + count - 1);
    const bool ok = mTable->insertRows(row, count);
    endInsertRows();
    return ok;
}

inline bool TableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (parent.isValid()) {
        return false;
    }

    if (count <= 0) {
        return false;
    }

    beginRemoveRows(parent, row, row + count - 1);
    const bool ok = mTable->removeRows(row, count);
    endRemoveRows();
    return ok;
}

inline bool TableModel::moveRows(const QModelIndex &sourceParent,
                          int sourceRow,
                          int count,
                          const QModelIndex &destinationParent,
                          int destinationChild)
{
    if (sourceParent.isValid() || destinationParent.isValid()) {
        return false;
    }
    if (count <= 0) {
        return false;
    }
    if (sourceRow + count > rowCount()) {
        return false;
    }
    if (destinationChild > rowCount()) {
        return false;
    }
    if ((sourceRow >= destinationChild) && (sourceRow + count <= destinationChild)) {
        return false;
    }
    if (!beginMoveRows(sourceParent,
                       sourceRow,
                       sourceRow + count - 1,
                       destinationParent,
                       destinationChild)) {
        return false;
    }
    mTable->moveRows(sourceRow, count, destinationChild);
    endMoveRows();
    return true;
}

inline RecordId TableModel::getIdOf(int row) const
{
    return mTable->getRecordIdByRow(row);
}

inline int TableModel::getPositionOf(const RecordId &id) const
{
    for (int row = 0; row < rowCount(); ++row) {
        if (getIdOf(row) == id) {
            return row;
        }
    }
    return -1;
}

class TgxIoHandler : public QImageIOHandler
{
public:
    explicit TgxIoHandler();
    ~TgxIoHandler() override;

    bool canRead() const override;
    bool read(QImage *image) override;
    bool write(const QImage &image) override;
};

class TgxPlugin : public QImageIOPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QImageIOHandlerFactoryInterface" FILE "tgxplugin.json")

public:
    explicit TgxPlugin(QObject *parent = nullptr);
    ~TgxPlugin() override;

    QImageIOPlugin::Capabilities capabilities(QIODevice *device,
                                              const QByteArray &format) const override;
    QImageIOHandler *create(QIODevice *device,
                            const QByteArray &format = QByteArray()) const override;
};

static inline const char *debugAlignmentToString(TileAlignment alignment)
{
    switch (alignment) {
    case TileAlignment::None:
        return "none";
    case TileAlignment::Center:
        return "center";
    case TileAlignment::Left:
        return "left";
    case TileAlignment::Right:
        return "right";
    }
    return "(invalid)";
}

class TileEditor : public DocumentEditor
{
public:
    TileEditor();
    ~TileEditor() override;

    std::unique_ptr<AbstractRecord> createBlankEntry() override;
    std::unique_ptr<SpriteLoader> createSpriteLoader() override;
    bool load(const Gm1FileData &fileData, LogModel *log) override;
    bool save(Gm1FileData *fileData, LogModel *log) override;
    bool renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result) override;

    TableId getEntryTableId() const override { return TableId::Tile; }
private:
    QRgb getDefaultTransparentPixel() const;

private:
    TileEditor(const TileEditor &) = delete;
    TileEditor &operator=(const TileEditor &) = delete;
};

class TileGraphicsItem : public SceneNode
{
public:
    explicit TileGraphicsItem(TileRecord *record, SceneNode *parent = nullptr);

    void setViewState(const ViewState &state) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    void moveTo(undo::Macro *macro, const QPoint &toPoint) override;

private:
    void createSubTiles(QGraphicsItem *parent, const TileRecord &record);

private:
    TileRecord *mRecord;
    EmptyNode *mOverlay;
};

class TileRecordData
{
public:
    TileRecordData()
        : mSize(1)
        , mBoxHeight(0)
    {}

    auto tileHeight() const { return 16; }

    auto tileWidth() const { return 30; }

    auto widthStride() const { return 32; }

    auto heightStride() const { return 16; }

    auto rect() const
    {
        return QRect(mPos,
                     QSize(widthStride() * (mSize - 1) + tileWidth(),
                           mBoxHeight + heightStride() * mSize));
    }

    int width() const { return rect().width(); }
    int height() const { return rect().height(); }

    int left() const { return rect().x(); }
    void setTop(int top) { mPos.setY(top); }

    int size() const { return mSize; }

    void setSize(int size) { mSize = qMax(1, size); }

    int top() const { return rect().y(); }
    void setLeft(int left) { mPos.setX(left); }

    int boxHeight() const { return mBoxHeight; }
    void setBoxHeight(int boxHeight) { mBoxHeight = boxHeight; }

private:
    QPoint mPos;

    //! 1 - regular tile
    //! 2 - square 2x2
    //! 3 - square 3x3
    int mSize;

    int mBoxHeight;
};

class TileRecord : public AbstractRecord, public TileRecordData
{
    Q_OBJECT

public:
    explicit TileRecord(const TileRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , TileRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new TileRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        TileRecordData::operator=(dynamic_cast<const TileRecordData &>(other));
    }
};

namespace undo {

class Command
{
public:
    virtual ~Command() = default;
    virtual bool redo() = 0;
    virtual bool undo() = 0;
    virtual QString name() const = 0;
    virtual int id() const { return -1; }
    virtual bool mergeWith(undo::Command *command) { return false; }
};

class Macro : public Command
{
public:
    explicit Macro(const QString &name)
        : mName(name)
    {}
    ~Macro() override = default;

    void add(std::unique_ptr<Command> command);
    bool redo() override;
    bool undo() override;

    QString name() const override;

    inline bool empty() const { return mCommands.empty(); }

private:
    QString mName;
    std::vector<std::unique_ptr<Command>> mCommands;
};

class Stack : public QObject
{
    Q_OBJECT

public:
    Stack();
    ~Stack() override;

    bool push(std::unique_ptr<Command> command);

    void clear();

    void undo();
    void redo();

    QAction *createUndoAction(QObject *parent, const QString &title);
    QAction *createRedoAction(QObject *parent, const QString &title);

signals:
    void redoChanged(Command *command);
    void undoChanged(Command *command);

private:
    std::vector<std::unique_ptr<Command>> mRedoed;
    std::vector<std::unique_ptr<Command>> mUndoed;
    std::unique_ptr<Macro> mCurrentMacro;
};

} // namespace undo




class ViewState final
{
public:
    ViewState() = default;

    ViewState(const RecordId &paletteId, bool showOverlay = false)
        : showOverlay(showOverlay)
        , paletteId(paletteId)
    {}

    bool showOverlay = false;
    RecordId paletteId;
};




struct ImportSprites : QAction
{
    using QAction::QAction;
};

struct ExportSprites : QAction
{
    using QAction::QAction;
};

struct LoadPalette : QAction
{
    using QAction::QAction;
};

struct SavePalette : QAction
{
    using QAction::QAction;
};

struct ChangeHue : QAction
{
    using QAction::QAction;
};

struct FindSpriteOrEntry : QAction
{
    using QAction::QAction;
};

struct RemoveSpriteOrEntry : QAction
{
    using QAction::QAction;
};

struct AddEntry : QAction
{
    using QAction::QAction;
};

struct MoveForward : QAction
{
    using QAction::QAction;
};

struct MoveBackward : QAction
{
    using QAction::QAction;
};

struct BringForward : QAction
{
    using QAction::QAction;
};

struct SendBack : QAction
{
    using QAction::QAction;
};

struct SelectAll : QAction
{
    using QAction::QAction;
};

struct SelectClear : QAction
{
    using QAction::QAction;
};

struct ScrollMode : QAction
{
    using QAction::QAction;
};

struct TransformMode : QAction
{
    using QAction::QAction;
};

struct AllowSelectSprites : QAction
{
    using QAction::QAction;
};

struct AllowSelectEntries : QAction
{
    using QAction::QAction;
};

struct SnapToGrid : QAction
{
    using QAction::QAction;
};

struct ShowGrid : QAction
{
    using QAction::QAction;
};

struct ShowOverlay : QAction
{
    using QAction::QAction;
};

class Window : public QWidget
{
    Q_OBJECT

public:
    explicit Window(QWidget *parent = nullptr);

    virtual bool focusEntry(const RecordId &id);

    virtual void route(ImportSprites *action) {}
    virtual void route(ExportSprites *action) {}
    virtual void route(LoadPalette *action) {}
    virtual void route(SavePalette *action) {}
    virtual void route(ChangeHue *action) {}
    virtual void route(FindSpriteOrEntry *action) {}
    virtual void route(RemoveSpriteOrEntry *action) {}
    virtual void route(AddEntry *action) {}
    virtual void route(MoveForward *action) {}
    virtual void route(MoveBackward *action) {}
    virtual void route(BringForward *action) {}
    virtual void route(SendBack *action) {}
    virtual void route(SelectAll *action) {}
    virtual void route(SelectClear *action) {}
    virtual void route(ScrollMode *action) {}
    virtual void route(TransformMode *action) {}
    virtual void route(AllowSelectSprites *action) {}
    virtual void route(AllowSelectEntries *action) {}
    virtual void route(SnapToGrid *action) {}
    virtual void route(ShowGrid *action) {}
    virtual void route(ShowOverlay *action) {}

    virtual void unroute(ImportSprites *action) {}
    virtual void unroute(ExportSprites *action) {}
    virtual void unroute(LoadPalette *action) {}
    virtual void unroute(SavePalette *action) {}
    virtual void unroute(ChangeHue *action) {}
    virtual void unroute(FindSpriteOrEntry *action) {}
    virtual void unroute(RemoveSpriteOrEntry *action) {}
    virtual void unroute(AddEntry *action) {}
    virtual void unroute(MoveForward *action) {}
    virtual void unroute(MoveBackward *action) {}
    virtual void unroute(BringForward *action) {}
    virtual void unroute(SendBack *action) {}
    virtual void unroute(SelectAll *action) {}
    virtual void unroute(SelectClear *action) {}
    virtual void unroute(ScrollMode *action) {}
    virtual void unroute(TransformMode *action) {}
    virtual void unroute(AllowSelectSprites *action) {}
    virtual void unroute(AllowSelectEntries *action) {}
    virtual void unroute(SnapToGrid *action) {}
    virtual void unroute(ShowGrid *action) {}
    virtual void unroute(ShowOverlay *action) {}

signals:
    void statusTextChanged(const QString &text);

private:
    Window(const Window &) = delete;
    Window &operator=(const Window &) = delete;
};





class World
{
public:
    void set(std::string msg);
    void many(boost::python::list msgs);
    std::string greet();
private:
    std::string mMsg;
};


class ZoomComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit ZoomComboBox(QWidget *parent = nullptr);

    void setGraphicsView(GridGraphicsView *view);

private:
    void myEditTextChanged(const QString &text);
    void myCurrentIndexChanged(int index);
    void viewScaleChanged(float delta);

private:
    GridGraphicsView *mView;
    ZoomModel *mModel;
};




class ZoomModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ZoomModel(QObject *parent = 0);

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int getValue(int index) const;
    int indexOf(int value) const;
    int defaultIndex() const;

private:
    std::vector<int> mZoomValues;
};

} // namespace gmtool

Q_DECLARE_METATYPE(gmtool::RecordId)
Q_DECLARE_METATYPE(gmtool::Severity)
Q_DECLARE_FLAGS(SeverityLevels, gmtool::Severity)
Q_DECLARE_OPERATORS_FOR_FLAGS(SeverityLevels)
Q_DECLARE_METATYPE(gmtool::TileAlignment)
Q_DECLARE_METATYPE(gmtool::TableId)
Q_DECLARE_METATYPE(gmtool::TransparencyMode)
