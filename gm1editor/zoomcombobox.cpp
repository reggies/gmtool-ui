#include "zoomcombobox.h"

#include "gridgraphicsview.h"
#include "zoommodel.h"

#include <QDebug>
#include <QRegExp>

namespace gmtool {

const static char *kValidatorRegExp = "(\\d+)[%]?";

ZoomComboBox::ZoomComboBox(QWidget *parent)
    : QComboBox(parent)
{
    mModel = new ZoomModel(this);
    setModel(mModel);
    setCurrentIndex(mModel->defaultIndex());
    setEditable(true);
    setMinimumContentsLength(6);
    setValidator(new QRegExpValidator(QRegExp(kValidatorRegExp), this));
    connect(this,
            QOverload<int>::of(&QComboBox::currentIndexChanged),
            this,
            &ZoomComboBox::myCurrentIndexChanged);
    connect(this, &QComboBox::editTextChanged, this, &ZoomComboBox::myEditTextChanged);
}

void ZoomComboBox::setGraphicsView(GridGraphicsView *view)
{
    if (mView == view) {
        return;
    }
    if (mView != nullptr) {
        disconnect(mView, nullptr, this, nullptr);
    }
    if (view != nullptr) {
        connect(view, &GridGraphicsView::scaleChanged, this, &ZoomComboBox::viewScaleChanged);
    }
    mView = view;
}

void ZoomComboBox::myCurrentIndexChanged(int index)
{
    if (mView == nullptr || index == -1)
        return;

    /*!
      getting current scale off the view.

      note that we don't use viewportTransform() because it is just the
      transform which is translated to the scrollbars position -
      simple optimization.

    */
    auto dp = QLineF(mView->transform().map(QPointF(0, 0)), mView->transform().map(QPointF(1, 0)));

    float factor = mModel->getValue(index) / 100.f / dp.length();

    mView->scale(factor, factor);
}

void ZoomComboBox::viewScaleChanged(float delta)
{
    Q_UNUSED(delta);

    if (mView == nullptr)
        return;

    auto dp = QLineF(mView->transform().map(QPointF(0, 0)), mView->transform().map(QPointF(1, 0)));

    int percent = dp.length() * 100.0;
    setEditText(tr("%1%").arg(percent));
}

void ZoomComboBox::myEditTextChanged(const QString &text)
{
    if (mView == nullptr || text.isEmpty())
        return;

    QRegExp re(kValidatorRegExp);
    if (!re.exactMatch(text))
        return;

    bool goodInt;
    int percent = re.cap(0).toInt(&goodInt);
    if (!goodInt)
        return;

    auto dp = QLineF(mView->transform().map(QPointF(0, 0)), mView->transform().map(QPointF(1, 0)));
    float factor = percent / 100.f / dp.length();
    mView->scale(factor, factor);
}

} // namespace gmtool
