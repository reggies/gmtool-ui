#include "basicspriteloader.h"

#include <QDebug>
#include <QMessageBox>

#include "documenteditor.h"
#include "logmodel.h"
#include "spriterecord.h"

namespace gmtool {

namespace fs = boost::filesystem;

BasicSpriteLoader::BasicSpriteLoader(DocumentEditor *editor,
                                     TransparencyMode mode,
                                     QRgb defaultTransparentPixel,
                                     QImage::Format format)
    : mFormat(format)
    , mEditor(editor)
    , mTransparentPixel(defaultTransparentPixel)
    , mTransparencyMode(mode)
{}

void BasicSpriteLoader::setPosition(const QPoint &position)
{
    mPosition = position;
}

std::vector<std::unique_ptr<AbstractRecord>> BasicSpriteLoader::loadSprites(
    const std::vector<boost::filesystem::path> &paths, QWidget *parent)
{
    std::vector<std::unique_ptr<AbstractRecord>> sprites;

    std::vector<fs::path> badPaths;
    for (const fs::path &path : paths) {
        QImage image;
        if (!image.load(path.string().c_str())) {
            if (mLog != nullptr) {
                mLog->addMessage(QObject::tr("Could not read image file: %1")
                                     .arg(QString::fromStdWString(path.wstring())),
                                 Severity::Error);
            } else {
                badPaths.push_back(path);
            }
        }

        SpriteRecord record;
        record.setTransparencyMode(mTransparencyMode);
        switch (mTransparencyMode) {
        case TransparencyMode::Alpha:
            /*!
              strip color components and use only alpha as red, green
              and blue.
            */
            if (image.hasAlphaChannel()) {
                QImage alphaChannel = image.alphaChannel();
                image.fill(0xFFFFFFFF);
                image.setAlphaChannel(alphaChannel);
            } else {
                if (mLog != nullptr) {
                    mLog->addMessage(QObject::tr("Image '%1' must contain alpha channel")
                                         .arg(QString::fromStdWString(path.wstring())),
                                     Severity::Error);
                } else {
                    badPaths.push_back(path);
                }
                continue;
            }
            break;
        case TransparencyMode::Color: {
            QBitmap alphaMask;
            if (image.hasAlphaChannel()) {
                //! The color depth of the returned image is 8-bit.
                QImage alphaChannel = image.alphaChannel();
                alphaChannel.invertPixels(QImage::InvertRgba);
                alphaMask.convertFromImage(alphaChannel);
            }
            /*!
              RGB555 for most of the formats. ARGB32 for fonts.
            */
            if (image.format() != mFormat) {
                image = image.convertToFormat(mFormat);
            }
            if (alphaMask.isNull()) {
                record.setTransparentPixel(mTransparentPixel);
            } else {
                record.setTransparencyMode(TransparencyMode::Color);
                record.setMask(alphaMask);
            }
        } break;
        case TransparencyMode::None: {
            /*!
              Just downsample the image to RGB555 to let
              the user see the result of saving .gm1.
            */
            if (image.format() != mFormat) {
                image = image.convertToFormat(mFormat);
            }
        } break;
        }
        record.setPosition(mPosition);
        record.setFilePath(path);
        record.setImage(image);
        sprites.emplace_back(record.clone());
    }

    if (!badPaths.empty()) {
        QString text = QObject::tr("Could not open the following images:");
        for (const fs::path &path : badPaths) {
            text += QString("\n\"%1\"").arg(path.string().c_str());
        }
        QMessageBox dialog(QMessageBox::Critical,
                           QObject::tr("Error"),
                           text,
                           QMessageBox::NoButton,
                           parent);
        dialog.exec();
    }

    return sprites;
}

void BasicSpriteLoader::setLogModel(LogModel *logModel)
{
    mLog = logModel;
}

} // namespace gmtool
