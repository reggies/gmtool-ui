#pragma once

#include "animationframegroup.h"
#include "recordid.h"
#include "window.h"

#include <QComboBox>

namespace gmtool {
class Document;
class GridScene;
class GridGraphicsView;
class AnimationNode;

class PreviewWindow : public Window
{
    Q_OBJECT
public:
    explicit PreviewWindow(Document *document, QWidget *parent = nullptr);
    ~PreviewWindow() override;

private:
    void prepareScene();
    void rebuildScene();

    void paletteReset();
    void paletteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

    void propertiesReset();
    void propertiesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

    void updateAnimation();

    void currentGroupChanged(int group);
    void paletteIndexChanged(int index);

    void populateToolbar(QToolBar *toolBar) const;

private:
    Document *mDoc;
    GridScene *mScene;
    GridGraphicsView *mView;
    AnimationNode *mSpriteItem;
    QTimer *mTimer;
    RecordId mFrameGroupId;
    RecordId mPaletteId;
};

} // namespace gmtool
