#pragma once

#include <QGraphicsObject>
#include <QMetaType>

#include <memory>

namespace gmtool {
class RecordId;

class AnimationNode : public QGraphicsObject
{
    Q_OBJECT
public:
    using QGraphicsObject::QGraphicsObject;

    virtual ~AnimationNode() {}

    virtual void reloadModelData() = 0;
    virtual void invalidatePalette() {}
    virtual void setPaletteId(const RecordId &paletteId) {}

    virtual void advance(float dtime) = 0;
    virtual float totalDuration() = 0;
    virtual int totalFrames() = 0;
    virtual void rewind(float pos) {}
};

} // namespace gmtool
