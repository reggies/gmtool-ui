#include "tgxplugin.h"

#include "tgxiohandler.h"

namespace gmtool {

TgxPlugin::TgxPlugin(QObject *parent)
    : QImageIOPlugin(parent)
{}

TgxPlugin::~TgxPlugin() {}

QImageIOPlugin::Capabilities TgxPlugin::capabilities(QIODevice *device,
                                                     const QByteArray &format) const
{
    if (format == "tgx")
        return QImageIOPlugin::Capabilities(CanRead | CanWrite);
    if (!format.isEmpty() || !device->isOpen())
        return 0;

    QImageIOPlugin::Capabilities cap;
    if (device->isReadable())
        cap |= CanRead;
    if (device->isWritable())
        cap |= CanWrite;
    return cap;
}

QImageIOHandler *TgxPlugin::create(QIODevice *device, const QByteArray &format) const
{
    QImageIOHandler *handler = new TgxIoHandler;
    handler->setDevice(device);
    handler->setFormat(format);
    return handler;
}

} // namespace gmtool
