#pragma once

namespace gmtool {
struct NodeAttribute
{
    enum {
        RecordId,
        ///< RecordId QVariant. Must be present for all model items.
        TableId,
        ///< TableId QVariant. Must be present if the item has containing table.
        TransformOffset,
        ///< If present, the items movement is biased by this QPoint.
        TransformTopLeft,
        ///< QPoint
        TransformBottomRight,
        ///< QPoint
    };
};
} // namespace gmtool
