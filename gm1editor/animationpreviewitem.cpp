#include "animationpreviewitem.h"

#include <cmath>
#include <set>

#include <QGraphicsItem>
#include <QPainter>

#include "animationeditor.h"
#include "colormask.h"
#include "logmodel.h"
#include "recordid.h"
#include "spriteplotter.h"
#include "tableid.h"
#include "tablemodel.h"

const static float kFramesPerSecond = 12.0;

namespace gmtool {

AnimationPreviewItem::AnimationPreviewItem(AnimationEditor *editor,
                                           const RecordId &animationFrameGroupId,
                                           QGraphicsItem *parent)
    : AnimationNode(parent)
    , mEditor(editor)
    , mFrameGroupId(animationFrameGroupId)
    , mAccumulatedTime(0)
    , mFrameIndex(0)
    , mFramesDirty(true)
{
    mPivot = mEditor->pivot();
    mPaletteId = mEditor->getModel(TableId::Palette)->getIdOf(0);

    setFlag(QGraphicsItem::ItemHasNoContents, true);

    if (auto entries = mEditor->getModel(TableId::Layout)) {
        connect(entries,
                &QAbstractItemModel::dataChanged,
                this,
                &AnimationPreviewItem::entriesChanged);
        connect(entries,
                &QAbstractItemModel::rowsInserted,
                this,
                &AnimationPreviewItem::entriesInserted);
        connect(entries,
                &QAbstractItemModel::rowsAboutToBeRemoved,
                this,
                &AnimationPreviewItem::entriesAboutToBeRemoved);
        connect(entries, &QAbstractItemModel::modelReset, this, &AnimationPreviewItem::entriesReset);
        connect(entries, &QAbstractItemModel::rowsMoved, this, &AnimationPreviewItem::entriesMoved);
    }

    // Sprites
    if (auto sprites = mEditor->getModel(TableId::Spritesheet)) {
        connect(sprites,
                &QAbstractItemModel::dataChanged,
                this,
                &AnimationPreviewItem::spritesChanged);
        connect(sprites,
                &QAbstractItemModel::rowsInserted,
                this,
                &AnimationPreviewItem::spritesInserted);
        connect(sprites,
                &QAbstractItemModel::rowsAboutToBeRemoved,
                this,
                &AnimationPreviewItem::spritesAboutToBeRemoved);
        connect(sprites, &QAbstractItemModel::modelReset, this, &AnimationPreviewItem::spritesReset);
        connect(sprites, &QAbstractItemModel::rowsMoved, this, &AnimationPreviewItem::spritesMoved);
    }

    setupFrames();
}

void AnimationPreviewItem::entriesInserted(const QModelIndex &parent, int first, int last)
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::entriesAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::entriesReset()
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::entriesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::entriesMoved(
    const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row)
{
    mFramesDirty = true;
    update();
}

QRectF AnimationPreviewItem::boundingRect() const
{
    return QRectF();
}

void AnimationPreviewItem::spritesInserted(const QModelIndex &parent, int first, int last)
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::spritesMoved(
    const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row)
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::spritesAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::spritesReset()
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::spritesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    mFramesDirty = true;
    update();
}

void AnimationPreviewItem::paint(QPainter *painter,
                                 const QStyleOptionGraphicsItem *option,
                                 QWidget *widget)
{}

void AnimationPreviewItem::advance(float dtime)
{

    AnimationFrameGroup group;
    group.assign(*mEditor->getModel(TableId::Animations)->getRecord(mFrameGroupId));

    setupFrames();
    const float frameDuration = 1.0f / kFramesPerSecond;
    mAccumulatedTime += dtime;
    if (mAccumulatedTime >= frameDuration) {
        while (mAccumulatedTime >= frameDuration) {
            mAccumulatedTime -= frameDuration;
            mFrameIndex = (mFrameIndex + 1) % group.frameCount;
        }
        int index = 0;
        for (auto child : childItems()) {
            if (index++ == mFrameIndex) {
                child->setVisible(true);
            } else {
                child->setVisible(false);
            }
        }
    }
}

std::vector<QPixmap> AnimationPreviewItem::getFrames() const
{
    auto paletteModel = mEditor->getModel(TableId::Palette);
    auto spriteModel = mEditor->getModel(TableId::Spritesheet);
    auto layoutModel = mEditor->getModel(TableId::Layout);

    SpritePlotter maskPlotter;
    SpritePlotter plotter;
    plotter.setTransparentIndex(0);
    for (int i = 0; i < spriteModel->rowCount(); ++i) {
        SpriteRecord record;
        record.assign(*spriteModel->getRecord(spriteModel->getIdOf(i)));
        plotter.addImage(record.image(), record.rect(), record.mask());
        if (!record.mask().isNull()) {
            maskPlotter.addImage(record.mask().toImage(), record.rect(), QBitmap());
        }
    }

    PaletteRecord palette;
    palette.assign(*paletteModel->getRecord(mPaletteId));
    QVector<QRgb> colorTable;
    colorTable.reserve(256);
    for (const QColor &color : palette.colors()) {
        colorTable.push_back(color.rgb());
    }

    LogModel log;
    std::vector<QPixmap> frames;

    AnimationFrameGroup group;
    group.assign(*mEditor->getModel(TableId::Animations)->getRecord(mFrameGroupId));

    frames.resize(group.frameCount);
    for (int i = 0; i < group.frameCount; ++i) {
        const int currentFrameIndex = group.frameStart + i * group.frameStep;
        if (currentFrameIndex >= layoutModel->rowCount()) {
            break;
        }
        AnimationRecord animation;
        animation.assign(*layoutModel->getRecord(layoutModel->getIdOf(currentFrameIndex)));
        if (!plotter.render(QImage::Format_Indexed8, animation.rect(), &log)) {
            continue;
        }
        if (!maskPlotter.render(QImage::Format_Grayscale8, animation.rect(), &log)) {
            continue;
        }
        QImage image = plotter.plot();
        image.setColorTable(colorTable);
        const QBitmap &mask = QBitmap::fromImage(maskPlotter.plot());
        QPixmap pixmap = QPixmap::fromImage(image);
        if (!mask.isNull()) {
            pixmap.setMask(mask);
        }
        frames[i] = pixmap;
    }

    return frames;
}

void AnimationPreviewItem::setupFrames()
{
    if (!mFramesDirty)
        return;

    for (auto child : childItems())
        delete child;

    mSize = QSize();
    for (const QPixmap &pixmap : getFrames()) {
        auto child = new QGraphicsPixmapItem(pixmap, this);
        mSize = mSize.expandedTo(pixmap.size());
        child->setVisible(false);
        child->setPos(-mPivot);
    }

    mFramesDirty = false;
}

void AnimationPreviewItem::reloadModelData() {}

float AnimationPreviewItem::totalDuration()
{
    return totalFrames() / kFramesPerSecond;
}

int AnimationPreviewItem::totalFrames()
{
    setupFrames();
    AnimationFrameGroup group;
    group.assign(*mEditor->getModel(TableId::Animations)->getRecord(mFrameGroupId));
    return group.frameCount;
}

void AnimationPreviewItem::setPaletteId(const RecordId &paletteId)
{
    if (mPaletteId != paletteId) {
        mPaletteId = paletteId;
        mFramesDirty = true;
        update();
    }
}

} // namespace gmtool
