#pragma once

#include <QDebug>
#include <QMetaType>
#include <QObject>

namespace gmtool {

enum class Severity { Error = 1, Warning = 2, Information = 3 };

inline QDebug operator<<(QDebug dbg, const Severity &s)
{
    switch (s) {
    case Severity::Error:
        dbg << QObject::tr("Severity Error");
        break;

    case Severity::Warning:
        dbg << QObject::tr("Severity Warning");
        break;

    case Severity::Information:
        dbg << QObject::tr("Severity Information");
        break;

    default:
        dbg << QObject::tr("Severity unknown");
        break;
    }

    return dbg;
}

} // namespace gmtool

Q_DECLARE_METATYPE(gmtool::Severity)

Q_DECLARE_FLAGS(SeverityLevels, gmtool::Severity)
Q_DECLARE_OPERATORS_FOR_FLAGS(SeverityLevels)
