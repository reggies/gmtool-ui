#pragma once

#include <QToolBar>
#include <QWidget>
#include <QCloseEvent>

#include "recordid.h"
#include "tableid.h"

namespace gmtool {
class Document;

struct ImportSprites : QAction
{
    using QAction::QAction;
};

struct ExportSprites : QAction
{
    using QAction::QAction;
};

struct LoadPalette : QAction
{
    using QAction::QAction;
};

struct SavePalette : QAction
{
    using QAction::QAction;
};

struct ChangeHue : QAction
{
    using QAction::QAction;
};

struct FindSpriteOrEntry : QAction
{
    using QAction::QAction;
};

struct RemoveSpriteOrEntry : QAction
{
    using QAction::QAction;
};

struct AddEntry : QAction
{
    using QAction::QAction;
};

struct MoveForward : QAction
{
    using QAction::QAction;
};

struct MoveBackward : QAction
{
    using QAction::QAction;
};

struct BringForward : QAction
{
    using QAction::QAction;
};

struct SendBack : QAction
{
    using QAction::QAction;
};

struct SelectAll : QAction
{
    using QAction::QAction;
};

struct SelectClear : QAction
{
    using QAction::QAction;
};

struct ScrollMode : QAction
{
    using QAction::QAction;
};

struct TransformMode : QAction
{
    using QAction::QAction;
};

struct AllowSelectSprites : QAction
{
    using QAction::QAction;
};

struct AllowSelectEntries : QAction
{
    using QAction::QAction;
};

struct SnapToGrid : QAction
{
    using QAction::QAction;
};

struct ShowGrid : QAction
{
    using QAction::QAction;
};

struct ShowOverlay : QAction
{
    using QAction::QAction;
};

class Window : public QWidget
{
    Q_OBJECT

public:
    explicit Window(QWidget *parent = nullptr);
    ~Window() override;

    virtual bool focusEntry(TableId table, const RecordId &id);

    virtual void route(ImportSprites *action) {}
    virtual void route(ExportSprites *action) {}
    virtual void route(LoadPalette *action) {}
    virtual void route(SavePalette *action) {}
    virtual void route(ChangeHue *action) {}
    virtual void route(FindSpriteOrEntry *action) {}
    virtual void route(RemoveSpriteOrEntry *action) {}
    virtual void route(AddEntry *action) {}
    virtual void route(MoveForward *action) {}
    virtual void route(MoveBackward *action) {}
    virtual void route(BringForward *action) {}
    virtual void route(SendBack *action) {}
    virtual void route(SelectAll *action) {}
    virtual void route(SelectClear *action) {}
    virtual void route(ScrollMode *action) {}
    virtual void route(TransformMode *action) {}
    virtual void route(AllowSelectSprites *action) {}
    virtual void route(AllowSelectEntries *action) {}
    virtual void route(SnapToGrid *action) {}
    virtual void route(ShowGrid *action) {}
    virtual void route(ShowOverlay *action) {}

    virtual void unroute(ImportSprites *action) {}
    virtual void unroute(ExportSprites *action) {}
    virtual void unroute(LoadPalette *action) {}
    virtual void unroute(SavePalette *action) {}
    virtual void unroute(ChangeHue *action) {}
    virtual void unroute(FindSpriteOrEntry *action) {}
    virtual void unroute(RemoveSpriteOrEntry *action) {}
    virtual void unroute(AddEntry *action) {}
    virtual void unroute(MoveForward *action) {}
    virtual void unroute(MoveBackward *action) {}
    virtual void unroute(BringForward *action) {}
    virtual void unroute(SendBack *action) {}
    virtual void unroute(SelectAll *action) {}
    virtual void unroute(SelectClear *action) {}
    virtual void unroute(ScrollMode *action) {}
    virtual void unroute(TransformMode *action) {}
    virtual void unroute(AllowSelectSprites *action) {}
    virtual void unroute(AllowSelectEntries *action) {}
    virtual void unroute(SnapToGrid *action) {}
    virtual void unroute(ShowGrid *action) {}
    virtual void unroute(ShowOverlay *action) {}

signals:
    void statusTextChanged(const QString &text);
    void closed();

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Window(const Window &) = delete;
    Window &operator=(const Window &) = delete;
};

} // namespace gmtool
