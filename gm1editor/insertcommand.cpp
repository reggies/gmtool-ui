#include "insertcommand.h"

#include <stdexcept>

#include "abstractrecord.h"
#include "tablemodel.h"

#include <QDebug>

namespace gmtool {

InsertCommand::InsertCommand(std::unique_ptr<AbstractRecord> record,
                             int position,
                             TableModel *model,
                             const QString &name)
    : mName(name)
    , mPos(position)
    , mModel(model)
    , mRecord(std::move(record))
{}

InsertCommand::~InsertCommand() = default;

bool InsertCommand::undo()
{
    return mModel->removeRows(mPos, 1);
}

bool InsertCommand::redo()
{
    if (!mModel->insertRows(mPos, 1)) {
        return false;
    }
    if (!mModel->assignRow(mPos, *mRecord)) {
        if (!mModel->removeRows(mPos, 1)) {
            qFatal("well this is probably perhaps maybe embarrassing");
        }
        return false;
    }
    return true;
}

QString InsertCommand::name() const
{
    if (mName.isEmpty()) {
        return QObject::tr("insert");
    }
    return mName;
}

} // namespace gmtool
