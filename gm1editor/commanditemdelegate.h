#pragma once

#include <QStyledItemDelegate>

namespace gmtool {

namespace undo {
class Stack;
}

class CommandItemDelegate : public QStyledItemDelegate
{
public:
    explicit CommandItemDelegate(QAbstractItemModel *model, undo::Stack *stack, QObject *parent);

    QWidget *createEditor(QWidget *parent,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const override;

    void paint(QPainter *painter,
               const QStyleOptionViewItem &option,
               const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;

    void setModelData(QWidget *editor,
                      QAbstractItemModel *model,
                      const QModelIndex &index) const override;

private:
    QAbstractItemModel *mModel;
    undo::Stack *mUndoStack;
};

} // namespace gmtool
