#pragma once

#include <boost/filesystem/path.hpp>
#include <boost/optional.hpp>

#include <QCheckBox>
#include <QComboBox>
#include <QDialog>
#include <QGroupBox>

#include "spriterecord.h"

namespace gmtool {
class AnimationEditor;
class GridScene;
class ColorEdit;
class GridGraphicsView;

class EmptyNode;
class SimpleSpriteSheet;

class AnimationLoadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AnimationLoadDialog(AnimationEditor *editor,
                                 const std::vector<boost::filesystem::path> &loadPaths,
                                 QWidget *parent = nullptr);
    virtual ~AnimationLoadDialog();

    std::vector<SpriteRecordData> sprites() const;

private:
    QVector<QRgb> activeColorTable() const;

    void rebuild();
    void prepareOriginal();

    void pickColor();

private:
    AnimationEditor *mEditor;

    GridScene *mScene;
    QCheckBox *mCheckDither;
    QGroupBox *mCheckMatchPalette;
    QComboBox *mBoxPalettes;

    QGroupBox *mCheckZeroIndexTransparency;
    QGroupBox *mCheckOverrideTransparency;

    GridGraphicsView *mView;
    ColorEdit *mTransparentColor;
    EmptyNode *mLayer;

    GridGraphicsView *mOriginalView;
    GridScene *mOriginalScene;

    struct SourceImage
    {
        SourceImage(const boost::filesystem::path &path, const QImage &image, const QPoint &topLeft)
            : path(path)
            , image(image)
            , topLeft(topLeft)
        {}
        boost::filesystem::path path;
        QImage image;
        QPoint topLeft;
    };

    std::vector<SourceImage> mImages;

    std::unique_ptr<SimpleSpriteSheet> mSpriteSheet;
};

} // namespace gmtool
