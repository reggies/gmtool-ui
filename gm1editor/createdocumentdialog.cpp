#include "createdocumentdialog.h"

#include <QDialogButtonBox>
#include <QLabel>
#include <QPointer>
#include <QVBoxLayout>

#include <QDebug>

#include "documenttypesmodel.h"
#include "editorfactory.h"

namespace gmtool {

CreateDocumentDialog::CreateDocumentDialog(QWidget *parent)
    : QDialog(parent)
{
    mModel = new DocumentTypesModel(this);

    auto buttons = new QDialogButtonBox(this);
    buttons->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttons, &QDialogButtonBox::accepted, this, &CreateDocumentDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &CreateDocumentDialog::reject);

    mView = new QListView(this);
    mView->setModel(mModel);
    mView->selectionModel()->setCurrentIndex(mModel->index(mModel->defaultIndex(), 0),
                                             QItemSelectionModel::Select);

    auto layout = new QVBoxLayout(this);
    layout->addWidget(new QLabel(tr("Choose document type")));
    layout->addWidget(mView);
    layout->addWidget(buttons);
}

DocumentType CreateDocumentDialog::selectedDocumentType() const
{
    return mModel->getValue(mView->selectionModel()->currentIndex().row());
}

std::unique_ptr<EditorFactory> CreateDocumentDialog::createEditorFactory(QWidget *parent)
{
    QPointer<CreateDocumentDialog> dialog = new CreateDocumentDialog(parent);

    dialog->exec();

    if (dialog == nullptr)
        return nullptr;

    if (dialog->result() == QDialog::Accepted) {
        std::unique_ptr<EditorFactory> factory(new EditorFactory);
        factory->setDocumentType(dialog->selectedDocumentType());
        return factory;
    }

    return nullptr;
}

} // namespace gmtool
