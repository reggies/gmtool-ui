#pragma once

#include <memory>

#include "recordid.h"
#include "tablemodel.h"
#include "undo.h"

namespace gmtool {

class RemoveCommand : public undo::Command
{
public:
    RemoveCommand(int position, TableModel *model, const QString &name = QString());

    ~RemoveCommand() override;

    bool undo() override;
    bool redo() override;

    QString name() const override;

private:
    QString mName;
    std::unique_ptr<AbstractRecord> mRecord;
    int mPos;
    TableModel *mModel;
};

} // namespace gmtool
