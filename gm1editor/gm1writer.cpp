#include "gm1writer.h"

#include <QDebug>

#include <algorithm>
#include <limits>

#include <boost/filesystem/fstream.hpp>

namespace gmtool {

namespace fs = boost::filesystem;

static inline bool is_error(gm1_status_t status)
{
    return gm1_error(status) != 0;
}

Gm1Writer::Gm1Writer(QObject *parent)
    : QObject(parent)
{}

static void swapGreenAndAlpha32(QImage *plot)
{
    for (int y = 0; y < plot->height(); ++y) {
        uint32_t *src = (uint32_t *) plot->scanLine(y);
        for (int x = 0; x < plot->width(); ++x, src++) {
            *src = (0xff00ff & *src) | ((0xff00 & *src) << 16) | ((0xff000000 & *src) >> 16);
        }
    }
}

static gm1_size_t writeToGm1(void *userdata, const gm1_u8_t *buffer, gm1_size_t count)
{
    std::ostream *out = static_cast<std::ostream *>(userdata);
    std::streampos pos = out->tellp();
    out->write(reinterpret_cast<const char *>(buffer), count);
    return out->tellp() - pos;
}

bool Gm1Writer::saveToFile(const boost::filesystem::path &filepath)
{
    fs::ofstream fout(filepath, std::ios::binary | std::ios::out);
    if (!fout) {
        emitError("could not open file; errno: %d", errno);
        return false;
    }

    gm1_ostream_t outputStream = {};
    outputStream.UserData = &fout;
    outputStream.Write = writeToGm1;

    gm1_status_t status = gm1_get_image_props(mFileData.Header.EntryClass, &mFileData.Properties);
    if (is_error(status)) {
        emitError("invalid header format; error code: %s (%02x)", gm1_errorstr(status), status);
        return false;
    }

    gm1_header_t stagedHeader = mFileData.Header;
    stagedHeader.EntryCount = static_cast<gm1_u32_t>(mFileData.Entries.size());
    stagedHeader.DataSize = 0;
    for (size_t i = 0; i < Gm1FileData::MaxPaletteCount; ++i) {
        for (size_t j = 0; j < Gm1FileData::MaxPaletteSize; ++j) {
            gm1_rgb_t rgb;
            rgb.Red = mFileData.Palettes[i][j].red();
            rgb.Green = mFileData.Palettes[i][j].green();
            rgb.Blue = mFileData.Palettes[i][j].blue();
            stagedHeader.ColorTable[i][j] = rgb;
        }
    }

    //! we will go to overwrite header later in the code flow
    std::streampos headerStart = fout.tellp();
    status = gm1_write_header(&outputStream, &stagedHeader);
    if (is_error(status)) {
        emitError("could not write header; error code: %s (%02x)", gm1_errorstr(status), status);
        return false;
    }

    //! also will overwrite sizes and offsets
    std::streampos headerEnd = fout.tellp();
    std::vector<gm1_entry_t> entries;
    for (const Gm1FileEntry &entry : mFileData.Entries) {
        entries.push_back(entry.Gm1);
    }
    status = gm1_write_entries(&outputStream,
                               mFileData.Header.EntryClass,
                               entries.data(),
                               entries.size());
    if (is_error(status)) {
        emitError("could not write entries; error code: %s (%02x)", gm1_errorstr(status), status);
        return false;
    }

    if (mFileData.Header.EntryClass == GM1_ENTRY_CLASS_GLYPH) {
        /*!
          \note that we cannot convert the image directly using
          QImage::convertToFormat since then it will disrupt
          our green and alpha channels.
        */
        swapGreenAndAlpha32(&mFileData.Plot);
        mFileData.Plot = mFileData.Plot.convertToFormat(QImage::Format_RGB555);
        if (mFileData.Plot.isNull()) {
            emitError("not enough memory");
            return false;
        }
    } else {
        /*!
          \todo this conversion has not been tested in any way
        */
        QImage::Format format = QImage::Format_RGB555;
        if (mFileData.Header.EntryClass == GM1_ENTRY_CLASS_TGX8)
            format = QImage::Format_Indexed8;
        if (mFileData.Plot.format() != format) {
            emitWarning("plot has incorrect pixel format and will be converted");
            mFileData.Plot = mFileData.Plot.convertToFormat(format);
            if (mFileData.Plot.isNull()) {
                emitError("not enough memory");
                return false;
            }
        }
    }

    std::streampos dataStart = fout.tellp();
    for (size_t i = 0; i < mFileData.Entries.size(); ++i) {
        QImage entrySlice = entryImage(i);
        if (entrySlice.isNull()) {
            emitError("not enough memory");
            return false;
        }

        gm1_u8_t colorKey[sizeof(gm1_u16_t)];
        memcpy(colorKey, &mFileData.Properties.ColorKey, sizeof(gm1_u16_t));
        gm1_image_t image = {};
        image.Width = entrySlice.width();
        image.Height = entrySlice.height();
        image.BytesPerPixel = entrySlice.depth() / 8;
        image.BytesPerRow = entrySlice.bytesPerLine();
        image.Pixels = const_cast<gm1_u8_t *>(entrySlice.constBits());
        if (mFileData.Properties.bHasColorKey != 0) {
            image.ColorKey = colorKey;
        }

        Gm1FileEntry entry = mFileData.Entries.at(i);
        std::streampos entryStart = fout.tellp();
        status = gm1_write_image(&image,
                                 mFileData.Header.EntryClass,
                                 &entry.Gm1.Header,
                                 &outputStream);
        if (is_error(status)) {
            emitError("could not write entry %d; error string: %s (%02x)",
                      i,
                      gm1_errorstr(status),
                      static_cast<int>(status));
            return false;
        }

        //! update offset of the entry
        std::streampos entryEnd = fout.tellp();
        mFileData.Entries.at(i).Gm1.Offset = static_cast<gm1_u32_t>(entryStart - dataStart);
        mFileData.Entries.at(i).Gm1.Size = static_cast<gm1_u32_t>(entryEnd - entryStart);
    }

    std::streampos dataEnd = fout.tellp();

    //! update DataSize field in the header and overwrite the file
    stagedHeader.DataSize = static_cast<gm1_u32_t>(dataEnd - dataStart);
    fout.seekp(headerStart, std::ios::beg);
    status = gm1_write_header(&outputStream, &stagedHeader);
    if (is_error(status)) {
        emitError("could not write header; error string: %s (%02x)", gm1_errorstr(status), status);
        return false;
    }

    //! clip entry plot positions so that they won't wrap on 2^16 boundary
    for (size_t i = 0; i < entries.size(); ++i) {
        const QRect &rect = mFileData.Entries.at(i).Rect.translated(-mFileData.Origin);

        if ((rect.left() < 0) || (rect.left() > std::numeric_limits<uint16_t>::max())) {
            emitError("could not fit horizontally entry %d with scene position %d:%d",
                      i,
                      rect.left(),
                      rect.top());
            return false;
        }

        if ((rect.top() < 0) || (rect.top() > std::numeric_limits<uint16_t>::max())) {
            emitError("could not fit vertically entry %d with scene position %d:%d",
                      i,
                      rect.left(),
                      rect.top());
            return false;
        }

        entries[i].Header.PosX = rect.left();
        entries[i].Header.PosY = rect.top();
        entries[i].Header.Width = rect.width();
        entries[i].Header.Height = rect.height();
    }

    fout.seekp(headerEnd, std::ios::beg);
    status = gm1_write_entries(&outputStream,
                               mFileData.Header.EntryClass,
                               entries.data(),
                               entries.size());
    if (is_error(status)) {
        emitError("could not write entries; error string: %s (%02x)", gm1_errorstr(status), status);
        return false;
    }

    return true;
}

QImage Gm1Writer::entryImage(size_t index) const
{
    /*!
      entryRect returns the image rect as it is specified by
      entry header
    */
    auto rect = mFileData.Entries.at(index).Rect;
    /*!
      The plot doesn't necessary conver the entire entry rect.
      Move image of origin relative to the plot.
     */

    rect.translate(-mFileData.Origin);

    return mFileData.Plot.copy(rect);
}

bool Gm1Writer::isNull() const
{
    return mFileData.Plot.isNull();
}

void Gm1Writer::emitError(const char *format, ...)
{
    va_list va;
    va_start(va, format);
    auto message = QString::vasprintf(format, va);
    va_end(va);
    emit diagnosticsError(message);
}

void Gm1Writer::emitWarning(const char *format, ...)
{
    va_list va;
    va_start(va, format);
    auto message = QString::vasprintf(format, va);
    va_end(va);
    emit diagnosticsWarning(message);
}

void Gm1Writer::setFileData(const Gm1FileData &fileData)
{
    mFileData = fileData;
}

} // namespace gmtool
