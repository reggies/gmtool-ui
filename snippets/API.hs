
-- Basic data types
newtype Rect = Rect Int Int Int Int
newtype RGBA = RGBA Int Int Int Int
newtype Index = Index Int

-- Image manipulation data types
newtype IndexedImage = IndexedImage [Index]
newtype ColorImage = ColorImage [RGBA]
data PixBuf = Indexed IndexedImage | TrueColor ColorImage

-- Strong type of sprites from the game itself
newtype Font = Font { rect :: Rect, bearingY :: Int }
newtype Tile = Tile { rect :: Rect, boxHeight :: Int, radius :: Int }
newtype Body = Body Rect
newtype Bitmap = Bitmap Rect
data Sprite = Body Body | Font Font | Tile Tile | Bitmap Bitmap

-- Weak data types for transient objects in the app
data FrameGroup = FrameGroup { start :: Int, step :: Int, count :: Int, backAndForth :: Bool }
data Animation = Animation { frameGroup :: FrameGroup, images :: [SpriteImage] }
data SpriteImage = SpriteImage { image :: Image, sprite :: Sprite }
newtype ColorTable = ColorTable [RGBA]
data Image = Image { rect :: Rect, pixbuf :: PixBuf }

-- indexedSprites :: [IndexedSprite]
-- indexedImages :: [IndexedImage]
-- colorSprites :: [ColorSprite]
-- colorImages :: [ColorImage]

-- entries :: [Entry]
sprites :: [Sprite]
bodies :: [Body]
font :: [Font]
tiles :: [Tile]
bitmap :: [Bitmap]
images :: [Image]
frameGroups :: [FrameGroup]
colorTables :: [ColorTable]

animations :: [Sprite] -> [Image] -> [FrameGroup] -> [Animation]

spriteImages :: [Sprite] -> [Image] -> [SpriteImage]

colorImage :: IndexedImage -> ColorTable -> ColorImage
colorImages :: [IndexedImage] -> ColorTable -> [ColorImage]

spriteGroups :: [Sprite] -> [FrameGroup] -> [SpriteGroup]

frames :: [Sprite] -> [Image] -> FrameGroup -> [Frame]
frames :: [SpriteImage] -> FrameGroup -> [Frame]
frames :: [Sprite] -> [Image] -> [ColorTable] -> [ColorImage]
