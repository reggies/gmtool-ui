#include "documentoperation.h"

#include <QDebug>
#include <QThread>

namespace gmtool {

DocumentOperation::DocumentOperation(QObject *parent)
    : QObject(parent)
{}

int DocumentOperation::currentProgress() const
{
    return mCurrentProgress;
}

int DocumentOperation::totalProgress() const
{
    return mTotalProgress;
}

void DocumentOperation::setProgress(int current, int total)
{
    mCurrentProgress = current;
    mTotalProgress = total;
    emit stateChanged();
}

bool DocumentOperation::isRunning() const
{
    return mIsRunning;
}

void DocumentOperation::setFinished()
{
    mIsRunning = false;
    mStatusString = tr("Done");
    emit stateChanged();
}

void DocumentOperation::setStarted()
{
    mIsRunning = true;
    setProgress(1, 1);
    emit stateChanged();
}

QString DocumentOperation::statusString() const
{
    return mStatusString;
}

void DocumentOperation::setStatusString(const QString &statusString)
{
    mStatusString = statusString;
    emit stateChanged();
}

bool DocumentOperation::pushOperation(Operation *operation)
{
    if (isRunning())
        return false;

    if (operation->parent() != nullptr) {
        qDebug("operation with valid parent");
        return false;
    }

    QThread *thread = new QThread;
    operation->moveToThread(thread);

    connect(operation, &Operation::progress, this, &DocumentOperation::setProgress);

    connect(thread, &QThread::started, this, &DocumentOperation::setStarted);

    connect(thread, &QThread::started, operation, &Operation::start);

    connect(thread, &QThread::finished, this, &DocumentOperation::setFinished);

    connect(operation, &Operation::finished, thread, &QThread::quit);

    connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    thread->start();

    mStatusString = operation->status();

    return true;
}

} // namespace gmtool
