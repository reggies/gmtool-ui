#pragma once

#include <QPlainTextEdit>

#include "window.h"
#include "document.h"
#include "repl.h"

namespace gmtool {
class Repl;

class ReplWindow : public Window {
    Q_OBJECT
public:
    explicit ReplWindow(Document *doc, QWidget *parent = nullptr);
    ~ReplWindow() override;

protected:
    bool eventFilter(QObject *obj, QEvent *event) override;

private:
    void processStdin(const std::string &input);
    void processStderr(const std::string &output);
    void processStdout(const std::string &output);

    void runCommand(const std::string &command);

    void readEvalPrint();

    bool historyBackwardForward(bool forward);
    void historyDone();
    void historyAbort();

    void runTests();

private:
    Document *mDoc;
    Repl *mRepl;

    QPlainTextEdit *mInput;
    QPlainTextEdit *mLog;

    QStringList mHistoryTop;
    QStringList mHistoryBottom;
    bool mCompleting = false;
    QString mInputBeforeCompletion;
};

}
