#pragma once

#include <QObject>

namespace gmtool {

class Operation : public QObject
{
    Q_OBJECT

public:
    explicit Operation(QObject *parent = 0);

    virtual void start() = 0;

    virtual QString status() const;

signals:
    void progress(int, int);
    void finished();

protected:
    void setProgress(int current, int total);
};

} // namespace gmtool
