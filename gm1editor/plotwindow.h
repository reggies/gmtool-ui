#pragma once

#include <memory>
#include <vector>

#include "nodefactory.h"
#include "recordid.h"
#include "window.h"

#include <QAction>
#include <QComboBox>
#include <QGraphicsItem>
#include <QPushButton>
#include <QToolBar>

namespace gmtool {
class Document;
class GridGraphicsView;
class GridScene;
class ModelTracker;
class SceneBuilder;
class SceneNode;
class RepeaterNode;
class SceneNode;
class ViewState;

enum class InteractionMode;

class PlotWindow : public Window
{
    Q_OBJECT

public:
    explicit PlotWindow(Document *document, QWidget *parent = nullptr);
    ~PlotWindow() override;

    void route(ImportSprites *action) override;
    void unroute(ImportSprites *action) override;

    void route(ExportSprites *action) override;
    void unroute(ExportSprites *action) override;

    void route(FindSpriteOrEntry *action) override;
    void unroute(FindSpriteOrEntry *action) override;

    void route(RemoveSpriteOrEntry *action) override;
    void unroute(RemoveSpriteOrEntry *action) override;

    void route(AddEntry *action) override;
    void unroute(AddEntry *action) override;

    void route(MoveForward *action) override;
    void unroute(MoveForward *action) override;

    void route(MoveBackward *action) override;
    void unroute(MoveBackward *action) override;

    void route(BringForward *action) override;
    void unroute(BringForward *action) override;

    void route(SendBack *action) override;
    void unroute(SendBack *action) override;

    void route(SelectAll *action) override;
    void unroute(SelectAll *action) override;

    void route(SelectClear *action) override;
    void unroute(SelectClear *action) override;

    void route(ScrollMode *action) override;
    void route(TransformMode *action) override;
    void route(AllowSelectSprites *action) override;
    void route(AllowSelectEntries *action) override;
    void route(SnapToGrid *action) override;
    void route(ShowGrid *action) override;
    void route(ShowOverlay *action) override;

    void setShowOverlay(bool showOverlay);

    void setScrollMode();
    void setTransformMode();

signals:
    void findRequested(TableId table, const RecordId &id);
    void cursorScenePosChanged(const QPoint &pos);

    void gotSpriteOrEntrySelected(bool any);
    void gotSpriteButNoEntrySelected(bool any);
    void gotSingleItemSelected(bool any);

private:
    void paletteIndexChanged(int index);
    void addImage();
    void saveImage();
    void findSelected();
    void addEntry();
    void remove();

    void populateToolbar(QToolBar *toolbar);

    void updateScene();

    void prepareScene();
    void prepareView();

    void createActions();

    void onSceneRectChanged(const QRectF &rect);

    void onSceneChanged(const QList<QRectF> &region);

    std::vector<SceneNode *> selectedNodes() const;

    void setViewState(const ViewState &state);

    void rebuildScene();

    void paletteReset();
    void paletteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

    void propertiesReset();
    void propertiesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);

    void updateInteractionMode();

    void setAllowSelectSprites(bool allow);
    void setAllowSelectEntries(bool allow);

    void itemsMovementFinished();
    void itemsMovementReset();

    void moveForward();
    void moveBackward();
    void bringForward();
    void sendBack();

    void selectAll();
    void selectClear();

    void selectedNodesChanged();

private:
    Document *mDoc;

    GridGraphicsView *mPlotView;
    GridScene *mScene;
    RecordId mPaletteId;
    QGraphicsItem *mProps = nullptr;

    RepeaterNode *mBgLayer;
    RepeaterNode *mLayoutLayer;

    bool mAllowSpritesSelection = true;
    bool mAllowEntriesSelection = true;

    bool mShowOverlay = true;

    QComboBox *mPaletteBox = nullptr;

    std::unique_ptr<ViewState> mViewState;
};

} // namespace gmtool
