#include "replwindow.h"

#include <QVBoxLayout>
#include <QPlainTextEdit>
#include <QSplitter>
#include <QApplication>
#include <QTimer>

#include <QDebug>

#include "replhighlighter.h"

namespace gmtool {

ReplWindow::ReplWindow(Document *doc, QWidget *parent)
    : Window(parent)
    , mDoc(doc)
{
    mRepl = new Repl(mDoc, this);
    QTextCharFormat format;
    format.setFontFixedPitch(true);
    mLog = new QPlainTextEdit;
    mLog->setCurrentCharFormat(format);
    mLog->setReadOnly(true);
    mLog->setUndoRedoEnabled(false);
    mLog->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
    auto textOption = mLog->document()->defaultTextOption();
    textOption.setFlags(textOption.flags() | QTextOption::ShowTabsAndSpaces);
    mLog->document()->setDefaultTextOption(textOption);
    mInput = new QPlainTextEdit;
    mInput->setCurrentCharFormat(format);
    mInput->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    mInput->document()->setDefaultTextOption(textOption);
    mInput->setMinimumHeight(mInput->contentsMargins().top() +
                             mInput->contentsMargins().bottom() +
                             mInput->fontMetrics().height() * 3);
    auto replHighlighter = new ReplHighlighter(mLog->document());
    auto splitter = new QSplitter(Qt::Vertical);
    splitter->addWidget(mLog);
    splitter->addWidget(mInput);
    auto vbox = new QVBoxLayout(this);
    vbox->setMargin(0);
    vbox->addWidget(splitter);
    setLayout(vbox);
    mInput->installEventFilter(this);
    connect(mRepl, &Repl::outputStderr, this, &ReplWindow::processStderr);
    connect(mRepl, &Repl::outputStdout, this, &ReplWindow::processStdout);
    connect(mRepl, &Repl::systemExitRaised, this, &QWidget::close);
    mLog->ensureCursorVisible();
    setWindowTitle(tr("Interactive console"));
    // Disable input field until the tests are done.
    mInput->setEnabled(false);
    QTimer::singleShot(0, this, &ReplWindow::runTests);
}

ReplWindow::~ReplWindow()
{
}

void ReplWindow::runTests()
{
    // runCommand("import sys");
    // runCommand(QString("sys.path.insert(0, \"%1\")")
    //            .arg(QApplication::applicationDirPath()).toStdString());
    // mRepl->loadScript("test_repl");

    // runCommand("test_repl.run_tests()");

    // runCommand("import code");
    // runCommand("code.interact()");

    mInput->setEnabled(true);
}

void ReplWindow::processStderr(const std::string &output)
{
    QTextCharFormat format;
    format.setForeground(Qt::darkRed);
    QTextCursor tc = mLog->textCursor();
    if (!output.empty()) {
        tc.movePosition(QTextCursor::End);
        for(QString line : QString::fromStdString(output).split(QRegExp("\\n+"))) {
            tc.insertText(line + "\n", format);
        }
    }
}

void ReplWindow::processStdout(const std::string &output)
{
    QTextCharFormat format;
    format.setForeground(Qt::black);
    QTextCursor tc = mLog->textCursor();
    if (!output.empty()) {
        tc.movePosition(QTextCursor::End);
        for(QString line : QString::fromStdString(output).split(QRegExp("\\n+"))) {
            tc.insertText(line + "\n", format);
        }
    }
}

void ReplWindow::processStdin(const std::string &input)
{
    QTextCharFormat format;
    format.setForeground(Qt::gray);
    format.setFontWeight(QFont::DemiBold);

    QTextCursor tc = mLog->textCursor();
    tc.movePosition(QTextCursor::End);
    bool first = true;
    for(QString line : QString::fromStdString(input).split(QRegExp("\\n+"))) {
        if (first) {
            tc.insertText(QString(">> %1\n").arg(line), format);
        } else {
            tc.insertText(QString(".. %1\n").arg(line), format);
        }
        first = false;
    }
}

bool ReplWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == mInput)
    {
        // Events that were injected using postEvent by the code down below
        // appears as non spontaneous
        if (event->spontaneous())
        {
            if (event->type() == QEvent::KeyPress)
            {
                QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
                if (keyEvent->key() == Qt::Key_Return)
                {
                    if (keyEvent->modifiers() == 0)
                    {
                        event->accept();
                        readEvalPrint();
                        return true;
                    }
                    else if (keyEvent->modifiers() == Qt::ShiftModifier)
                    {
                        event->accept();
                        QApplication::postEvent(
                            mInput,
                            new QKeyEvent(keyEvent->type(),
                                          keyEvent->key(),
                                          keyEvent->modifiers() & ~Qt::ShiftModifier));
                        return true;
                    }
                }
                else if (keyEvent->key() == Qt::Key_Up)
                {
                    if (historyBackwardForward(true))
                    {
                        event->accept();
                        return true;
                    }
                }
                else if (keyEvent->key() == Qt::Key_Down)
                {
                    if (historyBackwardForward(false))
                    {
                        event->accept();
                        return true;
                    }
                }
                else if (keyEvent->key() == Qt::Key_Escape)
                {
                    historyAbort();
                }
                else
                {
                    historyDone();
                }
            }
            else if (event->type() == QEvent::FocusOut)
            {
                historyAbort();
            }
        }
    }
    return Window::eventFilter(obj, event);
}

void ReplWindow::runCommand(const std::string &command)
{
    processStdin(command);
    if (!mRepl->execute(command)) {
        qCritical("executing command failed");
        for(QString line : QString::fromStdString(command).split(QRegExp("\\n+"))) {
            qCritical("%s\n", qPrintable(line));
        }
    }
}

static bool stackShiftTop(QStringList &from, QStringList &to, QString &top)
{
    if (from.isEmpty()) {
        return false;
    }
    top = from.front();
    // TBD: Leave only the last element in the source list
    // because input field already contains exactly the
    // same value. Pressing UP or DOWN once more will
    // yield the last element in history.
    // if (from.size() > 1)
    {
        from.removeFirst();
        to.prepend(top);
    }
    return true;
}

bool ReplWindow::historyBackwardForward(bool forward)
{
    // setup a new completion, if not yet
    if (!mCompleting)
    {
        // cleanup the mess left from previous completion
        std::reverse(mHistoryBottom.begin(), mHistoryBottom.end());
        // ... note that all previous skipped choices are appended
        // to the history list meaning that history completion state
        // is preserved to some extent
        mHistoryTop += mHistoryBottom;
        mHistoryBottom.clear();
        if (mHistoryTop.isEmpty()) {
            return false;
        }
        if (mInput->document()->lineCount() > 1) {
            return false;
        }
        mInputBeforeCompletion = mInput->toPlainText();
        // Prepending current input as a history entry so
        // that user can select it with Down arrow.
        // TBD: checking for empty string is somehow inconsistent
        // but convenient
        if (!mInputBeforeCompletion.isEmpty()) {
            mHistoryBottom.prepend(mInputBeforeCompletion);
        }
        mInput->clear();
        mCompleting = true;
    }

    // if we got there, then completion is already in progress or have just been started
    QString pick;
    if (( forward && stackShiftTop(mHistoryTop, mHistoryBottom, pick)) ||
        (!forward && stackShiftTop(mHistoryBottom, mHistoryTop, pick)))
    {
        mInput->clear();
        QTextCursor tc = mInput->textCursor();
        tc.insertText(pick);
        mInput->setTextCursor(tc);
    }

    return true;
}

void ReplWindow::readEvalPrint()
{
    QString scriptQString = mInput->toPlainText();
    std::string scriptStdString = scriptQString.toStdString();
    mInput->clear();
    runCommand(scriptStdString);
    if (mLog->textCursor().atEnd()) {
        mLog->ensureCursorVisible();
    }
    historyDone();
    if (!scriptQString.isEmpty()) {
        mHistoryBottom.removeAll(scriptQString);
        mHistoryTop.removeAll(scriptQString);
        mHistoryTop.prepend(scriptQString);
    }
}

void ReplWindow::historyDone()
{
    if (mCompleting)
    {
        mCompleting = false;
    }
}

void ReplWindow::historyAbort()
{
    if (mCompleting)
    {
        // TBD: If text string were provided by the user then
        // we had had to add it to the history.
        // Note that it must be either the bottom element of
        // the Bottom list, or the first element of the
        // Top list.
        // if (!mInputBeforeCompletion.isEmpty()) {

        //     if (!mHistoryBottom.isEmpty()) {
        //         mHistoryBottom.removeLast();
        //     } else if (!mHistoryTop.isEmpty()) {

        //     }
        // }

        mCompleting = false;
        mInput->setPlainText(mInputBeforeCompletion);
    }
}

} // namespace gmtool
