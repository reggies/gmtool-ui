#include "subtilewalk.h"

namespace gmtool {

int xSkew(int xAlign, int yAlign)
{
    return (yAlign / 2) - xAlign + (yAlign % 2);
}

int ySkew(int xAlign, int yAlign)
{
    return (yAlign / 2) + xAlign;
}

void walkSubTiles(const TileRecord &record, const std::function<void(SubTile)> &visitor)
{
    int partId = 0;

    for (int y = 0; y < 2 * record.size(); ++y) {
        for (int x = -record.size(); x < record.size(); ++x) {
            if (xSkew(x, y) < 0 || xSkew(x, y) >= record.size() || ySkew(x, y) < 0
                || ySkew(x, y) >= record.size()) {
                continue;
            }

            SubTile subtile;
            subtile.order = partId;
            subtile.xSkewed = xSkew(x, y);
            subtile.ySkewed = ySkew(x, y);
            subtile.xAligned = x;
            subtile.yAligned = y;

            QRect tileRect;
            tileRect.setLeft((record.size() * record.widthStride()
                              - (y % 2 != 0 ? record.widthStride() : 0) - record.widthStride())
                                 / 2
                             + x * record.widthStride());
            tileRect.setTop(record.height() - y * record.heightStride() / 2 - record.heightStride());
            tileRect.setWidth(record.tileWidth());
            tileRect.setHeight(record.tileHeight());

            tileRect.translate(record.rect().topLeft());

            subtile.tileBbox = tileRect;

            int boxHeight;
            // int boxX;
            // int boxWidth;

            QRect entryRect;
            TileAlignment alignment;
            if (xSkew(x, y) == record.size() - 1 && ySkew(x, y) == record.size() - 1) {
                alignment = TileAlignment::Center;
                boxHeight = tileRect.top() - record.rect().top();
                // boxX = 0;
                // boxWidth = tileRect.width();
                entryRect = tileRect.adjusted(0, -boxHeight, 0, 0);
            } else if (xSkew(x, y) == record.size() - 1) {
                alignment = TileAlignment::Left;
                boxHeight = tileRect.top() - record.rect().top();
                // boxX = 0;
                // boxWidth = tileRect.width() / 2;
                entryRect = tileRect.adjusted(0, -boxHeight, 0, 0);
            } else if (ySkew(x, y) == record.size() - 1) {
                alignment = TileAlignment::Right;
                boxHeight = tileRect.top() - record.rect().top();
                // boxX = tileRect.width() / 2;
                // boxWidth = tileRect.width() / 2;
                entryRect = tileRect.adjusted(0, -boxHeight, 0, 0);
            } else {
                alignment = TileAlignment::None;
                boxHeight = 0;
                // boxX = 0;
                // boxWidth = 0;
                entryRect = tileRect;
            }

            subtile.alignment = alignment;
            subtile.entryBbox = entryRect;
            subtile.boxHeight = boxHeight;

            visitor(subtile);

            ++partId;
        }
    }
}

} // namespace gmtool
