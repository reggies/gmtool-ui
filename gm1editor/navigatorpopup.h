#pragma once

#include <QGraphicsView>

namespace gmtool {

class NavigatorPopup : public QGraphicsView
{
    Q_OBJECT

public:
    explicit NavigatorPopup(QWidget *parent = nullptr);
    ~NavigatorPopup();

signals:
    void mouseMoved(const QPoint &localPos, const QSize &size);

protected:
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    bool event(QEvent *e) override;
};

} // namespace gmtool
