/*
  Copyright (c) 2016 Alexey Natalin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include "gm1_util.h"

#include <string.h>

extern gm1_status_t gm1_read_gfx_header(gm1_istream_t* istream, gm1_gfx_header_t* gfx)
{
    gm1_size_t readc = 0;
    readc += get_le32(istream, &gfx->Width);
    readc += get_le32(istream, &gfx->Height);
    return rwstatus(readc, 8);
}

extern gm1_status_t gm1_write_gfx_header(gm1_ostream_t* ostream, const gm1_gfx_header_t* gfx)
{
    gm1_size_t putc = 0;
    putc += put_le32(ostream, gfx->Width);
    putc += put_le32(ostream, gfx->Height);
    return rwstatus(putc, 8);
}

extern void gm1_get_gfx_image_props(gm1_image_props_t* props)
{
    memset(props, 0, sizeof(gm1_image_props_t));
    props->BytesPerPixel = 2;
    props->bHasColorKey = 0;
    gm1_set_default_color_key(props);
}

extern gm1_status_t gm1_read_gfx_image(gm1_istream_t* istream, gm1_image_t* image)
{
    return gm1_read_tgx(istream,
                        image->BytesPerPixel * image->Width * image->Height,
                        image);
}

extern gm1_status_t gm1_write_gfx_image(gm1_ostream_t* ostream, const gm1_image_t* image)
{
    return gm1_write_tgx(image, ostream);
}
