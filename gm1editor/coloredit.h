#pragma once

#include <QLineEdit>
#include <QWidget>

namespace gmtool {

class ColorEdit : public QWidget
{
    Q_OBJECT

public:
    explicit ColorEdit(QWidget *parent = nullptr);

    QColor currentColor() const;

public slots:
    void setCurrentColor(const QColor &color);

signals:
    void currentColorChanged();

private slots:
    void buttonPressed();
    void editingFinished();

private:
    QLineEdit *mLineEdit;
    QColor mCurrentColor;
};

} // namespace gmtool
