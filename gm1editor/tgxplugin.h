#pragma once

#include <QImageIOPlugin>

namespace gmtool {

class TgxPlugin : public QImageIOPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QImageIOHandlerFactoryInterface" FILE "tgxplugin.json")

public:
    explicit TgxPlugin(QObject *parent = nullptr);
    ~TgxPlugin() override;

    QImageIOPlugin::Capabilities capabilities(QIODevice *device,
                                              const QByteArray &format) const override;
    QImageIOHandler *create(QIODevice *device,
                            const QByteArray &format = QByteArray()) const override;
};

} // namespace gmtool
