#pragma once

#include <QAction>
#include <QObject>
#include <QString>

#include <memory>
#include <vector>

namespace gmtool {

namespace undo {

class Command
{
public:
    virtual ~Command() = default;
    virtual bool redo() = 0;
    virtual bool undo() = 0;
    virtual QString name() const = 0;
    virtual int id() const { return -1; }
    virtual bool mergeWith(undo::Command *command) { return false; }
};

class Macro : public Command
{
public:
    explicit Macro(const QString &name)
        : mName(name)
    {}
    ~Macro() override = default;

    void add(std::unique_ptr<Command> command);
    bool redo() override;
    bool undo() override;

    QString name() const override;

    inline bool empty() const { return mCommands.empty(); }

private:
    QString mName;
    std::vector<std::unique_ptr<Command>> mCommands;
};

class Stack : public QObject
{
    Q_OBJECT

public:
    Stack();
    ~Stack() override;

    bool push(std::unique_ptr<Command> command);

    void clear();

    void undo();
    void redo();

    QAction *createUndoAction(QObject *parent, const QString &title);
    QAction *createRedoAction(QObject *parent, const QString &title);

signals:
    void redoChanged(Command *command);
    void undoChanged(Command *command);

private:
    std::vector<std::unique_ptr<Command>> mRedoed;
    std::vector<std::unique_ptr<Command>> mUndoed;
    std::unique_ptr<Macro> mCurrentMacro;
};

} // namespace undo

} // namespace gmtool
