#include "previewwindow.h"

#include <QComboBox>
#include <QTimer>
#include <QVBoxLayout>

#include "animationeditor.h"
#include "animationframegroup.h"
#include "animationnode.h"
#include "document.h"
#include "gridgraphicsview.h"
#include "gridscene.h"
#include "nodefactory.h"
#include "palettecombobox.h"
#include "propertiesmodel.h"
#include "tablemodel.h"
#include "zoomcombobox.h"

namespace gmtool {

PreviewWindow::PreviewWindow(Document *document, QWidget *parent)
    : Window(parent)
    , mDoc(document)
    , mSpriteItem(nullptr)
{
    mScene = new GridScene(this);

    mView = new GridGraphicsView(this);
    mView->setRenderHint(QPainter::SmoothPixmapTransform, false);
    mView->setOptimizationFlag(QGraphicsView::DontSavePainterState, true);
    mView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
    mView->setScene(mScene);
    mView->setShowGrid(false);

    mTimer = new QTimer(this);
    mTimer->setInterval(30);
    connect(mTimer, &QTimer::timeout, this, &PreviewWindow::updateAnimation);
    mTimer->start();

    auto groupComboBox = new QComboBox(this);
    groupComboBox->setModel(mDoc->editor()->getModel(TableId::Animations));
    groupComboBox->setModelColumn(0);
    connect(groupComboBox,
            QOverload<int>::of(&QComboBox::currentIndexChanged),
            this,
            &PreviewWindow::currentGroupChanged);
    mFrameGroupId = mDoc->editor()->getModel(TableId::Animations)->getIdOf(0);

    auto hbox = new QHBoxLayout;
    hbox->setMargin(0);
    hbox->addWidget(groupComboBox);

    auto zoomBox = new ZoomComboBox(this);
    zoomBox->setGraphicsView(mView);
    hbox->addWidget(zoomBox);

    auto layout = new QVBoxLayout(this);
    layout->setSpacing(0);
    if (mDoc->editor()->getModel(TableId::Palette) != nullptr) {
        mPaletteId = mDoc->editor()->getModel(TableId::Palette)->getIdOf(0);
        auto paletteBox = new PaletteComboBox(mDoc->editor(), this);
        connect(paletteBox,
                QOverload<int>::of(&QComboBox::currentIndexChanged),
                this,
                &PreviewWindow::paletteIndexChanged);
        paletteBox->setCurrentIndex(0);
        hbox->addWidget(paletteBox);
    }

    layout->setMargin(0);
    layout->addWidget(mView);
    layout->addLayout(hbox);

    setWindowTitle(tr("Animation"));
    prepareScene();
}

PreviewWindow::~PreviewWindow() {}

void PreviewWindow::prepareScene()
{
    // Properties
    connect(mDoc->editor()->getPropertiesModel(),
            &QAbstractItemModel::dataChanged,
            this,
            &PreviewWindow::propertiesChanged);
    connect(mDoc->editor()->getPropertiesModel(),
            &QAbstractItemModel::modelReset,
            this,
            &PreviewWindow::propertiesReset);

    // Palette
    if (auto palette = mDoc->editor()->getModel(TableId::Palette)) {
        connect(palette, &QAbstractItemModel::dataChanged, this, &PreviewWindow::paletteChanged);
        connect(palette, &QAbstractItemModel::modelReset, this, &PreviewWindow::paletteReset);
    }

    auto factory = mDoc->editor()->createAnimationFactory();
    Q_ASSERT(factory != nullptr);
    mSpriteItem = factory->createNode(mFrameGroupId);
    mSpriteItem->setPaletteId(mPaletteId);
    mScene->addItem(mSpriteItem);
    mView->centerOn(QPoint(0, 0));
}

void PreviewWindow::paletteReset()
{
    rebuildScene();
}

void PreviewWindow::paletteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    rebuildScene();
}

void PreviewWindow::propertiesReset()
{
    rebuildScene();
}

void PreviewWindow::propertiesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    rebuildScene();
}

void PreviewWindow::rebuildScene()
{
    if (mSpriteItem != nullptr) {
        mScene->removeItem(mSpriteItem);
        delete mSpriteItem;
    }

    auto factory = mDoc->editor()->createAnimationFactory();
    Q_ASSERT(factory != nullptr);
    mSpriteItem = factory->createNode(mFrameGroupId);
    mSpriteItem->setPaletteId(mPaletteId);
    mScene->addItem(mSpriteItem);
    mView->centerOn(QPoint(0, 0));
}

void PreviewWindow::updateAnimation()
{
    auto dtime = mTimer->interval() / 1000.0;
    if (mSpriteItem != nullptr) {
        mSpriteItem->advance(dtime);
    }
}

void PreviewWindow::currentGroupChanged(int index)
{
    auto groupModel = mDoc->editor()->getModel(TableId::Animations);
    if (index < 0 || index >= groupModel->rowCount()) {
        return;
    }
    mFrameGroupId = groupModel->getIdOf(index);
    rebuildScene();
}

void PreviewWindow::paletteIndexChanged(int index)
{
    auto paletteModel = mDoc->editor()->getModel(TableId::Palette);
    if (paletteModel != nullptr) {
        mPaletteId = paletteModel->getIdOf(index);
        mSpriteItem->setPaletteId(mPaletteId);
    }
}

} // namespace gmtool
