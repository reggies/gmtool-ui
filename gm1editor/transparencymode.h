#pragma once

#include <QMetaType>

namespace gmtool {

enum class TransparencyMode {
    None,
    Color,
    Alpha,
};
}

Q_DECLARE_METATYPE(gmtool::TransparencyMode)
