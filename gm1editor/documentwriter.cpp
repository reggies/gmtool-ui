#include "documentwriter.h"

#include "gm1writer.h"
#include "logmodel.h"

namespace gmtool {

DocumentWriter::DocumentWriter(QObject *parent)
    : Operation(parent)
    , mLogModel(new LogModel(this))
{}

void DocumentWriter::start()
{
    Gm1Writer *writer = new Gm1Writer(this);
    connect(writer, &Gm1Writer::progress, this, &DocumentWriter::progress);
    connect(writer, &Gm1Writer::diagnosticsError, mLogModel, &LogModel::addError);
    connect(writer, &Gm1Writer::diagnosticsWarning, mLogModel, &LogModel::addWarning);
    writer->setFileData(mGm1);
    mSuccess = writer->saveToFile(mFilePath);
    writer->deleteLater();
    emit finished();
}

void DocumentWriter::setFilePath(const boost::filesystem::path &filePath)
{
    mFilePath = filePath;
}

void DocumentWriter::setFileData(const Gm1FileData &fileData)
{
    mGm1 = fileData;
}

boost::filesystem::path DocumentWriter::filePath() const
{
    return mFilePath;
}

QString DocumentWriter::status() const
{
    return tr("Saving");
}

LogModel *DocumentWriter::logs() const
{
    return mLogModel;
}

bool DocumentWriter::success() const
{
    return mSuccess;
}

} // namespace gmtool
