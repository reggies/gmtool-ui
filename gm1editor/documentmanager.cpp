#include "documentmanager.h"
#include "document.h"
#include "documentobject.h"

#include <boost/python.hpp>
#include <boost/python/list.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/module.hpp>
#include <boost/python/exec.hpp>
#include <frameobject.h>

#include <Python.h>

namespace bp = boost::python;

#if PY_MAJOR_VERSION >= 3
#define INIT_MODULE PyInit_gmtool
extern "C" PyObject* INIT_MODULE();
#else
#define INIT_MODULE initgmtool
extern "C" void INIT_MODULE();
#endif

namespace gmtool {

DocumentManager::DocumentManager(QObject *parent)
    : QObject(parent)
    , mAppPath(QCoreApplication::applicationFilePath().toUtf8())
{
    try
    {
        if (PyImport_AppendInittab("gmtool", INIT_MODULE) == -1)
            return;
        /* From blender:
         * PyC_SetHomePath */
        /* TBD: Py_SetProgramName is necessary for portable Python installations, so we put it here.
           But other parts are not ready yet
        */

        Py_SetProgramName(mAppPath.data());

        /* From blender:
         * Without this the `sys.stdout` may be set to 'ascii'
         * (it is on my system at least), where printing unicode values will raise
         * an error, this is highly annoying, another stumbling block for developers,
         * so use a more relaxed error handler and enforce utf-8 since the rest of
         * Blender is utf-8 too - campbell
         Py_SetStandardStreamEncoding("utf-8", "surrogateescape"); */


        /* Blender also sets these
           Py_FrozenFlag
           Py_IgnoreEnvironmentFlag
           Py_NoUserSiteDirectory
        */
        Py_Initialize();

        /* TBD: check error with Py_IsInitialized() */

        /* Blender initializes argv like this:
           PySys_SetObject("argv", py_argv);
         */

        /* Also, blender sets "executable" variable like this:
           PySys_SetObject ("executable", py_program_path);
            or like this
           PySys_SetObject ("executable", Py_None);
        */

        mInitialized = true;
    }
    catch (bp::error_already_set& e)
    {
        PyErr_Print();
        bp::handle_exception();
    }
}

DocumentManager::~DocumentManager()
{
    try
    {
        // TBD: Boost.Python doesn't support Py_Finalize yet, so don't call it!
        // TBD: this shall crash the application upon exit, so what?
        // TBD: memcheck complains, so fuck it
#if 0
        Py_Finalize();
#endif
    }
    catch (bp::error_already_set &e) {
        PyErr_Print();
        bp::handle_exception();
    }
}

Document *DocumentManager::createDocument()
{
    mNextDocumentNumber++;
    auto doc = new Document(this);
    doc->setName(tr("New document %1").arg(mNextDocumentNumber));
    mDocuments.insert(doc);
    return doc;
}

void DocumentManager::removeDocument(Document *document)
{
    const auto it = mDocuments.find(document);
    if (it != mDocuments.end()) {
        mDocuments.erase(it);
        document->deleteLater();
    }
}

} // namespace gmtool
