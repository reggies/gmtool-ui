#pragma once

#include <boost/optional.hpp>

#include <vector>

#include <QBitmap>
#include <QColor>
#include <QImage>
#include <QRect>

namespace gmtool {

class LogModel;

class SpritePlotter
{
public:
    SpritePlotter();

    void addImage(const QImage &image, const QRect &rect, const QBitmap &mask);
    void setTransparentPixel(uint pixel);
    void setTransparentIndex(int index);
    bool render(QImage::Format format, const QRect &bounds, LogModel *log);
    QImage plot() const;
    QPoint origin() const;

private:
    struct Item
    {
        QImage image;
        QRect rect;
        QBitmap mask;
    };

    std::vector<Item> mItems;
    QPoint mOrigin;
    QImage mPlot;
    boost::optional<uint> mTransparentPixel;
    boost::optional<int> mTransparentIndex;
};

} // namespace gmtool
