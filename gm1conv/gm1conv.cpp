#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/options_description.hpp>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#pragma GCC diagnostic pop

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <numeric>

#include <gm1.h>

#include <SDL.h>

#include "nullstream.hpp"
#include "utils.hpp"
#include "pngrw.hpp"

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace pt = boost::property_tree;

#ifndef WIN32
namespace boost
{
    namespace filesystem
    {
        std::istream& operator>>(std::istream &is, path &path)
        {
            /* Handle paths with spaces */
            path::string_type str;
            std::getline(is, str);
            path = str;
            return is;
        }
    }
}
#endif

namespace
{
    struct Gm1ToSdlColorConverter
    {
        SDL_Color operator()(const gm1_rgb_t& rgb)
        {
            SDL_Color color;
            memset(&color, 0, sizeof(SDL_Color));
            color.r = rgb.Red;
            color.g = rgb.Green;
            color.b = rgb.Blue;
            return color;
        }
    };

    struct SdlToGm1ColorConverter
    {
        gm1_rgb_t operator()(const SDL_Color& color)
        {
            gm1_rgb_t result;
            result.Red = color.r;
            result.Green = color.g;
            result.Blue = color.b;
            return result;
        }
    };
}

static gm1_size_t read_from_gm1(void* userdata, gm1_u8_t* buffer, gm1_size_t count)
{
    std::istream* is = static_cast<std::istream*>(userdata);
    is->read(reinterpret_cast<char*>(buffer), count);
    return static_cast<size_t>(is->gcount());
}

static gm1_size_t write_to_gm1(void* userdata, const gm1_u8_t* buffer, gm1_size_t count)
{
    std::ostream* out = static_cast<std::ostream*>(userdata);
    std::streampos pos = out->tellp();
    out->write(reinterpret_cast<const char*>(buffer), count);
    return out->tellp() - pos;
}

struct Args
{
    bool help = false;
    bool version = false;
    bool verbose = false;
    bool extract = false;
    bool create = false;
    bool bootstrap = false;
    fs::path source;
    fs::path target;
    int color_set = 0;
};

enum LogLevel {
    DEBUG,
    VERBOSE,
    WARN,
    ERR,
    FATAL
};

static LogLevel g_LogLevel = ERR;

std::ostream& log(LogLevel loglevel)
{
    static nullostream nullout;
    if(static_cast<int>(loglevel) < static_cast<int>(g_LogLevel)) {
        return nullout;
    } else {
        return std::cerr;
    }
}

fs::path get_header_path(const fs::path& dir)
{
    return dir / "header.conf";
}

fs::path get_palette_path(const fs::path& dir, unsigned int palette_idx)
{
    return dir / (std::to_string(palette_idx) + "-palette.png");
}

fs::path get_entry_image_path(const fs::path& dir, unsigned int entry_idx)
{
    return dir / (std::to_string(entry_idx) + "-img.png");
}

fs::path get_entry_header_path(const fs::path& dir, unsigned int entry_idx)
{
    return dir / (std::to_string(entry_idx) + ".conf");
}

fs::path get_plot_path(const fs::path& dir)
{
    return dir / "plot.png";
}

void print_error_log(const PngRw& png)
{
    if(!png.warnings().empty()) {
        log(WARN) << "==> libpng warnings:" << std::endl;
        for(const std::string& warning : png.warnings()) {
            log(WARN) << warning << std::endl;
        }
        log(WARN) << "<== libpng warnings" << std::endl;
    }

    if(!png.errors().empty()) {
        log(ERR) << "==> libpng error log:" << std::endl;
        for(const std::string& error : png.errors()) {
            log(ERR) << error << std::endl;
        }
        log(ERR) << "<== libpng error log" << std::endl;
    }
}

Uint32 get_packed_pixel(const Uint8* data, int bpp)
{
    switch(bpp) {
    case 1: return data[0];
    case 2: return data[0] | (data[1] << 8);
    case 3: return data[0] | (data[1] << 8) | (data[2] << 16);
    case 4: return data[0] | (data[1] << 8) | (data[2] << 16) | (data[3] << 24);
    default: return 0;
    }
}

void set_packed_pixel(Uint8* data, Uint32 pixel, int bpp)
{
    switch(bpp) {
    case 1: data[0] = pixel;
        break;
    case 2: data[1] = pixel >> 8,
            data[0] = pixel;
        break;
    case 3: data[2] = pixel >> 16,
            data[1] = pixel >> 8,
            data[0] = pixel;
        break;
    case 4: data[3] = pixel >> 24,
            data[2] = pixel >> 16,
            data[1] = pixel >> 8,
            data[0] = pixel;
        break;
    default:
        break;
    }
}

bool convert_pixel(Uint32 src, SDL_PixelFormat* srcfmt, Uint32 *dst, SDL_PixelFormat* dstfmt)
{
    std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
    surface.reset(SDL_CreateRGBSurface(0, 1, 1,
                                       srcfmt->BitsPerPixel,
                                       srcfmt->Rmask,
                                       srcfmt->Gmask,
                                       srcfmt->Bmask,
                                       srcfmt->Amask));
    if(!surface) {
        return false;
    }
    {
        SdlSurfaceLock lock(surface.get());
        set_packed_pixel(static_cast<Uint8*>(surface->pixels), src, srcfmt->BytesPerPixel);
    }
    surface.reset(SDL_ConvertSurface(surface.get(), dstfmt, 0));
    if(!surface) {
        return false;
    }
    SdlSurfaceLock lock(surface.get());
    *dst = get_packed_pixel(static_cast<Uint8*>(surface->pixels), dstfmt->BytesPerPixel);
    return true;
}

void dump_header(gm1_header_t* header, std::ostream* out)
{
    pt::ptree root;
    root.put("EntryCount", header->EntryCount);
    root.put("EntryClass", header->EntryClass);
    root.put("SizeCategory", header->Category);
    root.put("Width", header->Width);
    root.put("Height", header->Height);
    root.put("AnchorX", header->AnchorX);
    root.put("AnchorY", header->AnchorY);
    root.put("DataSize", header->DataSize);
    root.put("Unused1", header->Unused1);
    root.put("Unused2", header->Unused2);
    root.put("Unused3", header->Unused3);
    root.put("Unused4", header->Unused4);
    root.put("Unused5", header->Unused5);
    root.put("Unused6", header->Unused6);
    root.put("Unused7", header->Unused7);
    root.put("Unused8", header->Unused8);
    root.put("Unused9", header->Unused9);
    root.put("Unused10", header->Unused10);
    root.put("Unused11", header->Unused11);
    root.put("Unused12", header->Unused12);
    root.put("Unused13", header->Unused13);
    root.put("Unused14", header->Unused14);
    pt::write_ini(*out, root);
}

void dump_entry_header(gm1_entry_header_t* header, std::ostream* out)
{
    pt::ptree root;
    root.put("Width", header->Width);
    root.put("Height", header->Height);
    root.put("PosX", header->PosX);
    root.put("PosY", header->PosY);
    root.put("PartId", static_cast<int>(header->PartId));
    root.put("PartCount", static_cast<int>(header->PartSize));
    root.put("TileOffset", header->TileOffset);
    root.put("Alignment", static_cast<int>(header->Alignment));
    root.put("BoxOffset", static_cast<int>(header->BoxOffset));
    root.put("BoxWidth", static_cast<int>(header->BoxWidth));
    root.put("Flags", static_cast<int>(header->Flags));
    pt::write_ini(*out, root);
}

bool parse_header(gm1_header_t* header, std::istream* in)
{
    try {
        pt::ptree root;
        pt::read_ini(*in, root);
        header->EntryCount = root.get<int>("EntryCount");
        header->EntryClass = root.get<int>("EntryClass");
        header->Category = root.get<int>("SizeCategory", 0);
        header->Width = root.get<int>("Width", 0);
        header->Height = root.get<int>("Height", 0);
        header->AnchorX = root.get<int>("AnchorX", 0);
        header->AnchorY = root.get<int>("AnchorY", 0);
        header->DataSize = root.get<int>("DataSize", 0);
        header->Unused1 = root.get<int>("Unused1", 0);
        header->Unused2 = root.get<int>("Unused2", 0);
        header->Unused3 = root.get<int>("Unused3", 0);
        header->Unused4 = root.get<int>("Unused4", 0);
        header->Unused5 = root.get<int>("Unused5", 0);
        header->Unused6 = root.get<int>("Unused6", 0);
        header->Unused7 = root.get<int>("Unused7", 0);
        header->Unused8 = root.get<int>("Unused8", 0);
        header->Unused9 = root.get<int>("Unused9", 0);
        header->Unused10 = root.get<int>("Unused10", 0);
        header->Unused11 = root.get<int>("Unused11", 0);
        header->Unused12 = root.get<int>("Unused12", 0);
        header->Unused13 = root.get<int>("Unused13", 0);
        header->Unused14 = root.get<int>("Unused14", 0);
    } catch(const pt::ptree_error& err) {
        log(ERR) << "could not read header: " << err.what() << std::endl;
        return false;
    }

    return true;
}

bool parse_entry_header(gm1_entry_header_t* header, std::istream* in)
{
    try {
        pt::ptree root;
        pt::read_ini(*in, root);
        header->Width = root.get<int>("Width");
        header->Height = root.get<int>("Height");
        header->PosX = root.get<int>("PosX", 0);
        header->PosY = root.get<int>("PosY", 0);
        header->PartId = root.get<int>("PartId", 0);
        header->PartSize = root.get<int>("PartSize", 1);
        header->TileOffset = root.get<int>("TileOffset", 0);
        header->Alignment = root.get<int>("Alignment", 0);
        header->BoxOffset = root.get<int>("BoxOffset", 0);
        header->BoxWidth = root.get<int>("BoxWidth", 0);
        header->Flags = root.get<int>("Flags", 0);
    } catch(const pt::ptree_error& err) {
        log(ERR) << "could not read entry header: " << err.what() << std::endl;
        return false;
    }

    return true;
}

SDL_PixelFormat* mkfmt(const gm1_image_props_t* props)
{
    if(props->BytesPerPixel == 1) {
        return SDL_AllocFormat(SDL_PIXELFORMAT_INDEX8);
    } else if(props->BytesPerPixel == 2) {
        return SDL_AllocFormat(SDL_PIXELFORMAT_RGB555);
    } else {
        return NULL;
    }
}

SDL_Palette* create_palette_from_rgb(const gm1_rgb_t* rgb, size_t num_colors)
{
    std::vector<SDL_Color> colors(num_colors);
    std::transform(rgb, rgb + num_colors, colors.begin(), Gm1ToSdlColorConverter());

    std::unique_ptr<SDL_Palette, PaletteDeleter> palette;
    palette.reset(SDL_AllocPalette(colors.size()));
    if(!palette) {
        log(ERR) << "could not allocate palette: " << SDL_GetError() << std::endl;
        return NULL;
    }
    SDL_SetPaletteColors(palette.get(), colors.data(), 0, colors.size());
    return palette.release();
}

SDL_Surface* create_surface_for_palette(const gm1_rgb_t* rgb, size_t num_colors)
{
    std::unique_ptr<SDL_Palette, PaletteDeleter> palette;
    palette.reset(create_palette_from_rgb(rgb, num_colors));
    if(!palette) {
        return NULL;
    }

    std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
    surface.reset(SDL_CreateRGBSurface(0, num_colors, 1, 8, 0, 0, 0, 0));
    if(!surface) {
        log(ERR) << "could not create surface: " << SDL_GetError() << std::endl;
        return NULL;
    }

    SDL_SetSurfacePalette(surface.get(), palette.get());

    SdlSurfaceLock lock(surface.get());
    Uint8* pixels = static_cast<Uint8*>(surface->pixels);
    std::iota(pixels, pixels + surface->w, 0);

    return surface.release();
}

bool read_entry(SDL_Surface* surface, gm1_u32_t entry_class, gm1_size_t entry_size, gm1_entry_header_t* header, gm1_u16_t color_key, std::istream* in)
{
    SdlSurfaceLock lock(surface);

    gm1_image_t image;
    memset(&image, 0, sizeof(gm1_image_t));
    image.Width = surface->w;
    image.Height = surface->h;
    image.BytesPerPixel = surface->format->BytesPerPixel;
    image.BytesPerRow = surface->pitch;
    image.Pixels = static_cast<gm1_u8_t*>(surface->pixels);
    image.ColorKey = reinterpret_cast<gm1_u8_t*>(&color_key);

    gm1_istream_t is;
    memset(&is, 0, sizeof(gm1_istream_t));
    is.UserData = in;
    is.Read = read_from_gm1;

    gm1_status_t status = gm1_read_image(&is, entry_size, entry_class, header, &image);
    if(gm1_error(status)) {
        log(ERR) << "could not read image: " << gm1_errorstr(status) << std::endl;
        return false;
    }

    return true;
}

SDL_Surface* create_surface_for_entry(gm1_header_t* header, gm1_entry_t* entry, int color_set, std::istream* in)
{
    gm1_image_props_t props;
    gm1_get_image_props(header->EntryClass, &props);

    std::unique_ptr<SDL_PixelFormat, PixelFormatDeleter> entry_pf;
    entry_pf.reset(mkfmt(&props));
    if(!entry_pf) {
        return NULL;
    }

    std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
    surface.reset(SDL_CreateRGBSurface(0, entry->Header.Width, entry->Header.Height,
                                       entry_pf->BitsPerPixel,
                                       entry_pf->Rmask,
                                       entry_pf->Gmask,
                                       entry_pf->Bmask,
                                       entry_pf->Amask));

    if(!surface) {
        log(ERR) << "could not create surface: " << SDL_GetError() << std::endl;
        return NULL;
    }

    /* set color key and clear background */
    if(props.bHasColorKey) {
        SDL_FillRect(surface.get(), NULL, props.ColorKey);
        SDL_SetColorKey(surface.get(), SDL_TRUE, props.ColorKey);
    }

    /* read pixels */
    if(!read_entry(surface.get(), header->EntryClass, entry->Size, &entry->Header, props.ColorKey, in)) {
        return NULL;
    }

    if(props.BytesPerPixel == 1) {
        /* set default palette palette */
        std::unique_ptr<SDL_Palette, PaletteDeleter> palette;
        palette.reset(create_palette_from_rgb(header->ColorTable[color_set], GM1_NUM_COLORS));
        if(!palette)
            return NULL;
        SDL_SetSurfacePalette(surface.get(), palette.get());
    } else if(props.BytesPerPixel == 2) {
        /* convert surface to rgb24 */
        surface.reset(SDL_ConvertSurfaceFormat(surface.get(), SDL_PIXELFORMAT_RGB24, 0));
        if(!surface) {
            log(ERR) << "could not convert to 8 bit per channel: " << SDL_GetError() << std::endl;
            return NULL;
        }
    }
    return surface.release();
}

bool scrap_surface_colors(SDL_Surface* surface, std::vector<SDL_Color>& colors)
{
    SdlSurfaceLock lock(surface);
    for(int y = 0; y < surface->h; ++y) {
        for(int x = 0; x < surface->w; ++x) {
            SDL_Color color;
            SDL_GetRGBA(
                get_packed_pixel(
                    static_cast<Uint8*>(surface->pixels)
                    + y*surface->pitch
                    + x*surface->format->BytesPerPixel,
                    surface->format->BytesPerPixel),
                surface->format, &color.r, &color.g, &color.b, &color.a);
            colors.push_back(color);
        }
    }
    return true;
}

bool read_palette(gm1_rgb_t* rgb, gm1_size_t num_colors, std::istream* in)
{
    PngRw png_format;
    std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
    surface.reset(png_format.read(in));
    print_error_log(png_format);

    if(!surface) {
        log(ERR) << "could not load palette image" << std::endl;
        return false;
    }

    std::vector<SDL_Color> colors;
    scrap_surface_colors(surface.get(), colors);
    if(colors.size() != num_colors) {
        log(WARN) << "wrong number of colors in palette. fixing." << std::endl;
        SDL_Color null;
        memset(&null, 0, sizeof(SDL_Color));
        colors.resize(num_colors, null);
    }

    std::transform(colors.begin(), colors.end(), rgb, SdlToGm1ColorConverter());
    return true;
}

bool write_entry(SDL_Surface* surface, gm1_u32_t entry_class, gm1_entry_header_t* header, std::ostream* out)
{
    Uint32 color_key;
    SDL_GetColorKey(surface, &color_key);

    gm1_ostream_t entry_stream;
    memset(&entry_stream, 0, sizeof(gm1_ostream_t));
    entry_stream.UserData = out;
    entry_stream.Write = write_to_gm1;

    SdlSurfaceLock lock(surface);
    gm1_image_t image;
    memset(&image, 0, sizeof(gm1_image_t));
    image.Width = surface->w;
    image.Height = surface->h;
    image.BytesPerPixel = surface->format->BytesPerPixel;
    image.BytesPerRow = surface->pitch;
    image.Pixels = static_cast<gm1_u8_t*>(surface->pixels);
    image.ColorKey = reinterpret_cast<gm1_u8_t*>(&color_key);

    gm1_status_t status = gm1_write_image(&image, entry_class, header, &entry_stream);
    if(gm1_error(status)) {
        log(ERR) << "could not write entry: " << gm1_errorstr(status) << std::endl;
        return false;
    }
    return true;
}

bool write_surface(SDL_Surface* surface, std::ostream* out)
{
    PngRw rw;
    bool res = rw.write(surface, out);
    print_error_log(rw);
    return res;
}

SDL_Surface* convert_colorkey_to_alpha(SDL_Surface* surface)
{
    Uint32 color_key;
    if(SDL_GetColorKey(surface, &color_key) == -1) {
        return surface;
    }

    std::unique_ptr<SDL_PixelFormat, PixelFormatDeleter> fmt;
    fmt.reset(SDL_AllocFormat(SDL_PIXELFORMAT_ABGR8888));
    if(!fmt) {
        log(ERR) << "could not create format: " << SDL_GetError() << std::endl;
        return NULL;
    }

    std::unique_ptr<SDL_Surface, SurfaceDeleter> tmp;
    tmp.reset(SDL_CreateRGBSurface(0, surface->w, surface->h,
                                   fmt->BitsPerPixel,
                                   fmt->Rmask,
                                   fmt->Gmask,
                                   fmt->Bmask,
                                   fmt->Amask));
    if(!tmp) {
        log(ERR) << "could not create surface: " << SDL_GetError() << std::endl;
        return NULL;
    }

    /* fill with black transparent pixels */
    SDL_FillRect(tmp.get(), NULL, 0);

    if(0 != SDL_BlitSurface(surface, NULL, tmp.get(), NULL)) {
        log(ERR) << "could not blit surface: " << SDL_GetError() << std::endl;
        return NULL;
    }

    return tmp.release();
}

SDL_Surface* convert_alpha_to_colorkey(SDL_Surface* surface, Uint32 color_key)
{
    Uint32 format = SDL_MasksToPixelFormatEnum(surface->format->BitsPerPixel,
                                               surface->format->Rmask,
                                               surface->format->Gmask,
                                               surface->format->Bmask,
                                               0);
    std::unique_ptr<SDL_PixelFormat, PixelFormatDeleter> fmt;
    fmt.reset(SDL_AllocFormat(format));
    if(!fmt) {
        log(ERR) << "could not allocate pixel format: " << SDL_GetError() << std::endl;
        return NULL;
    }

    if(!convert_pixel(color_key, surface->format, &color_key, fmt.get())) {
        return NULL;
    }

    std::unique_ptr<SDL_Surface, SurfaceDeleter> tmp;
    tmp.reset(SDL_CreateRGBSurface(0, surface->w, surface->h,
                                   fmt->BitsPerPixel,
                                   fmt->Rmask,
                                   fmt->Gmask,
                                   fmt->Bmask,
                                   fmt->Amask));
    if(!tmp) {
        log(ERR) << "could not allocate surface: " << SDL_GetError() << std::endl;
        return NULL;
    }

    /* TODO should we convert color key to destination format ? */
    SDL_FillRect(tmp.get(), NULL, color_key);
    SDL_SetColorKey(tmp.get(), SDL_TRUE, color_key);

    if(0 != SDL_BlitSurface(surface, NULL, tmp.get(), NULL)) {
        log(ERR) << "could not blit surface: " << SDL_GetError() << std::endl;
        return NULL;
    }

    return tmp.release();
}

bool get_plot_size(gm1_entry_t* entries, gm1_size_t num_entries, gm1_u32_t* width, gm1_u32_t* height)
{
    gm1_u32_t w = 0;
    gm1_u32_t h = 0;
    for(gm1_entry_t* entry = entries; entry != entries + num_entries; ++entry) {
        w = std::max<gm1_u32_t>(w, entry->Header.PosX + entry->Header.Width);
        h = std::max<gm1_u32_t>(h, entry->Header.PosY + entry->Header.Height);
    }
    *width = w;
    *height = h;
    return true;
}

bool extract_plot(const fs::path& target_dir,
                  gm1_header_t* header,
                  gm1_entry_t* entries,
                  int color_set,
                  std::istream* fin)
{
    gm1_u32_t w = 0;
    gm1_u32_t h = 0;
    if(!get_plot_size(entries, header->EntryCount, &w, &h)) {
        return false;
    }

    std::streamoff data_start = fin->tellg();

    /* get pixel format of the first entry */
    std::unique_ptr<SDL_Surface, SurfaceDeleter> tmp;
    if(header->EntryCount != 0) {
        tmp.reset(create_surface_for_entry(header, entries, color_set, fin));
    }
    if(!tmp) {
        return false;
    }

    /* create plot */
    std::unique_ptr<SDL_Surface, SurfaceDeleter> plot;
    plot.reset(SDL_CreateRGBSurface(0, w, h,
                                    tmp->format->BitsPerPixel,
                                    tmp->format->Rmask,
                                    tmp->format->Gmask,
                                    tmp->format->Bmask,
                                    tmp->format->Amask));
    if(!plot) {
        log(ERR) << "could not create surface: " << SDL_GetError() << std::endl;
        return false;
    }


    gm1_image_props_t props;
    memset(&props, 0, sizeof(gm1_image_props_t));
    gm1_status_t status = gm1_get_image_props(header->EntryClass, &props);
    if(gm1_error(status)) {
        return false;
    }

    /* set plot default palette */
    if(props.BytesPerPixel == 1) {
        std::unique_ptr<SDL_Palette, PaletteDeleter> palette;
        palette.reset(create_palette_from_rgb(header->ColorTable[color_set], GM1_NUM_COLORS));
        if(!palette) {
            return false;
        }
        SDL_SetSurfacePalette(plot.get(), palette.get());
    }

    std::unique_ptr<SDL_PixelFormat, PixelFormatDeleter> entry_pf;
    entry_pf.reset(mkfmt(&props));
    if(!entry_pf) {
        return false;
    }

    /* dummy value */
    Uint32 color_key = 0x23;

    /* set color key and background color */
    if(props.bHasColorKey) {
        if(props.BytesPerPixel == 2) {
            if(!convert_pixel(props.ColorKey, entry_pf.get(), &color_key, plot->format)) {
                return false;
            }
        } else {
            /* the palette is unchanged and the pixels should be too */
            color_key = props.ColorKey;
        }
        SDL_SetColorKey(plot.get(), SDL_TRUE, color_key);

        log(VERBOSE) << " original color key: " << std::hex << props.ColorKey
                     << " plot color key: " << color_key << std::dec << std::endl;
    }

    SDL_FillRect(plot.get(), NULL, color_key);

    /* render entries */
    for(gm1_u32_t i = 0; i < header->EntryCount; ++i) {
        gm1_entry_t entry = entries[i];

        fs::ofstream fout(get_entry_header_path(target_dir, i), std::ios::binary);
        if(!fout) {
            log(ERR) << "could not write file: " << get_entry_header_path(target_dir, i) << std::endl;
            return false;
        }
        dump_entry_header(&entry.Header, &fout);
        fin->seekg(data_start + static_cast<std::streamoff>(entry.Offset));
        std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
        surface.reset(create_surface_for_entry(header, &entry, color_set, fin));
        if(!surface) {
            log(ERR) << "could not dump entry" << std::endl;
            return false;
        }
        SDL_Rect dstrect;
        dstrect.x = entry.Header.PosX;
        dstrect.y = entry.Header.PosY;
        dstrect.w = entry.Header.Width;
        dstrect.h = entry.Header.Height;
        if(0 != SDL_BlitSurface(surface.get(), NULL, plot.get(), &dstrect)) {
            log(ERR) << "could not blit surface: " << SDL_GetError() << std::endl;
            return false;
        }
    }

    if(props.bHasColorKey && props.BytesPerPixel == 2) {
        /* try to convert to rgba */
        std::unique_ptr<SDL_Surface, SurfaceDeleter> rgba_surface;
        rgba_surface.reset(convert_colorkey_to_alpha(plot.get()));
        if(!rgba_surface) {
            log(WARN) << "could not convert to RGBA format" << std::endl;
        } else {
            plot.swap(rgba_surface);
        }
    }

    log(VERBOSE) << "plot image: " << get_plot_path(target_dir) << std::endl;
    log(VERBOSE) << " width: " << plot->w
                 << " height: " << plot->h
                 << " depth: " << static_cast<int>(plot->format->BitsPerPixel)
                 << " rmask: " << std::hex << plot->format->Rmask
                 << " gmask: " << plot->format->Gmask
                 << " bmask: " << plot->format->Bmask
                 << " amask: " << plot->format->Amask << std::dec
                 << std::endl;

    /* write plot to file */
    fs::ofstream fout(get_plot_path(target_dir), std::ios::binary);
    if(!fout) {
        log(ERR) << "could not write file: " << get_plot_path(target_dir) << std::endl;
        return false;
    }
    if(!write_surface(plot.get(), &fout)) {
        return false;
    }
    return true;
}

bool gm1_extract(const Args& args)
{
    if(args.target.empty()) {
        log(FATAL) << "target directory is not specified" << std::endl;
        return false;
    }

    fs::create_directories(args.target);
    fs::ifstream fin(args.source, std::ios::binary);
    if(!fin) {
        log(ERR) << "could not open file: " << args.source << std::endl;
        return false;
    }

    gm1_istream_t in;
    in.UserData = &fin;
    in.Read = read_from_gm1;

    gm1_header_t header;
    gm1_status_t status = gm1_read_header(&in, &header);
    if(gm1_error(status)) {
        log(ERR) << "could not read header: " << gm1_errorstr(status) << std::endl;
        return false;
    }

    fs::ofstream fout;
    fout.open(get_header_path(args.target));
    dump_header(&header, &fout);
    fout.close();

    for(gm1_u32_t i = 0; i < GM1_NUM_PALETTES; ++i) {
        fout.open(get_palette_path(args.target, i), std::ios::binary);
        std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
        surface.reset(create_surface_for_palette(header.ColorTable[i], GM1_NUM_COLORS));
        if(!surface) {
            log(ERR) << "could not dump palette: " << get_palette_path(args.target, i) << std::endl;
            return false;
        }
        if(!write_surface(surface.get(), &fout)) {
            return false;
        }
        fout.close();
    }

    std::vector<gm1_entry_t> entries(header.EntryCount);
    status = gm1_read_entries(&in, header.EntryClass, entries.data(), entries.size());
    if(gm1_error(status)) {
        log(ERR) << "could not read entries: " << gm1_errorstr(status) << std::endl;
        return false;
    }

    return extract_plot(args.target, &header, entries.data(), args.color_set, &fin);
}

bool read_plot(const fs::path& source_dir,
               gm1_header_t* header,
               gm1_entry_t* entries,
               std::ostream* out)
{
    fs::ifstream fin;
    for(gm1_size_t i = 0; i < header->EntryCount; ++i) {
        gm1_entry_t* entry = entries + i;
        fin.open(get_entry_header_path(source_dir, i));
        if(!parse_entry_header(&entry->Header, &fin)) {
            log(ERR) << "could not parse entry header file: " << get_entry_header_path(source_dir, i) << std::endl;
            return false;
        }
        fin.close();
    }

    fin.open(get_plot_path(source_dir), std::ios::binary);
    PngRw png;
    std::unique_ptr<SDL_Surface, SurfaceDeleter> plot;
    plot.reset(png.read(&fin));
    print_error_log(png);
    if(!plot) {
        log(ERR) << "could not read plot image: " << get_plot_path(source_dir) << std::endl;
        return false;
    }
    fin.close();

    log(VERBOSE) << "plot path: " << get_plot_path(source_dir) << std::endl;
    log(VERBOSE) << " width: " << plot->w
                 << " height: " << plot->h
                 << " depth: " << static_cast<int>(plot->format->BitsPerPixel)
                 << " rmask: " << std::hex << plot->format->Rmask
                 << " gmask: " << plot->format->Gmask
                 << " bmask: " << plot->format->Bmask
                 << " amask: " << plot->format->Amask << std::dec
                 << std::endl;

    /* check plot size */
    gm1_u32_t w;
    gm1_u32_t h;
    if(!get_plot_size(entries, header->EntryCount, &w, &h)) {
        return false;
    }

    if(w != static_cast<gm1_u32_t>(plot->w) || h != static_cast<gm1_u32_t>(plot->h)) {
        log(WARN) << "image size mismatch" << std::endl;
    }

    /* create entries pixel format which is suitable for libgm1 */
    gm1_image_props_t props;
    std::unique_ptr<SDL_PixelFormat, PixelFormatDeleter> entry_pf;
    gm1_get_image_props(header->EntryClass, &props);
    entry_pf.reset(mkfmt(&props));
    if(!entry_pf) {
        return false;
    }

    /* strip alpha channel if needed and convert it to color key */
    if(SDL_ISPIXELFORMAT_ALPHA(plot->format->format)) {
        Uint32 color_key;
        if(props.bHasColorKey) {
            color_key = props.ColorKey;
        } else {
            log(WARN) << "no background color for alpha specified" << std::endl;
            color_key = 0;
        }
        if(!convert_pixel(color_key, entry_pf.get(), &color_key, plot->format)) {
            return false;
        }
        plot.reset(convert_alpha_to_colorkey(plot.get(), color_key));
        if(!plot) {
            log(ERR) << "could not convert alpha to color key" << std::endl;
            return false;
        }
    }

    /* get plot color key  */
    Uint32 color_key;
    bool has_color_key = (SDL_GetColorKey(plot.get(), &color_key) != -1);

    /* unset SDL_SRCCOLORKEY as we can blit surface */
    SDL_SetColorKey(plot.get(), SDL_FALSE, 0);

    /* check palette presence and size */
    if(props.BytesPerPixel == 1) {
        if(plot->format->palette == NULL ||
           plot->format->palette->ncolors != 256) {
            log(ERR) << "expect paletted image with 256 colors" << std::endl;
            return false;
        }
    }

    /* configure color key */
    if(props.bHasColorKey) {
        if(has_color_key) {
            log(VERBOSE) << "original color key: " << std::hex << color_key << std::dec << std::endl;
            /* convert color key for entries */
            if(entry_pf->format != plot->format->format) {
                if(!convert_pixel(color_key, plot->format, &color_key, entry_pf.get())) {
                    return false;
                }
            }
        } else {
            log(VERBOSE) << "no original color key" << std::endl;
            color_key = props.ColorKey;
        }
        has_color_key = true;
        log(VERBOSE) << "effective color key: " << std::hex << color_key << std::dec << std::endl;
    }

    /* read individual entries from plot */
    for(gm1_size_t i = 0; i < header->EntryCount; ++i) {
        gm1_entry_t* entry = entries + i;

        /* prepare surface for entry */
        std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
        surface.reset(SDL_CreateRGBSurface(0, entry->Header.Width, entry->Header.Height,
                                           entry_pf->BitsPerPixel,
                                           entry_pf->Rmask,
                                           entry_pf->Gmask,
                                           entry_pf->Bmask,
                                           entry_pf->Amask));
        if(!surface) {
            log(ERR) << "could not create surface: " << SDL_GetError() << std::endl;
            return false;
        }

        SDL_SetSurfacePalette(surface.get(), plot->format->palette);
        SDL_SetColorKey(surface.get(), (has_color_key ? SDL_TRUE : SDL_FALSE), color_key);

        /* copy & convert plot region into entry surface */
        SDL_Rect srcrect;
        srcrect.x = entry->Header.PosX;
        srcrect.y = entry->Header.PosY;
        srcrect.w = entry->Header.Width;
        srcrect.h = entry->Header.Height;
        if(0 != SDL_BlitSurface(plot.get(), &srcrect, surface.get(), NULL)) {
            log(ERR) << "could not blit surface: " << SDL_GetError() << std::endl;
            return false;
        }

        /* write pixels to gm stream */
        entry->Offset = out->tellp();
        if(!write_entry(surface.get(), header->EntryClass, &entry->Header, out)) {
            return false;
        }
        entry->Size = static_cast<gm1_u32_t>(out->tellp()) - entry->Offset;
    }

    return true;
}

bool gm1_create(const Args& args)
{
    if(args.source.empty()) {
        log(FATAL) << "source gm1 is not specified" << std::endl;
        return false;
    }
    
    fs::ifstream fin;

    gm1_header_t header;
    fin.open(get_header_path(args.source));
    if(!fin) {
        log(ERR) << "could not find file: " << get_header_path(args.source) << std::endl;
        return false;
    }
    if(!parse_header(&header, &fin)) {
        log(ERR) << "could not parse header" << std::endl;
        return false;
    }
    fin.close();

    for(gm1_size_t i = 0; i < GM1_NUM_PALETTES; ++i) {
        fin.open(get_palette_path(args.source, i), std::ios::binary);
        if(!fin) {
            log(ERR) << "could not find file: " << get_palette_path(args.source, i) << std::endl;
            return false;
        }
        if(!read_palette(header.ColorTable[i], GM1_NUM_COLORS, &fin)) {
            log(ERR) << "could not read palette: " << get_palette_path(args.source, i) << std::endl;
            return false;
        }
        fin.close();
    }

    std::ostringstream oss;
    std::vector<gm1_entry_t> entries(header.EntryCount);
    if(!read_plot(args.source, &header, entries.data(), &oss)) {
        return false;
    }

    const std::string &entries_data = oss.str();
    header.DataSize = entries_data.size();

    fs::ofstream fout(args.target, std::ios::binary);

    gm1_ostream_t out;
    out.UserData = &fout;
    out.Write = write_to_gm1;

    gm1_status_t status;
    status = gm1_write_header(&out, &header);
    if(gm1_error(status)) {
        log(ERR) << "could not write header: " << gm1_errorstr(status) << std::endl;
        return false;
    }

    status = gm1_write_entries(&out, header.EntryClass, entries.data(), entries.size());
    if(gm1_error(status)) {
        log(ERR) << "could not write entries: " << gm1_errorstr(status) << std::endl;
        return false;
    }

    fout.write(entries_data.c_str(), entries_data.size());
    fout.close();

    return true;
}

bool gm1_bootstrap(const Args& args)
{
    if(args.target.empty()) {
        log(FATAL) << "target directory is not specified" << std::endl;
        return false;
    }
    fs::create_directories(args.target);

    gm1_header_t header;
    memset(&header, 0, sizeof(gm1_header_t));
    header.EntryCount = 1;

    fs::ofstream fout;
    fout.open(get_header_path(args.target));
    dump_header(&header, &fout);
    fout.close();

    for(gm1_u32_t i = 0; i < GM1_NUM_PALETTES; ++i) {
        gm1_rgb_t palette[GM1_NUM_COLORS];
        memset(palette, 0, sizeof(palette));
        fout.open(get_palette_path(args.target, i), std::ios::binary);
        std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
        surface.reset(create_surface_for_palette(palette, GM1_NUM_COLORS));
        if(!surface) {
            log(ERR) << "could not dump palette: " << get_palette_path(args.target, i) << std::endl;
            return false;
        }
        if(!write_surface(surface.get(), &fout)) {
            return false;
        }
        fout.close();
    }

    gm1_entry_header_t entry_header;
    memset(&entry_header, 0, sizeof(gm1_entry_header_t));
    fout.open(get_entry_header_path(args.target, 0));
    dump_entry_header(&entry_header, &fout);
    fout.close();

    return true;
}

int gm1conv_main(int argc, char* argv[])
{
    Args args;

    po::options_description exclusive("Command switches");
    exclusive.add_options()
        ("extract,x", po::bool_switch(&args.extract), "Extract specified .gm1")
        ("create,c", po::bool_switch(&args.create), "Create new .gm1")
        ("bootstrap,b", po::bool_switch(&args.bootstrap), "Bootstrap new .gm1")
        ;

    po::options_description options("All options");
    options.add_options()
        ("help,h", po::bool_switch(&args.help), "Print this message")
        ("version", po::bool_switch(&args.version), "Print version")
        ("verbose,v", po::bool_switch(&args.verbose), "Allow more verbosity")
        ("color-set", po::value(&args.color_set), "Set default palette index (0-9)")
        ("source,s", po::value(&args.source), "Source path")
        ("target,t", po::value(&args.target), "Target path")
        ;

    po::options_description visible;
    visible.add(exclusive);
    visible.add(options);

    po::positional_options_description unnamed;
    unnamed.add("source", 1);
    unnamed.add("target", 1);

    po::parsed_options parsed = po::command_line_parser(argc, argv)
        .options(visible)
        .positional(unnamed)
        .run();

    po::variables_map vars;
    po::store(parsed, vars);
    po::notify(vars);

    if(args.help) {
        std::cout << visible << std::endl;
        return EXIT_SUCCESS;
    }

    unsigned int num_exclusive_switches = 0;
    for(bool value : {args.create, args.bootstrap, args.extract}) {
        if(value)
            ++num_exclusive_switches;
    }
    if(num_exclusive_switches != 1) {
        log(FATAL) << "You should specify one of these options" << std::endl
                   << exclusive << std::endl;
        return EXIT_FAILURE;
    }

    if(args.color_set < 0 || args.color_set >= GM1_NUM_PALETTES) {
        log(FATAL) << "--color-set should match the number of palettes: " << (GM1_NUM_PALETTES-1) << std::endl;
        return EXIT_FAILURE;
    }

    if(args.verbose) {
        g_LogLevel = VERBOSE;
    }

    if(args.target.empty()) {
        args.target = args.source.filename();
    }

    bool res;
    if(args.create) {
        res = gm1_create(args);
    } else if(args.extract) {
        res = gm1_extract(args);
    } else if(args.bootstrap) {
        res = gm1_bootstrap(args);
    }

    return res ? EXIT_SUCCESS : EXIT_FAILURE;
}

int main(int argc, char *argv[])
{
	try
	{
        return gm1conv_main(argc, argv);
	}
	catch(const std::exception& err)
	{
		std::cerr << err.what() << std::endl;
		return EXIT_FAILURE;
	}
}
