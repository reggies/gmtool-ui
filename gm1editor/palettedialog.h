#pragma once

#include <QComboBox>
#include <QDialog>

#include "recordid.h"

namespace gmtool {

class DocumentEditor;

class PaletteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PaletteDialog(DocumentEditor *editor, const QString &text, QWidget *parent = nullptr);

    static RecordId getPaletteId(DocumentEditor *editor, QWidget *parent);

    RecordId selectedId() const;

private:
    DocumentEditor *mEditor;
    QComboBox *mPaletteBox;
};

} // namespace gmtool
