#include "editorfactory.h"

#include "animationeditor.h"
#include "bitmapeditor.h"
#include "fonteditor.h"
#include "tileeditor.h"

#include <stdexcept>

namespace gmtool {

EditorFactory::EditorFactory()
{}

bool EditorFactory::isTypeSupported(const DocumentType &type)
{
    EditorFactory factory;
    factory.setDocumentType(type);
    return factory.createEmptyEditor().operator bool();
}

std::unique_ptr<DocumentEditor> EditorFactory::createEmptyEditor() const
{
    switch (mType) {
    case DocumentType::Anim:
        return std::unique_ptr<DocumentEditor>(new AnimationEditor);

    case DocumentType::Tile:
        return std::unique_ptr<DocumentEditor>(new TileEditor);

    case DocumentType::Font:
        return std::unique_ptr<DocumentEditor>(new FontEditor);

    case DocumentType::Bitmap: {
        int flags = BitmapEditor::NoFlags;
        if (mMagicBitmap) {
            flags |= BitmapEditor::MagicBitmap;
        }
        return std::unique_ptr<DocumentEditor>(new BitmapEditor(flags));
    }

    case DocumentType::Static: {
        int flags = BitmapEditor::UseTransparency;
        if (mConstSize) {
            flags |= BitmapEditor::LayoutIsConstSize;
        }
        return std::unique_ptr<DocumentEditor>(new BitmapEditor(flags));
    }

    case DocumentType::Invalid:
        break;
    }

    return nullptr;
}

std::unique_ptr<DocumentEditor> EditorFactory::createEditor(LogModel *log) const
{
    auto editor = createEmptyEditor();
    if (mFileData) {
        if (!editor->load(*mFileData, log))
            return nullptr;
    }
    return editor;
}

void EditorFactory::setDocumentType(const DocumentType &type)
{
    mType = type;
}

void EditorFactory::setFileData(const Gm1FileData &fileData)
{
    mFileData = fileData;
}

void EditorFactory::setConstSize(bool constSize)
{
    mConstSize = constSize;
}

void EditorFactory::setMagicBitmap(bool magicBitmap)
{
    mMagicBitmap = magicBitmap;
}

} // namespace gmtool
