#include "framemanager.h"

#include <algorithm>
#include <memory>

#include "document.h"
#include "frame.h"

namespace gmtool {

FrameManager::FrameManager(QObject *parent)
    : QObject(parent)
{}

FrameManager::~FrameManager()
{
    for (Frame *frame : mFrames)
        frame->deleteLater();
}

Frame *FrameManager::createFrame(Document *document)
{
    auto frame = new Frame(document, nullptr);

    connect(frame, &Frame::newDocumentRequested, this, &FrameManager::newDocumentRequested);

    connect(frame, &Frame::openDocumentRequested, this, &FrameManager::openDocumentRequested);

    connect(frame, &Frame::closeDocumentRequested, this, &FrameManager::closeDocumentRequested);

    connect(frame, &Frame::cloneFrameRequested, this, &FrameManager::cloneFrameRequested);

    connect(frame, &Frame::closeFrameRequested, this, &FrameManager::closeFrameRequested);

    mFrames.insert(frame);
    frame->show();

    return frame;
}

int FrameManager::getFramesCount(const Document *document) const
{
    return std::count_if(mFrames.begin(), mFrames.end(), [document](auto frame) {
        return frame->document() == document;
    });
}

void FrameManager::removeFrames(const Document *document)
{
    decltype(mFrames) frames;

    for (auto frame : mFrames) {
        if (frame->document() == document) {
            frame->deleteLater();
        } else {
            frames.insert(frame);
        }
    }

    mFrames.swap(frames);
}

void FrameManager::removeFrame(Frame *frame)
{
    const auto it = mFrames.find(frame);
    if (it != mFrames.end()) {
        frame->deleteLater();
        mFrames.erase(it);
    }
}

void FrameManager::cloneFrameRequested(Frame *frame)
{
    createFrame(frame->document());
}

void FrameManager::closeFrameRequested(Frame *frame)
{
    Document *document = frame->document();
    if (getFramesCount(document) == 1) {
        /// \todo check if modified document should be saved
        emit closeDocumentRequested(document);
    } else {
        frame->setCloseAllowed(true);
        frame->close();
        removeFrame(frame);
    }
}

} // namespace gmtool
