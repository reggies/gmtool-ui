#pragma once

#include <QString>
#include <QVariant>

namespace gmtool {

class PropertyItem
{
public:
    PropertyItem(const QString &name, QVariant::Type type)
        : mName(name)
        , mValue(type)
    {}

    QString name() const { return mName; }
    QVariant value() const { return mValue; }

    void setName(const QString &name) { mName = name; }
    bool setValue(const QVariant &value)
    {
        if (value.canConvert(mValue.type()))
            mValue = value;
        return value.canConvert(mValue.type());
    }

    void setEditable(bool editable) { mIsEditable = editable; }
    bool isEditable() const { return mIsEditable; }

private:
    bool mIsEditable = false;
    QString mName;
    QVariant mValue;
};

} // namespace gmtool
