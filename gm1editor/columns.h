#pragma once

#include <cstdint>
#include <functional>
#include <limits>

#include "animationframegroup.h"
#include "animationrecord.h"
#include "paletterecord.h"
#include "spriterecord.h"
#include "tablecolumn.h"
#include "tilealignment.h"
#include "tilerecord.h"

namespace gmtool {
namespace columns {

Q_DECL_CONSTEXPR static QRect kSceneBounds = QRect(0,
                                                   0,
                                                   std::numeric_limits<std::uint16_t>::max(),
                                                   std::numeric_limits<std::uint16_t>::max());

template<class RecordT>
class MutableLeft : public MyColumn<int, RecordT>
{
public:
    MutableLeft()
        : MyColumn<int, RecordT>(QObject::tr("Left"), &RecordT::left, &RecordT::setLeft)
    {}

    bool check(const RecordT &record, const int &left) const override
    {
        QRect bbox = record.rect();
        bbox.moveLeft(left);
        return (kSceneBounds & bbox) == bbox;
    }
};

template<class RecordT>
class MutableTop : public MyColumn<int, RecordT>
{
public:
    MutableTop()
        : MyColumn<int, RecordT>(QObject::tr("Top"), &RecordT::top, &RecordT::setTop)
    {}

    bool check(const RecordT &record, const int &top) const override
    {
        QRect bbox = record.rect();
        bbox.moveTop(top);
        return (kSceneBounds & bbox) == bbox;
    }
};

template<class RecordT>
class ConstWidth : public MyColumn<int, RecordT>
{
public:
    ConstWidth()
        : MyColumn<int, RecordT>(QObject::tr("Width"), &RecordT::width)
    {}
};

template<class RecordT>
class ConstHeight : public MyColumn<int, RecordT>
{
public:
    ConstHeight()
        : MyColumn<int, RecordT>(QObject::tr("Height"), &RecordT::height)
    {}
};

template<class RecordT>
class ConstFilePath : public MyColumn<QString, RecordT>
{
public:
    ConstFilePath()
        : MyColumn<QString, RecordT>(QObject::tr("File"), &RecordT::filePath)
    {}
};

template<class RecordT>
class MutableWidth : public MyColumn<int, RecordT>
{
public:
    MutableWidth()
        : MyColumn<int, RecordT>(QObject::tr("Width"), &RecordT::width, &RecordT::setWidth)
    {}

    bool check(const RecordT &record, const int &width) const override
    {
        if (width <= 0)
            return false;
        QRect bbox = record.rect();
        bbox.setWidth(width);
        return (kSceneBounds & bbox) == bbox;
    }
};

template<class RecordT>
class MutableHeight : public MyColumn<int, RecordT>
{
public:
    MutableHeight()
        : MyColumn<int, RecordT>(QObject::tr("Height"), &RecordT::height, &RecordT::setHeight)
    {}

    bool check(const RecordT &record, const int &height) const override
    {
        if (height <= 0)
            return false;
        QRect bbox = record.rect();
        bbox.setHeight(height);
        return (kSceneBounds & bbox) == bbox;
    }
};

template<class RecordT>
class Color : public MyColumn<QColor, RecordT>
{
public:
    explicit Color(int index)
        : MyColumn<QColor, RecordT>(QString("#%1").arg(index),
                                    std::bind(&RecordT::colorAt, std::placeholders::_1, index),
                                    std::bind(&RecordT::setColorAt,
                                              std::placeholders::_1,
                                              index,
                                              std::placeholders::_2))
    {}
};

template<class RecordT>
class MutableBoxHeight : public MyColumn<int, RecordT>
{
public:
    MutableBoxHeight()
        : MyColumn<int, RecordT>(QObject::tr("Box"), &RecordT::boxHeight, &RecordT::setBoxHeight)
    {}

    bool check(const RecordT &entry, const int &box) const override
    {
        if (box < 0) {
            return false;
        }
        TileRecord temp;
        temp.assign(entry);
        temp.setBoxHeight(box);
        return (kSceneBounds & temp.rect()) == temp.rect();
    }
};

template<class RecordT>
class MutableTileSize : public MyColumn<int, RecordT>
{
public:
    MutableTileSize()
        : MyColumn<int, RecordT>(QObject::tr("Tile Size"), &RecordT::size, &RecordT::setSize)
    {}

    bool check(const RecordT &entry, const int &value) const override
    {
        if (value < 1 || value > 16) {
            return false;
        }
        TileRecord temp;
        temp.assign(entry);
        temp.setSize(value);
        return (kSceneBounds & temp.rect()) == temp.rect();
    }
};

template<class RecordT>
class MutableFontBearing : public MyColumn<int, RecordT>
{
public:
    MutableFontBearing()
        : MyColumn<int, RecordT>(QObject::tr("Bearing"), &RecordT::bearing, &RecordT::setBearing)
    {}
};

class PalettePreview : public TableColumn<PaletteRecord>
{
public:
    PalettePreview()
        : TableColumn(QObject::tr("Name"))
    {}

    QVariant displayData(const PaletteRecord &record) const override { return record.name(); }
};

class ConstName : public TableColumn<AnimationFrameGroup>
{
public:
    ConstName()
        : TableColumn(QObject::tr("Name"))
    {}

    QVariant displayData(const AnimationFrameGroup &record) const override { return record.name; }
};

} // namespace columns
} // namespace gmtool
