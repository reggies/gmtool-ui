#pragma once

#include <vector>

#include <QAbstractListModel>

namespace gmtool {

class ZoomModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit ZoomModel(QObject *parent = 0);

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int getValue(int index) const;
    int indexOf(int value) const;
    int defaultIndex() const;

private:
    std::vector<int> mZoomValues;
};

} // namespace gmtool
