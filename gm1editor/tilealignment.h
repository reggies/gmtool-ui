#pragma once

#include <QMetaType>

namespace gmtool {

enum class TileAlignment { Left, Right, Center, None };

static inline const char *debugAlignmentToString(TileAlignment alignment)
{
    switch (alignment) {
    case TileAlignment::None:
        return "none";
    case TileAlignment::Center:
        return "center";
    case TileAlignment::Left:
        return "left";
    case TileAlignment::Right:
        return "right";
    }
    return "(invalid)";
}

} // namespace gmtool

Q_DECLARE_METATYPE(gmtool::TileAlignment)
