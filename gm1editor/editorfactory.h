#pragma once

#include <boost/optional.hpp>

#include "documenttype.h"
#include "gm1filedata.h"

#include <memory>

namespace gmtool {
class DocumentEditor;
class LogModel;

class EditorFactory
{
public:
    EditorFactory();

    static bool isTypeSupported(const DocumentType &type);

    std::unique_ptr<DocumentEditor> createEditor(LogModel *log) const;
    std::unique_ptr<DocumentEditor> createEmptyEditor() const;

    void setDocumentType(const DocumentType &type);
    void setFileData(const Gm1FileData &fileData);
    void setConstSize(bool constSize);
    void setMagicBitmap(bool magicBitmap);

private:
    DocumentType mType = DocumentType::Invalid;
    boost::optional<Gm1FileData> mFileData;
    bool mConstSize = false;
    ///< documents of Static type can have constant sized layout entries.
    ///< ignored for other types

    bool mMagicBitmap = false;
    ///< applied to bitmap archives of unusual type
};

} // namespace gmtool
