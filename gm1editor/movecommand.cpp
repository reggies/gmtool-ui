#include "movecommand.h"

#include <QDebug>
#include <QObject>

namespace gmtool {

MoveCommand::MoveCommand(QAbstractItemModel *model, int row, int count, int dest)
    : mModel(model)
    , mRow(row)
    , mCount(count)
    , mDest(dest)
{}

MoveCommand::~MoveCommand() = default;

bool MoveCommand::undo()
{
    /*!
      From Qt docs:

      ".. the rows will be placed before the destinationChild index.
      That is, if you wish to move rows 0 and 1 so they will become rows 1 and
      2, destinationChild should be 3. In this case, the new index for the
      source row i (which is between sourceFirst and sourceLast) is equal to
      (destinationChild-sourceLast-1+i)."
     */
    if (mRow < mDest) {
        return mModel->moveRows(QModelIndex(),
                                mDest - mCount,
                                mCount,
                                QModelIndex(),
                                mRow + mCount - 1);
    } else {
        return mModel->moveRows(QModelIndex(),
                                mDest - mCount + 1,
                                mCount,
                                QModelIndex(),
                                mRow + mCount);
    }
}

bool MoveCommand::redo()
{
    return mModel->moveRows(QModelIndex(), mRow, mCount, QModelIndex(), mDest);
}

QString MoveCommand::name() const
{
    return QObject::tr("Move rows");
}

} // namespace gmtool
