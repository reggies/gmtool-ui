#include "colorsarraywidget.h"

#include <QPainter>

namespace gmtool {

ColorsArrayWidget::ColorsArrayWidget(QWidget *parent)
    : QWidget(parent)
{
    setAutoFillBackground(false);
}

void ColorsArrayWidget::setColors(const QVector<QColor> &colors)
{
    mColors = colors;
    update();
}

void ColorsArrayWidget::paintEvent(QPaintEvent *paintEvent)
{
    QPainter painter(this);

    if (mColors.empty())
        return;

    for (int i = 0; i < mColors.size(); ++i) {
        painter.fillRect(QRect(i, 0, i + 1, height()), mColors.at(i));
    }
}

QSize ColorsArrayWidget::sizeHint() const
{
    return QSize(mColors.size(), 5);
}

} // namespace gmtool