#pragma once

#include <functional>
#include <map>

#include "recordid.h"
#include "tableid.h"

namespace gmtool {
class SceneNode;
class AnimationNode;

template<class T>
class Factory final
{
public:
    using creator_fn_t = std::function<T *(RecordId)>;
    using self_type = Factory<T>;

public:
    Factory() {}
    ~Factory() {}

    T *createNode(const RecordId &id) const
    {
        if (mCreator)
            return mCreator(id);
        else
            return nullptr;
    }

    void setNodeCreator(creator_fn_t creator)
    {
        mCreator = creator;
    }

private:
    creator_fn_t mCreator;
};

using NodeFactory = Factory<SceneNode>;
using AnimationFactory = Factory<AnimationNode>;

} // namespace gmtool
