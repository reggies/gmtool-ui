#pragma once

#include <QAbstractListModel>

#include <vector>

#include "logmessage.h"

namespace gmtool {

class LogModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit LogModel(QObject *parent = 0);
    ~LogModel() override;

    void clear();

    void merge(const LogModel *other);

    void addMessage(const QString &text, Severity severity = Severity::Error);

    void addError(const QString &text);
    void addWarning(const QString &text);

    void error(const char *format, ...);
    void warning(const char *format, ...);
    void info(const char *format, ...);

    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    int errorCount() const;
    int warningCount() const;

    void markAsViewed();

    enum Roles {
        SeverityRole = Qt::UserRole,
    };

signals:
    void messagesChanged();

private:
    std::vector<LogMessage> mMessages;
};

} // namespace gmtool
