#ifndef TAGGEDRECORDS_H_
#define TAGGEDRECORDS_H_

#if 0
enum class RecordTag {
    Animation,
    Font,
    Bitmap,
    Tile,
    Sprite,
    Palette,
    FrameGroup
};

struct TaggedRecordData {
    RecordTag tag;
    union {
        AnimationRecordData animation;
        SpriteRecordData sprite;
        FontRecordData font;
        PaletteRecordData palette;
        TileRecordData tile;
        AnimationFrameGroup frameGroup;
    } u;
};

class TaggedRecord : public AbstractRecord, TaggedRecordData {
    Q_OBJECT

public:
    explicit TaggedRecord(const TaggedRecordData &data = {}, QObject* parent = nullptr)
        : AbstractRecord(parent)
        , AnimationFrameGroupRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new TaggedRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        TaggedRecordData::operator=(dynamic_cast<const TaggedRecordData &>(other));
    }
};

#endif


#endif
