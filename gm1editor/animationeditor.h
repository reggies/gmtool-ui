#pragma once

#include <set>

#include "animationframegroup.h"
#include "animationrecord.h"
#include "documenteditor.h"
#include "gm1filedata.h"
#include "paletterecord.h"
#include "recordset.h"
#include "spriterecord.h"

namespace gmtool {
class PropertyItem;
class TableModel;

class AnimationEditor : public DocumentEditor
{
public:
    AnimationEditor();
    ~AnimationEditor() override;

    PropertiesModel *getPropertiesModel() override;
    TableModel *getModel(TableId table) override;

    std::unique_ptr<AbstractRecord> createBlankEntry() override;
    std::unique_ptr<NodeFactory> createLayoutFactory() override;
    std::unique_ptr<NodeFactory> createSpriteFactory() override;
    std::unique_ptr<AnimationFactory> createAnimationFactory() override;

    bool load(const Gm1FileData &fileData, LogModel *log) override;
    bool save(Gm1FileData *fileData, LogModel *log) override;

    std::unique_ptr<SpriteLoader> createSpriteLoader() override;

    bool renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result) override;

    QPoint pivot() const;

private:
    void setupEntriesTableModel();
    void setupColorTableModel();
    void setupSpritesTableModel();
    void setupPropertiesModel();
    void setupAnimationsTableModel();

private:
    RecordSet<AnimationRecord> mEntries;
    RecordSet<PaletteRecord> mColorTable;
    RecordSet<SpriteRecord> mSprites;
    RecordSet<AnimationFrameGroup> mAnimationGroups;

    std::unique_ptr<TableModel> mEntriesTableModel;
    std::unique_ptr<TableModel> mColorTableModel;
    std::unique_ptr<TableModel> mSpritesTableModel;
    std::unique_ptr<PropertiesModel> mPropertiesModel;
    std::unique_ptr<TableModel> mAnimationsTableModel;

    Gm1FileData mGm1;

    PropertyItem *mXPivotProp;
    PropertyItem *mYPivotProp;

private:
    AnimationEditor(const AnimationEditor &) = delete;
    AnimationEditor &operator=(const AnimationEditor &) = delete;
};

} // namespace gmtool
