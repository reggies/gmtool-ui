#pragma once

#include <QDialog>
#include <QTabWidget>
#include <QTableView>

#include "window.h"

namespace gmtool {
class Document;

class LogViewer : public Window
{
    Q_OBJECT

public:
    explicit LogViewer(Document *document, QWidget *parent);
    ~LogViewer() override;

    void showErrors();
    void showWarnings();

    void clearAll();

private:
    void createGeneralTab();
    void createErrorsTab();
    void createWarningsTab();

private:
    Document *mDoc;

    QWidget *mGeneralTab;
    QWidget *mErrors;
    QWidget *mWarnings;

    QTableView *mGeneralView;
    QTableView *mErrorsView;
    QTableView *mWarningsView;

    QTabWidget *mTabWidget;
};

} // namespace gmtool
