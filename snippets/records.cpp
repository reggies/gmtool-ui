#include "records.h"


#include "framesmodel.h"
#include "spriteplotter.h"
#include "logmodel.h"

#include <QDebug>

namespace gmtool {

void FramesRecord::createImagesAndMasks() const
{
    mData.group = *mModel->mFrameGroupModel->getRecordById(mFrameGroupId);
    mData.createImagesAndMasks(
        mModel->mFrameGroupModel,
        mModel->mSpriteModel,
        mModel->mLayoutModel
        );
}

void FramesRecordData::createImagesAndMasks(
        AnimationGroupModel *groupModel,
        SpriteModel *spriteModel,
        AnimationModel *animationModel)
{
    if (clean)
        return;

    qDebug() << Q_FUNC_INFO << this;

    SpritePlotter maskPlotter;
    SpritePlotter plotter;
    plotter.setTransparentIndex(0);
    for (int i = 0; i < spriteModel->rowCount(); ++i) {
        const SpriteRecord *record = spriteModel->getRecord(i);
        plotter.addImage(record->image(), record->rect(), record->mask());
        if (!record->mask().isNull()) {
            maskPlotter.addImage(record->mask().toImage(), record->rect(), QBitmap());
        }
        qDebug() << Q_FUNC_INFO << "plot" << i;
    }

    LogModel log;
    images.resize(group.frameCount);
    masks.resize(group.frameCount);
    for (int i = 0; i < group.frameCount; ++i) {
        const int currentFrameIndex = group.frameStart + i * group.frameStep;
        if (currentFrameIndex >= animationModel->rowCount()) {
            break;
        }
        const AnimationRecord *animation = animationModel->getRecord(currentFrameIndex);
        if (!plotter.render(QImage::Format_Indexed8, animation->rect(), &log)) {
            continue;
        }
        if (!maskPlotter.render(QImage::Format_Grayscale8, animation->rect(), &log)) {
            continue;
        }
        images[i] = plotter.plot();
        masks[i] = QBitmap::fromImage(maskPlotter.plot());
        qDebug() << Q_FUNC_INFO << "render" << i;
    }

    qDebug() << Q_FUNC_INFO << images.size();

    clean = true;
}

void FramesRecord::markDirty()
{
    mData.clean = false;
    emit changed();
}

AnimationFrameGroupRecordData FramesRecord::frameGroup() const
{
    createImagesAndMasks();
    return mData.group;
}

std::vector<QImage> FramesRecord::images() const
{
    createImagesAndMasks();
    return mData.images;
}

std::vector<QBitmap> FramesRecord::masks() const
{
    createImagesAndMasks();
    return mData.masks;
}

QPoint FramesRecord::pivot() const
{
    createImagesAndMasks();
    return mData.pivot;
}

QString FramesRecord::toString() const
{
    createImagesAndMasks();
    return mData.group.toString();
}

} // namespace gmtool

