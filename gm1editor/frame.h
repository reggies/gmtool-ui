#pragma once

#include <boost/filesystem/path.hpp>

#include <QCloseEvent>
#include <QLabel>
#include <QMainWindow>
#include <QPointer>
#include <QProgressBar>

#include <map>
#include <set>

#include "document.h"
#include "recordid.h"
#include "window.h"

namespace gmtool {

class PlotWindow;
class LogViewer;
class Window;

class Frame : public QMainWindow
{
    Q_OBJECT

public:
    explicit Frame(Document *document, QWidget *parent = 0);
    ~Frame() override;

    const Document *document() const;
    Document *document();

    Window *focusWindow();

    /// returns nullptr if none
    const Window *focusWindow() const;

    bool isWindowFocused(Window *window) const;
    void closeAllWindows();
    void setCloseAllowed(bool allowed);

    void updateCursorInfo(const QPoint &pos);

signals:
    void newDocumentRequested(Frame *frame);
    void openDocumentRequested(Frame *frame, const boost::filesystem::path &path);
    void closeDocumentRequested(Document *document);
    void cloneFrameRequested(Frame *frame);
    void closeFrameRequested(Frame *frame);

protected:
    void closeEvent(QCloseEvent *event) override;
    bool eventFilter(QObject *receiver, QEvent *event) override;

private:
    void newDocument();
    void openDocument();
    void saveDocument();
    void saveAsDocument();
    void closeDocument();
    void revertDocument();

    void cloneFrame();

    void showEntryTable();
    void showPlot();
    void showColorTable();
    void showSprites();
    void showProperties();
    void showPreview();
    void showRepl();

    void updateOperation();

    void updateTitle();

    /// called per Document::editorChanged
    void updateEditor();
    void updateMenus();

    void setupFileMenu();
    void setupEditMenu();
    void setupViewMenu();
    void setupWindowMenu();
    void setupToolsMenu();
    void setupSpritesMenu();
    void setupColorTableMenu();

    /// returned object is owned by this Frame
    QDockWidget *addWindow(Window *window);

    void findRequested(TableId table, const RecordId &id);

    QString documentContainingDir() const;

    void linkActivated(const QString &link);

    void updateMessages();
    void showMessageLog();

    void applicationFocusChanged(QWidget *old, QWidget *now);

    void windowClosed(Window *window);

    void routeAllActions(Window *window);
    void unrouteAllActions(Window *window);

private:
    Document *mDocument;
    ///< many views can share one document

    std::set<Window *> mWindows;
    ///< owned by this object

    std::set<QDockWidget *> mDocks;
    ///< owner by this

    Window *mLastFocusedWindow = nullptr;
    ///< not owned; can be invalidated at any time

    QProgressBar *mOperationProgress;
    QLabel *mOperationStatus;

    QLabel *mCursorPosSection;

    bool mCloseAllowed = false;

    QWidget *mWarningsPanel;
    QLabel *mWarnings;
    QWidget *mErrorsPanel;
    QLabel *mErrors;

    QDockWidget *mViewerDock;
    QPointer<LogViewer> mCurrentViewer;

    QToolBar *mFileBar;
    QToolBar *mEditBar;
    QToolBar *mViewBar;
    QToolBar *mWindowBar;
    QToolBar *mToolsBar;

    QToolBar *mColorTableBar;
    QToolBar *mSpritesBar;

    //
    // SpritesBar
    //
    ImportSprites *mImportSprites; //    +SpritesWindow +PlotWindow
    ExportSprites *mExportSprites; //   +SpritesWindow +PlotWindow

    //
    // ColorTable
    //
    LoadPalette *mLoadPalette; //   +ColorTable
    SavePalette *mSavePalette; //    +ColorTable
    ChangeHue *mChangeHue;     //    +ColorTable

    //
    // Global data
    //
    FindSpriteOrEntry *mFindSpriteOrEntry;     //         +SpritesWindow +PlotWindow +EntryWindow
    RemoveSpriteOrEntry *mRemoveSpriteOrEntry; //         +SpritesWindow +PlotWindow +EntryWindow
    AddEntry *mAddEntry;                       //        +PlotWindow +EntryWindow
    MoveForward *mMoveForward;                 //  +SpritesWindow +PlotWindow
    MoveBackward *mMoveBackward;               //   +SpritesWindow +PlotWindow
    BringForward *mBringForward;               //   +SpritesWindow +PlotWindow
    SendBack *mSendBack;                       //  +SpritesWindow +PlotWindow
    SelectAll *mSelectAll;     //   +SpritesWindow +PlotWindow +EntryWindow +ColorTable
    SelectClear *mSelectClear; //    +SpritesWindow +PlotWindow +EntryWindow +ColorTable

    //
    // Frame tools
    //
    ScrollMode *mScrollMode;
    TransformMode *mTransformMode;
    AllowSelectSprites *mAllowSelectSprites;
    AllowSelectEntries *mAllowSelectEntries;

    //
    // Frame view
    //
    SnapToGrid *mSnapToGrid;
    ShowGrid *mShowGrid;
    ShowOverlay *mShowOverlay;

private:
    Frame(const Frame &) = delete;
    Frame &operator=(const Frame &) = delete;
};

} // namespace gmtool
