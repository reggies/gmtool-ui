#pragma once

#include <QBitmap>
#include <QColor>
#include <QDebug>
#include <QImage>
#include <QLine>
#include <QPoint>
#include <QRect>
#include <QRegion>
#include <QString>

#include <boost/filesystem/path.hpp>

#include <limits>
#include <memory>

#include "abstractrecord.h"
#include "colormask.h"
#include "gm1filedata.h"
#include "transparencymode.h"

namespace gmtool {

class AnimationRecordData
{
public:
    QRect mRect;
};

class AnimationRecord : public AbstractRecord, public AnimationRecordData
{
    Q_OBJECT

public:
    explicit AnimationRecord(const AnimationRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , AnimationRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new AnimationRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        AnimationRecordData::operator=(dynamic_cast<const AnimationRecordData &>(other));
        emit positionChanged();
    }

    QRect rect() const { return mRect; }
    void setRect(const QRect &rect)
    {
        mRect = rect;
        emit positionChanged();
    }

    int left() const { return mRect.x(); }
    int width() const { return mRect.width(); }
    int top() const { return mRect.y(); }
    int height() const { return mRect.height(); }

    void setLeft(int value)
    {
        mRect.moveLeft(value);
        emit positionChanged();
    }
    void setWidth(int value)
    {
        mRect.setWidth(value);
        emit positionChanged();
    }
    void setTop(int value)
    {
        mRect.moveTop(value);
        emit positionChanged();
    }
    void setHeight(int value)
    {
        mRect.setHeight(value);
        emit positionChanged();
    }

signals:
    void positionChanged();
};

class TileRecordData
{
public:
    TileRecordData() = default;

    auto tileHeight() const { return 16; }

    auto tileWidth() const { return 30; }

    auto widthStride() const { return 32; }

    auto heightStride() const { return 16; }

    auto rect() const
    {
        return QRect(mPos,
                     QSize(widthStride() * mSize + tileWidth(),
                           mBoxHeight + heightStride() * (mSize + 1)));
    }

    int width() const { return rect().width(); }
    int height() const { return rect().height(); }

    int left() const { return rect().x(); }
    void setTop(int top) { mPos.setY(top); }

    int size() const { return mSize + 1; }

    void setSize(int size) { mSize = qMax(1, size) - 1; }

    int top() const { return rect().y(); }
    void setLeft(int left) { mPos.setX(left); }

    int boxHeight() const { return mBoxHeight; }
    void setBoxHeight(int boxHeight) { mBoxHeight = boxHeight; }

private:
    QPoint mPos;

    //! Because tile can not be smaller than 1x1 store it as zero-indexed value.
    //! 0 - regular tile 1x1
    //! 1 - square 2x2
    //! 2 - square 3x3
    int mSize = 0;

    int mBoxHeight = 0;
};

class TileRecord : public AbstractRecord, public TileRecordData
{
    Q_OBJECT

public:
    explicit TileRecord(const TileRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , TileRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new TileRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        TileRecordData::operator=(dynamic_cast<const TileRecordData &>(other));
    }
};

class BitmapRecordData
{
public:
    BitmapRecordData() = default;

    QRect rect() const { return mRect; }
    void setRect(const QRect &rect) { mRect = rect; }

    int left() const { return mRect.x(); }
    int width() const { return mRect.width(); }
    int top() const { return mRect.y(); }
    int height() const { return mRect.height(); }

    void setLeft(int value) { mRect.moveLeft(value); }
    void setWidth(int value) { mRect.setWidth(value); }
    void setTop(int value) { mRect.moveTop(value); }
    void setHeight(int value) { mRect.setHeight(value); }

private:
    QRect mRect;
};

class BitmapRecord : public AbstractRecord, public BitmapRecordData
{
    Q_OBJECT

public:
    explicit BitmapRecord(const BitmapRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , BitmapRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new BitmapRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        BitmapRecordData::operator=(dynamic_cast<const BitmapRecordData &>(other));
    }
};

class FontRecordData
{
public:
    FontRecordData() = default;

    QRect rect() const { return mRect; }
    void setRect(const QRect &rect) { mRect = rect; }

    int width() const { return mRect.width(); }
    void setWidth(int width) { mRect.setWidth(width); }

    int height() const { return mRect.height(); }
    void setHeight(int height) { mRect.setHeight(height); }

    int top() const { return mRect.top(); }
    void setTop(int top) { mRect.moveTop(top); }

    int left() const { return mRect.left(); }
    void setLeft(int left) { mRect.moveLeft(left); }

    int bearing() const { return mBearing; }
    void setBearing(int bearing) { mBearing = bearing; }

    QLine baseLine() const { return QLine(0, mBearing, mRect.width(), mBearing); }

private:
    QRect mRect;
    int mBearing = 0;
};

class FontRecord : public AbstractRecord, public FontRecordData
{
    Q_OBJECT

public:
    explicit FontRecord(const FontRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , FontRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new FontRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        FontRecordData::operator=(dynamic_cast<const FontRecordData &>(other));
    }
};

class SpriteRecordData
{
public:
    QImage mImg;
    mutable QBitmap mMask;
    QPoint mPos;
    QRgb mTransparentPixel = 0;
    TransparencyMode mTransparencyMode = TransparencyMode::None;
    boost::filesystem::path mPath;
};

class SpriteRecord : public AbstractRecord, public SpriteRecordData
{
    Q_OBJECT

public:
    explicit SpriteRecord(const SpriteRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , SpriteRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new SpriteRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        SpriteRecordData::operator=(dynamic_cast<const SpriteRecordData &>(other));
        emit imageChanged();
        emit positionChanged();
    }

    auto isNull() const { return mImg.isNull() && mPos.isNull(); }

    QBitmap mask() const
    {
        if (mMask.isNull() && (mTransparencyMode == TransparencyMode::Color)) {
            mMask = createColorMask(mTransparentPixel, mImg);
        }
        return mMask;
    }

    auto image() const { return mImg; }
    void setImage(const QImage &image)
    {
        mImg = image;
        emit imageChanged();
    }
    auto position() const { return mPos; }
    void setPosition(const QPoint &position)
    {
        mPos = position;
        emit positionChanged();
    }
    auto left() const { return mPos.x(); }
    void setLeft(int left)
    {
        mPos.setX(left);
        emit positionChanged();
    }
    auto top() const { return mPos.y(); }
    void setTop(int top)
    {
        mPos.setY(top);
        emit positionChanged();
    }
    auto width() const { return mImg.width(); }
    auto height() const { return mImg.height(); }
    auto rect() const { return QRect(mPos, mImg.size()); }

    void setTransparentPixel(const QRgb &color)
    {
        mTransparentPixel = color;
        mMask = QBitmap();
        emit imageChanged();
    }

    void setMask(const QBitmap &mask)
    {
        mMask = mask;
        emit imageChanged();
    }

    void setTransparencyMode(TransparencyMode mode)
    {
        mTransparencyMode = mode;
        emit imageChanged();
    }

    auto filePath() const { return mPath; }
    void setFilePath(const boost::filesystem::path &path) { mPath = path; }

signals:
    void positionChanged();
    void imageChanged();
};

class PaletteRecordData
{
public:
    Gm1FileData::palette_t colors() const { return mColors; }

    void setName(const QString &name) { mName = name; }

    QString name() const { return mName; }

    void setColors(const Gm1FileData::palette_t &colors) { mColors = colors; }

    void setColorAt(int index, const QColor &color) { mColors.at(index) = color; }

    size_t colorCount() const { return mColors.size(); }

    QColor colorAt(int index) const { return mColors.at(index); }

private:
    Gm1FileData::palette_t mColors;
    QString mName;
};

class PaletteRecord : public AbstractRecord, public PaletteRecordData
{
    Q_OBJECT

public:
    explicit PaletteRecord(const PaletteRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , PaletteRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new PaletteRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        PaletteRecordData::operator=(dynamic_cast<const PaletteRecordData &>(other));
    }
};

struct AnimationFrameGroupRecordData
{
    AnimationFrameGroupRecordData() = default;
    AnimationFrameGroupRecordData(
        const QString &name, int frameStart, int frameStep, int frameCount, bool backAndForth)
        : name(name)
        , frameStart(frameStart)
        , frameStep(frameStep)
        , frameCount(frameCount)
        , backAndForth(backAndForth)
    {}

    AnimationFrameGroupRecordData(const AnimationFrameGroupRecordData &) = default;
    AnimationFrameGroupRecordData &operator=(const AnimationFrameGroupRecordData &) = default;

    bool indexInGroup(int group) const {
        if (frameCount == 0 || frameStep == 0) {
            return false;
        } else {
            Q_ASSERT(frameStep > 0);
            return
                (group - frameStart) % frameStep == 0 &&
                (group - frameStart) / frameStep < frameCount;
        }
    }

    QString name;
    int frameStart = 0;
    int frameStep = 0;
    int frameCount = 1;
    bool backAndForth = false;

    QString toString() const {
        return name;
    }
};

class AnimationFrameGroup : public AbstractRecord, public AnimationFrameGroupRecordData
{
    Q_OBJECT

public:
    explicit AnimationFrameGroup(const AnimationFrameGroupRecordData &data = {},
                                 QObject *parent = nullptr)
        : AbstractRecord(parent)
        , AnimationFrameGroupRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new AnimationFrameGroup(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        AnimationFrameGroupRecordData::operator=(dynamic_cast<const AnimationFrameGroupRecordData &>(other));
    }
};

} // namespace gmtool
