#include "undo.h"

#include <boost/range/adaptor/reversed.hpp>

#include <QDebug>

namespace gmtool {

namespace undo {

void Macro::add(std::unique_ptr<Command> command)
{
    mCommands.emplace_back(std::move(command));
}
bool Macro::redo()
{
    auto it = mCommands.begin();
    while (it != mCommands.end()) {
        if (!(*it)->redo()) {
            break;
        }
        ++it;
    }
    if (it != mCommands.end()) {
        while (it != mCommands.begin()) {
            --it;
            if (it != mCommands.begin()) {
                (*it)->undo();
            }
        }
        return false;
    }
    return true;
}

bool Macro::undo()
{
    for (auto &command : mCommands | boost::adaptors::reversed) {
        if (!command->undo()) {
            qFatal("failing undo is not handled");
        }
    }
    return true;
}

QString Macro::name() const
{
    return mName;
}

Stack::Stack() = default;

Stack::~Stack() = default;

bool Stack::push(std::unique_ptr<Command> command)
{
    if (!command->redo()) {
        return false;
    }
    mUndoed.clear();
    mRedoed.push_back(std::move(command));
    emit undoChanged(mRedoed.back().get());
    emit redoChanged(nullptr);
    return true;
}

void Stack::clear()
{
    mUndoed.clear();
    mRedoed.clear();
    emit undoChanged(nullptr);
    emit redoChanged(nullptr);
}

void Stack::undo()
{
    if (mRedoed.empty())
        return;

    auto command = std::move(mRedoed.back());
    mRedoed.pop_back();
    if (!command->undo()) {
        mRedoed.push_back(std::move(command));
    } else {
        mUndoed.push_back(std::move(command));
        emit undoChanged(mRedoed.empty() ? nullptr : mRedoed.back().get());
        emit redoChanged(mUndoed.back().get());
    }
}

void Stack::redo()
{
    if (mUndoed.empty())
        return;

    auto command = std::move(mUndoed.back());
    mUndoed.pop_back();
    if (!command->redo()) {
        mUndoed.push_back(std::move(command));
    } else {
        mRedoed.push_back(std::move(command));
        emit undoChanged(mRedoed.back().get());
        emit redoChanged(mUndoed.empty() ? nullptr : mUndoed.back().get());
    }
}

QAction *Stack::createUndoAction(QObject *parent, const QString &title)
{
    auto undo = new QAction(title, parent);
    undo->setShortcuts(QKeySequence::Undo);
    connect(this, &Stack::undoChanged, undo, [undo, title](Command *command) {
        if (command == nullptr) {
            undo->setText(title);
            undo->setEnabled(false);
        } else {
            auto name = command->name();
            if (name.isEmpty()) {
                undo->setText(title);
            } else {
                undo->setText(tr("%1 '%2'").arg(title).arg(name));
            }
            undo->setEnabled(true);
        }
    });
    connect(undo, &QAction::triggered, this, &Stack::undo);
    return undo;
}

QAction *Stack::createRedoAction(QObject *parent, const QString &title)
{
    auto redo = new QAction(title, parent);
    redo->setShortcuts(QKeySequence::Redo);
    connect(this, &Stack::redoChanged, redo, [redo, title](Command *command) {
        if (command == nullptr) {
            redo->setText(title);
            redo->setEnabled(false);
        } else {
            auto name = command->name();
            if (name.isEmpty()) {
                redo->setText(title);
            } else {
                redo->setText(tr("%1 '%2'").arg(title).arg(name));
            }
            redo->setEnabled(true);
        }
    });
    connect(redo, &QAction::triggered, this, &Stack::redo);
    return redo;
}

} // namespace undo

} // namespace gmtool
