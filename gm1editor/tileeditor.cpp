#include "tileeditor.h"

#include <sstream>
#include <vector>

#include <boost/range/adaptor/map.hpp>

#include <QPainter>

#include "basicspriteloader.h"
#include "columns.h"
#include "logmodel.h"
#include "nodefactory.h"
#include "propertiesmodel.h"
#include "scenenode.h"
#include "spritegraphicsitem.h"
#include "spriteloader.h"
#include "spriteplotter.h"
#include "subtilewalk.h"
#include "tableadapter.h"
#include "tablemodel.h"
#include "tilegraphicsitem.h"

//                          +-----+
//                     +----+     +----+
//                     |               |
//                +----+               +----+
//           +----+                         +----+
//      +----+    +----+               +----+    +----+
//      |              |               |              |
// +----+              +----+     +----+              +----+
// |                        |-----|                        |
// +----+              +----+     +----+              +----+
//      |              |               |              |
//      +----+    +----+               +----+    +----+
//           +----+                         +----+
//                +----+               +----+
//                     |               |
//                     +----+     +----+
//                          +-----+

namespace gmtool {

TileEditor::TileEditor()
    : mEntriesTableModel(new TableModel(nullptr))
    , mSpritesTableModel(new TableModel(nullptr))
    , mPropertiesModel(new PropertiesModel)
{
    setupEntriesTableModel();
    setupSpritesTableModel();
}

TileEditor::~TileEditor() = default;

void TileEditor::setupEntriesTableModel()
{
    std::unique_ptr<TableAdapter<TileRecord>> adapter(new TableAdapter<TileRecord>(mEntries));
    adapter->addColumn<columns::MutableLeft<TileRecord>>();
    adapter->addColumn<columns::MutableTop<TileRecord>>();
    adapter->addColumn<columns::MutableTileSize<TileRecord>>();
    adapter->addColumn<columns::MutableBoxHeight<TileRecord>>();
    mEntriesTableModel->reset(std::unique_ptr<AbstractTable>(adapter.release()));
}

void TileEditor::setupSpritesTableModel()
{
    std::unique_ptr<TableAdapter<SpriteRecord>> adapter(new TableAdapter<SpriteRecord>(mSprites));
    adapter->addColumn<columns::MutableLeft<SpriteRecord>>();
    adapter->addColumn<columns::MutableTop<SpriteRecord>>();
    adapter->addColumn<columns::ConstWidth<SpriteRecord>>();
    adapter->addColumn<columns::ConstHeight<SpriteRecord>>();
    mSpritesTableModel->reset(std::unique_ptr<AbstractTable>(adapter.release()));
}

TableModel *TileEditor::getModel(TableId table)
{
    switch (table) {
    case TableId::Gm1_Tile:
    case TableId::Layout:
        return mEntriesTableModel.get();
    case TableId::Palette:
        return nullptr;
    case TableId::Spritesheet:
        return mSpritesTableModel.get();
    case TableId::Invalid:
    case TableId::Gm1_Animation:
    case TableId::Gm1_Bitmap:
    case TableId::Gm1_Glyph:
    case TableId::Animations:
        return nullptr;
    }
    return nullptr;
}

PropertiesModel *TileEditor::getPropertiesModel()
{
    return mPropertiesModel.get();
}

std::unique_ptr<AbstractRecord> TileEditor::createBlankEntry()
{
    return TileRecord().clone();
}

std::unique_ptr<NodeFactory> TileEditor::createLayoutFactory()
{
    std::unique_ptr<NodeFactory> fact(new NodeFactory);
    fact->setNodeCreator([this](const RecordId &id) {
        return new TileGraphicsItem(this, id, nullptr);
    });
    return fact;
}

std::unique_ptr<NodeFactory> TileEditor::createSpriteFactory()
{
    std::unique_ptr<NodeFactory> fact(new NodeFactory);
    fact->setNodeCreator([this](const RecordId &id) {
        return new SpriteGraphicsItem(this, id, nullptr);
    });
    return fact;
}

std::unique_ptr<SpriteLoader> TileEditor::createSpriteLoader()
{
    return std::unique_ptr<SpriteLoader>(
        new BasicSpriteLoader(this, TransparencyMode::Color, getDefaultTransparentPixel()));
}

void createTiles(const Gm1FileData &fileData, LogModel *log, RecordSet<TileRecord> *result)
{
    for (size_t i = 0; i < fileData.entryCount();) {
        QRect bbox;

        int nextPartId = 0;
        while ((i < fileData.entryCount()) && (fileData.Entries.at(i).partId() == nextPartId)) {
            bbox |= fileData.Entries.at(i).entryRect();
            ++nextPartId;
            //! \todo check part alignment
            ++i;
        }

        // find the size of the tile group
        int size = -1;
        if (nextPartId != -1) {
            for (int guess = 1; guess * guess <= nextPartId; ++guess) {
                if (nextPartId == guess * guess) {
                    size = guess;
                    break;
                }
            }
        }

        // validate size of the tile
        if (size == -1) {
            std::ostringstream oss;
            oss << "unexpected end of tile group:" << std::endl
                << "nextPartId=" << nextPartId << std::endl
                << "index=" << i << std::endl;
            // qDebug(oss.str().c_str());
            log->addMessage(QString::fromStdString(oss.str()), Severity::Warning);
            continue;
        }

        TileRecord record;
        if ((((bbox.width() + record.widthStride() - record.tileWidth()) % record.widthStride())
             != 0)
            || (bbox.width() < record.tileWidth())) {
            std::ostringstream oss;
            oss << "entry bbox has incorrect width:" << std::endl
                << "width=" << bbox.width() << std::endl;
            // qDebug(oss.str().c_str());
            log->addMessage(QString::fromStdString(oss.str()), Severity::Warning);
            continue;
        }

        int guessSize = (bbox.width() + record.widthStride() - record.tileWidth())
                        / record.widthStride();

        if (bbox.height() < guessSize * record.tileHeight()) {
            //! the rect is too low to be a valid tile object
            std::ostringstream oss;
            oss << "could not guess size of entry:" << std::endl
                << "guessSize=" << guessSize << std::endl
                << "tileHeight=" << record.tileHeight() << std::endl
                << "height=" << bbox.height() << std::endl
                << "width=" << bbox.width() << std::endl;
            // qDebug(oss.str().c_str());
            log->addMessage(QString::fromStdString(oss.str()), Severity::Warning);
            continue;
        }

        record.setTop(bbox.top());
        record.setLeft(bbox.left());
        record.setBoxHeight(bbox.height() - record.tileHeight() * guessSize);
        record.setSize(guessSize);

        result->append(record);
    }
}

bool TileEditor::load(const Gm1FileData &fileData, LogModel *log)
{
    mGm1 = fileData;

    //! load plot image
    SpriteRecord plot;
    plot.setPosition(mGm1.plotOrigin());
    plot.setImage(mGm1.plotImage());
    if (mGm1.hasTransparency()) {
        plot.setTransparencyMode(TransparencyMode::Color);
        plot.setTransparentPixel(mGm1.transparentPixel());
    }

    mSprites.clear();
    mSprites.insert(plot, 0);
    setupSpritesTableModel();

    mEntries.clear();
    createTiles(mGm1, log, &mEntries);

    setupEntriesTableModel();

    mPropertiesModel->reset();
    return true;
}

size_t enumSubTiles(const TileRecord &record, size_t startIdx, Gm1FileData *gm1)
{
    size_t count = 0;

    walkSubTiles(record, [&count, &record, startIdx, gm1](SubTile subtile) {
        Gm1FileEntry &entry = gm1->Entries.at(startIdx + subtile.order);
        entry.setPartId(subtile.order);
        entry.setPartSize(record.size() * record.size());
        entry.setEntryRect(subtile.entryBbox);
        entry.setAlignment(subtile.alignment);

        Gm1FileEntry::TileBox box;
        switch (subtile.alignment) {
        case TileAlignment::Left:
            box.left = 0;
            box.width = subtile.tileBbox.width() / 2 + 1;
            box.height = subtile.boxHeight;
            break;
        case TileAlignment::Center:
            box.left = 0;
            box.width = subtile.tileBbox.width();
            box.height = subtile.boxHeight;
            break;
        case TileAlignment::Right:
            box.left = (subtile.tileBbox.width() - 1) / 2;
            box.width = subtile.tileBbox.width() - box.left;
            box.height = subtile.boxHeight;
            break;
        case TileAlignment::None:
            box.left = 0;
            box.width = 0;
            box.height = 0;
            break;
        }
        entry.setBox(box);
        ++count;
    });

    return count;
}

bool TileEditor::save(Gm1FileData *fileData, LogModel *log)
{
    Gm1FileData gm1 = mGm1;
    gm1.setEntryClass(GM1_ENTRY_CLASS_TILE);

    /*!
      \todo I dont like that records tile records and
      entries in the gm1filedata are not sharing the same
      rectangle. For the later it is computed by
      enumSubTiles.
    */

    // count tiles
    int tileCount = 0;
    for (size_t i = 0; i < mEntries.size(); ++i) {
        const RecordId &id = mEntries.getIdAt(i);
        const TileRecord &record = mEntries.get(id);
        tileCount += record.size() * record.size();
    }

    gm1.setEntryCount(tileCount);
    size_t entryIndex = 0;
    for (size_t i = 0; i < mEntries.size(); ++i) {
        const RecordId &id = mEntries.getIdAt(i);
        const TileRecord &record = mEntries.get(id);
        entryIndex += enumSubTiles(record, entryIndex, &gm1);
    }

    SpritePlotter plotter;
    plotter.setTransparentPixel(getDefaultTransparentPixel());

    for (const SpriteRecord &record : mSprites | boost::adaptors::map_values) {
        plotter.addImage(record.image(), record.rect(), record.mask());
    }

    QRect entriesRect;
    for (const TileRecord &record : mEntries | boost::adaptors::map_values) {
        entriesRect |= record.rect();
    }

    if (!plotter.render(QImage::Format_RGB555, entriesRect, log)) {
        log->error("error rendering final image plot");
        return false;
    }

    gm1.setPlotImage(plotter.plot());
    gm1.setPlotOrigin(plotter.origin());

    *fileData = gm1;

    return true;
}

bool TileEditor::renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result)
{
    SpritePlotter plotter;
    plotter.setTransparentPixel(getDefaultTransparentPixel());

    for (const RecordId &id : ids) {
        const auto &record = mSprites.get(id);
        plotter.addImage(record.image(), record.rect(), record.mask());
    }

    if (!plotter.render(QImage::Format_RGB555, QRect(), log)) {
        return false;
    }

    *result = plotter.plot();

    return true;
}

QRgb TileEditor::getDefaultTransparentPixel() const
{
    gm1_image_props_t props;
    gm1_get_gfx_image_props(&props);
    return props.ColorKey;
}

std::unique_ptr<AnimationFactory> TileEditor::createAnimationFactory()
{
    return nullptr;
}

} // namespace gmtool
