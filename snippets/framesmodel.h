#pragma once

#include <QAbstractItemModel>

#include "tablemodel.h"
#include "framesrecord.h"

#include <vector>

namespace gmtool {

class FramesModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    explicit FramesModel(QObject *parent = 0);
    ~FramesModel() override = default;

    void setLayoutModel(AnimationModel *layoutModel);
    void setFrameGroupModel(AnimationGroupModel *frameGroupModel);
    void setSpriteModel(SpriteModel *spriteModel);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    /// returns -1 on error
    int findIndexByFrameGroupId(const RecordId &id) const;

    FramesRecord* getRecordByIndex(int row);
    const FramesRecord* getRecordByIndex(int row) const;

private:
    void entryInserted(const QModelIndex &parent, int first, int last);
    void entryAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void entryReset();
    void entryChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void entryMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    void spriteInserted(const QModelIndex &parent, int first, int last);
    void spriteAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void spriteReset();
    void spriteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void spriteMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    void groupInserted(const QModelIndex &parent, int first, int last);
    void groupAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void groupReset();
    void groupChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void groupMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    void createImagesAndMasks(FramesRecord *record) const;

    void markRecordAsDirty(FramesRecord *record);
    void invalidateAllRecords();

    void rebuildRecordList();
    
private:
    AnimationModel *mLayoutModel = nullptr;
    AnimationGroupModel *mFrameGroupModel = nullptr;
    SpriteModel *mSpriteModel = nullptr;

    std::vector<FramesRecord *> mRecords;

    friend class FramesRecord;

private:
    FramesModel(const FramesModel &) = delete;
    FramesModel &operator=(const FramesModel &) = delete;
};

}
