#pragma once

#include <QImage>
#include <QObject>
#include <QPoint>

#include "spriteloader.h"
#include "transparencymode.h"

namespace gmtool {
class DocumentEditor;
class LogModel;

class BasicSpriteLoader : public SpriteLoader
{
public:
    explicit BasicSpriteLoader(DocumentEditor *editor,
                               TransparencyMode mode,
                               QRgb defaultTransparentPixel = 0,
                               QImage::Format format = QImage::Format_RGB555);

    void setPosition(const QPoint &position) override;

    std::vector<std::unique_ptr<AbstractRecord>> loadSprites(
        const std::vector<boost::filesystem::path> &paths, QWidget *parent) override;

    void setLogModel(LogModel *logModel) override;

private:
    QImage::Format mFormat;
    DocumentEditor *mEditor;
    QPoint mPosition;
    QRgb mTransparentPixel;
    TransparencyMode mTransparencyMode;
    LogModel *mLog = nullptr;
};

} // namespace gmtool
