#!/usr/bin/env bash

mkdir /tmp/gmtool-build
cd /tmp/gmtool-build
cmake /tmp/gmtool
make -j8
