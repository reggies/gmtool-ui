#include "replhighlighter.h"

#include <QDebug>
#include <QRegExp>
#include <QRegularExpressionMatchIterator>

#include <sstream>
#include <string>

namespace gmtool {

ReplHighlighter::ReplHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{}

void ReplHighlighter::highlightUsingFormat(const QString &text,
                                           const QRegularExpression &expr,
                                           const QTextCharFormat &format)
{
    QRegularExpressionMatchIterator i = expr.globalMatch(text);
    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();
        setFormat(match.capturedStart(), match.capturedLength(), format);
    }
    
}

void ReplHighlighter::highlightBlock(const QString &text)
{
#if 0
    QTextCharFormat stdinFormat;
    stdinFormat.setForeground(Qt::gray);

    QTextCharFormat stdoutFormat;
    stdoutFormat.setForeground(Qt::black);

    QTextCharFormat stderrFormat;
    stderrFormat.setForeground(Qt::darkRed);

    QRegularExpression stdin("^(\\.\\.|>>) .*");
    QRegularExpression stdout("^stdout:.*");
    QRegularExpression stderr("^stderr:.*");

    highlightUsingFormat(text, stderr, stderrFormat);
    highlightUsingFormat(text, stdout, stdoutFormat);
    highlightUsingFormat(text, stdin, stdinFormat);
#endif
}

}
