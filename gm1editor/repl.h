#pragma once

#include <QObject>

#include <boost/python.hpp>
#include <boost/python/exec.hpp>
#include <frameobject.h>

namespace gmtool {
class Document;

namespace bp = boost::python;

// a. ReplEnvironment
// b. PythonCommunicationPipe
// c. ReplWindowController
// d. PythonWindowContext

/// This class holds a local namespace for interpreter
class Repl : public QObject {
    Q_OBJECT
public:
    explicit Repl(Document *document, QObject *parent = nullptr);
    ~Repl() override;

    bool loadScript(const std::string &scriptName);
    bool grabStdout(std::string &output);
    bool grabStderr(std::string &error);
    bool execute(const std::string &commands);

signals:
    void outputStderr(const std::string &output);
    void outputStdout(const std::string &output);
    void systemExitRaised();

private:
    bool createWorld();
    bool redirectStdHandles(bp::dict &globals);
    bool evaluate(const std::string &commands);
    bool getSystemExitExceptionObject(bp::object &systemExit);

private:
    bp::dict mLocals;
    Document *mDoc;
};


}
