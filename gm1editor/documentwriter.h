#pragma once

#include <boost/filesystem/path.hpp>

#include "gm1filedata.h"
#include "logmodel.h"
#include "operation.h"

namespace gmtool {

class DocumentWriter : public Operation
{
    Q_OBJECT
public:
    explicit DocumentWriter(QObject *parent = 0);

    void setFilePath(const boost::filesystem::path &filePath);

    LogModel *logs() const;

    bool success() const;

    boost::filesystem::path filePath() const;

    void setFileData(const Gm1FileData &fileData);

    QString status() const override;

public slots:
    void start() override;

private:
    Gm1FileData mGm1;
    boost::filesystem::path mFilePath;
    LogModel *mLogModel;
    bool mSuccess = false;

    DocumentWriter(const DocumentWriter &) = delete;
    DocumentWriter &operator=(const DocumentWriter &) = delete;
};

} // namespace gmtool
