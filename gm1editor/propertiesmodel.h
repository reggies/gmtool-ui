#pragma once

#include <QAbstractTableModel>

#include <vector>

#include "propertyitem.h"

namespace gmtool {

class PropertiesModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit PropertiesModel(QObject *parent = 0);
    ~PropertiesModel() override;

    void reset();

    template<class... U>
    PropertyItem *addPropertyItem(U &&... args)
    {
        auto prop = new PropertyItem(std::forward<U>(args)...);
        mItems.push_back(prop);
        return prop;
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant headerData(int section,
                        Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

private:
    // We want an array of pointers since we want some sort of
    // persistency of these objects outside of the model.
    std::vector<PropertyItem *> mItems;
};

} // namespace gmtool
