/*
  Copyright (c) 2016 Alexey Natalin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef GM1_H_
#define GM1_H_

#include "gm1_conf.h"

#ifdef __cplusplus
extern "C" {
#endif

/*! The number of palettes in each gm1_header */
#define GM1_NUM_PALETTES 10

/* The number of colors in each palette */
#define GM1_NUM_COLORS 256

/*! RGB tripplet. */
typedef struct gm1_rgb_s
{
    gm1_u8_t Red;
    gm1_u8_t Green;
    gm1_u8_t Blue;
} gm1_rgb_t;

/*! Converts specified gm1_rgb_t struct to the 16-bit pixel. */
extern void gm1_pack_rgb(const gm1_rgb_t* rgb, gm1_u16_t* pixel);

/*! Converts 16-bit pixel to the gm1_rgb_t structure. */
extern void gm1_unpack_rgb(gm1_u16_t pixel, gm1_rgb_t* rgb);

/*! Header of the GM1 file. */
typedef struct gm1_header_s
{
    gm1_u32_t Unused1;
    gm1_u32_t Unused2;
    gm1_u32_t Unused3;
    gm1_u32_t EntryCount;
    gm1_u32_t Unused4;
    /*! Enumeration. \sa gm1_entry_class_t */
    gm1_u32_t EntryClass;
    gm1_u32_t Unused5;
    gm1_u32_t Unused6;
    /*! Enumeration. \sa gm1_size_category_t */
    gm1_u32_t Category;
    gm1_u32_t Unused7;
    gm1_u32_t Unused8;
    gm1_u32_t Unused9;
    gm1_u32_t Width;
    gm1_u32_t Height;
    gm1_u32_t Unused10;
    gm1_u32_t Unused11;
    gm1_u32_t Unused12;
    gm1_u32_t Unused13;
    /*! Sprite horizontal center. Only for animation. */
    gm1_u32_t AnchorX;
    /*! Sprite vertical center. Only for animation. */
    gm1_u32_t AnchorY;
    gm1_u32_t DataSize;
    gm1_u32_t Unused14;
    /*! Color sets. Only for animation. */
    gm1_rgb_t ColorTable[GM1_NUM_PALETTES][GM1_NUM_COLORS];
} gm1_header_t;

/*! Header of an each entry in the GM1 file. */
typedef struct gm1_entry_header_s
{
    gm1_u16_t Width;
    gm1_u16_t Height;
    gm1_u16_t PosX;
    gm1_u16_t PosY;
    gm1_u8_t PartId;
    gm1_u8_t PartSize;
    gm1_u16_t TileOffset;
    /*! Enumeration. \sa gm1_alignment_t */
    gm1_u8_t Alignment;
    gm1_u8_t BoxOffset;
    gm1_u8_t BoxWidth;
    /*! wtf */
    gm1_u8_t Flags;
} gm1_entry_header_t;

/*! Determines everything about file contents. */
typedef enum gm1_entry_class
{
    /*!
      RGB 5 bit per channel, 2 bytes per pixel images.

      This class allows transparency.

      anim_buildings2.gm1
      anim_castle.gm1
      anim_goods.gm1
      anim_tunnelors_guild.gm1
      cracks.gm1
      enemy_faces.gm1
      face800-blank.gm1
      floats.gm1
      icons_placeholders.gm1
      interface_army.gm1
      interface_buttons.gm1
      interface_icons2.gm1
      interface_icons3.gm1
      interface_ruins.gm1
      interface_slider_bar.gm1
      mapedit_buttons.gm1
      oil_dropped.gm1
      rock_chips.gm1
    */
    GM1_ENTRY_CLASS_TGX16 = 1,

    /*!
      Indexed RGB 256 colors, 1 byte per pixel images.

      This class also allows transparency.

      gm1_header_t: ColorTable contains color sets for the images.

      gm1_header_t: AnchorX and AnchorY represent an offset of the image center.

      Tree_Chestnut.gm1
      anim_armourer.gm1
      anim_baker.gm1
      anim_blacksmith.gm1
      anim_boiled_oil.gm1
      anim_brewer.gm1
      anim_chopping_block.gm1
      anim_dungeon.gm1
      anim_farmer.gm1
      anim_flag_small.gm1
      anim_flags.gm1
      anim_fletcher.gm1
      anim_healer.gm1
      anim_hunter.gm1
      anim_inn.gm1
      anim_iron_miner.gm1
      anim_market.gm1
      anim_maypole.gm1
      anim_pitch_dugout.gm1
      anim_poleturner.gm1
      anim_quarry.gm1
      anim_shields.gm1
      anim_stables.gm1
      anim_stake.gm1
      anim_tanner.gm1
      anim_tunnels.gm1
      anim_windmill.gm1
      anim_woodcutter.gm1
      body_animal_burning_big.gm1
      body_animal_burning_small.gm1
      body_archer.gm1
      body_armourer.gm1
      body_baker.gm1
      body_ballista.gm1
      body_battering_ram.gm1
      body_bear.gm1
      body_blacksmith.gm1
      body_boy.gm1
      body_brewer.gm1
      body_catapult.gm1
      body_chicken.gm1
      body_chicken_brown.gm1
      body_cow.gm1
      body_crossbowman.gm1
      body_crow.gm1
      body_deer.gm1
      body_dog.gm1
      body_drunkard.gm1
      body_farmer.gm1
      body_fighting_monk.gm1
      body_fire_eater.gm1
      body_fireman.gm1
      body_fletcher.gm1
      body_ghost.gm1
      body_girl.gm1
      body_healer.gm1
      body_horse_trader.gm1
      body_hunter.gm1
      body_innkeeper.gm1
      body_iron_miner.gm1
      body_jester.gm1
      body_juggler.gm1
      body_knight.gm1
      body_knight_top.gm1
      body_ladder_bearer.gm1
      body_lady.gm1
      body_lord.gm1
      body_maceman.gm1
      body_man_burning.gm1
      body_mangonel.gm1
      body_miller.gm1
      body_missile.gm1
      body_missile_2.gm1
      body_missile_cow.gm1
      body_missile_fire.gm1
      body_mother.gm1
      body_ox.gm1
      body_peasant.gm1
      body_pikeman.gm1
      body_pitch_worker.gm1
      body_poleturner.gm1
      body_priest.gm1
      body_rabbit.gm1
      body_seagull.gm1
      body_shield.gm1
      body_siege_engineer.gm1
      body_siege_tower.gm1
      body_spearman.gm1
      body_splash.gm1
      body_stonemason.gm1
      body_swordsman.gm1
      body_tanner.gm1
      body_tent.gm1
      body_trader.gm1
      body_trebutchet.gm1
      body_tunnelor.gm1
      body_wolf.gm1
      body_woodcutter.gm1
      tree_apple.gm1
      tree_birch.gm1
      tree_oak.gm1
      tree_pine.gm1
      tree_shrub1.gm1
      tree_shrub2.gm1
    */
    GM1_ENTRY_CLASS_TGX8,

    /*!
      RGB 5 bit per channel, 2 bytes per pixel.

      Allows transparency.

      killing_pits.gm1
      pitch_ditches.gm1
      tile_buildings1.gm1
      tile_buildings2.gm1
      tile_burnt.gm1
      tile_castle.gm1
      tile_churches.gm1
      tile_data.gm1
      tile_farmland.gm1
      tile_flatties.gm1
      tile_goods.gm1
      tile_land3.gm1
      tile_land8.gm1
      tile_land_and_stones.gm1
      tile_land_macros.gm1
      tile_rocks8.gm1
      tile_ruins.gm1
      tile_sea8.gm1
      tile_sea_new_01.gm1
      tile_workshops.gm1
    */
    GM1_ENTRY_CLASS_TILE,

    /*!
      RGB 5 bit per channel, 2 bytes per pixel.

      Allows transparency.

      Alpha channel is stored in green channel.

      gm1_entry_header_t: TileOffset represents bearing of a glyph.

      font_slanted.gm1
      font_stronghold.gm1
      font_stronghold_aa.gm1
    */
    GM1_ENTRY_CLASS_GLYPH,

    /*!
      RGB 5 bit per channel, 2 bytes per pixel.

      No transparency.

      tile_chevrons.gm1
      tile_cliffs.gm1
      tile_rocks_chevrons.gm1
      tile_walls.gm1
    */
    GM1_ENTRY_CLASS_BITMAP,

    /*!
      RGB 5 bit per channel, 2 bytes per pixel.

      Allows transparency.

      anim_campaign_map_flags.gm1
      anim_dancing_bear.gm1
      anim_dog_cage.gm1
      anim_drawbridge.gm1
      anim_ducking_stool.gm1
      anim_gallows.gm1
      anim_gibbet.gm1
      anim_heads.gm1
      anim_killing_pits.gm1
      anim_rack.gm1
      anim_stocks.gm1
      anim_whitecaps.gm1
      army_units.gm1
      blast3.gm1
      body_brazier.gm1
      body_disease.gm1
      body_fire.gm1
      body_fire2.gm1
      body_gate.gm1
      body_steam.gm1
      cursors.gm1
      float_pop_circ.gm1
      floats_new.gm1
      icons_front_end.gm1
      icons_front_end_builder.gm1
      icons_front_end_combat.gm1
      icons_front_end_economics.gm1
      mini_cursors.gm1
      scribe.gm1
      smoke-30x30.gm1
      puff of smoke.gm1
    */
    GM1_ENTRY_CLASS_TGX16CONSTSIZE,

    /*!
      RGB 5 bit per channel, 2 bytes per pixel.

      No transparency.

      tile_rivers.gm1
    */
    GM1_ENTRY_CLASS_ANOTHERBITMAP,

} gm1_entry_class_t;

/*! Wtf */
typedef enum gm1_alignment
{
    GM1_ALIGN_NONE,
    GM1_ALIGN_CENTER,
    GM1_ALIGN_LEFT,
    GM1_ALIGN_RIGHT
} gm1_alignment_t;

/*! Category of size of an image.

  Has no particular use.
*/
typedef enum gm1_size_category
{
    GM1_SIZE_UNUSED0,
    GM1_SIZE_30,
    GM1_SIZE_55,
    GM1_SIZE_75,
    GM1_SIZE_UNUSED1,
    GM1_SIZE_100,
    GM1_SIZE_110,
    GM1_SIZE_130,
    GM1_SIZE_UNUSED2,
    GM1_SIZE_185,
    GM1_SIZE_250,
    GM1_SIZE_180,
} gm1_size_category_t;

/*! Possible values for gm1_status_t */
typedef enum gm1_error_codes
{
    /*! No error */
    GM1_SUCCESS,

    /*! Unknown error */
    GM1_FAILURE,

    /*! Read or write error */
    GM1_EIO,

    /*! One or more parameters are invalid */
    GM1_EPARAM,

    /*! Invalid entry class */
    GM1_EENTRYCLASS,

    /*! Unexpected end of file */
    GM1_EUNDERFLOW,

    /*! Internal error */
    GM1_EOVERFLOW,

    /*! Bad TGX data in file */
    GM1_ETGXCHUNK,

    /*! Bad TGX data in file */
    GM1_ETGXCHUNKLEN,
} gm1_error_codes_t;

/*! Returns 1 if there was an error indicated by the \a status. */
extern int gm1_error(gm1_status_t status);

/*! Converts status to an error string.

  \sa gm1_error_codes_t
*/
extern const char* gm1_errorstr(gm1_status_t status);

/*! Stores description of an entry. */
typedef struct gm1_entry_s
{
    /*! Size of the data in bytes. */
    gm1_u32_t Size;
    /*! Offset from the first byte of the first entry. */
    gm1_u32_t Offset;
    /*! Header of the image. */
    gm1_entry_header_t Header;
} gm1_entry_t;

/*! Descriptive characteristics of the image. */
typedef struct gm1_image_props_s
{
    gm1_u32_t BytesPerPixel;
    gm1_u8_t bHasColorKey;
    gm1_u16_t ColorKey;
} gm1_image_props_t;

/*! Extract properties of the image.
  \param entry_class GM1 entry class.
  \param entry_header Header of the entry.
  \param props Properties of the image.
  \sa gm1_image_props_t
*/
extern gm1_status_t gm1_get_image_props(gm1_u32_t entry_class, gm1_image_props_t* props);

/*! Image representation. */
typedef struct gm1_image_s
{
    gm1_u8_t* Pixels;
    gm1_u32_t Width;
    gm1_u32_t Height;
    gm1_u32_t BytesPerPixel;
    gm1_u32_t BytesPerRow;
    gm1_u8_t* ColorKey;
} gm1_image_t;

typedef GM1_CALLBACK(gm1_size_t, *gm1_istream_rd_fn, (void*, gm1_u8_t*, gm1_size_t));
typedef GM1_CALLBACK(gm1_size_t, *gm1_ostream_wr_fn, (void*, const gm1_u8_t*, gm1_size_t));

/*! Read data callback. */
typedef struct gm1_istream_s
{
    void* UserData;
    gm1_istream_rd_fn Read;
} gm1_istream_t;

/*! Write data callback. */
typedef struct gm1_ostream_s
{
    void* UserData;
    gm1_ostream_wr_fn Write;
} gm1_ostream_t;

/*! Write header.

  Table of the common fields.
  ~~~
  |--------------|----------------------|---------------------------------------|
  |Header field  |Field type            |Description                            |
  |--------------|----------------------|---------------------------------------|
  |EntryCount    |gm1_u32_t             |The number of entries in the archive.  |
  |EntryClass    |gm1_entry_class_t     |Class of images.                       |
  |Category      |gm1_size_category_t   |Category of size.                      |
  |Width         |gm1_u32_t             |Unused at the moment.                  |
  |Height        |gm1_u32_t             |Unused at the moment.                  |
  |DataSize      |gm1_u32_t             |Total size of compressed data.         |
  |--------------|----------------------|---------------------------------------|
  ~~~

  Table of fields for GM1_ENTRY_CLASS_TGX8.
  ~~~
  |--------------|----------------------|---------------------------------------|
  |Header field  |Field type            |Description                            |
  |--------------|----------------------|---------------------------------------|
  |AnchorX       |gm1_u32_t             |Central point of the sprite.           |
  |AnchorY       |gm1_u32_t             |Central point of the sprite.           |
  |ColorTable    |gm1_rgb_t[][]         |An animation palette set.              |
  |--------------|----------------------|---------------------------------------|
  ~~~

  \param ostream Write callback.
  \param header Header to write.

  \note All unset fields should be 0.

  \sa gm1_header_t gm1_ostream_t
*/
extern gm1_status_t gm1_write_header(gm1_ostream_t* ostream, const gm1_header_t* header);

/*! Parse header.

  Result header is suitable for gm1_write_header as \a Header argument.

  \param istream Read callback.
  \param header Header to read.

  \sa gm1_write_header gm1_header_t
*/
extern gm1_status_t gm1_read_header(gm1_istream_t* istream, gm1_header_t* header);

/*! Write entries to the specified destination.

  \param ostream Write callback.
  \param entries Array of entries.
  \param entry_class GM1 Entry class.g
  \param entry_count Number of entries.

  \note \a Size field of gm1_entry_t should be valid.
  \note \a Offset field of gm1_entry_t should be valid.

  Table of the common fields.
  ~~~
  |--------------|-------------------|------------------------------------------|
  |Header field  |Field type         |Description                               |
  |--------------|-------------------|------------------------------------------|
  |Width         |gm1_u16_t          |Width of an image.                        |
  |Height        |gm1_u16_t          |Height of an image.                       |
  |PosX          |gm1_u16_t          |Horizontal position on the editor plane.  |
  |PosY          |gm1_u16_t          |Vertical position on the editor plane     |
  |Flags         |gm1_u8_t           |Unused. Should be 0.                      |
  |--------------|-------------------|------------------------------------------|
  ~~~

  Table of GM1_ENTRY_CLASS_TGX8 fields.
  ~~~
  |--------------|-------------------|------------------------------------------|
  |Header field  |Field type         |Description                               |
  |--------------|-------------------|------------------------------------------|
  |PartId        |gm1_u8_t           |A number of tile in tile group.           |
  |PartSize      |gm1_u8_t           |The number of tiles in tile group.        |
  |TileOffset    |gm1_u16_t          |An offset from the topmost box pixel.     |
  |Alignment     |gm1_alignment_t    |An alignment of the tile.                 |
  |BoxOffset     |gm1_u8_t           |An offset from the leftmost box pixel.    |
  |BoxWidth      |gm1_u8_t           |Width of the box.                         |
  |--------------|-------------------|------------------------------------------|
  ~~~

  Table of GM1_ENTRY_CLASS_GLYPH fields.
  ~~~
  |--------------|-------------------|------------------------------------------|
  |Header field  |Field type         |Description                               |
  |--------------|-------------------|------------------------------------------|
  |TileOffset    |gm1_u16_t          |Base line.                                |
  |--------------|-------------------|------------------------------------------|
  ~~~

  Table of GM1_ENTRY_CLASS_BITMAP and GM1_ENTRY_CLASS_ANTOHER_BITMAP fields.
  ~~~
  |--------------|-------------------|------------------------------------------|
  |Header field  |Field type         |Description                               |
  |--------------|-------------------|------------------------------------------|
  |Height        |gm1_u16_t          |Height of the image plus 7.               |
  |--------------|-------------------|------------------------------------------|
  ~~~

  \sa gm1_write_tile gm1_read_tile gm1_entry_t
*/
extern gm1_status_t gm1_write_entries(gm1_ostream_t* ostream, gm1_u32_t entry_class, const gm1_entry_t* entries, gm1_size_t entry_count);

/*! Read all entries headers.

  \param istream Read callback.
  \param entries Storage for the entry list.
  \param entry_class GM1 Entry class.
  \param entry_count Size of the entry list.

  \note \a entry_count should be the same as gm1_header_t.

  \note You should call this function to read all entries at once.
 */
extern gm1_status_t gm1_read_entries(gm1_istream_t* istream, gm1_u32_t entry_class, gm1_entry_t* entries, gm1_size_t entry_count);

/*! Helper function that call appropriate reader for \a EntryClass.
  \param istream Read callback.
  \param read_max Size of the data in bytes. You may want to use Size field of the gm1_entry_t.
  \param entry_class Entry class of the header.
  \param header Entry header.
  \param image Image to read.
*/
extern gm1_status_t gm1_read_image(gm1_istream_t* istream,
                                   gm1_size_t read_max,
                                   gm1_u32_t entry_class,
                                   const gm1_entry_header_t* header,
                                   gm1_image_t* image);

/*! Helper function that call appropriate writer for \a entry_class.
  \param image Image to write.
  \param entry_class GM1 Entry class.
  \param header Header to write.
  \param ostream Write callback.
 */
extern gm1_status_t gm1_write_image(const gm1_image_t* image,
                                    gm1_u32_t entry_class,
                                    const gm1_entry_header_t* header,
                                    gm1_ostream_t* ostream);

/*! Decode image with transparency.
  \param istream Read callback.
  \param read_max Size of the data in bytes. You may want to use Size field of the gm1_entry_t.
  \param image Image to read.g

  \note Suitable for GM1_ENTRY_CLASS_TGX16, GM1_ENTRY_CLASS_TGX8 and GM1_ENTRY_CLASS_TGX16CONSTSIZE.

  \note Also suitable for .TGX files of the gfx/ directory of the game.

  \sa gm1_write_tgx
*/
extern gm1_status_t gm1_read_tgx(gm1_istream_t* istream, gm1_size_t read_max, gm1_image_t* image);

/*! Encode image with transparent pixels.
  \param image Image to write.
  \param ostream Write callback.
  \sa gm1_read_tgx
*/
extern gm1_status_t gm1_write_tgx(const gm1_image_t* image, gm1_ostream_t* ostream);

/*! Decode glyph.

  \note Suitable for GM1_ENTRY_CLASS_GLYPH.
  \param istream Read callback.
  \param read_max Size of the data in bytes.
  \param image Image to read.
  \sa gm1_write_glyph
*/
extern gm1_status_t gm1_read_glyph(gm1_istream_t* istream, gm1_size_t read_max, gm1_image_t* image);

/*! Encode glyph. Suitable for GM1_ENTRY_CLASS_GLYPH.

  \param image Image to write.
  \param ostream Write callback.
  \sa gm1_read_glyph
*/
extern gm1_status_t gm1_write_glyph(const gm1_image_t* image, gm1_ostream_t* ostream);

/*! Encode tile.

  \note This function intended to use with GM1_ENTRY_CLASS_TILE.

  The explanation of the algorithm is given in gm1_write_tile.

  \param istream Read callback.
  \param read_max Maximum number of bytes to read.

  \sa gm1_write_tile
*/
extern gm1_status_t gm1_read_tile(gm1_istream_t* istream,
                                  gm1_size_t read_max,
                                  gm1_u32_t box_offset,
                                  gm1_u32_t box_width,
                                  gm1_u32_t tile_offset,
                                  gm1_image_t* image);

/*! Decode image with tile.
  \param image Input image.
  \param box_offset Column of the leftmost pixel of the box.
  \param box_width Number of columns in the box.
  \param tile_offset Row of the topmost pixel of the tile.
  \param ostream Write callback.

  The encoding algorithm splits the image on a box part and on a tile part.

  The box part is the following rect area within \a image:

  Top: 0

  Left: \a box_offset

  Width: \a box_width

  Height: \a tile_offset + TILE_HEIGHT / 2

  The tile part is the rhombus of 30x16 pixels at the bottom of the \a image.

  Top: \a tile_offset

  Left: 0

  Width: 30

  Height: 16

  \note \a box_offset + \a box_width <= image->Width.
  \note \a tile_offset + TILE_HEIGHT <= image->Height

  \sa gm1_read_tile
*/
extern gm1_status_t gm1_write_tile(const gm1_image_t* image,
                                   gm1_u32_t box_offset,
                                   gm1_u32_t box_width,
                                   gm1_u32_t tile_offset,
                                   gm1_ostream_t* ostream);

/*! Copy pixels from the specified stream.

  This function should be used with GM1_ENTRY_CLASS_BITMAP or GM1_ENTRY_CLASS_ANOTHERBITMAP.

  \sa gm1_write_bitmap
*/
extern gm1_status_t gm1_read_bitmap(gm1_istream_t* istream, gm1_image_t* image);

/*! Copies pixels to the specified stream.
  \sa gm1_read_bitmap
*/
extern gm1_status_t gm1_write_bitmap(const gm1_image_t* image, gm1_ostream_t* ostream);

/*! Simple header for the tgx files. */
typedef struct gm1_gfx_header
{
    gm1_u32_t Width;
    gm1_u32_t Height;
} gm1_gfx_header_t;

/*! Reads header of TGX file. */
extern gm1_status_t gm1_read_gfx_header(gm1_istream_t* istream, gm1_gfx_header_t* gfx_header);

/*! Writes header of TGX file. */
extern gm1_status_t gm1_write_gfx_header(gm1_ostream_t* ostream, const gm1_gfx_header_t* gfx_header);

/*! Get image properties of the tgx file.
  Call this function to get width, height and depth of the image.
  \return Properties of the GM1_ENTRY_CLASS_TGX16 entry class.
*/
extern void gm1_get_gfx_image_props(gm1_image_props_t* props);

/*! Reads TGX image. */
extern gm1_status_t gm1_read_gfx_image(gm1_istream_t* istream, gm1_image_t* image);

/*! Writes TGX image. */
extern gm1_status_t gm1_write_gfx_image(gm1_ostream_t* ostream, const gm1_image_t* image);

#ifdef __cplusplus
}
#endif

#endif // GM1_H_
