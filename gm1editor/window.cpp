#include "window.h"

#include <QApplication>
#include <QDebug>
#include <QEvent>

#include "document.h"

namespace gmtool {

Window::Window(QWidget *parent)
    : QWidget(parent)
{
    setFocusPolicy(Qt::StrongFocus);
}

Window::~Window()
{}

bool Window::focusEntry(TableId table, const RecordId &id)
{
    Q_UNUSED(table);
    Q_UNUSED(id);
    return false;
}

void Window::closeEvent(QCloseEvent *event)
{
    emit closed();
    event->accept();
}

} // namespace gmtool
