#include "gm1loader.h"

#include <QDebug>

#include <algorithm>

#include <boost/filesystem/fstream.hpp>

namespace gmtool {

const static int kGm1TileHeight = 16;
const static int kGm1TileWidth = 30;

namespace fs = boost::filesystem;

static inline bool is_error(gm1_status_t status)
{
    return gm1_error(status) != 0;
}

Gm1Loader::Gm1Loader(QObject *parent)
    : QObject(parent)
{}

static void swapGreenAndAlpha32(QImage *plot)
{
    for (int y = 0; y < plot->height(); ++y) {
        uint32_t *src = (uint32_t *) plot->scanLine(y);
        for (int x = 0; x < plot->width(); ++x, src++) {
            *src = (0xff00ff & *src) | ((0xff00 & *src) << 16) | ((0xff000000 & *src) >> 16);
        }
    }
}

static gm1_size_t readFromGm1(void *userdata, gm1_u8_t *buffer, gm1_size_t count)
{
    std::istream *is = static_cast<std::istream *>(userdata);
    is->read(reinterpret_cast<char *>(buffer), count);
    return static_cast<gm1_size_t>(is->gcount());
}

static void getPlotBounds(const Gm1FileEntry *entries, gm1_size_t num_entries, QRect *bounds)
{
    QRect tmp;
    for (const Gm1FileEntry *entry = entries; entry != entries + num_entries; ++entry) {
        gm1_entry_header_t header = entry->Gm1.Header;
        //! \todo check for overflow
        QRect rect(header.PosX, header.PosY, header.Width, header.Height);
        tmp |= rect;
    }
    *bounds = tmp;
}

bool Gm1Loader::initPlot()
{
    //! estimate image size
    QRect bounds;
    getPlotBounds(mFileData.Entries.data(), mFileData.Entries.size(), &bounds);
    QImage::Format format = QImage::Format_RGB555;
    if (mFileData.Header.EntryClass == GM1_ENTRY_CLASS_TGX8)
        format = QImage::Format_Indexed8;
    mFileData.Origin = bounds.topLeft();
    mFileData.Plot = QImage(bounds.width(), bounds.height(), format);
    if (mFileData.Plot.isNull()) {
        emitError("not enough memory");
        return false;
    }
    if (mFileData.Properties.bHasColorKey == 1) {
        mFileData.Plot.fill(mFileData.Properties.ColorKey);
    }
    return true;
}

bool Gm1Loader::readEntries(gm1_istream_t *is)
{
    gm1_status_t status;
    std::vector<gm1_entry_t> entries(mFileData.Header.EntryCount);
    status = gm1_read_entries(is, mFileData.Header.EntryClass, entries.data(), entries.size());
    mFileData.Entries.resize(mFileData.Header.EntryCount);
    for (size_t i = 0; i < mFileData.Entries.size(); ++i) {
        const auto &header = entries.at(i).Header;
        mFileData.Entries.at(i).Gm1 = entries.at(i);
        mFileData.Entries.at(i).Rect = QRect(header.PosX, header.PosY, header.Width, header.Height);
    }
    if (is_error(status)) {
        emitError("could not read entries; error code: %s (%02x)",
                  gm1_errorstr(status),
                  static_cast<int>(status));
        return false;
    }
    if (mFileData.Header.EntryClass == GM1_ENTRY_CLASS_TILE) {
        /*! check for broken Stronghold Image Toolbox crap before going even
         * deeper */
        for (size_t i = 0; i < mFileData.Entries.size(); ++i) {
            auto &header = mFileData.Entries[i].Gm1.Header;
            if (header.Height < header.TileOffset + kGm1TileHeight) {
                /*! fix the crap */
                emitWarning("entry has incorrect height (Crappy Image Toolbox?); "
                            "index: %zu, x: %d, y: %d, "
                            "bad height: %d, fixed height: %d",
                            i,
                            header.PosX,
                            header.PosY,
                            header.Height,
                            header.TileOffset + kGm1TileHeight);
                header.Height = header.TileOffset + kGm1TileHeight;
            }
        }
    }
    return true;
}

bool Gm1Loader::readHeader(gm1_istream_t *is)
{
    gm1_status_t status = gm1_read_header(is, &mFileData.Header);
    if (is_error(status)) {
        emitError("could not read header; error code: %d", errno);
        return false;
    }

    status = gm1_get_image_props(mFileData.Header.EntryClass, &mFileData.Properties);
    if (is_error(status)) {
        emitError("invalid file header; entry class: %02x, error code: %s (%02x)",
                  mFileData.Header.EntryClass,
                  gm1_errorstr(status),
                  static_cast<int>(status));
        return false;
    }

    return true;
}

bool Gm1Loader::readEntry(gm1_istream_t *is, size_t index)
{
    gm1_entry_t &entry = mFileData.Entries.at(index).Gm1;

    /*!
      a few sanity checks. a violation of any of those
      is a clear sign of busted initPlot.
    */
    Q_ASSERT(entry.Header.PosX >= mFileData.Origin.x());
    Q_ASSERT(entry.Header.PosY >= mFileData.Origin.y());
    Q_ASSERT(entry.Header.PosX - mFileData.Origin.x() < mFileData.Plot.width());
    Q_ASSERT(entry.Header.PosY - mFileData.Origin.y() < mFileData.Plot.height());

    QImage slice(mFileData.Plot.scanLine(entry.Header.PosY - mFileData.Origin.y())
                     + (entry.Header.PosX - mFileData.Origin.x()) * mFileData.Plot.depth() / 8,
                 entry.Header.Width,
                 entry.Header.Height,
                 mFileData.Plot.bytesPerLine(),
                 mFileData.Plot.format());

    gm1_image_t image = {};
    image.Pixels = slice.bits();
    image.Width = slice.width();
    image.Height = slice.height();
    image.BytesPerPixel = slice.depth() / 8;
    image.BytesPerRow = slice.bytesPerLine();

    gm1_status_t status = gm1_read_image(is,
                                         entry.Size,
                                         mFileData.Header.EntryClass,
                                         &entry.Header,
                                         &image);
    if (is_error(status)) {
        emitError("error while reading entry %d; error code: %s (%02x)",
                  index,
                  gm1_errorstr(status),
                  static_cast<int>(status));
        return false;
    }

    return true;
}

bool Gm1Loader::loadFromFile(const boost::filesystem::path &filepath)
{
    //! setup input stream
    fs::ifstream fin(filepath, std::ios::binary | std::ios::in);
    if (!fin) {
        emitError("could not open file; errno: %d", errno);
        return false;
    }

    gm1_istream_t inputStream = {};
    inputStream.UserData = &fin;
    inputStream.Read = readFromGm1;

    if (!readHeader(&inputStream)) {
        return false;
    }

    if (!readEntries(&inputStream)) {
        return false;
    }

    if (!initPlot()) {
        return false;
    }

    emit progress(0, mFileData.Entries.size());

    std::streamoff dataStart = fin.tellg();
    for (size_t i = 0; i < mFileData.Entries.size(); ++i) {
        emit progress(i + 1, mFileData.Entries.size());
        fin.seekg(dataStart + static_cast<std::streamoff>(mFileData.Entries.at(i).Gm1.Offset),
                  std::ios::beg);
        if (!readEntry(&inputStream, i)) {
            return false;
        }
    }

    if (mFileData.Header.EntryClass == GM1_ENTRY_CLASS_GLYPH) {
        /*!
          when handling fonts convert plot to the 32-bit color image and
          set its alpha channel properly.
        */
        mFileData.Plot = mFileData.Plot.convertToFormat(QImage::Format_ARGB32);
        if (mFileData.Plot.isNull()) {
            emitError("not enough memory");
            return false;
        }
        swapGreenAndAlpha32(&mFileData.Plot);
    }

    //! decode color table
    for (gm1_size_t i = 0; i < Gm1FileData::MaxPaletteCount; ++i) {
        const gm1_rgb_t *palette = mFileData.Header.ColorTable[i];
        std::transform(palette,
                       palette + Gm1FileData::MaxPaletteSize,
                       mFileData.Palettes.at(i).begin(),
                       [](const gm1_rgb_t &rgb) { return QColor(rgb.Red, rgb.Green, rgb.Blue); });
    }

    return true;
}

bool Gm1Loader::isNull() const
{
    return mFileData.Plot.isNull();
}

Gm1FileData Gm1Loader::fileData() const
{
    return mFileData;
}

void Gm1Loader::emitError(const char *format, ...)
{
    va_list va;
    va_start(va, format);
    auto message = QString::vasprintf(format, va);
    va_end(va);
    emit diagnosticsError(message);
}

void Gm1Loader::emitWarning(const char *format, ...)
{
    va_list va;
    va_start(va, format);
    auto message = QString::vasprintf(format, va);
    va_end(va);
    emit diagnosticsWarning(message);
}

} // namespace gmtool
