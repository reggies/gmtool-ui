#include "severityfiltermodel.h"

#include "logmodel.h"

namespace gmtool {

SeverityFilterModel::SeverityFilterModel(const QFlags<Severity> &severity, QObject *parent)
    : QSortFilterProxyModel(parent)
    , mSeverity(severity)
{}

SeverityFilterModel::~SeverityFilterModel() = default;

bool SeverityFilterModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QVariant data = sourceModel()->data(sourceModel()->index(sourceRow, 0, sourceParent),
                                        static_cast<int>(LogModel::SeverityRole));
    if (!data.isValid()) {
        return false;
    }

    return mSeverity.testFlag(data.value<Severity>());
}

} // namespace gmtool
