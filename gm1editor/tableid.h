#pragma once

#include <QMetaType>
#include <QObject>
#include <QString>

namespace gmtool {

enum class TableId {
    Invalid, Layout, Palette, Spritesheet, Animations,
    Gm1_Tile,
    Gm1_Glyph,
    Gm1_Animation,
    Gm1_Bitmap,
};

inline QString getTableElementName(TableId tableId)
{
    switch (tableId) {
    case TableId::Invalid:
        return QString();
    case TableId::Layout:
        return QObject::tr("Item");
    case TableId::Palette:
        return QObject::tr("Palette");
    case TableId::Spritesheet:
        return QObject::tr("Sprite");
    case TableId::Animations:
        return QObject::tr("Animation");
    case TableId::Gm1_Tile:
        return QObject::tr("Gm1Tile");
    case TableId::Gm1_Glyph:
        return QObject::tr("Gm1Glyph");
    case TableId::Gm1_Animation:
        return QObject::tr("Gm1Animation");
    case TableId::Gm1_Bitmap:
        return QObject::tr("Gm1Bitmap");
    }
    return QString();
}

} // namespace gmtool

Q_DECLARE_METATYPE(gmtool::TableId)
