#include "engine.h"

#include <QApplication>
#include <QDebug>
#include <QTimer>

#include <iostream>
#include <vector>

#include "createdocumentdialog.h"
#include "document.h"
#include "documentmanager.h"
#include "documenttype.h"
#include "editorfactory.h"
#include "frame.h"
#include "framemanager.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

namespace po = boost::program_options;

struct path_with_spaces
{
    path_with_spaces() = default;
    explicit path_with_spaces(const boost::filesystem::path &path)
        : path(path)
    {}
    boost::filesystem::path path;
};

std::wistream &operator>>(std::wistream &is, path_with_spaces &path)
{
    std::wstring str;
    std::getline(is, str);
    path = path_with_spaces(str);
    return is;
}

namespace gmtool {

Engine::Engine()
{
    mFrameMgr = new FrameManager(this);
    mDocMgr = new DocumentManager(this);

    QIcon::setThemeName("gm1editor");

    connect(mFrameMgr, &FrameManager::newDocumentRequested, this, &Engine::newDocumentRequest);
    connect(mFrameMgr, &FrameManager::openDocumentRequested, this, &Engine::openDocumentRequest);
    connect(mFrameMgr, &FrameManager::closeDocumentRequested, this, &Engine::closeDocumentRequest);
}

int Engine::exec()
{
    bool showHelp = false;
    std::vector<path_with_spaces> files;

    po::options_description options("Default");
    // clang-format off
    options.add_options()
        ("help,h", po::bool_switch(&showHelp), "Show help")
        ("files", po::wvalue(&files)->multitoken(), "Files to open")
        ;
    // clang-format on

    po::positional_options_description pos;
    pos.add("files", -1);

    /*!
      Extract list of arguments from QApplication::arguments().
      This list should not contain any arguments intended for the Qt itself.
      Also on Windows this a convenient way to work with ::GetCommandLineW()
      and to not mess with Unicode conversions.
    */

    QStringList dirtyArguments = QCoreApplication::arguments();
    if (!dirtyArguments.empty()) {
        /*!
          skip executable file name because boost::program_options won't
          handle it for us.
        */

        boost::filesystem::path argPath(dirtyArguments.first().toStdWString());
        boost::filesystem::path appPath(QApplication::applicationFilePath().toStdWString());

        if (boost::filesystem::equivalent(argPath, appPath)) {
            dirtyArguments.removeFirst();
        }
    }

    std::vector<std::wstring> args;
    args.reserve(dirtyArguments.size());
    for (const QString &arg : dirtyArguments) {
        args.push_back(arg.toStdWString());
    }

    auto parsed = po::wcommand_line_parser(args).options(options).positional(pos).run();

    po::variables_map vars;
    po::store(parsed, vars);
    po::notify(vars);

    if (showHelp) {
        std::cout << options << std::endl;
        return 0;
    }

    if (files.empty()) {
        auto doc = mDocMgr->createDocument();
        mFrameMgr->createFrame(doc);
        return QApplication::exec();
    }

    // convert to complete and canonical paths
    for (path_with_spaces &file : files) {
        file.path = boost::filesystem::absolute(file.path);
        if (boost::filesystem::exists(file.path)) {
            file.path = boost::filesystem::canonical(file.path);
        }
    }

    QTimer::singleShot(0, [this, files]() {
        for (const path_with_spaces &file : files) {
            auto doc = mDocMgr->createDocument();
            mFrameMgr->createFrame(doc);
            doc->loadFromFile(file.path);
        }
    });
    return QApplication::exec();
}

void Engine::newDocumentRequest(Frame *frame)
{
    auto factory = CreateDocumentDialog::createEditorFactory(frame);
    if (!factory)
        return;

    if (!frame->document()->isEmpty()) {
        Document *doc = mDocMgr->createDocument();
        mFrameMgr->createFrame(doc);
        doc->setEditorFactory(*factory);
    } else {
        frame->document()->setEditorFactory(*factory);
    }
}

void Engine::openDocumentRequest(Frame *sender, const boost::filesystem::path &path)
{
    if (!sender->document()->isEmpty()) {
        Document *doc = mDocMgr->createDocument();
        mFrameMgr->createFrame(doc);
        doc->loadFromFile(path);
    } else {
        sender->document()->loadFromFile(path);
    }
}

void Engine::closeDocumentRequest(Document *document)
{
    mFrameMgr->removeFrames(document);
    mDocMgr->removeDocument(document);
}

} // namespace gmtool
