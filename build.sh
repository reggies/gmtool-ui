#!/usr/bin/env bash

IMG=gmtool-bionic-cid
LID=gmtool-bionic
docker build -t $LID ./docker
docker stop $IMG
docker rm $IMG
docker run --name $IMG -v $(pwd):/tmp/gmtool/. -t $LID
docker stop $IMG
