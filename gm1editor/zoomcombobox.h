#pragma once

#include <QComboBox>

namespace gmtool {
class ZoomModel;
class GridGraphicsView;

class ZoomComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit ZoomComboBox(QWidget *parent = nullptr);

    void setGraphicsView(GridGraphicsView *view);

private:
    void myEditTextChanged(const QString &text);
    void myCurrentIndexChanged(int index);
    void viewScaleChanged(float delta);

private:
    GridGraphicsView *mView = nullptr;
    ZoomModel *mModel;
};

} // namespace gmtool
