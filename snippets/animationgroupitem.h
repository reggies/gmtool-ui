#pragma once

#include <vector>

#include "animationframegroup.h"
#include "animationnode.h"
#include "recordid.h"
#include "framesrecord.h"

namespace gmtool {
class AnimationEditor;

class AnimationPreviewItem : public AnimationNode
{
    Q_OBJECT
public:
    explicit AnimationPreviewItem(
        FramesRecord *record,
        PaletteModel *palette,
        QGraphicsItem *parent = nullptr);

    void reloadModelData() override;

    void setViewState(const ViewState &view) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void advanceTime(float dtime) override;

    float totalDuration() override;
    int totalFrames() override;

private:
    std::vector<QPixmap> getFrames() const;

    void setupFrames();

    void recordChanged();

private:
    const FramesRecord *mRecord;
    PaletteModel *mPalette;
    QSize mSize;
    float mAccumulatedTime = 0;
    int mFrameIndex = 0;
    QPoint mPivot;
    RecordId mPaletteId;
    bool mFramesDirty = true;
};

} // namespace gmtool
