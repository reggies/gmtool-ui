#include "palettepreview.h"

#include "paletterecord.h"

#include <QHBoxLayout>

#include "colorsarraywidget.h"

namespace gmtool {

PalettePreview::PalettePreview(const PaletteRecord &record, QWidget *parent)
    : QWidget(parent)
    , mColorsArray(new ColorsArrayWidget(this))
{
    mLabel = new QLabel(record.name(), this);
    mLabel->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    mLabel->setText(record.name());
    mLabel->setMinimumWidth(64);
    mLabel->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

    reset(record);

    auto layout = new QHBoxLayout(this);
    layout->setMargin(5);
    layout->addWidget(mLabel);
    layout->addWidget(mColorsArray);
}

void PalettePreview::reset(const PaletteRecord &record)
{
    mLabel->setText(record.name());

    QVector<QColor> colors;

    //! \note we are pretty sure that the number of colors will remain the same
    for (size_t i = 0; i < record.colorCount(); ++i) {
        colors.push_back(record.colorAt(i));
    }

    mColorsArray->setColors(colors);
}

} // namespace gmtool
