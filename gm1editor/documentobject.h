/**
Q: what policy data.image_list[0] should have?
    a. reference to existing object
    b. new object
Q: what type data.image_list[0] should have?
    a. dto with all the data from model
    b. dto with reference to model
    c. original model object

If it is a reference to DTO, then it could be stored in
    a. DocumentObject
    b. ImageList

Let's say this is the interface

class DTO:
    def assign(self): pass
    def clone(self): pass
    def pos(self): pass
    def set_pos(self, pos): pass
    ... other attributes ...

Then we have 4 options to implement it:

    - put a RecordId inside
    - put an index inside
    - put a reference to AbstractRecord inside
    - put all the data inside

In other words, image_list[0] is:

    - reference existing to abstract record
    - reference to DTO with RecordID, which is owned by DocumentObject
    - reference to DTO with RecordID, which is owned by ImageList
    - reference to DTO with reference to AbstractRecord which is owned by DocumentObject
    - reference to DTO with reference to AbstractRecord which is owned by ImageList
    - reference to DTO with all the data inside which is owned by DocumentObject
    - reference to DTO with all the data inside which is owned by ImageList
    - reference to DTO with all index
    - new object
    ...

Some use cases:


*/

#pragma once

#include <QtGui>
#include <QtWidgets>

#include <QDebug>

#include <iomanip>
#include <string>
#include <sstream>
#include <vector>

#include <boost/python.hpp>

#include "undo.h"
#include "documenteditor.h"

namespace gmtool {

namespace python {

class Point2D {
public:
    Point2D() = default;
    Point2D(const QPoint &point)
        : mX(point.x()), mY(point.y())
    {}
    Point2D(int x, int y)
        : mX(x), mY(y)
    {}

    int x() const { return mX; }
    int y() const { return mY; }

    void setX(int x) { mX = x; }
    void setY(int y) { mY = y; }

    QPoint toQPoint() const {
        return QPoint(mX, mY);
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << std::showpos << "Point2D(" << mX << mY << ")";
        return oss.str();
    }

private:
    int mX = 0;
    int mY = 0;
};

class Rect2D {
public:
    Rect2D() = default;
    Rect2D(int x, int y, int w, int h)
        : mX(x), mY(y), mWidth(w), mHeight(h)
    {}
    Rect2D(const QRect &qrect)
        : mX(qrect.left()), mY(qrect.top()), mWidth(qrect.width()), mHeight(qrect.height())
    {}

    Point2D topLeft() const { return Point2D(mX, mY); }
    void setTopLeft(const Point2D& topLeft) {mX=topLeft.x();mY=topLeft.y();}

    int x() const { return mX; }
    int y() const { return mY; }
    int width() const { return mWidth; }
    int height() const { return mHeight; }

    void setX(int x) { mX=x; }
    void setY(int y) { mY=y; }
    void setWidth(int width) { mWidth=width; }
    void setHeight(int height) { mHeight=height; }

    QRect toQRect() const {
        return QRect(mX, mY, mWidth, mHeight);
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << std::showpos << "Rect(" << mX << mY
            << mWidth << 'x' << std::noshowpos << mHeight << ")";
        return oss.str();
    }

private:
    int mX = 0;
    int mY = 0;
    int mWidth = 0;
    int mHeight = 0;
};

class ColorRGBA {
public:
    ColorRGBA() = default;
    ColorRGBA(int red, int green, int blue, int alpha)
        : mRed(red), mGreen(green), mBlue(blue), mAlpha(alpha)
    {}

    int red() const { return mRed; }
    void setRed(int red) { mRed = red; }

    int green() const { return mGreen; }
    void setGreen(int green) { mGreen = green; }

    int blue() const { return mBlue; }
    void setBlue(int blue) { mBlue = blue; }

    int alpha() const { return mAlpha; }
    void setAlpha(int alpha) { mAlpha = alpha; }

    std::string toString() const {
        std::ostringstream oss;
        oss << '#' << std::hex
            << std::setw(2) << std::setfill('0') << mAlpha
            << std::setw(2) << std::setfill('0') << mRed
            << std::setw(2) << std::setfill('0') << mGreen
            << std::setw(2) << std::setfill('0') << mBlue;
        return oss.str();
    }

private:
    int mRed = 0;
    int mGreen = 0;
    int mBlue = 0;
    int mAlpha = 0;
};

class Animation {
public:
    Animation(const Rect2D &rect) : mRect(rect) {}

    Point2D pos() const { return mRect.topLeft(); };
    void setPos(const Point2D& pos) { mRect.setTopLeft(pos); }

    Rect2D rect() const { return mRect; };
    void setRect(const Rect2D &rect) { mRect = rect; };

private:
    Rect2D mRect;
};

class Tile {
public:
    Point2D pos() const { return mPos; };
    void setPos(const Point2D& pos) { mPos = pos; }

    int tileWidth() const { return 30; }
    int tileHeight() const { return 16; }

    int widthStride() const { return 32; }
    int heightStride() const { return 16; }

    Rect2D rect() const {
        return Rect2D(
            mPos.x(),                                    // x
            mPos.y(),                                    // y
            widthStride() * (mSize - 1) + tileWidth(),   // w
            mBoxHeight + heightStride() * mSize);        // h
    };

    void setSize(int size) { mSize = size; }
    void setBoxHeight(int boxHeight) { mBoxHeight = boxHeight; }

    int size() const { return mSize; }
    int boxHeight() const { return mBoxHeight; }

private:
    Point2D mPos;
    int mBoxHeight = 0;
    int mSize = 1;
};

class Glyph {
public:
    Point2D pos() const { return mRect.topLeft(); };
    void setPos(const Point2D& pos) { mRect.setTopLeft(pos); }

    Rect2D rect() const { return mRect; };
    void setRect(const Rect2D &rect) { mRect = rect; };

    int bearing() const { return mBearing; }
    void setBearing(int bearing) { mBearing = bearing; }

private:
    Rect2D mRect;
    int mBearing = 0;
};

class Bitmap {
public:
    Point2D pos() const { return mRect.topLeft(); };
    void setPos(const Point2D& pos) { mRect.setTopLeft(pos); }

    Rect2D rect() const { return mRect; };
    void setRect(const Rect2D &rect) { mRect = rect; };

private:
    Rect2D mRect;
};

template<class PixelT>
class ImageBase {
public:
    ImageBase()
    {
        qDebug() << Q_FUNC_INFO;
    }

    ImageBase(int width, int height)
        : mWidth(width), mHeight(height)
    {
        qDebug() << Q_FUNC_INFO;
        mPixels.resize(width * height);
    }

    ImageBase(const uchar *pixels, int width, int height, int pitch)
        : mWidth(width), mHeight(height)
    {
        qDebug() << Q_FUNC_INFO;
        mPixels.reserve(width * height);
        for(int i = 0; i < height; ++i) {
            mPixels.insert(mPixels.end(),
                           (PixelT *)(&((const char *)pixels)[pitch*i]),
                           (PixelT *)(&((const char *)pixels)[pitch*i+width*sizeof(PixelT)]));
        }
    }


    ~ImageBase()
    {
        qDebug() << Q_FUNC_INFO;
    }

    int width() const { return mWidth; }
    void setWidth(int width) { mWidth = width; }

    int height() const { return mHeight; }
    void setHeight(int height) { mHeight = height; }

    std::vector<PixelT> pixels() const { return mPixels; };
    void setPixels(const std::vector<PixelT>& pixels) { mPixels = pixels; }

    QImage toQImage() const;

private:
    std::vector<PixelT> mPixels;
    int mWidth = 0;
    int mHeight = 0;
};


using Image = ImageBase<uint16_t>;
using IndexedImage = ImageBase<uint8_t>;

template<> QImage Image::toQImage() const;
template<> QImage IndexedImage::toQImage() const;

struct ISprite {
    virtual ~ISprite() {}
    virtual Point2D pos() const = 0;
    virtual void setPos(const Point2D& pos) = 0;
    // TBD: Image is 16-bit per pixel which is incompatible with indexed images
    virtual Image image() const = 0;
    virtual void setImage(const Image& image) = 0;
    virtual ISprite* clone() const = 0;
};

class SpriteData : public ISprite {
public:
    SpriteData(const Point2D &topLeft = Point2D(), const Image &image = Image())
        : mPos(topLeft), mImage(image) {}
    ~SpriteData() {}
    Point2D pos() const override;
    void setPos(const Point2D& pos) override;
    Image image() const override;
    void setImage(const Image& img) override;
    SpriteData* clone() const override {
        return new SpriteData(mPos);
    }

    std::string toString() const;
private:
    Point2D mPos;
    Image mImage;
};

class SpriteDelegate : public ISprite
{
public:
    SpriteDelegate(undo::Stack *stack, DocumentEditor *editor, const RecordId &id)
        : mStack(stack), mEditor(editor), mRecordId(id)
    {
        qDebug() << Q_FUNC_INFO;
    }

    ~SpriteDelegate()
    {
        qDebug() << Q_FUNC_INFO;
    }

    // void setData(const SpriteData &spriteData);
    // SpriteData data() const;

    Point2D pos() const override;
    void setPos(const Point2D& pos) override;

    Image image() const override;
    void setImage(const Image& pos) override;

    SpriteDelegate* clone() const override {
        return new SpriteDelegate(mStack, mEditor, mRecordId);
    }

private:
    RecordId mRecordId;
    undo::Stack *mStack;
    DocumentEditor *mEditor;
};

class ImageList {
public:
    ImageList(undo::Stack *stack, DocumentEditor *editor);

    size_t len() const;
    // lengthHint;
    SpriteDelegate* getItem(int index);
    void setItem(int index, const ISprite &elem);
    void delItem(int index);
    // delItem;
    // missing;
    // iter;
    // reversed;
    SpriteDelegate* insert(int index, const ISprite &elem);

private:
    undo::Stack *mStack;
    DocumentEditor *mEditor;
};

/// @brief Class that is instantiated for a document as a singleton
/// @python class gmtool.DocumentObject
/// @python object gmtool.data
class DocumentObject
{
public:
    DocumentObject(undo::Stack *stack, DocumentEditor *editor);
    virtual ~DocumentObject();

    ImageList* imageList() {
        return mImageList;
    }

    SpriteDelegate* addImage();

private:
    undo::Stack *mStack;
    DocumentEditor *mEditor;

    ImageList *mImageList;
};

}

}
