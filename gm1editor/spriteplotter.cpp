#include "spriteplotter.h"

#include <QDebug>
#include <QPainter>

#include "logmodel.h"

namespace gmtool {

SpritePlotter::SpritePlotter() = default;

void SpritePlotter::addImage(const QImage &image, const QRect &rect, const QBitmap &mask)
{
    mItems.push_back({image, rect, mask});
}

void SpritePlotter::setTransparentPixel(uint pixel)
{
    mTransparentPixel = boost::make_optional(pixel);
}

void SpritePlotter::setTransparentIndex(int index)
{
    mTransparentIndex = boost::make_optional(index);
}

bool SpritePlotter::render(QImage::Format format, const QRect &bounds, LogModel *log)
{
    QRect plotRect = bounds;
    for (const Item &item : mItems) {
        plotRect |= item.rect;
    }

    if (!bounds.isNull()) {
        plotRect &= bounds;
    }

    if (plotRect.width() == 0 || plotRect.height() == 0) {
        log->error("image is empty");
        return false;
    }

    /*! see QPainter limitations */
    if (plotRect.width() > std::numeric_limits<short int>::max()
        || plotRect.height() > std::numeric_limits<short int>::max()) {
        log->error("image is too large: width = %d, height = %d, limits = %dx%d",
                   plotRect.width(),
                   plotRect.height(),
                   std::numeric_limits<short int>::max(),
                   std::numeric_limits<short int>::max());
        return false;
    }

    mPlot = QImage(plotRect.width(), plotRect.height(), format);

    if (mPlot.isNull()) {
        log->error("out of memory");
        return false;
    }

    QImage dstimg;

    //     |
    //     |
    // ----+--------------------------------------- Plot X
    //     |
    //     |
    //     |         |                            |
    //     |       --+----------+-----------------+--
    //     |         |          |                 |
    //     |         |          |                 |
    //     |         |          |                 |
    //     |         |          |    item.rect    |
    //     |         |          |                 |
    //     |         |          |                 |
    //     |         |          |                 |
    //     |         |          +-----------------+
    //     |         |                            |
    //     |         |                            |
    //     |         |       plotRect             |
    //     |         |                            |
    //     |         |                            |
    //     |     ----+----------------------------+------
    //     |         |                            |
    //               |                            |
    //     Plot Y                                 |

    if (format == QImage::Format_Indexed8) {
        /*!
          QPainter doesn't support rendering on Format_Indexed8 QImages.
          Workaround. Reinterpret pixel data as Format_Grayscale8.
         */
        dstimg = QImage(mPlot.bits(),
                        mPlot.width(),
                        mPlot.height(),
                        mPlot.bytesPerLine(),
                        QImage::Format_Grayscale8);

        dstimg.fill(mTransparentIndex ? *mTransparentIndex : 0);
    } else {
        dstimg = QImage(mPlot.bits(),
                        mPlot.width(),
                        mPlot.height(),
                        mPlot.bytesPerLine(),
                        mPlot.format());

        dstimg.fill(mTransparentPixel ? *mTransparentPixel : 0);
    }

    QPainter painter(&dstimg);
    for (const SpritePlotter::Item &item : mItems) {
        /*!
          srcrect's origin is moved to the origin of the item.rect;
          \note it is not safe to use QImage::offset()
        */
        const QRect &srcrect = QRect(QPoint(0, 0), item.rect.size());
        /*!
          dstrect's origin is moved to the origin of the plotRect;
        */
        const QRect &dstrect = QRect(item.rect.topLeft() - plotRect.topLeft(), item.rect.size());

        if (format == QImage::Format_Indexed8) {
            if (item.image.format() != QImage::Format_Indexed8) {
                /*!
                  We are forced to rejected such a case.
                  Indexed images are special in that sense
                  that QPainter's cannot operate on them without color table.
                  Unfortunately, we don't have palette assigned to an image.
                */

                log->error("unexpected image format: %d", item.image.format());

                return false;
            }

            QImage gray(item.image.constBits(),
                        item.image.width(),
                        item.image.height(),
                        item.image.bytesPerLine(),
                        QImage::Format_Grayscale8);

            const QRegion &oldClipRegion = painter.clipRegion();
            if (!item.mask.isNull()) {
                /*
                  Conditional jump or move depends on uninitialised value(s)
                 */
                painter.setClipRegion(
                    QRegion(item.mask.copy(srcrect)).translated(dstrect.topLeft()));
            }
            painter.drawImage(dstrect, gray.copy(srcrect));
            if (!item.mask.isNull()) {
                painter.setClipRegion(oldClipRegion);
            }
        } else {
            QPixmap fragment = QPixmap::fromImage(item.image.copy(srcrect));
            if (!item.mask.isNull()) {
                fragment.setMask(item.mask.copy(srcrect));
            }
            painter.drawPixmap(dstrect, fragment);
        }
    }

    mOrigin = plotRect.topLeft();

    return true;
}

QImage SpritePlotter::plot() const
{
    return mPlot;
}

QPoint SpritePlotter::origin() const
{
    return mOrigin;
}

} // namespace gmtool
