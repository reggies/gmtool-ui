#pragma once

#include <QMetaType>
#include <QString>

namespace gmtool {

class RecordId final
{
public:
    RecordId()
        : RecordId(-1)
    {}
    explicit RecordId(int id)
        : mId(id)
    {}

    auto isValid() const { return mId != -1; }

    auto toString() const { return QString::number(mId); }

    bool operator<(const RecordId &that) const { return mId < that.mId; }

    bool operator==(const RecordId &that) const { return mId == that.mId; }

    bool operator!=(const RecordId &that) const { return mId != that.mId; }

private:
    int mId;
};

} // namespace gmtool

Q_DECLARE_METATYPE(gmtool::RecordId)
