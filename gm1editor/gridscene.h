#pragma once

#include <boost/optional.hpp>
#include <memory>

#include <QGraphicsScene>

namespace gmtool {

enum class InteractionMode {
    Scroll,
    Transform,
    Pick,
};

class GridScene : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit GridScene(QObject *parent = nullptr);
    virtual ~GridScene();

    QPoint mouseScenePos() const;
    void setSnapToGrid(bool enable);
    bool snapToGrid() const;
    void setInteractionMode(InteractionMode mode);
    InteractionMode interactionMode() const;

    int gridSize() const;

    bool isTransforming() const;
    QList<QGraphicsItem *> transformingItems() const;

    void setTileBackground(bool tileBackground);
    void setSolidBackground(bool solidBackground);

    //! Color picking should be banned from this API because
    //! GridScene is going to be reused with non SpriteGraphicsItems
    //! in PreviewWindow.
    boost::optional<QColor> pickedColor() const;

    bool isPicking() const;
    boost::optional<QColor> pixelAt(const QPoint &point) const;

    QPoint transformingTargetPos() const;

signals:
    void interactionModeChanged();

    void cursorMoved(const QPoint &pos);
    void snapToGridChanged(bool value);
    void itemsMovementStarted();
    void itemsMovementFinished();
    void itemsMovementReset();

    void pickingChanged();
    void pickingStarted();
    void pickingFinished();
    void pickingReset();

protected:
    bool event(QEvent *event) override;

    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

    void drawBackground(QPainter *painter, const QRectF &rect) override;

    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

private:
    void prepareTransparentGrid();

    bool startTransforming(const QPoint &anchorScenePos);
    void stopTransforming(bool reset);
    void updateItemsPos(bool ignoreSnapToGrid);

    void stopPicking(bool reset);

    void updatePicking(const QPoint &point);

    QPointF snapItem(const QPointF &point, const QPoint &transformOrigin);

    int keyboardMovementMagnitude(Qt::KeyboardModifiers modifiers) const;
    QPoint keyboardMovementVector(int key, Qt::KeyboardModifiers modifiers) const;

private:
    bool mSnapToGrid = false;
    InteractionMode mMode = InteractionMode::Scroll;
    QPoint mMousePos;
    ///< in scene coordinates

    bool mDrawTransparentGri = false;

    bool mIsTransforming = false;
    QList<QGraphicsItem *> mTransformingItems;

    bool mIsPicking = false;
    boost::optional<QColor> mPickedColor;

    bool mTileBackground = true;

    QWidget *mTransformingInputGrabber = nullptr;
    QPoint mTransformingTargetPos;

    bool mIsTransformingWithKeyboard = false;
};

} // namespace gmtool
