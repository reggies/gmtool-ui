#pragma once

#include "graphicsitemtypes.h"
#include "recordid.h"
#include "scenenode.h"
#include "spriterecord.h"

namespace gmtool {
class ISpriteSheet;

class SpriteGraphicsItem : public SceneNode
{
public:
    enum { Type = SpriteGraphicsItemType };

    explicit SpriteGraphicsItem(ISpriteSheet *spritesheet,
                                const RecordId &id,
                                SceneNode *parent = nullptr);

    void setViewState(const ViewState &state) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    void invalidatePalette() override;

    QColor pixelAt(const QPoint &point);

    std::unique_ptr<AbstractRecord> movedRecord(const QPoint &toPoint) override;

    int type() const override;

private:
    void createPixmap(bool force = false);

    void spriteChanged();
    void positionChanged();

private:
    ISpriteSheet *mSpriteSheet;
    const SpriteRecord *mRecord;
    RecordId mRecordId;
    RecordId mPaletteId;
    QPixmap mCachedPixmap;
    QSize mRecordSize;
};

} // namespace gmtool
