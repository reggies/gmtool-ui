#pragma once

#include <QColor>

#include "abstractrecord.h"
#include "gm1filedata.h"

namespace gmtool {

class PaletteRecordData
{
public:
    Gm1FileData::palette_t colors() const { return mColors; }

    void setName(const QString &name) { mName = name; }

    QString name() const { return mName; }

    void setColors(const Gm1FileData::palette_t &colors) { mColors = colors; }

    void setColorAt(int index, const QColor &color) { mColors.at(index) = color; }

    size_t colorCount() const { return mColors.size(); }

    QColor colorAt(int index) const { return mColors.at(index); }

private:
    Gm1FileData::palette_t mColors;
    QString mName;
};

class PaletteRecord : public AbstractRecord, public PaletteRecordData
{
    Q_OBJECT

public:
    explicit PaletteRecord(const PaletteRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , PaletteRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new PaletteRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        *((PaletteRecordData *) this) = dynamic_cast<const PaletteRecordData &>(other);
    }
};

} // namespace gmtool
