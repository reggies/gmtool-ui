#pragma once

#include "recordid.h"

namespace gmtool {

class ViewState final
{
public:
    ViewState() {}

    bool showOverlay = false;
    RecordId paletteId;
};

} // namespace gmtool
