#pragma once

#include <set>

#include "documenteditor.h"
#include "gm1filedata.h"
#include "recordset.h"
#include "spriterecord.h"
#include "tilerecord.h"

namespace gmtool {

class TileEditor : public DocumentEditor
{
public:
    TileEditor();
    ~TileEditor() override;

    PropertiesModel *getPropertiesModel() override;
    TableModel *getModel(TableId table) override;
    std::unique_ptr<AbstractRecord> createBlankEntry() override;
    std::unique_ptr<NodeFactory> createLayoutFactory() override;
    std::unique_ptr<NodeFactory> createSpriteFactory() override;
    std::unique_ptr<SpriteLoader> createSpriteLoader() override;
    bool load(const Gm1FileData &fileData, LogModel *log) override;
    bool save(Gm1FileData *fileData, LogModel *log) override;
    bool renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result) override;
    std::unique_ptr<AnimationFactory> createAnimationFactory() override;

private:
    void setupEntriesTableModel();
    void setupSpritesTableModel();

    QRgb getDefaultTransparentPixel() const;

private:
    RecordSet<TileRecord> mEntries;
    RecordSet<SpriteRecord> mSprites;

    std::unique_ptr<TableModel> mEntriesTableModel;
    std::unique_ptr<TableModel> mSpritesTableModel;
    std::unique_ptr<PropertiesModel> mPropertiesModel;

    Gm1FileData mGm1;

private:
    TileEditor(const TileEditor &) = delete;
    TileEditor &operator=(const TileEditor &) = delete;
};

} // namespace gmtool
