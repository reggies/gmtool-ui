#include "spriteswindow.h"

#include <boost/filesystem/path.hpp>
#include <boost/range/adaptor/reversed.hpp>

#include <set>

#include <QDebug>
#include <QFileDialog>
#include <QToolBar>
#include <QVBoxLayout>

#include "animationeditor.h"
#include "animationloaddialog.h"
#include "commanditemdelegate.h"
#include "document.h"
#include "insertcommand.h"
#include "movecommand.h"
#include "removecommand.h"
#include "spriteloader.h"

namespace gmtool {

namespace fs = boost::filesystem;

SpritesWindow::SpritesWindow(Document *doc, QWidget *parent)
    : Window(parent)
    , mDoc(doc)
{
    setWindowTitle(tr("Background images"));

    mView = new QTableView(this);
    mView->setModel(mDoc->editor()->getModel(TableId::Spritesheet));
    mView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    mView->setSelectionBehavior(QAbstractItemView::SelectRows);
    mView->setContextMenuPolicy(Qt::ActionsContextMenu);
    connect(mView->selectionModel(),
            &QItemSelectionModel::selectionChanged,
            this,
            &SpritesWindow::selectionChanged);

    mView->setItemDelegate(new CommandItemDelegate(mDoc->editor()->getModel(TableId::Spritesheet),
                                                   mDoc->undoStack(),
                                                   this));

    auto layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->addWidget(mView);
}

SpritesWindow::~SpritesWindow() = default;

bool SpritesWindow::focusEntry(TableId table, const RecordId &id)
{
    if (table == TableId::Spritesheet) {
        int row = mDoc->editor()->getModel(table)->getPositionOf(id);
        if (row != -1) {
            mView->selectRow(row);
            return true;
        }
    }
    return false;
}

void SpritesWindow::add()
{
    mDoc->importSprites(this);
}

void SpritesWindow::save()
{
    TableModel *model = mDoc->editor()->getModel(TableId::Spritesheet);
    const QModelIndexList &selection = mView->selectionModel()->selectedIndexes();
    std::set<RecordId> ids;
    for (const QModelIndex &index : selection) {
        ids.insert(model->getIdOf(index.row()));
    }
    mDoc->exportSprites(this, ids);
}

void SpritesWindow::remove()
{
    TableModel *model = mDoc->editor()->getModel(TableId::Spritesheet);
    const QModelIndexList &selection = mView->selectionModel()->selectedIndexes();
    std::set<int> rows;
    for (const QModelIndex &index : selection) {
        rows.insert(index.row());
    }
    if (rows.empty()) {
        return;
    }
    auto macro = std::make_unique<undo::Macro>(tr("Remove %1 sprite(s)").arg(rows.size()));
    for (int row : rows | boost::adaptors::reversed) {
        macro->add(std::make_unique<RemoveCommand>(row, model));
    }
    if (!mDoc->undoStack()->push(std::move(macro))) {
        emit statusTextChanged(tr("Could not remove %1 sprites").arg(rows.size()));
    } else {
        emit statusTextChanged(tr("Removed %1 sprites").arg(rows.size()));
    }
}

void SpritesWindow::bringForward()
{
    std::set<int> rows;
    for (const auto &index : mView->selectionModel()->selectedIndexes()) {
        rows.insert(index.row());
    }
    mDoc->bringForward(rows);
}

void SpritesWindow::sendBack()
{
    std::set<int> rows;
    for (const auto &index : mView->selectionModel()->selectedIndexes()) {
        rows.insert(index.row());
    }
    mDoc->sendBack(rows);
}

void SpritesWindow::moveForward()
{
    std::set<int> rows;
    for (const auto &index : mView->selectionModel()->selectedIndexes()) {
        rows.insert(index.row());
    }
    mDoc->moveForward(rows);
}

void SpritesWindow::moveBackward()
{
    std::set<int> rows;
    for (const auto &index : mView->selectionModel()->selectedIndexes()) {
        rows.insert(index.row());
    }
    mDoc->moveBackward(rows);
}

void SpritesWindow::selectionChanged(const QItemSelection &selected,
                                     const QItemSelection &deselected)
{
    emit gotAnyRowSelected(mView->selectionModel()->hasSelection());
}

void SpritesWindow::route(RemoveSpriteOrEntry *action)
{
    connect(action, &QAction::triggered, this, &SpritesWindow::remove);
    connect(this, &SpritesWindow::gotAnyRowSelected, action, &QAction::setEnabled);
    mView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void SpritesWindow::route(ImportSprites *action)
{
    connect(action, &QAction::triggered, this, &SpritesWindow::add);
    mView->addAction(action);
}

void SpritesWindow::route(ExportSprites *action)
{
    connect(action, &QAction::triggered, this, &SpritesWindow::save);
    connect(this, &SpritesWindow::gotAnyRowSelected, action, &QAction::setEnabled);
    mView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void SpritesWindow::route(MoveForward *action)
{
    connect(action, &QAction::triggered, this, &SpritesWindow::moveForward);
    connect(this, &SpritesWindow::gotAnyRowSelected, action, &QAction::setEnabled);
    mView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void SpritesWindow::route(MoveBackward *action)
{
    connect(action, &QAction::triggered, this, &SpritesWindow::moveBackward);
    connect(this, &SpritesWindow::gotAnyRowSelected, action, &QAction::setEnabled);
    mView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void SpritesWindow::route(BringForward *action)
{
    connect(action, &QAction::triggered, this, &SpritesWindow::bringForward);
    connect(this, &SpritesWindow::gotAnyRowSelected, action, &QAction::setEnabled);
    mView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void SpritesWindow::route(SendBack *action)
{
    connect(action, &QAction::triggered, this, &SpritesWindow::sendBack);
    connect(this, &SpritesWindow::gotAnyRowSelected, action, &QAction::setEnabled);
    mView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void SpritesWindow::unroute(RemoveSpriteOrEntry *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mView->removeAction(action);
    action->setDisabled(true);
}

void SpritesWindow::unroute(ImportSprites *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mView->removeAction(action);
    action->setDisabled(true);
}

void SpritesWindow::unroute(ExportSprites *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mView->removeAction(action);
    action->setDisabled(true);
}

void SpritesWindow::unroute(MoveForward *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mView->removeAction(action);
    action->setDisabled(true);
}

void SpritesWindow::unroute(MoveBackward *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mView->removeAction(action);
    action->setDisabled(true);
}

void SpritesWindow::unroute(BringForward *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mView->removeAction(action);
    action->setDisabled(true);
}

void SpritesWindow::unroute(SendBack *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mView->removeAction(action);
    action->setDisabled(true);
}

} // namespace gmtool
