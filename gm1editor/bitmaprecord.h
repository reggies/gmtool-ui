#pragma once

#include "abstractrecord.h"

#include <QRect>

namespace gmtool {

class BitmapRecordData
{
public:
    BitmapRecordData() = default;

    QRect rect() const { return mRect; }
    void setRect(const QRect &rect) { mRect = rect; }

    int left() const { return mRect.x(); }
    int width() const { return mRect.width(); }
    int top() const { return mRect.y(); }
    int height() const { return mRect.height(); }

    void setLeft(int value) { mRect.moveLeft(value); }
    void setWidth(int value) { mRect.setWidth(value); }
    void setTop(int value) { mRect.moveTop(value); }
    void setHeight(int value) { mRect.setHeight(value); }

private:
    QRect mRect;
};

class BitmapRecord : public AbstractRecord, public BitmapRecordData
{
    Q_OBJECT

public:
    explicit BitmapRecord(const BitmapRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , BitmapRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new BitmapRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        *((BitmapRecordData *) this) = dynamic_cast<const BitmapRecordData &>(other);
    }

    std::unique_ptr<AbstractRecord> movedRecord(int x, int y) const
    {
        BitmapRecordData data(*this);
        data.setTop(y);
        data.setLeft(x);
        return BitmapRecord(data, parent()).clone();
    }
};

} // namespace gmtool
