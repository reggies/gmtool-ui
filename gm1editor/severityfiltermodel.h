#pragma once

#include <QSortFilterProxyModel>

namespace gmtool {
enum class Severity;

class SeverityFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    explicit SeverityFilterModel(const QFlags<Severity> &severity, QObject *parent = 0);
    ~SeverityFilterModel() override;

    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
    QFlags<Severity> mSeverity;
};

} // namespace gmtool
