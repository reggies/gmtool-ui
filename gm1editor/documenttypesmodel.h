#pragma once

#include <QAbstractListModel>
#include <QString>

#include "documenttype.h"

namespace gmtool {

class DocumentTypesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit DocumentTypesModel(QObject *parent = 0);

    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    void setRecentIndex(int index);
    int recentIndex() const;

    DocumentType getValue(int index) const;

    // returns -1 if nothing found
    int indexOf(const DocumentType &type) const;
    int defaultIndex() const;

private:
    QString getDocumentTypeName(const DocumentType &type) const;

private:
    std::vector<DocumentType> mTypes;
    int mRecentIndex = -1;

    DocumentTypesModel(DocumentTypesModel const &) = delete;
    DocumentTypesModel &operator=(DocumentTypesModel const &) = delete;
};

} // namespace gmtool
