#include "colormask.h"

#include <cstdint>

#include <QDebug>

namespace gmtool {

static inline uint32_t swap32(uint32_t val)
{
    return ((val & 0xff) << 24) | ((val & 0xff00) << 8) | ((val & 0xff0000) >> 8)
           | ((val & 0xff000000) >> 24);
}

static inline uint16_t swap16(uint16_t val)
{
    return ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
}

QBitmap createColorMask(QRgb color, const QImage &image)
{
    QImage res(image.width(), image.height(), QImage::Format_MonoLSB);

    if (image.depth() < 8) {
        qCritical("could mask out color with depth %d", image.depth());
        return QBitmap();
    }

    res.fill(0);

    for (int y = 0; y < image.height(); ++y) {
        const auto src = image.constScanLine(y);
        auto dst = res.scanLine(y);
        for (int x = 0; x < image.width(); ++x) {
            QRgb pixel = 0;
            switch (image.depth() / 8) {
            case 4:
                pixel = (pixel << 8) | (src[x * 4 + 3]);
                pixel = (pixel << 8) | (src[x * 4 + 2]);
                pixel = (pixel << 8) | (src[x * 4 + 1]);
                pixel = (pixel << 8) | (src[x * 4 + 0]);
                break;
            case 3:
                pixel = (pixel << 8) | (src[x * 3 + 2]);
                pixel = (pixel << 8) | (src[x * 3 + 1]);
                pixel = (pixel << 8) | (src[x * 3 + 0]);
                break;
            case 2:
                pixel = (pixel << 8) | (src[x * 2 + 1]);
                pixel = (pixel << 8) | (src[x * 2 + 0]);
                break;
            case 1:
                pixel = (pixel << 8) | (src[x * 1 + 0]);
                break;
            }
            if (pixel == color) {
                dst[x / 8] = dst[x / 8] | (1 << (x % 8));
            }
        }
    }

    return QBitmap::fromImage(res);
}

} // namespace gmtool
