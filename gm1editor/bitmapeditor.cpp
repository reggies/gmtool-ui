#include "bitmapeditor.h"

#include <algorithm>
#include <vector>

#include <boost/range/adaptor/map.hpp>

#include <QDebug>
#include <QPainter>

#include "abstractrecord.h"
#include "basicspriteloader.h"
#include "bitmapgraphicsitem.h"
#include "columns.h"
#include "logmodel.h"
#include "nodefactory.h"
#include "propertiesmodel.h"
#include "spritegraphicsitem.h"
#include "spriteplotter.h"
#include "tableadapter.h"
#include "tablemodel.h"

#include <gm1.h>

namespace gmtool {

BitmapEditor::BitmapEditor(int flags)
    : mEntriesTableModel(new TableModel(nullptr))
    , mSpritesTableModel(new TableModel(nullptr))
    , mPropertiesModel(new PropertiesModel)
    , mFlags(flags)
{
    setupEntriesTableModel();
    setupSpritesTableModel();
}

BitmapEditor::~BitmapEditor() = default;

void BitmapEditor::setupEntriesTableModel()
{
    std::unique_ptr<TableAdapter<BitmapRecord>> adapter(new TableAdapter<BitmapRecord>(mEntries));
    adapter->addColumn<columns::MutableLeft<BitmapRecord>>();
    adapter->addColumn<columns::MutableTop<BitmapRecord>>();
    if ((mFlags & BitmapEditor::LayoutIsConstSize) != 0) {
        adapter->addColumn<columns::ConstWidth<BitmapRecord>>();
        adapter->addColumn<columns::ConstHeight<BitmapRecord>>();
    } else {
        adapter->addColumn<columns::MutableWidth<BitmapRecord>>();
        adapter->addColumn<columns::MutableHeight<BitmapRecord>>();
    }
    mEntriesTableModel->reset(std::unique_ptr<AbstractTable>(adapter.release()));
}

void BitmapEditor::setupSpritesTableModel()
{
    std::unique_ptr<TableAdapter<SpriteRecord>> images(new TableAdapter<SpriteRecord>(mSprites));
    images->addColumn<columns::MutableLeft<SpriteRecord>>();
    images->addColumn<columns::MutableTop<SpriteRecord>>();
    images->addColumn<columns::ConstWidth<SpriteRecord>>();
    images->addColumn<columns::ConstHeight<SpriteRecord>>();
    mSpritesTableModel->reset(std::unique_ptr<AbstractTable>(images.release()));
}

TableModel *BitmapEditor::getModel(TableId table)
{
    switch (table) {
    case TableId::Gm1_Bitmap:
    case TableId::Layout:
        return mEntriesTableModel.get();
    case TableId::Palette:
        return nullptr;
    case TableId::Spritesheet:
        return mSpritesTableModel.get();
    case TableId::Invalid:
        return nullptr;
    case TableId::Gm1_Animation:
    case TableId::Gm1_Tile:
    case TableId::Gm1_Glyph:
    case TableId::Animations:
        return nullptr;
    }
    return nullptr;
}

PropertiesModel *BitmapEditor::getPropertiesModel()
{
    return mPropertiesModel.get();
}

std::unique_ptr<AbstractRecord> BitmapEditor::createBlankEntry()
{
    return BitmapRecord().clone();
}

std::unique_ptr<NodeFactory> BitmapEditor::createLayoutFactory()
{
    std::unique_ptr<NodeFactory> fact(new NodeFactory);
    fact->setNodeCreator([this](const RecordId &id) {
        return new BitmapGraphicsItem(this, id, nullptr);
    });
    return fact;
}

std::unique_ptr<NodeFactory> BitmapEditor::createSpriteFactory()
{
    std::unique_ptr<NodeFactory> fact(new NodeFactory);
    fact->setNodeCreator([this](const RecordId &id) {
        return new SpriteGraphicsItem(this, id, nullptr);
    });
    return fact;
}

std::unique_ptr<SpriteLoader> BitmapEditor::createSpriteLoader()
{
    if ((mFlags & BitmapEditor::UseTransparency) != 0) {
        return std::unique_ptr<SpriteLoader>(
            new BasicSpriteLoader(this, TransparencyMode::Color, getDefaultTransparentPixel()));
    } else {
        return std::unique_ptr<SpriteLoader>(new BasicSpriteLoader(this, TransparencyMode::None));
    }
}

void createBitmaps(const Gm1FileData &fileData, RecordSet<BitmapRecord> *result)
{
    for (const Gm1FileEntry &entry : fileData.Entries) {
        BitmapRecord record;
        record.setLeft(entry.entryRect().left());
        record.setTop(entry.entryRect().top());
        record.setWidth(entry.entryRect().width());
        record.setHeight(entry.entryRect().height());
        result->append(record);
    }
}

bool BitmapEditor::load(const Gm1FileData &fileData, LogModel *log)
{
    Q_UNUSED(log);

    mGm1 = fileData;

    //! load plot image
    SpriteRecord plot;
    plot.setPosition(mGm1.plotOrigin());
    plot.setImage(mGm1.plotImage());
    if (mGm1.hasTransparency()) {
        plot.setTransparencyMode(TransparencyMode::Color);
        plot.setTransparentPixel(mGm1.transparentPixel());
    }

    mSprites.clear();
    mSprites.insert(plot, 0);
    setupSpritesTableModel();

    mEntries.clear();
    createBitmaps(mGm1, &mEntries);
    setupEntriesTableModel();

    mPropertiesModel->reset();

    return true;
}

QRgb BitmapEditor::getDefaultTransparentPixel() const
{
    gm1_image_props_t props;
    gm1_get_gfx_image_props(&props);
    return props.ColorKey;
}

auto BitmapEditor::getGm1EntryClass() const
{
    if ((mFlags & BitmapEditor::UseTransparency) != 0) {
        if ((mFlags & BitmapEditor::LayoutIsConstSize) != 0) {
            return GM1_ENTRY_CLASS_TGX16CONSTSIZE;
        } else {
            return GM1_ENTRY_CLASS_TGX16;
        }
    } else {
        if ((mFlags & BitmapEditor::MagicBitmap) != 0) {
            return GM1_ENTRY_CLASS_ANOTHERBITMAP;
        } else {
            return GM1_ENTRY_CLASS_BITMAP;
        }
    }
}

bool BitmapEditor::save(Gm1FileData *fileData, LogModel *log)
{
    Gm1FileData gm1 = mGm1;
    gm1.setEntryClass(getGm1EntryClass());

    gm1.setEntryCount(mEntries.size());
    for (size_t i = 0; i < mEntries.size(); ++i) {
        // \todo find a way to specify box width and box offset
        const RecordId &id = mEntries.getIdAt(i);
        const BitmapRecord &record = mEntries.get(id);
        gm1.Entries.at(i).setEntryRect(record.rect());
    }

    SpritePlotter plotter;
    plotter.setTransparentPixel(getDefaultTransparentPixel());

    for (const SpriteRecord &record : mSprites | boost::adaptors::map_values) {
        plotter.addImage(record.image(), record.rect(), record.mask());
    }

    QRect entriesRect;
    for (const BitmapRecord &record : mEntries | boost::adaptors::map_values) {
        entriesRect |= record.rect();
    }

    if (!plotter.render(QImage::Format_RGB555, entriesRect, log)) {
        log->error("error rendering final plot image");
        return false;
    }

    gm1.setPlotImage(plotter.plot());
    gm1.setPlotOrigin(plotter.origin());

    *fileData = gm1;

    return true;
}

bool BitmapEditor::renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result)
{
    SpritePlotter plotter;

    plotter.setTransparentPixel(getDefaultTransparentPixel());

    for (const RecordId &id : ids) {
        const auto &record = mSprites.get(id);
        if ((mFlags & BitmapEditor::UseTransparency) != 0) {
            plotter.addImage(record.image(), record.rect(), record.mask());
        } else {
            plotter.addImage(record.image(), record.rect(), QBitmap());
        }
    }

    if (!plotter.render(QImage::Format_RGB555, QRect(), log)) {
        return false;
    }

    *result = plotter.plot();

    return true;
}

std::unique_ptr<AnimationFactory> BitmapEditor::createAnimationFactory()
{
    return nullptr;
}

} // namespace gmtool
