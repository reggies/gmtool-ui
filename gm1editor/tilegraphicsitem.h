#pragma once

#include "recordid.h"
#include "scenenode.h"
#include "tilerecord.h"

namespace gmtool {
class DocumentEditor;
class TileEditor;
class EmptyNode;

class TileGraphicsItem : public SceneNode
{
public:
    explicit TileGraphicsItem(TileEditor *editor, const RecordId &id, SceneNode *parent);

    void setViewState(const ViewState &state) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    std::unique_ptr<AbstractRecord> movedRecord(const QPoint &toPoint) override;

private:
    void createSubTiles(QGraphicsItem *parent, const TileRecord &record);

private:
    RecordId mRecordId;
    TileEditor *mEditor;
    const TileRecord *mRecord;
    EmptyNode *mOverlay;
};

} // namespace gmtool
