#pragma once

#include <QAction>
#include <QTableView>

#include "window.h"

namespace gmtool {
class Document;

class EntryTableWindow : public Window
{
    Q_OBJECT

public:
    explicit EntryTableWindow(Document *document, QWidget *parent = nullptr);

    bool focusEntry(TableId table, const RecordId &id) override;

    void route(RemoveSpriteOrEntry *action) override;
    void unroute(RemoveSpriteOrEntry *action) override;

    void route(AddEntry *action) override;
    void unroute(AddEntry *action) override;

signals:
    void gotAnyRowSelected(bool any);

private:
    void add();
    void remove();

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    Document *mDoc;
    QTableView *mTableView;
};

} // namespace gmtool
