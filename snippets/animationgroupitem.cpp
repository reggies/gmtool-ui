#include "animationgroupitem.h"

#include <cmath>
#include <set>

#include <QGraphicsItem>
#include <QPainter>

#include "animationeditor.h"
#include "colormask.h"
#include "logmodel.h"
#include "recordid.h"
#include "spriteplotter.h"
#include "tableid.h"
#include "tablemodel.h"
#include "viewstate.h"

const static float kFramesPerSecond = 12.0;

namespace gmtool {

AnimationPreviewItem::AnimationPreviewItem(FramesRecord *record, PaletteModel *palette, QGraphicsItem *parent)
    : AnimationNode(parent)
    , mRecord(record)
    , mPalette(palette)
{
    mPaletteId = mPalette->getIdOf(0);
    mPivot = mRecord->pivot();

    setFlag(QGraphicsItem::ItemHasNoContents, true);

    setupFrames();
}


void AnimationPreviewItem::recordChanged()
{
    mFramesDirty = true;
    mPivot = mRecord->pivot();
    update();
}

QRectF AnimationPreviewItem::boundingRect() const
{
    return QRectF();
}

void AnimationPreviewItem::paint(QPainter *painter,
                                 const QStyleOptionGraphicsItem *option,
                                 QWidget *widget)
{}

void AnimationPreviewItem::advanceTime(float dtime)
{
    setupFrames();
    auto frameGroup = mRecord->frameGroup();
    const float frameDuration = 1.0f / kFramesPerSecond;
    mAccumulatedTime += dtime;
    if (mAccumulatedTime >= frameDuration) {
        while (mAccumulatedTime >= frameDuration) {
            mAccumulatedTime -= frameDuration;
            mFrameIndex = (mFrameIndex + 1) % frameGroup.frameCount;
        }
        int index = 0;
        for (auto child : childItems()) {
            if (index++ == mFrameIndex) {
                child->setVisible(true);
            } else {
                child->setVisible(false);
            }
        }
    }
}

std::vector<QPixmap> AnimationPreviewItem::getFrames() const
{
    auto paletteModel = mPalette;

    const PaletteRecord *palette = paletteModel->getRecordById(mPaletteId);
    QVector<QRgb> colorTable;
    colorTable.reserve(256);
    for (const QColor &color : palette->colors()) {
        colorTable.push_back(color.rgb());
    }

    std::vector<QPixmap> frames;
    auto frameGroup = mRecord->frameGroup();
    frames.resize(frameGroup.frameCount);

    auto masks = mRecord->masks();
    auto images = mRecord->images();
    for (int index = 0; index < images.size(); ++index) {
        QImage image = images.at(index);
        QBitmap mask = masks.at(index);
        image.setColorTable(colorTable);

        QPixmap pixmap = QPixmap::fromImage(image);
        if (!mask.isNull()) {
            pixmap.setMask(mask);
        }
        frames[index] = pixmap;
    }

    return frames;
}

void AnimationPreviewItem::setupFrames()
{
    if (!mFramesDirty)
        return;

    for (auto child : childItems())
        delete child;

    mSize = QSize();
    for (const QPixmap &pixmap : getFrames()) {
        auto child = new QGraphicsPixmapItem(pixmap, this);
        mSize = mSize.expandedTo(pixmap.size());
        child->setVisible(false);
        child->setPos(-mPivot);
    }

    mFramesDirty = false;
}

void AnimationPreviewItem::reloadModelData() {}

float AnimationPreviewItem::totalDuration()
{
    return totalFrames() / kFramesPerSecond;
}

int AnimationPreviewItem::totalFrames()
{
    setupFrames();
    auto frameGroup = mRecord->frameGroup();
    return frameGroup.frameCount;
}

void AnimationPreviewItem::setViewState(const ViewState &view)
{
    if (mPaletteId != view.paletteId) {
        mPaletteId = view.paletteId;
        mFramesDirty = true;
        update();
    }
}

} // namespace gmtool
