#pragma once

#include <QColor>
#include <QImage>

#include <boost/filesystem/path.hpp>

#include <gm1.h>

#include "gm1filedata.h"

namespace gmtool {

class Gm1Loader : public QObject
{
    Q_OBJECT
public:
    explicit Gm1Loader(QObject *parent = nullptr);

    bool isNull() const;

    bool loadFromFile(const boost::filesystem::path &filepath);

    Gm1FileData fileData() const;

signals:
    void progress(int current, int total);
    void diagnosticsError(const QString &message);
    void diagnosticsWarning(const QString &message);

private:
    QImage entryImage(size_t index) const;

    void emitError(const char *format, ...);
    void emitWarning(const char *format, ...);

    bool readHeader(gm1_istream_t *is);
    bool readEntries(gm1_istream_t *is);
    bool readEntry(gm1_istream_t *is, size_t index);
    bool initPlot();

private:
    Gm1FileData mFileData;
};

} // namespace gmtool
