#pragma once

#include "fontrecord.h"
#include "recordid.h"
#include "scenenode.h"

namespace gmtool {
class FontEditor;

class FontGraphicsItem : public SceneNode
{
public:
    explicit FontGraphicsItem(FontEditor *editor, const RecordId &id, SceneNode *parent);

    void setViewState(const ViewState &state) override;
    QRectF boundingRect() const override;
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    std::unique_ptr<AbstractRecord> movedRecord(const QPoint &toPoint) override;

private:
    RecordId mRecordId;
    FontEditor *mEditor;
    const FontRecord *mRecord;
    bool mShowOverlay = true;
};

} // namespace gmtool
