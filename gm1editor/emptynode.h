#pragma once

#include <QGraphicsItem>

#include "scenenode.h"
#include "viewstate.h"

namespace gmtool {

class EmptyNode : public QGraphicsItem
{
public:
    explicit EmptyNode(QGraphicsItem *parent)
        : QGraphicsItem(parent)
    {}

    QRectF boundingRect() const override { return QRectF(); }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override
    {}

    void setPropogatePaletteChanges(bool propogatePaletteChanges)
    {
        mPropogatePaletteChanges = propogatePaletteChanges;
    }

    void invalidatePalette()
    {
        if (mPropogatePaletteChanges) {
            for (QGraphicsItem *item : childItems()) {
                qgraphicsitem_cast<SceneNode *>(item)->invalidatePalette();
            }
        }
    }

    void setViewState(const ViewState &state)
    {
        for (QGraphicsItem *item : childItems()) {
            qgraphicsitem_cast<SceneNode *>(item)->setViewState(state);
        }
    }

private:
    bool mPropogatePaletteChanges = false;
};

} // namespace gmtool
