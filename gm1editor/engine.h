#pragma once

#include <QObject>

#include <boost/filesystem/path.hpp>

namespace gmtool {

class Document;
class DocumentManager;
class EditorFactory;
class FrameManager;
class Frame;

class Engine : public QObject
{
    Q_OBJECT

public:
    Engine();

    int exec();

private:
    void newDocumentRequest(Frame *frame);
    void openDocumentRequest(Frame *sender, const boost::filesystem::path &path);
    void closeDocumentRequest(Document *document);

private:
    DocumentManager *mDocMgr;
    FrameManager *mFrameMgr;

    Engine(const Engine &) = delete;
    Engine &operator=(const Engine &) = delete;
};

} // namespace gmtool
