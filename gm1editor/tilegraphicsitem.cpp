#include "tilegraphicsitem.h"

#include <QDebug>
#include <QGraphicsSimpleTextItem>
#include <QPainter>

#include "emptynode.h"
#include "nodeattribute.h"
#include "subtilewalk.h"
#include "tableid.h"
#include "tablemodel.h"
#include "tileeditor.h"
#include "viewstate.h"

namespace gmtool {

static QPolygonF getRhombusPolygon(const QRect &rect)
{
    QRect r = rect.adjusted(0, 0, 1, 1);
    QPolygonF poly;
    poly.reserve(5);
    poly << QPointF(r.center().x(), r.top()) << QPointF(r.right(), r.center().y())
         << QPointF(r.center().x(), r.bottom()) << QPointF(r.left(), r.center().y())
         << QPointF(r.center().x(), r.top());
    return poly;
}

static QPixmap getTilePixmap(const QColor &color)
{
    QPixmap pix(32, 16);
    pix.fill(QColor(0, 0, 0, 0));
    QPainter painter(&pix);
    painter.setBackgroundMode(Qt::TransparentMode);
    QPainterPath path;
    path.addPolygon(getRhombusPolygon(QRect(-1, 0, 32, 16)));
    painter.fillPath(path, color);
    return pix;
}

static QPixmap getEvenTilePixmap()
{
    static QPixmap even = getTilePixmap(QColor(128, 128, 250, 255));
    return even;
}

static QPixmap getOddTilePixmap()
{
    static QPixmap odd = getTilePixmap(QColor(250, 128, 128, 255));
    return odd;
}

TileGraphicsItem::TileGraphicsItem(TileEditor *editor, const RecordId &id, SceneNode *parent)
    : SceneNode(parent)
    , mRecordId(id)
    , mEditor(editor)
{
    setData(NodeAttribute::RecordId, QVariant::fromValue(id));
    setData(NodeAttribute::TableId, QVariant::fromValue(TableId::Layout));
    mRecord = mEditor->getModel(TableId::Layout)->getRecordAs<TileRecord>(mRecordId);
    setPos(mRecord->rect().topLeft());
    mOverlay = new EmptyNode(this);
    createSubTiles(mOverlay, *mRecord);
}

QRectF TileGraphicsItem::boundingRect() const
{
    return QRectF(0, 0, mRecord->width(), mRecord->height());
}

void TileGraphicsItem::setViewState(const ViewState &state)
{
    mOverlay->setVisible(state.showOverlay);
}

void TileGraphicsItem::paint(QPainter *painter,
                             const QStyleOptionGraphicsItem *option,
                             QWidget *widget)
{
    //! \todo do manual line rasterization
    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, false);
    QPen pen;
    pen.setCosmetic(true);
    if (isSelected()) {
        pen.setColor(QColor(255, 0, 0));
    }
    painter->setPen(pen);
    QVarLengthArray<QPointF, 16> points;
    //! bottom of the rhombus
    points << QPointF(0, mRecord->height() - mRecord->size() * mRecord->tileHeight() / 2);
    points << QPointF(mRecord->width() / 2, mRecord->height());
    points << QPointF(mRecord->width(), mRecord->height() - mRecord->size() * mRecord->tileHeight() / 2);
    //! top rectangular box
    points << QPointF(mRecord->width(), 0);
    points << QPointF(0, 0);
    //! close polygon
    points << points.front();
    painter->drawPolygon(points.data(), points.size());
    painter->restore();
}

void TileGraphicsItem::createSubTiles(QGraphicsItem *parent, const TileRecord &record)
{
    walkSubTiles(record, [this, parent, &record](SubTile subtile) {
        auto rhombus = new QGraphicsPixmapItem(parent);
        if ((subtile.xSkewed % 2) != (subtile.ySkewed % 2)) {
            rhombus->setPixmap(getEvenTilePixmap());
        } else {
            rhombus->setPixmap(getOddTilePixmap());
        }
        rhombus->setOpacity(0.5);
        rhombus->setFlag(QGraphicsItem::ItemStacksBehindParent, true);
        rhombus->setPos(subtile.tileBbox.topLeft() - record.rect().topLeft());
    });
}

std::unique_ptr<AbstractRecord> TileGraphicsItem::movedRecord(const QPoint &toPoint)
{
    return mRecord->movedRecord(toPoint.x(), toPoint.y());
}

void TileGraphicsItem::reloadModelData()
{
    prepareGeometryChange();
    setPos(mRecord->rect().topLeft());
    for (auto child : mOverlay->childItems()) {
        delete child;
    }
    createSubTiles(mOverlay, *mRecord);
}

} // namespace gmtool
