#include "colortablewindow.h"

#include <QColorDialog>
#include <QEventLoop>
#include <QFileDialog>
#include <QHeaderView>
#include <QImage>
#include <QMessageBox>
#include <QPointer>
#include <QVBoxLayout>

#include <functional>
#include <set>

#include "assigncommand.h"
#include "commanditemdelegate.h"
#include "document.h"
#include "logmodel.h"
#include "modifycommand.h"
#include "paletteitemdelegate.h"
#include "paletterecord.h"
#include "tablemodel.h"

namespace gmtool {

ColorTableWindow::ColorTableWindow(Document *document, QWidget *parent)
    : Window(parent)
    , mDoc(document)
{
    setWindowTitle(tr("Color table"));

    mTableView = new QTableView(this);
    mTableView->setModel(mDoc->editor()->getModel(TableId::Palette));
    mTableView->setItemDelegate(new CommandItemDelegate(mDoc->editor()->getModel(TableId::Palette),
                                                        mDoc->undoStack(),
                                                        this));
    mTableView->setItemDelegateForColumn(0,
                                         new PaletteItemDelegate(mDoc->editor()->getModel(
                                                                     TableId::Palette),
                                                                 this));
    connect(mTableView->selectionModel(),
            &QItemSelectionModel::selectionChanged,
            this,
            &ColorTableWindow::selectionChanged);

    mTableView->setContextMenuPolicy(Qt::ActionsContextMenu);
    mTableView->resizeColumnsToContents();

    auto layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->addWidget(mTableView);
}

void ColorTableWindow::loadPalette()
{
    std::set<int> rows;
    for (const QModelIndex &index : mTableView->selectionModel()->selectedIndexes()) {
        rows.insert(index.row());
    }
    if (rows.empty()) {
        return;
    }
    QStringList filters;
    filters << tr("Portable Network Graphics (*.png)") << tr("Any files (*.*)");
    QStringList paths = QFileDialog::getOpenFileNames(this,
                                                      tr("Select files"),
                                                      QString(),
                                                      filters.join(";;"));
    if (paths.isEmpty()) {
        return;
    }
    QMap<int, PaletteRecordData> entries;
    for (int row : rows) {
        bool succeed = false;
        while (!succeed && !paths.isEmpty()) {
            const QString path = paths.takeFirst();
            QImage image;
            if (image.load(path)) {
                image = image.convertToFormat(QImage::Format_Indexed8,
                                              Qt::PreferDither | Qt::DiffuseDither);
                if (image.isNull()) {
                    mDoc->logModel()->warning("could not apply dithering");
                }
                QVector<QRgb> palette = image.colorTable();
                Gm1FileData::palette_t colors;
                palette.resize(static_cast<int>(colors.size()));
                std::transform(palette.begin(),
                               palette.end(),
                               colors.begin(),
                               std::ptr_fun(&QColor::fromRgb));
                auto originalId = mDoc->editor()->getModel(TableId::Palette)->getIdOf(row);
                PaletteRecord record;
                record.assign(*mDoc->editor()->getModel(TableId::Palette)->getRecord(originalId));
                record.setColors(colors);
                entries.insert(row, record);
                succeed = true;
            } else {
                mDoc->logModel()->warning("could not load image %s", path.toUtf8().data());
            }
        }
    }
    if (entries.isEmpty()) {
        return;
    }
    TableModel *model = mDoc->editor()->getModel(TableId::Palette);
    auto macro = std::make_unique<undo::Macro>(tr("Load %1 palette(s)").arg(entries.size()));
    for (int row : entries.keys()) {
        macro->add(
            std::make_unique<AssignCommand>(row, model, PaletteRecord(entries[row], nullptr)));
    }
    if (!mDoc->undoStack()->push(std::move(macro))) {
        QMessageBox::critical(this, tr("Error"), tr("Could not import palettes"));
    }
}

void ColorTableWindow::savePalette()
{
    const QModelIndex &index = mTableView->selectionModel()->currentIndex();
    if (!index.isValid()) {
        return;
    }
    QStringList filters;
    filters << tr("Portable Network Graphics (*.png)") << tr("Any files (*.*)");
    const QString &savePath = QFileDialog::getSaveFileName(this,
                                                           tr("Save palette"),
                                                           QString(),
                                                           filters.join(";;"));
    if (!savePath.isNull()) {
        TableModel *model = mDoc->editor()->getModel(TableId::Palette);
        PaletteRecord record;
        record.assign(*model->getRecord(model->getIdOf(index.row())));
        QImage image(static_cast<int>(record.colors().size()), 1, QImage::Format_RGB32);
        for (int i = 0; i < image.width(); ++i) {
            image.setPixel(i, 0, record.colors().at(i).rgb());
        }
        if (!image.save(savePath)) {
            mDoc->logModel()->error("could not save image to file %s", savePath.toUtf8().data());
        }
    }
}

void ColorTableWindow::changeHue()
{
    auto selection = mTableView->selectionModel()->selectedIndexes();
    if (selection.empty()) {
        return;
    }
    QPointer<QColorDialog> dlg = new QColorDialog(this);
    dlg->exec();
    if (dlg == nullptr || dlg->result() != QDialog::Accepted) {
        return;
    }
    QColor newColor = dlg->selectedColor();
    auto macro = std::make_unique<undo::Macro>(tr("Colorize %1 colors").arg(selection.size()));
    for (const QModelIndex &index : selection) {
        if (index.data().type() == QVariant::Color) {
            QColor original = index.data().value<QColor>();
            original.setHsl(newColor.hslHue(), original.hslSaturation(), original.lightness());
            macro->add(std::make_unique<ModifyCommand>(index,
                                                       original,
                                                       mDoc->editor()->getModel(TableId::Palette)));
        }
    }
    if (!macro->empty()) {
        if (!mDoc->undoStack()->push(std::move(macro))) {
            QMessageBox::critical(this, tr("Error"), tr("Could not apply color"));
        }
    }
}

void ColorTableWindow::selectionChanged(const QItemSelection &selected,
                                        const QItemSelection &deselected)
{
    Q_UNUSED(selected);
    Q_UNUSED(deselected);

    auto selection = mTableView->selectionModel()->selection();
    std::set<int> selectedRows;
    for (auto index : selection.indexes()) {
        selectedRows.insert(index.row());
    }

    emit gotSingleSelectedRow(selectedRows.size() == 1);
    emit gotAnySelection(selectedRows.size() > 0);
}

void ColorTableWindow::route(ChangeHue *action)
{
    connect(action, &QAction::triggered, this, &ColorTableWindow::changeHue);
    connect(this, &ColorTableWindow::gotAnySelection, action, &QAction::setEnabled);
    mTableView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void ColorTableWindow::unroute(ChangeHue *action)
{
    disconnect(action, nullptr, this, nullptr);
    disconnect(this, nullptr, action, nullptr);
    mTableView->removeAction(action);
    action->setDisabled(true);
}

void ColorTableWindow::route(LoadPalette *action)
{
    connect(action, &QAction::triggered, this, &ColorTableWindow::loadPalette);
    connect(this, &ColorTableWindow::gotSingleSelectedRow, action, &QAction::setEnabled);
    mTableView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void ColorTableWindow::unroute(LoadPalette *action)
{
    disconnect(action, nullptr, this, nullptr);
    disconnect(this, nullptr, action, nullptr);
    mTableView->removeAction(action);
    action->setDisabled(true);
}

void ColorTableWindow::route(SavePalette *action)
{
    connect(action, &QAction::triggered, this, &ColorTableWindow::savePalette);
    connect(this, &ColorTableWindow::gotSingleSelectedRow, action, &QAction::setEnabled);
    mTableView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void ColorTableWindow::unroute(SavePalette *action)
{
    disconnect(action, nullptr, this, nullptr);
    disconnect(this, nullptr, action, nullptr);
    mTableView->removeAction(action);
    action->setDisabled(true);
}

} // namespace gmtool
