#include "animationgraphicsitem.h"

#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>

#include "animationeditor.h"
#include "animationpivotgraphicsitem.h"
#include "assigncommand.h"
#include "nodeattribute.h"
#include "tableid.h"
#include "tablemodel.h"
#include "viewstate.h"

namespace gmtool {

AnimationGraphicsItem::AnimationGraphicsItem(AnimationEditor *editor,
                                             const RecordId &id,
                                             SceneNode *parent)
    : SceneNode(parent)
    , mRecordId(id)
    , mEditor(editor)
{
    setData(NodeAttribute::RecordId, QVariant::fromValue(id));
    setData(NodeAttribute::TableId, QVariant::fromValue(TableId::Layout));

    mPivot = new AnimationPivotGraphicsItem(editor, id, this);

    mRecord = mEditor->getModel(TableId::Layout)->getRecordAs<AnimationRecord>(mRecordId);

    connect(mRecord,
            &AnimationRecord::positionChanged,
            this,
            &AnimationGraphicsItem::positionChanged);

    setPos(mRecord->rect().topLeft());
}

QRectF AnimationGraphicsItem::boundingRect() const
{
    return QRectF(0, 0, mRecord->width(), mRecord->height());
}

void AnimationGraphicsItem::setViewState(const ViewState &state)
{
    mPivot->setVisible(state.showOverlay);
}

void AnimationGraphicsItem::paint(QPainter *painter,
                                  const QStyleOptionGraphicsItem *option,
                                  QWidget *widget)
{
    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, false);
    QPen pen;
    pen.setCosmetic(true);
    if (isSelected()) {
        pen.setColor(QColor(255, 0, 0));
    }
    painter->setPen(pen);
    painter->drawRect(boundingRect());
    painter->restore();
}

void AnimationGraphicsItem::reloadModelData()
{
    positionChanged();
}

std::unique_ptr<AbstractRecord> AnimationGraphicsItem::movedRecord(const QPoint &toPoint)
{
    return mRecord->movedRecord(toPoint.x(), toPoint.y());
}

void AnimationGraphicsItem::positionChanged()
{
    prepareGeometryChange();
    setPos(mRecord->rect().topLeft());
}

} // namespace gmtool
