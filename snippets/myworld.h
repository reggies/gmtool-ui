#pragma once

#include <string>
#include <sstream>
#include <vector>

#include <boost/python.hpp>

namespace gmtool {
enum class SpriteType {BodyPart, Glyph, TilePart, Bitmap};
enum class PaletteId {Dummy, Blue, Red, Orange, Yellow, Purple, Black, Cyan, Green, Dummy2};

struct Rect {};
struct Image {};
struct Rgba {};
struct SpriteDescriptor {};

class Sprite {
public:
    Rect rect() const;
    Image image() const;
    SpriteType type() const;
    int bearingY() const;
    SpriteDescriptor descriptor() const;
private:
    Rect mRect;
    Image mImage;
    int mBearingY;
    SpriteType mType;
    Document *mDoc;
private:
    friend class World;
    Sprite(Document *doc);
};

class Palette {
public:
    std::vector<Rgba> colors() const;
    void setColor(int index, Rgba color);
private:
    std::vector<Rgba> mColors;
    Document *mDoc;
protected:
    friend class World;
    Palette(Document *doc);
};

class World {
public:
    std::vector<Sprite*> sprites() const;
    std::vector<Palette*> palettes() const;
    Palette* getPaletteById(PaletteId id) const;
    void removeSprite(int index);
    void addSprite(int index, SpriteDescriptor sprite);
protected:
    explicit World(Document *doc);
private:
    Document *mDoc;
};

}
