#pragma once

#include <QGraphicsItem>

#include "bitmaprecord.h"
#include "recordid.h"
#include "scenenode.h"

namespace gmtool {
class DocumentEditor;
class BitmapEditor;
class BitmapPivotGraphicsItem;

class BitmapGraphicsItem : public SceneNode
{
public:
    explicit BitmapGraphicsItem(BitmapEditor *editor,
                                const RecordId &id,
                                SceneNode *parent = nullptr);

    void setViewState(const ViewState &state) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    std::unique_ptr<AbstractRecord> movedRecord(const QPoint &toPoint) override;

private:
    RecordId mRecordId;
    BitmapEditor *mEditor;
    const BitmapRecord *mRecord;
};

} // namespace gmtool
