#pragma once

#include <algorithm>
#include <map>
#include <set>
#include <vector>

#include "abstractrecord.h"
#include "recordid.h"

namespace gmtool {

template<class RecordT>
class RecordSet
{
private:
    using storage_type = std::map<RecordId, RecordT>;

public:
    using const_iterator = typename storage_type::const_iterator;
    using iterator = typename storage_type::iterator;
    using value_type = typename storage_type::value_type;

public:
    RecordSet() = default;

    std::set<RecordId> keys() const { return {mIds.begin(), mIds.end()}; }

    std::size_t size() const { return mIds.size(); }

    template<typename SizeT>
    RecordId getIdAt(SizeT position) const
    {
        return mIds.at(position);
    }

    const RecordT &get(const RecordId &id) const { return mEntries.at(id); }

    RecordT &get(const RecordId &id) { return mEntries.at(id); }

    void set(const RecordId &id, const AbstractRecord &record) { mEntries.at(id).assign(record); }

    template<typename SizeT>
    RecordId insert(const AbstractRecord &record, SizeT position)
    {
        const RecordId id(mIdCounter);
        ++mIdCounter;

        mEntries[id].assign(record);
        mIds.insert(mIds.begin() + position, id);

        return id;
    }

    RecordId append(const AbstractRecord &record) { return insert(record, size()); }

    void remove(const RecordId &id)
    {
        auto it = mEntries.find(id);
        if (it != mEntries.end()) {
            mIds.erase(std::remove(mIds.begin(), mIds.end(), id), mIds.end());
            mEntries.erase(it);
        }
    }

    bool contains(const RecordId &id) const { return mEntries.find(id) != mEntries.end(); }

    void clear()
    {
        mEntries.clear();
        mIds.clear();
    }

    const_iterator begin() const { return mEntries.begin(); }

    const_iterator end() const { return mEntries.end(); }

    iterator begin() { return mEntries.begin(); }

    iterator end() { return mEntries.end(); }

    using id_storage_type = typename std::vector<RecordId>;
    using id_iterator = typename id_storage_type::iterator;
    using id_value_type = typename id_storage_type::value_type;

    id_iterator idBegin() { return mIds.begin(); }

    id_iterator idEnd() { return mIds.end(); }

private:
    int mIdCounter = 0;

    std::vector<RecordId> mIds;
    std::map<RecordId, RecordT> mEntries;

private:
    RecordSet(const RecordSet &) = delete;
    RecordSet &operator=(const RecordSet &) = delete;
};

} // namespace gmtool
