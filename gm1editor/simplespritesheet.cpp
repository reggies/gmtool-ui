#include "simplespritesheet.h"

#include "tableadapter.h"
#include "tablemodel.h"

namespace gmtool {

SimpleSpriteSheet::SimpleSpriteSheet(TableModel *palette)
    : mPalette(palette)
    , mSpriteTable(new TableModel(nullptr))
{
    std::unique_ptr<TableAdapter<SpriteRecord>> images(new TableAdapter<SpriteRecord>(mSprites));
    mSpriteTable->reset(std::unique_ptr<AbstractTable>(images.release()));
}

SimpleSpriteSheet::~SimpleSpriteSheet() = default;

void SimpleSpriteSheet::append(const SpriteRecord &record)
{
    mSprites.append(*record.clone());
}

} // namespace gmtool
