#include "fonteditor.h"

#include <boost/range/adaptor/map.hpp>

#include <memory>
#include <vector>

#include "basicspriteloader.h"
#include "columns.h"
#include "fontgraphicsitem.h"
#include "logmodel.h"
#include "nodefactory.h"
#include "propertiesmodel.h"
#include "spritegraphicsitem.h"
#include "spriteplotter.h"
#include "tableadapter.h"
#include "tablemodel.h"

namespace gmtool {

FontEditor::FontEditor()
    : mEntriesTableModel(new TableModel(nullptr))
    , mSpritesTableModel(new TableModel(nullptr))
    , mPropertiesModel(new PropertiesModel)
{
    setupEntriesTableModel();
    setupSpritesTableModel();
}

FontEditor::~FontEditor() = default;

void FontEditor::setupEntriesTableModel()
{
    std::unique_ptr<TableAdapter<FontRecord>> adapter(new TableAdapter<FontRecord>(mEntries));
    adapter->addColumn<columns::MutableLeft<FontRecord>>();
    adapter->addColumn<columns::MutableTop<FontRecord>>();
    adapter->addColumn<columns::MutableWidth<FontRecord>>();
    adapter->addColumn<columns::MutableHeight<FontRecord>>();
    adapter->addColumn<columns::MutableFontBearing<FontRecord>>();
    mEntriesTableModel->reset(std::unique_ptr<AbstractTable>(adapter.release()));
}

void FontEditor::setupSpritesTableModel()
{
    std::unique_ptr<TableAdapter<SpriteRecord>> adapter(new TableAdapter<SpriteRecord>(mSprites));
    adapter->addColumn<columns::MutableLeft<SpriteRecord>>();
    adapter->addColumn<columns::MutableTop<SpriteRecord>>();
    adapter->addColumn<columns::ConstWidth<SpriteRecord>>();
    adapter->addColumn<columns::ConstHeight<SpriteRecord>>();
    mSpritesTableModel->reset(std::unique_ptr<AbstractTable>(adapter.release()));
}

TableModel *FontEditor::getModel(TableId table)
{
    switch (table) {
    case TableId::Gm1_Glyph:
    case TableId::Layout:
        return mEntriesTableModel.get();
    case TableId::Palette:
        return nullptr;
    case TableId::Spritesheet:
        return mSpritesTableModel.get();
    case TableId::Invalid:
    case TableId::Gm1_Animation:
    case TableId::Gm1_Bitmap:
    case TableId::Gm1_Tile:
    case TableId::Animations:
        return nullptr;
    }
    return nullptr;
}

PropertiesModel *FontEditor::getPropertiesModel()
{
    return mPropertiesModel.get();
}

std::unique_ptr<AbstractRecord> FontEditor::createBlankEntry()
{
    FontRecord record;
    record.setRect(QRect(0, 0, 20, 30));
    return record.clone();
}

std::unique_ptr<NodeFactory> FontEditor::createLayoutFactory()
{
    std::unique_ptr<NodeFactory> f(new NodeFactory);
    f->setNodeCreator([this](const RecordId &id) {
        return new FontGraphicsItem(this, id, nullptr);
    });
    return f;
}

std::unique_ptr<NodeFactory> FontEditor::createSpriteFactory()
{
    std::unique_ptr<NodeFactory> f(new NodeFactory);
    f->setNodeCreator([this](const RecordId &id) {
        return new SpriteGraphicsItem(this, id, nullptr);
    });
    return f;
}

std::unique_ptr<SpriteLoader> FontEditor::createSpriteLoader()
{
    auto loader = new BasicSpriteLoader(this, TransparencyMode::Alpha, 0, QImage::Format_ARGB32);
    return std::unique_ptr<SpriteLoader>(loader);
}

void createGlyphs(const Gm1FileData &fileData, RecordSet<FontRecord> *result)
{
    for (const Gm1FileEntry &entry : fileData.Entries) {
        FontRecord record;
        record.setLeft(entry.entryRect().left());
        record.setTop(entry.entryRect().top());
        record.setWidth(entry.entryRect().width());
        record.setHeight(entry.entryRect().height());
        record.setBearing(entry.fontBearing());
        result->append(record);
    }
}

bool FontEditor::load(const Gm1FileData &fileData, LogModel *log)
{
    Q_UNUSED(log);

    mGm1 = fileData;

    //! load plot image
    SpriteRecord plot;
    plot.setPosition(mGm1.plotOrigin());
    plot.setImage(mGm1.plotImage());

    mSprites.clear();
    mSprites.insert(plot, 0);
    setupSpritesTableModel();

    mEntries.clear();
    createGlyphs(mGm1, &mEntries);
    setupEntriesTableModel();

    mPropertiesModel->reset();

    return true;
}

bool FontEditor::save(Gm1FileData *fileData, LogModel *log)
{
    Gm1FileData gm1 = mGm1;
    gm1.setEntryClass(GM1_ENTRY_CLASS_GLYPH);

    gm1.setEntryCount(mEntries.size());
    for (size_t i = 0; i < mEntries.size(); ++i) {
        const RecordId &id = mEntries.getIdAt(i);
        const FontRecord &record = mEntries.get(id);
        gm1.Entries.at(i).setEntryRect(record.rect());
        gm1.Entries.at(i).setFontBearing(record.bearing());
    }

    SpritePlotter plotter;

    for (const SpriteRecord &record : mSprites | boost::adaptors::map_values) {
        //! \todo not sure whether I need the mask
        plotter.addImage(record.image(), record.rect(), record.mask());
    }

    QRect entriesRect;
    for (const FontRecord &record : mEntries | boost::adaptors::map_values) {
        entriesRect |= record.rect();
    }

    if (!plotter.render(QImage::Format_ARGB32, entriesRect, log)) {
        log->error("error rendering final plot image");
        return false;
    }

    gm1.setPlotImage(plotter.plot());
    gm1.setPlotOrigin(plotter.origin());

    *fileData = gm1;

    return true;
}

bool FontEditor::renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result)
{
    SpritePlotter plotter;

    for (const RecordId &id : ids) {
        const SpriteRecord &record = mSprites.get(id);
        //! \todo do I need mask?
        plotter.addImage(record.image(), record.rect(), QBitmap());
    }

    //! note that we can't use anything without alpha channel since
    //! then all transparency will be lost.
    if (!plotter.render(QImage::Format_ARGB32, QRect(), log)) {
        return false;
    }

    *result = plotter.plot();

    return true;
}

std::unique_ptr<AnimationFactory> FontEditor::createAnimationFactory()
{
    return nullptr;
}

} // namespace gmtool
