#pragma once

#include <QRect>

#include "abstractrecord.h"

namespace gmtool {

class AnimationRecordData
{
public:
    QRect mRect;
};

class AnimationRecord : public AbstractRecord, public AnimationRecordData
{
    Q_OBJECT

public:
    explicit AnimationRecord(const AnimationRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , AnimationRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new AnimationRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        *((AnimationRecordData *) this) = dynamic_cast<const AnimationRecordData &>(other);
        emit positionChanged();
    }

    std::unique_ptr<AbstractRecord> movedRecord(int x, int y) const
    {
        AnimationRecordData data(*this);
        data.mRect.moveLeft(x);
        data.mRect.moveTop(y);
        return AnimationRecord(data, parent()).clone();
    }

    QRect rect() const { return mRect; }
    void setRect(const QRect &rect)
    {
        mRect = rect;
        emit positionChanged();
    }

    int left() const { return mRect.x(); }
    int width() const { return mRect.width(); }
    int top() const { return mRect.y(); }
    int height() const { return mRect.height(); }

    void setLeft(int value)
    {
        mRect.moveLeft(value);
        emit positionChanged();
    }
    void setWidth(int value)
    {
        mRect.setWidth(value);
        emit positionChanged();
    }
    void setTop(int value)
    {
        mRect.moveTop(value);
        emit positionChanged();
    }
    void setHeight(int value)
    {
        mRect.setHeight(value);
        emit positionChanged();
    }

signals:
    void positionChanged();
};

} // namespace gmtool
