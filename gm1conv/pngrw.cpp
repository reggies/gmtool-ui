#include "pngrw.hpp"

#include <memory>
#include <algorithm>
#include <vector>
#include <iostream>
#include <functional>

#include <SDL.h>
#include <png.h>

#include "utils.hpp"

namespace
{
    struct SdlToPngColorConverter
    {
        png_color operator()(const SDL_Color& color)
        {
            png_color result;
            result.red = color.r;
            result.green = color.g;
            result.blue = color.b;
            return result;
        }
    };

    struct ScopedGuard
    {
        ScopedGuard(std::function<void()> at_exit)
            : at_exit_(at_exit) {}
        ~ScopedGuard()
        {
            at_exit_();
        }
    private:
        std::function<void()> at_exit_;
    };
}

struct PngRwPrivate
{
    union
    {
        std::istream* is;
        std::ostream* os;
    };
    std::vector<std::string> errors;
    std::vector<std::string> warnings;
};

static void png_error_func(png_structp png_ptr, png_const_charp message)
{
    PngRwPrivate* p = static_cast<PngRwPrivate*>(png_get_io_ptr(png_ptr));
    p->errors.push_back(std::string("png error: ") + message);
	longjmp(png_jmpbuf(png_ptr), 1);
}

static void png_warn_func(png_structp png_ptr, png_const_charp message)
{
    PngRwPrivate* p = static_cast<PngRwPrivate*>(png_get_io_ptr(png_ptr));
    p->warnings.push_back(std::string("png warning: ") + message);
}

static void png_read_func(png_structp png_ptr, png_bytep data, png_size_t length)
{
    PngRwPrivate* p = static_cast<PngRwPrivate*>(png_get_io_ptr(png_ptr));
	p->is->read(reinterpret_cast<char*>(data), length);
}

static void png_write_func(png_structp png_ptr, png_bytep data, png_size_t length)
{
    PngRwPrivate* p = static_cast<PngRwPrivate*>(png_get_io_ptr(png_ptr));
    p->os->write(reinterpret_cast<char*>(data), length);
}

static void png_flush_func(png_structp png_ptr)
{
    (void)(png_ptr);
}

PngRw::PngRw()
{
    p_ = new PngRwPrivate;
}

PngRw::~PngRw()
{
    delete p_;
}

std::vector<std::string> PngRw::errors() const
{
    return p_->errors;
}

std::vector<std::string> PngRw::warnings() const
{
    return p_->warnings;
}

SDL_Surface* PngRw::read(std::istream* is)
{
    p_->errors.clear();
    p_->warnings.clear();

    p_->is = is;

    png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, png_error_func, png_warn_func);
	if(png_ptr == NULL) {
        p_->errors.push_back("could not allocate png read struct");
        return NULL;
    }
	png_infop info_ptr = png_create_info_struct(png_ptr);
    ScopedGuard scoped_png([&png_ptr, &info_ptr]() {
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);
    });
	if(info_ptr == NULL) {
        p_->errors.push_back("could not allocate info ptr");
        return NULL;
    }
    if(setjmp(png_jmpbuf(png_ptr))) {
        p_->errors.push_back("read aborted");
        return NULL;
    }
	png_set_read_fn(png_ptr, p_, png_read_func);
	png_read_info(png_ptr, info_ptr);

    png_uint_32 width = 0;
    png_uint_32 height = 0;
    int bit_depth = 0;
    int color_type = 0;
    int interlace_type = 0;

    png_get_IHDR(png_ptr, info_ptr, &width, &height,
                 &bit_depth, &color_type, &interlace_type, NULL, NULL);

    png_set_strip_16(png_ptr);
    png_set_packing(png_ptr);
    if(color_type == PNG_COLOR_TYPE_GRAY) {
        png_set_expand(png_ptr);
    }
    png_read_update_info(png_ptr, info_ptr);
    png_get_IHDR(png_ptr, info_ptr, &width, &height,
                 &bit_depth, &color_type, &interlace_type, NULL, NULL);

    Uint32 format;

	switch(color_type) {
    case PNG_COLOR_TYPE_PALETTE:
        format = SDL_PIXELFORMAT_INDEX8;
        break;

    case PNG_COLOR_TYPE_RGB:
        format = SDL_PIXELFORMAT_RGB24;
        break;

    case PNG_COLOR_TYPE_RGB_ALPHA:
        format = SDL_PIXELFORMAT_ABGR8888;
        break;
        
    case PNG_COLOR_TYPE_GRAY:
    case PNG_COLOR_TYPE_GRAY_ALPHA:
    default:
        p_->errors.push_back("unsupported color type");
        return NULL;
	}

    std::unique_ptr<SDL_PixelFormat, PixelFormatDeleter> fmt;
    fmt.reset(SDL_AllocFormat(format));
    if(!fmt) {
        return NULL;
    }

    std::unique_ptr<SDL_Surface, SurfaceDeleter> surface;
    surface.reset(SDL_CreateRGBSurface(0, width, height,
                                       fmt->BitsPerPixel,
                                       fmt->Rmask,
                                       fmt->Gmask,
                                       fmt->Bmask,
                                       fmt->Amask));

    if(!surface) {
        p_->errors.push_back(std::string("could not allocate surface: ") + SDL_GetError());
        return NULL;
    }

    std::vector<png_bytep> rows(height);
    SdlSurfaceLock lock(surface.get());
    for(png_uint_32 y = 0; y < height; ++y) {
        rows[y] = static_cast<png_bytep>(surface->pixels) + surface->pitch*y;
    }
    png_read_image(png_ptr, rows.data());

    /* read palette */
    if(surface->format->palette) {
        png_colorp colors;
        int num_colors;

        if(PNG_INFO_PLTE != png_get_PLTE(png_ptr, info_ptr, &colors, &num_colors)) {
            p_->errors.push_back("could not read PLTE chunk");
			return NULL;
		}

        std::unique_ptr<SDL_Palette, PaletteDeleter> palette;
        palette.reset(SDL_AllocPalette(num_colors));
        if(!palette) {
            p_->errors.push_back("could not allocate palette");
            return NULL;
        }

        std::vector<SDL_Color> sdl_colors;
        if (color_type == PNG_COLOR_TYPE_GRAY) {
            sdl_colors.resize(256);
            for (size_t i = 0; i < sdl_colors.size(); ++i) {
                sdl_colors[i].r = i;
                sdl_colors[i].g = i;
                sdl_colors[i].b = i;
            }
        } else if (num_colors > 0) {
            sdl_colors.resize(num_colors);
            for (int i = 0; i < num_colors; ++i) {
                sdl_colors[i].b = colors[i].blue;
                sdl_colors[i].g = colors[i].green;
                sdl_colors[i].r = colors[i].red;
            }
        }

        SDL_SetPaletteColors(palette.get(), sdl_colors.data(), 0, sdl_colors.size());
        SDL_SetSurfacePalette(surface.get(), palette.get());
    }
    
	return surface.release();
}


bool PngRw::write(SDL_Surface* surface, std::ostream* os)
{
    p_->errors.clear();
    p_->warnings.clear();

    p_->os = os;

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, png_error_func, png_warn_func);
    if (png_ptr == NULL) {
        p_->errors.push_back("could not allocate png write struct");
        return false;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if(info_ptr == NULL) {
        p_->errors.push_back("could not allocate png info");
        return false;
    }
    ScopedGuard scoped_png([&png_ptr, &info_ptr]() {
        png_destroy_write_struct(&png_ptr, &info_ptr);
    });

    if(setjmp(png_jmpbuf(png_ptr))) {
        p_->errors.push_back("write aborted");
		return false;
	}
    png_set_write_fn(png_ptr, p_, png_write_func, png_flush_func);

    SdlSurfaceLock locker(surface);
    std::vector<png_color> colors;
    if(surface->format->palette) {
        colors.resize(surface->format->palette->ncolors);
        std::transform(surface->format->palette->colors,
                       surface->format->palette->colors + colors.size(),
                       colors.begin(),
                       SdlToPngColorConverter());
    }

    unsigned int color_type;
    switch(surface->format->format)
    {
    case SDL_PIXELFORMAT_INDEX8:
        color_type = PNG_COLOR_TYPE_PALETTE;
        break;

    case SDL_PIXELFORMAT_RGB24:
        color_type = PNG_COLOR_TYPE_RGB;
        break;
        
    case SDL_PIXELFORMAT_ABGR8888:
        color_type = PNG_COLOR_TYPE_RGB_ALPHA;
        break;
        
    default:
        return false;
    }

    png_set_IHDR(png_ptr, info_ptr, surface->w, surface->h, 8, color_type,
                 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    
    if(color_type == PNG_COLOR_TYPE_PALETTE) {
        png_set_PLTE(png_ptr, info_ptr, colors.data(), colors.size());
    }

    std::vector<png_bytep> rows(surface->h);
    for(int i = 0; i < surface->h; ++i) {
        rows[i] = static_cast<png_bytep>(surface->pixels) + surface->pitch*i;
    }

    png_write_info(png_ptr, info_ptr);
    png_write_image(png_ptr, rows.data());
    png_write_end(png_ptr, info_ptr);
    return true;
}
