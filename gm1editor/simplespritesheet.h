#pragma once

#include "recordset.h"
#include "spriterecord.h"
#include "spritesheet.h"

#include <memory>

namespace gmtool {
class TableModel;

class SimpleSpriteSheet : public ISpriteSheet
{
public:
    explicit SimpleSpriteSheet(TableModel *palette);
    ~SimpleSpriteSheet() override;

    TableModel *getSpritesheet() override { return mSpriteTable.get(); }
    TableModel *getPalette() override { return mPalette; }

    void append(const SpriteRecord &record);

private:
    RecordSet<SpriteRecord> mSprites;
    std::unique_ptr<TableModel> mSpriteTable;
    TableModel *mPalette;
};

} // namespace gmtool
