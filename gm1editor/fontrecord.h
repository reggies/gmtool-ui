#pragma once

#include "abstractrecord.h"

#include <QLine>
#include <QRect>

namespace gmtool {
class FontRecordData
{
public:
    FontRecordData()
        : mBearing(0)
    {}

    QRect rect() const { return mRect; }

    void setRect(const QRect &rect) { mRect = rect; }

    int width() const { return mRect.width(); }

    void setWidth(int width) { mRect.setWidth(width); }

    int height() const { return mRect.height(); }

    void setHeight(int height) { mRect.setHeight(height); }

    int top() const { return mRect.top(); }

    void setTop(int top) { mRect.moveTop(top); }

    int left() const { return mRect.left(); }

    void setLeft(int left) { mRect.moveLeft(left); }

    int bearing() const { return mBearing; }

    void setBearing(int bearing) { mBearing = bearing; }

    QLine baseLine() const { return QLine(0, mBearing, mRect.width(), mBearing); }

private:
    QRect mRect;
    int mBearing;
};

class FontRecord : public AbstractRecord, public FontRecordData
{
    Q_OBJECT

public:
    explicit FontRecord(const FontRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , FontRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new FontRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        *((FontRecordData *) this) = dynamic_cast<const FontRecordData &>(other);
    }

    std::unique_ptr<AbstractRecord> movedRecord(int x, int y) const
    {
        FontRecordData data(*this);
        data.setTop(y);
        data.setLeft(x);
        return FontRecord(data, parent()).clone();
    }
};

} // namespace gmtool
