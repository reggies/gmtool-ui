#pragma once

#include <QSyntaxHighlighter>

#include <string>

namespace gmtool {

class ReplHighlighter : public QSyntaxHighlighter {
    Q_OBJECT

public:
    ReplHighlighter(QTextDocument *parent);

    void highlightUsingFormat(const QString &text,
                              const QRegularExpression &expr,
                              const QTextCharFormat &format);

protected:
    void highlightBlock(const QString &text) override;
};

}
