#include "animationloaddialog.h"

#include "animationeditor.h"
#include "coloredit.h"
#include "emptynode.h"
#include "gridgraphicsview.h"
#include "gridscene.h"
#include "palettecombobox.h"
#include "paletteitemdelegate.h"
#include "simplespritesheet.h"
#include "spritegraphicsitem.h"
#include "spritesheet.h"
#include "tableid.h"
#include "tablemodel.h"
#include "viewstate.h"

#include <QDialogButtonBox>
#include <QEventLoop>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QHBoxLayout>
#include <QImageReader>
#include <QPointer>
#include <QSplitter>
#include <QToolBar>
#include <QToolButton>
#include <QVBoxLayout>

#include <QDebug>

/*
  General note on the animation loading:

  The source image could be of the following properties:

  1. Indexed image;
    1.1. With known palette structure (e.g. saved by gmtool);
        1.1.1. With known image structure (e.g. saved by gmtool);
        1.1.2. With unknown image structure (e.g. edited by gimp);
    1.2. With unknown palette structure (e.g. edited by gimp);
        1.1.1. With known image structure;
        1.1.2. With unknown image structure;
  2. True color (RGB32, RGB24, RGB555, etc);
    2.1. With alpha;
    2.2. Without alpha;

 */

namespace gmtool {

AnimationLoadDialog::AnimationLoadDialog(AnimationEditor *editor,
                                         const std::vector<boost::filesystem::path> &loadPaths,
                                         QWidget *parent)
    : QDialog(parent)
    , mEditor(editor)
{
    QPoint topLeft(0, 0);
    for (const auto &path : loadPaths) {
        QImageReader reader(QString::fromStdString(path.string()));
        QImage frame;
        QSize imageSize = reader.size();
        int maximumHeight = 0;
        // FIXME: add support for background color
        // if (reader.supportsOption(QImageIOHandler::BackgroundColor)) {
        //     reader.setBackgroundColor(QColor(255, 0, 255, 0));
        // }
        // FIXME: reader.size() and reader.currentFrameRect() must be handled
        while (reader.read(&frame)) {
            if (!frame.isNull()) {
                mImages.emplace_back(path, frame, topLeft);
                topLeft.rx() += imageSize.isValid() ? imageSize.width() : frame.width();
                if (!imageSize.isValid()) {
                    maximumHeight = qMax(maximumHeight, frame.height());
                }
            } else {
                // FIXME: out of memory
            }
        }
        topLeft.ry() += imageSize.isValid() ? imageSize.height() : maximumHeight;
        topLeft.rx() = 0;
    }

    mOriginalScene = new GridScene(this);
    mScene = new GridScene(this);

    mCheckDither = new QCheckBox(tr("Dither"), this);
    connect(mCheckDither, &QCheckBox::stateChanged, this, &AnimationLoadDialog::rebuild);

    mCheckMatchPalette = new QGroupBox(tr("Match palette"), this);
    mCheckMatchPalette->setFlat(true);
    mCheckMatchPalette->setCheckable(true);
    connect(mCheckMatchPalette, &QGroupBox::clicked, this, &AnimationLoadDialog::rebuild);

    mBoxPalettes = new PaletteComboBox(editor, this);
    void (QComboBox::*currentIndexChanged)(int) = &QComboBox::currentIndexChanged;
    connect(mBoxPalettes, currentIndexChanged, this, &AnimationLoadDialog::rebuild);

    mCheckZeroIndexTransparency = new QGroupBox(tr("Zero index as transparency"), this);
    mCheckZeroIndexTransparency->setFlat(true);
    mCheckZeroIndexTransparency->setCheckable(true);
    connect(mCheckZeroIndexTransparency, &QGroupBox::clicked, this, &AnimationLoadDialog::rebuild);

    mCheckOverrideTransparency = new QGroupBox(tr("Override transparency"), this);
    mCheckOverrideTransparency->setFlat(true);
    mCheckOverrideTransparency->setCheckable(true);
    connect(mCheckOverrideTransparency, &QGroupBox::clicked, this, &AnimationLoadDialog::rebuild);

    auto pickTransparentColorMode = new QAction(tr("Pick transparent color"), this);
    pickTransparentColorMode->setIcon(QIcon::fromTheme("eyedropper"));
    connect(pickTransparentColorMode, &QAction::triggered, this, &AnimationLoadDialog::pickColor);
    auto pickTransparentColorModeButton = new QToolButton(this);
    pickTransparentColorModeButton->setDefaultAction(pickTransparentColorMode);

    mTransparentColor = new ColorEdit(this);
    connect(mTransparentColor, &ColorEdit::currentColorChanged, this, &AnimationLoadDialog::rebuild);

    auto checkOverrideTransparencyLayout = new QHBoxLayout(mCheckOverrideTransparency);
    checkOverrideTransparencyLayout->addWidget(pickTransparentColorModeButton);
    checkOverrideTransparencyLayout->addWidget(mTransparentColor);
    mCheckOverrideTransparency->setLayout(checkOverrideTransparencyLayout);

    auto checkZeroIndexTransparencyLayout = new QHBoxLayout(mCheckZeroIndexTransparency);
    checkZeroIndexTransparencyLayout->addWidget(mCheckOverrideTransparency);
    mCheckZeroIndexTransparency->setLayout(checkZeroIndexTransparencyLayout);

    auto checkMatchPaletteLayout = new QHBoxLayout(mCheckMatchPalette);
    checkMatchPaletteLayout->addWidget(mCheckZeroIndexTransparency);
    mCheckMatchPalette->setLayout(checkMatchPaletteLayout);

    auto buttons = new QDialogButtonBox(this);
    buttons->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttons, &QDialogButtonBox::accepted, this, &AnimationLoadDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &AnimationLoadDialog::reject);

    auto toolbar = new QToolBar(this);
    toolbar->addWidget(mCheckDither);
    toolbar->addWidget(mBoxPalettes);
    toolbar->addWidget(mCheckMatchPalette);
    toolbar->addSeparator();
    toolbar->addWidget(buttons);
    toolbar->setMovable(false);
    toolbar->setOrientation(Qt::Vertical);

    mView = new GridGraphicsView(this);
    mView->setShowGrid(false);
    mView->setScene(mScene);

    mOriginalView = new GridGraphicsView(this);
    mOriginalView->setShowGrid(false);
    mOriginalView->setScene(mOriginalScene);

    auto splitter = new QSplitter(this);
    splitter->setOrientation(Qt::Vertical);
    splitter->setOpaqueResize(false);
    splitter->addWidget(mOriginalView);
    splitter->addWidget(mView);

    auto layout = new QHBoxLayout(this);
    layout->addWidget(splitter);
    layout->addWidget(toolbar);

    prepareOriginal();
    rebuild();
    setSizeGripEnabled(true);
    setMinimumSize(320, 240);
    mCheckMatchPalette->setChecked(false);
}

AnimationLoadDialog::~AnimationLoadDialog() = default;

QVector<QRgb> AnimationLoadDialog::activeColorTable() const
{
    const int currentPalette = mBoxPalettes->currentIndex();
    TableModel *model = mEditor->getModel(TableId::Palette);

    PaletteRecord palette;
    palette.assign(*model->getRecord(model->getIdOf(currentPalette)));

    QVector<QRgb> rgbs;
    for (const QColor &color : palette.colors())
        rgbs.push_back(color.rgb());

    return rgbs;
}

QImage convertRgbToIndexed(const QImage &image,
                           const QVector<QRgb> &rgbs,
                           bool zeroIndexAsTransparency,
                           Qt::ImageConversionFlags flags)
{
    /* Synopsis:
       rgb has 256 colors always. The game treats rgb[0] as
       a color key. If the image has an alpha we must not treat
       any of its pixels as rgb[0].

       0. Calculate an alpha bitmask for the original image;
       1. Remove rgb[0] and then perform a color space lookup;
       2. After match successfully found insert rgb[0] into palette and
       recalculate the result indexed image so that no pixel
       will ever point to rgb[0];
       3. Erase all pixels matching alpha bitmask.
     */

    if (zeroIndexAsTransparency) {
        return image.convertToFormat(QImage::Format_Indexed8, rgbs, flags);
    }

    QVector<QRgb> colorTable = rgbs;
    colorTable.erase(colorTable.begin());

    QImage tmp = image.convertToFormat(QImage::Format_Indexed8, colorTable, flags);

    for (int row = 0; row < tmp.height(); ++row) {
        uchar *scanLine = tmp.scanLine(row);
        for (int col = 0; col < tmp.width(); ++col) {
            Q_ASSERT(scanLine[col] != 255);
            scanLine[col]++;
        }
    }

    colorTable.insert(colorTable.begin(), rgbs[0]);

    tmp.setColorTable(colorTable);
    return tmp;
}

void AnimationLoadDialog::prepareOriginal()
{
    mOriginalScene->clear();
    for (const SourceImage &source : mImages) {
        auto item = new QGraphicsPixmapItem(QPixmap::fromImage(source.image));
        item->setPos(source.topLeft);
        mOriginalScene->addItem(item);
    }
}

void AnimationLoadDialog::rebuild()
{
    mScene->clear();
    mSpriteSheet.reset(new SimpleSpriteSheet(mEditor->getModel(TableId::Palette)));
    for (const SpriteRecordData &sprite : sprites()) {
        mSpriteSheet->append(SpriteRecord(sprite, nullptr));
    }
    mLayer = new EmptyNode(nullptr);
    mLayer->setPropogatePaletteChanges(true);
    mScene->addItem(mLayer);
    mLayer->setFlag(QGraphicsItem::ItemHasNoContents, true);
    for (int i = 0; i < mSpriteSheet->getSpritesheet()->rowCount(); ++i) {
        auto item = new SpriteGraphicsItem(mSpriteSheet.get(),
                                           mSpriteSheet->getSpritesheet()->getIdOf(i));
        item->setParentItem(mLayer);
        item->setZValue(i);
    }
    ViewState state;
    state.paletteId = mEditor->getModel(TableId::Palette)->getIdOf(mBoxPalettes->currentIndex());
    mLayer->setViewState(state);
}

std::vector<SpriteRecordData> AnimationLoadDialog::sprites() const
{
    const bool dither = mCheckDither->isChecked();
    const bool matchPalette = mCheckMatchPalette->isChecked();
    const bool zeroIndexAsTransparency = mCheckZeroIndexTransparency->isChecked();
    const bool overrideTransparency = mCheckOverrideTransparency->isChecked();
    Qt::ImageConversionFlags conversionFlags = Qt::AutoColor | Qt::NoOpaqueDetection;
    if (dither) {
        conversionFlags = Qt::PreferDither | Qt::DiffuseDither;
    }
    // FIXME: it seems that dither is applied regardless of these options

    QVector<QRgb> rgbs = activeColorTable();
    if (overrideTransparency) {
        rgbs[0] = mTransparentColor->currentColor().rgb();
    }

    std::vector<SpriteRecordData> sprites;
    for (const SourceImage &source : mImages) {
        QImage image = source.image;
        QBitmap maskBitmap;
        if (image.hasAlphaChannel()) {
            maskBitmap.convertFromImage(image.createAlphaMask());
        }
        if (dither) {
            image = image.convertToFormat(QImage::Format_RGB555, conversionFlags);
            image = image.convertToFormat(QImage::Format_RGB32, conversionFlags);
        }
        if (matchPalette) {
            image = image.convertToFormat(QImage::Format_RGB32, conversionFlags);
            image = convertRgbToIndexed(image, rgbs, zeroIndexAsTransparency, conversionFlags);
        } else {
            image = image.convertToFormat(QImage::Format_Indexed8, conversionFlags);
        }

        // FIXME: merge maskBitmap with overriden transparency mask

        SpriteRecord entry;
        if (maskBitmap.isNull()) {
            entry.setTransparencyMode(TransparencyMode::Color);
            entry.setTransparentPixel(0);
        } else {
            entry.setTransparencyMode(TransparencyMode::Alpha);
            entry.setMask(maskBitmap);
        }
        entry.setImage(image);
        entry.setFilePath(source.path);
        entry.setPosition(source.topLeft);

        sprites.emplace_back(entry);
    }

    return sprites;
}

void AnimationLoadDialog::pickColor()
{
    QEventLoop loop;

    QCursor oldCursor = mOriginalView->cursor();
    InteractionMode oldMode = mOriginalScene->interactionMode();
    QColor oldColor = mTransparentColor->currentColor();
    bool isAborted = false;

    connect(mOriginalScene, &GridScene::pickingReset, [&isAborted]() { isAborted = true; });
    connect(mOriginalScene, &GridScene::pickingReset, &loop, &QEventLoop::quit);
    connect(mOriginalScene, &GridScene::pickingFinished, &loop, &QEventLoop::quit);
    connect(mOriginalScene, &GridScene::pickingChanged, mTransparentColor, [this]() {
        if (auto pickedColor = mOriginalScene->pickedColor()) {
            mTransparentColor->setCurrentColor(*pickedColor);
        } else {
            // TODO use default transparent color
            mTransparentColor->setCurrentColor(QColor(255, 0, 255));
        }
    });

    mOriginalView->setCursor(Qt::CrossCursor);
    mOriginalScene->setInteractionMode(InteractionMode::Pick);
    mTransparentColor->blockSignals(true);

    QPointer<QObject> guard = this;
    (void) loop.exec();
    if (guard.isNull())
        return;

    auto newColor = mTransparentColor->currentColor();
    mTransparentColor->setCurrentColor(oldColor);
    mTransparentColor->blockSignals(false);
    if (!isAborted) {
        mTransparentColor->setCurrentColor(newColor);
    }
    mOriginalScene->setInteractionMode(oldMode);
    mOriginalView->setCursor(oldCursor);
}

} // namespace gmtool
