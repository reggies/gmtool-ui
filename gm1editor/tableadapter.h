#pragma once

#include <algorithm>
#include <memory>

#include "abstracttable.h"
#include "recordset.h"
#include "tablecolumn.h"

namespace gmtool {

/// Templated adapter of RecordSet for Qt's object model.
template<class RecordT>
class TableAdapter : public AbstractTable
{
public:
    TableAdapter(RecordSet<RecordT> &records)
        : mRecords(records)
    {}

    template<class Column, class... Args>
    void addColumn(Args &&... args)
    {
        mColumns.emplace_back();
        mColumns.back().reset(new Column(std::forward<Args>(args)...));
    }

    virtual bool setData(int row, int col, const QVariant &value) override
    {
        return mColumns.at(col)->setData(mRecords.get(mRecords.getIdAt(row)), value);
    }

    virtual QVariant displayData(int row, int col) const override
    {
        return mColumns.at(col)->displayData(mRecords.get(mRecords.getIdAt(row)));
    }

    virtual QVariant editData(int row, int col) const override
    {
        return mColumns.at(col)->editData(mRecords.get(mRecords.getIdAt(row)));
    }

    virtual int rowCount() const override { return static_cast<int>(mRecords.size()); }

    virtual int columnCount() const override { return static_cast<int>(mColumns.size()); }

    virtual Qt::ItemFlags flags(int column) const override { return mColumns.at(column)->flags(); }

    virtual QVariant headerData(int column) const override
    {
        return mColumns.at(column)->headerData();
    }

    virtual bool insertRows(int row, int count) override
    {
        for (int i = 0; i < count; ++i) {
            mRecords.insert(RecordT(), row + i);
        }
        return true;
    }

    virtual bool removeRows(int row, int count) override
    {
        for (int i = 0; i < count; ++i) {
            mRecords.remove(mRecords.getIdAt(row));
        }
        return true;
    }

    virtual RecordId getRecordIdByRow(int row) const override { return mRecords.getIdAt(row); }

    virtual const AbstractRecord *getRecord(const RecordId &id) const override
    {
        return &mRecords.get(id);
    }

    virtual AbstractRecord *getRecord(const RecordId &id) override { return &mRecords.get(id); }

    virtual void setRecord(int row, const AbstractRecord &record) override
    {
        mRecords.set(mRecords.getIdAt(row), record);
    }

    virtual void moveRows(int source, int count, int dest) override
    {
        if (source < dest) {
            std::rotate(mRecords.idBegin() + source,
                        mRecords.idBegin() + source + count,
                        mRecords.idBegin() + dest);
        } else {
            std::rotate(mRecords.idBegin() + dest,
                        mRecords.idBegin() + source,
                        mRecords.idBegin() + source + count);
        }
    }

private:
    RecordSet<RecordT> &mRecords;

    std::vector<std::unique_ptr<TableColumn<RecordT>>> mColumns;

    TableAdapter(const TableAdapter &) = delete;
    TableAdapter &operator=(const TableAdapter &) = delete;
};

} // namespace gmtool
