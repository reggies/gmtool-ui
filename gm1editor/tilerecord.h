#pragma once

#include <QColor>
#include <QDebug>
#include <QPoint>
#include <QRect>

#include "abstractrecord.h"

namespace gmtool {

class TileRecordData
{
public:
    TileRecordData() = default;

    auto tileHeight() const { return 16; }

    auto tileWidth() const { return 30; }

    auto widthStride() const { return 32; }

    auto heightStride() const { return 16; }

    auto rect() const
    {
        return QRect(mPos,
                     QSize(widthStride() * (mSize - 1) + tileWidth(),
                           mBoxHeight + heightStride() * mSize));
    }

    int width() const { return rect().width(); }
    int height() const { return rect().height(); }

    int left() const { return rect().x(); }
    void setTop(int top) { mPos.setY(top); }

    int size() const { return mSize; }

    void setSize(int size) { mSize = qMax(1, size); }

    int top() const { return rect().y(); }
    void setLeft(int left) { mPos.setX(left); }

    int boxHeight() const { return mBoxHeight; }
    void setBoxHeight(int boxHeight) { mBoxHeight = boxHeight; }

private:
    QPoint mPos;

    //! 1 - regular tile
    //! 2 - square 2x2
    //! 3 - square 3x3
    int mSize = 1;

    int mBoxHeight = 0;
};

class TileRecord : public AbstractRecord, public TileRecordData
{
    Q_OBJECT

public:
    explicit TileRecord(const TileRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , TileRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new TileRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        *((TileRecordData *) this) = dynamic_cast<const TileRecordData &>(other);
    }

    std::unique_ptr<AbstractRecord> movedRecord(int x, int y) const
    {
        TileRecordData data(*this);
        data.setTop(y);
        data.setLeft(x);
        return TileRecord(data, parent()).clone();
    }
};

} // namespace gmtool
