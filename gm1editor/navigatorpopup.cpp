#include "navigatorpopup.h"

#include <QDebug>
#include <QEvent>
#include <QMouseEvent>

namespace gmtool {

NavigatorPopup::NavigatorPopup(QWidget *parent)
    : QGraphicsView(parent)
{
    setWindowFlag(Qt::Popup, true);
    setCursor(Qt::SizeAllCursor);
}

NavigatorPopup::~NavigatorPopup() {}

void NavigatorPopup::mouseMoveEvent(QMouseEvent *event)
{
    QGraphicsView::mouseMoveEvent(event);
    emit mouseMoved(event->pos(), size());
}

void NavigatorPopup::mouseReleaseEvent(QMouseEvent *event)
{
    QGraphicsView::mouseReleaseEvent(event);
    hide();
}

bool NavigatorPopup::event(QEvent *e)
{
    if (e->type() == QEvent::MouseMove) {
        mouseMoveEvent(static_cast<QMouseEvent *>(e));
        e->accept();
        return true;
    }
    if (e->type() == QEvent::MouseButtonRelease) {
        mouseReleaseEvent(static_cast<QMouseEvent *>(e));
        e->accept();
        return true;
    }
    return QGraphicsView::event(e);
}

} // namespace gmtool
