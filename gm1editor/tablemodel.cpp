#include "tablemodel.h"

#include <QDebug>

namespace gmtool {

TableModel::TableModel(std::unique_ptr<AbstractTable> table, QObject *parent)
    : QAbstractItemModel(parent)
    , mTable(std::move(table))
{}

void TableModel::reset(std::unique_ptr<AbstractTable> table)
{
    beginResetModel();
    mTable = std::move(table);
    endResetModel();
}

int TableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return mTable->rowCount();
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if ((index.row() >= rowCount()) || (index.column() >= columnCount())) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return mTable->displayData(index.row(), index.column());
    } else if (role == Qt::EditRole) {
        return mTable->editData(index.row(), index.column());
    }

    return QVariant();
}

QModelIndex TableModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid())
        return QModelIndex();

    if (row < 0 || row >= rowCount())
        return QModelIndex();

    if (column < 0 || column >= columnCount())
        return QModelIndex();

    return QAbstractItemModel::createIndex(row, column);
}

QModelIndex TableModel::parent(const QModelIndex & /* index */) const
{
    return QModelIndex();
}

int TableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return mTable->columnCount();
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    if (orientation == Qt::Horizontal) {
        return mTable->headerData(section);
    } else {
        return QVariant();
    }
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    if (index.column() >= columnCount()) {
        return 0;
    }

    return mTable->flags(index.column());
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role != Qt::EditRole) {
        return false;
    }

    if (index.row() >= rowCount() || index.column() >= columnCount()) {
        return false;
    }

    if (mTable->setData(index.row(), index.column(), value)) {
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

bool TableModel::insertRows(int row, int count, const QModelIndex &parent)
{
    if (parent.isValid()) {
        return false;
    }

    if (count <= 0) {
        return false;
    }

    beginInsertRows(parent, row, row + count - 1);
    const bool ok = mTable->insertRows(row, count);
    endInsertRows();
    return ok;
}

bool TableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (parent.isValid()) {
        return false;
    }

    if (count <= 0) {
        return false;
    }

    beginRemoveRows(parent, row, row + count - 1);
    const bool ok = mTable->removeRows(row, count);
    endRemoveRows();
    return ok;
}

bool TableModel::moveRows(const QModelIndex &sourceParent,
                          int sourceRow,
                          int count,
                          const QModelIndex &destinationParent,
                          int destinationChild)
{
    if (sourceParent.isValid() || destinationParent.isValid()) {
        return false;
    }
    if (count <= 0) {
        return false;
    }
    if (sourceRow + count > rowCount()) {
        return false;
    }
    if (destinationChild > rowCount()) {
        return false;
    }
    if ((sourceRow >= destinationChild) && (sourceRow + count <= destinationChild)) {
        return false;
    }
    if (!beginMoveRows(sourceParent,
                       sourceRow,
                       sourceRow + count - 1,
                       destinationParent,
                       destinationChild)) {
        return false;
    }
    mTable->moveRows(sourceRow, count, destinationChild);
    endMoveRows();
    return true;
}

bool TableModel::assignRecord(const RecordId &id, const AbstractRecord &record)
{
    int row = getPositionOf(id);
    return assignRow(row, record);
}

bool TableModel::assignRow(int row, const AbstractRecord &record)
{
    if (row < 0 || row >= rowCount())
        return false;

    mTable->setRecord(row, record);

    emit dataChanged(index(row, 0), index(row, columnCount() - 1));
    return true;
}

const AbstractRecord *TableModel::getRecord(const RecordId &id) const
{
    return mTable->getRecord(id);
}

RecordId TableModel::getIdOf(int row) const
{
    return mTable->getRecordIdByRow(row);
}

int TableModel::getPositionOf(const RecordId &id) const
{
    for (int row = 0; row < rowCount(); ++row) {
        if (getIdOf(row) == id) {
            return row;
        }
    }
    return -1;
}

} // namespace gmtool
