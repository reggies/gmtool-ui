#include "palettedialog.h"

#include <QDialogButtonBox>
#include <QLabel>
#include <QPointer>
#include <QVBoxLayout>

#include "documenteditor.h"
#include "palettecombobox.h"
#include "paletteitemdelegate.h"
#include "propertiesmodel.h"
#include "tableid.h"
#include "tablemodel.h"

namespace gmtool {

PaletteDialog::PaletteDialog(DocumentEditor *editor, const QString &text, QWidget *parent)
    : QDialog(parent)
    , mEditor(editor)
{
    auto buttons = new QDialogButtonBox(this);
    buttons->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttons, &QDialogButtonBox::accepted, this, &PaletteDialog::accept);
    connect(buttons, &QDialogButtonBox::rejected, this, &PaletteDialog::reject);

    mPaletteBox = new PaletteComboBox(editor, this);

    auto layout = new QVBoxLayout(this);
    if (!text.isEmpty()) {
        layout->addWidget(new QLabel(text, this));
    }
    layout->addWidget(mPaletteBox);
    layout->addWidget(buttons);
}

RecordId PaletteDialog::getPaletteId(DocumentEditor *editor, QWidget *parent)
{
    QPointer<PaletteDialog> dialog = new PaletteDialog(editor, tr("Select palette"), parent);

    dialog->exec();

    if (dialog == nullptr)
        return RecordId();

    if (dialog->result() == QDialog::Accepted) {
        return dialog->selectedId();
    }

    return RecordId();
}

RecordId PaletteDialog::selectedId() const
{
    if (mPaletteBox->currentIndex() != -1) {
        auto model = mEditor->getModel(TableId::Palette);
        if (model == nullptr)
            return RecordId();
        return model->getIdOf(mPaletteBox->currentIndex());
    }
    return RecordId();
}

} // namespace gmtool
