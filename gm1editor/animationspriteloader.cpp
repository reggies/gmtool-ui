#include "animationspriteloader.h"

#include <QDebug>
#include <QImage>
#include <QPointer>

#include <boost/filesystem/path.hpp>

#include "animationeditor.h"
#include "animationloaddialog.h"
#include "spriterecord.h"

namespace gmtool {

AnimationSpriteLoader::AnimationSpriteLoader(AnimationEditor *editor)
    : mEditor(editor)
{}

void AnimationSpriteLoader::setPosition(const QPoint &point)
{
    mPos = point;
}

std::vector<std::unique_ptr<AbstractRecord>> AnimationSpriteLoader::loadSprites(
    const std::vector<boost::filesystem::path> &loadPaths, QWidget *parent)
{
    std::vector<std::unique_ptr<AbstractRecord>> sprites;

    QPointer<AnimationLoadDialog> dialog = new AnimationLoadDialog(mEditor, loadPaths, parent);
    dialog->exec();
    if (dialog == nullptr)
        return {};

    if (dialog->result() != QDialog::Accepted)
        return sprites;

    std::vector<SpriteRecordData> records = dialog->sprites();
    for (SpriteRecordData &entry : dialog->sprites()) {
        entry.mPos += mPos;
        sprites.emplace_back(SpriteRecord(entry, nullptr).clone());
    }

    return sprites;
}

} // namespace gmtool
