#!/usr/bin/env python

import struct
import PIL
import PIL.Image
import PIL.ImageShow

sprite = data.image_list[0].image
width = sprite.width
height = sprite.height
pixels = sprite.pixels
buf = struct.pack("H" * width * height, *pixels)
size = (width, height)
image = PIL.Image.frombuffer("L", size, buf)
PIL.ImageShow.show(image)
