#pragma once

#include "window.h"

#include <QTreeView>

namespace gmtool {

class PropertiesWindow : public Window
{
    Q_OBJECT

public:
    explicit PropertiesWindow(Document *doc, QWidget *parent = nullptr);

private:
    QTreeView *mView;

private:
    PropertiesWindow(const PropertiesWindow &) = delete;
    PropertiesWindow &operator=(const PropertiesWindow &) = delete;
};

} // namespace gmtool
