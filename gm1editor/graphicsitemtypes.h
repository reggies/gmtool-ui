#pragma once

#include <QGraphicsItem>

namespace gmtool {

enum {
    SpriteGraphicsItemType = QGraphicsItem::UserType + 1,
};
}
