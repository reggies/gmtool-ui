#pragma once

#include <QGraphicsObject>

#include <memory>

#include "nodeattribute.h"
#include "nodefactory.h"
#include "recordid.h"
#include "scenenode.h"
#include "tableid.h"
#include "tablemodel.h"
#include "viewstate.h"
#include "undo.h"

namespace gmtool {

class RepeaterNode : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit RepeaterNode(TableModel *table, std::unique_ptr<NodeFactory> nodeFactory, QGraphicsObject *parent);
    QRectF boundingRect() const override { return QRectF(); }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    void allowSelection(bool allow);
    void selectAll();
    void unselectAll();
    void setViewState(const ViewState &state);
    void setPropogatePaletteChanges(bool propogatePaletteChanges);
    void invalidatePalette();
    void removeNodes(int first, int last);
    void insertNodes(int first, int last);
    void moveNodes(int start, int end, int row);

    std::vector<RecordId> selectedIds() const;
    std::vector<SceneNode *> selectedNodes() const;
    std::vector<int> selectedIndices() const;
    int selectedCount() const;

    void removeSelectedNodes(undo::Macro *macro);

signals:
    void selectionChanged();

private:
    void itemsInserted(const QModelIndex &parent, int first, int last);
    void itemsAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void itemsReset();
    void itemsMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

private:
    TableModel *mTable;
    std::unique_ptr<NodeFactory> mNodeFactory;
    bool mPropogatePaletteChanges = false;
    std::unique_ptr<ViewState> mViewState;
};

} // namespace gmtool
