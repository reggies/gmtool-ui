#pragma once

#include <QGraphicsObject>
#include <QMetaType>

#include <memory>

namespace gmtool {
class AbstractRecord;
class ViewState;
enum class TableId;

class SceneNode : public QGraphicsObject
{
public:
    using QGraphicsObject::QGraphicsObject;

    virtual ~SceneNode() {}

    virtual void reloadModelData() = 0;

    virtual void invalidatePalette() {}

    virtual void setViewState(const ViewState &view) {}

    virtual std::unique_ptr<AbstractRecord> movedRecord(const QPoint &toPoint) { return nullptr; }
};

} // namespace gmtool
