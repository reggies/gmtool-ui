#pragma once

#include <QImageIOHandler>

namespace gmtool {

class TgxIoHandler : public QImageIOHandler
{
public:
    explicit TgxIoHandler();
    ~TgxIoHandler() override;

    bool canRead() const override;
    bool read(QImage *image) override;
    bool write(const QImage &image) override;
};

} // namespace gmtool
