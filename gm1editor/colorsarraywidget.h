#pragma once

#include <QWidget>

namespace gmtool {

class ColorsArrayWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ColorsArrayWidget(QWidget *parent = nullptr);

    void setColors(const QVector<QColor> &colors);

protected:
    void paintEvent(QPaintEvent *paintEvent) override;

    QSize sizeHint() const override;

private:
    void createPixmap();

private:
    QVector<QColor> mColors;
    QPixmap mPixmap;
};

} // namespace gmtool
