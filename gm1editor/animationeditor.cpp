#include "animationeditor.h"

#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>

#include "abstractrecord.h"
#include "animationframegroup.h"
#include "animationgraphicsitem.h"
#include "animationpreviewitem.h"
#include "animationspriteloader.h"
#include "columns.h"
#include "logmodel.h"
#include "nodefactory.h"
#include "propertiesmodel.h"
#include "spritegraphicsitem.h"
#include "spriteplotter.h"
#include "tableadapter.h"
#include "tablemodel.h"

#include <QDebug>
#include <QFile>
#include <QPainter>
#include <QTextStream>

#include <gm1.h>

#include <algorithm>
#include <string>

#include <yaml-cpp/yaml.h>

namespace gmtool {

static const char *kPaletteNames[] = {
    "Dummy",
    "Blue",
    "Red",
    "Orange",
    "Yellow",
    "Purple",
    "Black",
    "Cyan",
    "Green",
    "Dummy",
};

static const char *eightDirFromIndex(int index)
{
    switch (index) {
    case 0:
        return "NE";
    case 1:
        return "E";
    case 2:
        return "SE";
    case 3:
        return "S";
    case 4:
        return "SW";
    case 5:
        return "W";
    case 6:
        return "NW";
    case 7:
        return "N";
    default:
        return nullptr;
    }
}

static const char *fourDirFromIndex(int index)
{
    switch (index) {
    case 0:
        return "E";
    case 1:
        return "S";
    case 2:
        return "W";
    case 3:
        return "N";
    default:
        return nullptr;
    }
}

static int makeAnimationGroups(RecordSet<AnimationFrameGroup> &groups,
                               int firstIndex,
                               int directionCount,
                               int frameCount,
                               const QString &prefix)
{
    int totalFrames = 0;
    for (int i = 0; i < directionCount; ++i) {
        QString name;
        if (directionCount == 8) {
            name = QString("%1_%2").arg(prefix).arg(eightDirFromIndex(i));
        } else if (directionCount == 4) {
            name = QString("%1_%2").arg(prefix).arg(fourDirFromIndex(i));
        } else {
            name = prefix;
        }
        AnimationFrameGroup group(
            AnimationFrameGroupRecordData(name, firstIndex + i, directionCount, frameCount, false));
        groups.append(group);
        totalFrames += group.frameCount;
    }
    return totalFrames;
}

static void makeArcherAnimationGroups(RecordSet<AnimationFrameGroup> &groups)
{
    makeAnimationGroups(groups, 0, 1, 16, QObject::tr("Test"));

    QFile file(":/scenes.yaml");

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << Q_FUNC_INFO << "could not open scenes!";
        return;
    }

    QTextStream istream(&file);
    QString data = istream.readAll();
    YAML::Node root = YAML::Load(data.toStdString());
    YAML::Node units = root["units"];

    for (YAML::const_iterator unit = units.begin(); unit != units.end(); ++unit) {
        for (YAML::const_iterator container = unit->second.begin(); container != unit->second.end();
             ++container) {
            int groupStart = 0;

            for (const YAML::Node &group : container->second) {
                QString name = QString("%1_%2")
                                   .arg(container->first.as<std::string>().c_str())
                                   .arg(group["name"].as<std::string>().c_str());

                groupStart += makeAnimationGroups(groups,
                                                  groupStart,
                                                  group["dirs"].as<int>(),
                                                  group["frames"].as<int>(),
                                                  name);
            }
        }
    }
}

AnimationEditor::AnimationEditor()
    : mEntriesTableModel(new TableModel(nullptr))
    , mColorTableModel(new TableModel(nullptr))
    , mSpritesTableModel(new TableModel(nullptr))
    , mPropertiesModel(new PropertiesModel)
    , mAnimationsTableModel(new TableModel(nullptr))
{
    setupEntriesTableModel();

    for (size_t i = 0; i < Gm1FileData::MaxPaletteCount; ++i) {
        PaletteRecord record;
        if (i < Gm1FileData::MaxPaletteCount) {
            record.setName(kPaletteNames[i]);
        }
        mColorTable.insert(record, mColorTable.size());
    }

    makeArcherAnimationGroups(mAnimationGroups);

    setupColorTableModel();
    setupSpritesTableModel();
    setupPropertiesModel();
    setupAnimationsTableModel();
}

AnimationEditor::~AnimationEditor() = default;

void AnimationEditor::setupAnimationsTableModel()
{
    std::unique_ptr<TableAdapter<AnimationFrameGroup>> adapter(
        new TableAdapter<AnimationFrameGroup>(mAnimationGroups));
    adapter->addColumn<columns::ConstName>();
    mAnimationsTableModel->reset(std::unique_ptr<AbstractTable>(adapter.release()));
}

void AnimationEditor::setupEntriesTableModel()
{
    std::unique_ptr<TableAdapter<AnimationRecord>> adapter(
        new TableAdapter<AnimationRecord>(mEntries));
    adapter->addColumn<columns::MutableLeft<AnimationRecord>>();
    adapter->addColumn<columns::MutableTop<AnimationRecord>>();
    adapter->addColumn<columns::MutableWidth<AnimationRecord>>();
    adapter->addColumn<columns::MutableHeight<AnimationRecord>>();
    mEntriesTableModel->reset(std::unique_ptr<AbstractTable>(adapter.release()));
}

void AnimationEditor::setupColorTableModel()
{
    std::unique_ptr<TableAdapter<PaletteRecord>> adapter(
        new TableAdapter<PaletteRecord>(mColorTable));

    adapter->addColumn<columns::PalettePreview>();

    for (int i = 0; i < Gm1FileData::MaxPaletteSize; ++i) {
        adapter->addColumn<columns::Color<PaletteRecord>>(i);
    }

    mColorTableModel->reset(std::unique_ptr<AbstractTable>(adapter.release()));
}

void AnimationEditor::setupPropertiesModel()
{
    mPropertiesModel->removeRows(0, mPropertiesModel->rowCount());
    mXPivotProp = mPropertiesModel->addPropertyItem(QObject::tr("Pivot x"), QVariant::Int);
    mXPivotProp->setEditable(true);
    mYPivotProp = mPropertiesModel->addPropertyItem(QObject::tr("Pivot y"), QVariant::Int);
    mYPivotProp->setEditable(true);
    mPropertiesModel->reset();
}

void AnimationEditor::setupSpritesTableModel()
{
    std::unique_ptr<TableAdapter<SpriteRecord>> images(new TableAdapter<SpriteRecord>(mSprites));
    images->addColumn<columns::MutableLeft<SpriteRecord>>();
    images->addColumn<columns::MutableTop<SpriteRecord>>();
    images->addColumn<columns::ConstWidth<SpriteRecord>>();
    images->addColumn<columns::ConstHeight<SpriteRecord>>();
    mSpritesTableModel->reset(std::unique_ptr<AbstractTable>(images.release()));
}

TableModel *AnimationEditor::getModel(TableId table)
{
    switch (table) {
    case TableId::Gm1_Animation:
    case TableId::Layout:
        return mEntriesTableModel.get();
    case TableId::Palette:
        return mColorTableModel.get();
    case TableId::Spritesheet:
        return mSpritesTableModel.get();
    case TableId::Animations:
        return mAnimationsTableModel.get();
    case TableId::Gm1_Tile:
    case TableId::Gm1_Glyph:
    case TableId::Gm1_Bitmap:
    case TableId::Invalid:
        return nullptr;
    }
    return nullptr;
}

PropertiesModel *AnimationEditor::getPropertiesModel()
{
    return mPropertiesModel.get();
}

std::unique_ptr<AbstractRecord> AnimationEditor::createBlankEntry()
{
    AnimationRecord temp;
    temp.setRect(QRect(0, 0, 100, 100));
    return temp.clone();
}

std::unique_ptr<NodeFactory> AnimationEditor::createLayoutFactory()
{
    std::unique_ptr<NodeFactory> fact(new NodeFactory);
    fact->setNodeCreator([this](const RecordId &id) {
        return new AnimationGraphicsItem(this, id);
    });
    return fact;
}

std::unique_ptr<NodeFactory> AnimationEditor::createSpriteFactory()
{
    std::unique_ptr<NodeFactory> fact(new NodeFactory);
    fact->setNodeCreator([this](const RecordId &id) {
        return new SpriteGraphicsItem(this, id);
    });
    return fact;
}

std::unique_ptr<AnimationFactory> AnimationEditor::createAnimationFactory()
{
    auto factory = std::make_unique<AnimationFactory>();
    factory->setNodeCreator([this](const RecordId &id) {
        return new AnimationPreviewItem(this, id);
    });
    return factory;
}

bool AnimationEditor::load(const Gm1FileData &fileData, LogModel *log)
{
    Q_UNUSED(log);

    mGm1 = fileData;

    //! load plot image
    SpriteRecord plot;
    plot.setPosition(mGm1.plotOrigin());
    plot.setImage(mGm1.plotImage());
    if (mGm1.hasTransparency()) {
        plot.setTransparencyMode(TransparencyMode::Color);
        plot.setTransparentPixel(mGm1.transparentPixel());
    }

    mSprites.clear();
    mSprites.insert(plot, 0);
    setupSpritesTableModel();

    //! load palettes
    mColorTable.clear();
    for (size_t i = 0; i < mGm1.paletteCount(); ++i) {
        PaletteRecord record;
        if (i < Gm1FileData::MaxPaletteCount) {
            record.setName(kPaletteNames[i]);
        }
        record.setColors(mGm1.paletteAt(i));
        mColorTable.insert(record, i);
    }
    setupColorTableModel();

    //! decode entries
    mEntries.clear();
    for (size_t i = 0; i < mGm1.entryCount(); ++i) {
        AnimationRecord record;
        record.setRect(mGm1.Entries.at(i).entryRect());
        mEntries.insert(record, i);
    }
    setupEntriesTableModel();

    mXPivotProp->setValue(QVariant::fromValue<int>(mGm1.pivot().x()));
    mYPivotProp->setValue(QVariant::fromValue<int>(mGm1.pivot().y()));
    mPropertiesModel->reset();

    return true;
}

bool AnimationEditor::renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result)
{
    SpritePlotter plotter;
    plotter.setTransparentIndex(0);

    for (const RecordId &id : ids) {
        const auto &record = mSprites.get(id);
        plotter.addImage(record.image(), record.rect(), record.mask());
    }

    if (!plotter.render(QImage::Format_Indexed8, QRect(), log)) {
        return false;
    }

    *result = plotter.plot();

    return true;
}

bool AnimationEditor::save(Gm1FileData *fileData, LogModel *log)
{
    Gm1FileData gm1 = mGm1;

    gm1.setEntryClass(GM1_ENTRY_CLASS_TGX8);

    for (size_t i = 0; i < gm1.paletteCount(); ++i) {
        gm1.setPaletteAt(i, mColorTable.get(mColorTable.getIdAt(i)).colors());
    }

    gm1.setEntryCount(mEntries.size());
    for (size_t i = 0; i < gm1.entryCount(); ++i) {
        const AnimationRecord &record = mEntries.get(mEntries.getIdAt(i));
        gm1.Entries.at(i).setEntryRect(record.rect());
    }

    gm1.setPivot(pivot());

    SpritePlotter plotter;
    plotter.setTransparentIndex(0);
    for (const SpriteRecord &record : mSprites | boost::adaptors::map_values) {
        plotter.addImage(record.image(), record.rect(), record.mask());
    }

    QRect entriesRect;
    for (const AnimationRecord &record : mEntries | boost::adaptors::map_values) {
        entriesRect |= record.rect();
    }

    if (!plotter.render(QImage::Format_Indexed8, entriesRect, log)) {
        log->error("error rendering final plot image");
        return false;
    }

    gm1.setPlotImage(plotter.plot());
    gm1.setPlotOrigin(plotter.origin());

    *fileData = gm1;

    return true;
}

std::unique_ptr<SpriteLoader> AnimationEditor::createSpriteLoader()
{
    return std::unique_ptr<SpriteLoader>(new AnimationSpriteLoader(this));
}

QPoint AnimationEditor::pivot() const
{
    return QPoint(mXPivotProp->value().toInt(), mYPivotProp->value().toInt());
}

} // namespace gmtool
