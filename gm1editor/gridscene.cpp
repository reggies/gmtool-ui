#include "gridscene.h"

#include <QApplication>
#include <QDebug>
#include <QEvent>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QPainter>
#include <QPointer>
#include <QRectF>

#include <algorithm>
#include <cmath>

#include "mathutils.inl"
#include "nodeattribute.h"
#include "spritegraphicsitem.h"

namespace gmtool {

static QPixmap createTransparentGrid()
{
    QPixmap grid(16, 16);
    QPainter painter(&grid);
    const auto darker = QColor(128, 128, 128) - QColor(20, 20, 20);
    const auto lighter = QColor(128, 128, 128) + QColor(20, 20, 20);
    painter.fillRect(QRect(0, 0, 16, 16), darker);
    painter.fillRect(QRect(0, 0, 8, 8), lighter);
    painter.fillRect(QRect(8, 8, 8, 8), lighter);
    return grid;
}

static QPixmap getTransparentGrid()
{
    static QPixmap grid = createTransparentGrid();
    return grid;
}

GridScene::GridScene(QObject *parent)
    : QGraphicsScene(parent)
{}

GridScene::~GridScene() = default;

void GridScene::setSnapToGrid(bool enable)
{
    if (mSnapToGrid != enable) {
        mSnapToGrid = enable;
        emit snapToGridChanged(enable);
    }
}

bool GridScene::snapToGrid() const
{
    return mSnapToGrid;
}

void GridScene::setInteractionMode(InteractionMode mode)
{
    mMode = mode;
    void interactionModeChanged();
}

InteractionMode GridScene::interactionMode() const
{
    return mMode;
}

QList<QGraphicsItem *> GridScene::transformingItems() const
{
    return mTransformingItems;
}

bool GridScene::isTransforming() const
{
    return mIsTransforming;
}

void GridScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    mMousePos = floorPoint(event->scenePos());
    if ((mMode == InteractionMode::Transform) && ((event->buttons() & Qt::LeftButton) != 0)) {
        if (mIsTransforming) {
            stopTransforming(false);
            QGraphicsScene::mousePressEvent(event);
            return;
        }
        if (event->button() == Qt::LeftButton) {
            if (((event->modifiers() & Qt::ShiftModifier) != 0)
                || ((event->modifiers() & Qt::ControlModifier) != 0)) {
                for (auto item : items(event->scenePos())) {
                    if ((item->flags() & QGraphicsItem::ItemIsSelectable) != 0) {
                        item->setSelected(true);
                        event->accept();
                    }
                }
            } else {
                /*!
                  \todo one cannot entrust event to the scene before
                  doing any math on it.
                  we are doing this _only_ to update selected items.
                */
                QGraphicsScene::mousePressEvent(event);
            }
        }
        if (event->isAccepted()) {
            if (startTransforming(mMousePos)) {
                mTransformingInputGrabber = event->widget();
                Q_ASSERT(mTransformingInputGrabber != nullptr);
                event->widget()->grabMouse();
                event->widget()->grabKeyboard();
                return;
            }
        }
    } else if (mMode == InteractionMode::Pick) {
        if (event->button() == Qt::LeftButton) {
            mIsPicking = true;
            updatePicking(mMousePos);
            event->accept();
            emit pickingStarted();
        } else {
            stopPicking(true);
            event->accept();
            return;
        }
    } else if (event->button() == Qt::RightButton) {
        /*!
          accept mouse press event so the selection won't be cleared
          upon context menu action
         */
        event->accept();
        return;
    } else {
        QGraphicsScene::mousePressEvent(event);
    }
}

void GridScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    mMousePos = floorPoint(event->scenePos());
    emit cursorMoved(mMousePos);
    if (mMode == InteractionMode::Transform && mIsTransforming && !mIsTransformingWithKeyboard) {
        /*!
          just for safety, detect state transition violations
         */
        if (!(event->buttons() & Qt::LeftButton)) {
            stopTransforming(false);
        } else {
            mTransformingTargetPos = mMousePos;
            updateItemsPos(false);
        }
    } else if (mMode == InteractionMode::Pick && mIsPicking) {
        updatePicking(mMousePos);
    }
    QGraphicsScene::mouseMoveEvent(event);
}

void GridScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    mMousePos = floorPoint(event->scenePos());
    if ((mMode == InteractionMode::Transform) && (mIsTransforming)
        && (!mIsTransformingWithKeyboard)) {
        stopTransforming(false);
    } else if ((mMode == InteractionMode::Pick) && (mIsPicking)) {
        updatePicking(mMousePos);
        stopPicking(false);
    }
    QGraphicsScene::mouseReleaseEvent(event);
}

QPoint GridScene::mouseScenePos() const
{
    return mMousePos;
}

void GridScene::drawBackground(QPainter *painter, const QRectF &rect)
{
    if (mTileBackground) {
        painter->save();
        const QTransform xform = painter->transform();
        const QRectF &transparentRect = xform.mapRect(rect);
        painter->resetTransform();
        painter->fillRect(transparentRect, getTransparentGrid());
        painter->restore();
    } else {
        painter->fillRect(rect, QColor(Qt::lightGray));
    }

    QLineF lines[]
        = {painter->transform().map(QLine(QPoint(rect.left(), 0), QPoint(rect.right(), 0))),
           painter->transform().map(QLine(QPoint(0, rect.top()), QPoint(0, rect.bottom())))};

    QPen pen;
    pen.setCosmetic(true);
    pen.setWidth(2.0f);
    pen.setColor(QColor(0, 0, 0));

    painter->save();
    painter->resetTransform();
    painter->setPen(pen);
    painter->drawLines(lines, 2);
    painter->restore();
}

int GridScene::gridSize() const
{
    return 8;
}

bool GridScene::event(QEvent *event)
{
    if (mIsTransforming || mIsPicking) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        switch (event->type()) {
        case QEvent::MouseButtonPress:
        case QEvent::MouseButtonRelease:
        case QEvent::MouseMove:
        case QEvent::KeyPress:
        case QEvent::KeyRelease:
        case QEvent::ShortcutOverride:
            /*!
              escape should reset mouse grab, but with keyboard we don't use
              mouse grab
             */
            if ((mIsTransforming && !mIsTransformingWithKeyboard) || mIsPicking) {
                if (keyEvent->matches(QKeySequence::Cancel)) {
                    if (mIsTransforming) {
                        stopTransforming(true);
                    } else if (mIsPicking) {
                        stopPicking(true);
                    }
                }
                event->accept();
                return true;
            }
            break;
        default:
            return QGraphicsScene::event(event);
        }
    }
    return QGraphicsScene::event(event);
}

void GridScene::stopTransforming(bool reset)
{
    if (mIsTransforming) {
        mIsTransforming = false;
        if (reset) {
            emit itemsMovementReset();
        } else {
            emit itemsMovementFinished();
        }
        if (mTransformingInputGrabber != nullptr) {
            mTransformingInputGrabber->releaseKeyboard();
            mTransformingInputGrabber->releaseMouse();
            mTransformingInputGrabber = nullptr;
        }
    }
}

void GridScene::stopPicking(bool reset)
{
    mIsPicking = false;
    if (reset) {
        emit pickingReset();
    } else {
        emit pickingFinished();
    }
}

void GridScene::updatePicking(const QPoint &point)
{
    mPickedColor = pixelAt(point);
    emit pickingChanged();
}

boost::optional<QColor> GridScene::pickedColor() const
{
    return mPickedColor;
}

bool GridScene::isPicking() const
{
    return mIsPicking;
}

boost::optional<QColor> GridScene::pixelAt(const QPoint &point) const
{
    for (auto item : items(point)) {
        if (item->type() == SpriteGraphicsItem::Type) {
            auto sprite = qgraphicsitem_cast<SpriteGraphicsItem *>(item);
            Q_ASSERT(sprite != nullptr);
            return sprite->pixelAt(floorPoint(sprite->mapFromScene(point)));
        } else if (item->type() == QGraphicsPixmapItem::Type) {
            auto gpi = qgraphicsitem_cast<QGraphicsPixmapItem *>(item);
            Q_ASSERT(gpi != nullptr);
            return gpi->pixmap().toImage().pixelColor(floorPoint(gpi->mapFromScene(point)));
        }
    }
    return boost::none;
}

void GridScene::setTileBackground(bool tileBackground)
{
    mTileBackground = tileBackground;
}

void GridScene::setSolidBackground(bool solidBackground)
{
    mTileBackground = !solidBackground;
}

void GridScene::keyPressEvent(QKeyEvent *event)
{
    if (mMode == InteractionMode::Transform) {
        switch (event->key()) {
        case Qt::Key_Up:
        case Qt::Key_Left:
        case Qt::Key_Right:
        case Qt::Key_Down:
            if (startTransforming(QPoint(0, 0))) {
                mIsTransformingWithKeyboard = true;
                mTransformingTargetPos = QPoint(0, 0);
            }
            if (mIsTransforming && mIsTransformingWithKeyboard) {
                mTransformingTargetPos += keyboardMovementVector(event->key(), event->modifiers());
                updateItemsPos(true);
                event->accept();
                return;
            }
        default:
            break;
        }
    }
    QGraphicsScene::keyPressEvent(event);
}

void GridScene::keyReleaseEvent(QKeyEvent *event)
{
    if (mIsTransforming && mIsTransformingWithKeyboard) {
        if (!event->isAutoRepeat()) {
            mIsTransformingWithKeyboard = false;
            stopTransforming(false);
            event->accept();
            return;
        }
    }
    QGraphicsScene::keyPressEvent(event);
}

bool GridScene::startTransforming(const QPoint &anchorScenePos)
{
    if (mIsTransforming) {
        return false;
    }
    mTransformingItems.clear();
    mTransformingItems.reserve(selectedItems().size());
    for (QGraphicsItem *item : selectedItems()) {
        if ((item->flags() & QGraphicsItem::ItemIsSelectable) != 0) {
            mTransformingItems << item;
        }
    }
    if (mTransformingItems.isEmpty()) {
        return false;
    }
    QRegion transformRegion;
    for (QGraphicsItem *item : mTransformingItems) {
        transformRegion |= item->boundingRegion(item->sceneTransform());
    }
    mIsTransforming = true;
    for (QGraphicsItem *item : mTransformingItems) {
        item->setData(NodeAttribute::TransformOffset, anchorScenePos - item->pos());
        item->setData(NodeAttribute::TransformTopLeft,
                      item->pos() - transformRegion.boundingRect().topLeft());
        item->setData(NodeAttribute::TransformBottomRight,
                      item->pos() - transformRegion.boundingRect().bottomRight());
    }
    emit itemsMovementStarted();
    return true;
}

QPoint GridScene::transformingTargetPos() const
{
    return mTransformingTargetPos;
}

QPointF GridScene::snapItem(const QPointF &point, const QPoint &transformOrigin)
{
    if (mSnapToGrid) {
        QPointF newPoint;
        newPoint.setX(point.x() - fmod((point - transformOrigin).x(), gridSize()));
        newPoint.setY(point.y() - fmod((point - transformOrigin).y(), gridSize()));
        return newPoint;
    }
    return point;
}

void GridScene::updateItemsPos(bool ignoreSnapToGrid)
{
    for (auto item : transformingItems()) {
        auto node = qgraphicsitem_cast<SceneNode *>(item);
        if (node == nullptr) {
            continue;
        }
        QPoint transformOffset = node->data(NodeAttribute::TransformOffset).toPoint();
        QPoint transformTopLeft = node->data(NodeAttribute::TransformTopLeft).toPoint();
        if (ignoreSnapToGrid) {
            node->setPos(transformingTargetPos() - transformOffset);
        } else {
            QPointF newPos = snapItem(transformingTargetPos() - transformOffset, transformTopLeft);
            node->setPos(newPos);
        }
    }
}

int GridScene::keyboardMovementMagnitude(Qt::KeyboardModifiers modifiers) const
{
    if (mSnapToGrid)
        return gridSize();
    if ((modifiers & Qt::ShiftModifier) != 0)
        return gridSize();
    return 1;
}

QPoint GridScene::keyboardMovementVector(int key, Qt::KeyboardModifiers modifiers) const
{
    switch (key) {
    case Qt::Key_Up:
        return QPoint(0, -keyboardMovementMagnitude(modifiers));
    case Qt::Key_Down:
        return QPoint(0, keyboardMovementMagnitude(modifiers));
    case Qt::Key_Left:
        return QPoint(-keyboardMovementMagnitude(modifiers), 0);
    case Qt::Key_Right:
        return QPoint(keyboardMovementMagnitude(modifiers), 0);
    default:
        return QPoint();
    }
}

} // namespace gmtool
