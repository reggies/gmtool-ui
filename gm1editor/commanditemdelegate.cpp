#include "commanditemdelegate.h"

#include <QComboBox>
#include <QDebug>
#include <QPainter>

#include "coloredit.h"
#include "modifycommand.h"

namespace gmtool {

/// Get data from default editor widget. Awkward way.
class FakeTableModel : public QAbstractTableModel
{
public:
    explicit FakeTableModel(const QAbstractItemModel *model)
        : mModel(model)
    {}

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override
    {
        return mModel->data(index, role);
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const override
    {
        return mModel->rowCount(parent);
    }

    int columnCount(const QModelIndex &parent = QModelIndex()) const override
    {
        return mModel->columnCount(parent);
    }

    bool setData(const QModelIndex &index, const QVariant &data, int role = Qt::EditRole) override
    {
        Q_UNUSED(index);
        Q_UNUSED(role);
        mData = data;
        return true;
    }

    QVariant getData() const { return mData; }

private:
    const QAbstractItemModel *mModel;
    QVariant mData;
};

CommandItemDelegate::CommandItemDelegate(QAbstractItemModel *model,
                                         undo::Stack *stack,
                                         QObject *parent)
    : QStyledItemDelegate(parent)
    , mModel(model)
    , mUndoStack(stack)
{}

QWidget *CommandItemDelegate::createEditor(QWidget *parent,
                                           const QStyleOptionViewItem &option,
                                           const QModelIndex &index) const
{
    const QVariant &data = index.data(Qt::EditRole);

    if (data.type() == QVariant::Color) {
        auto editor = new ColorEdit(parent);
        //! \note editor can grab the focus which will consequently destroy it
        editor->setFocusProxy(parent);
        return editor;
    } else {
        return QStyledItemDelegate::createEditor(parent, option, index);
    }
}

void CommandItemDelegate::paint(QPainter *painter,
                                const QStyleOptionViewItem &option,
                                const QModelIndex &index) const
{
    const QVariant &editData = index.data(Qt::EditRole);

    if (editData.type() == QVariant::Color) {
        painter->save();
        if ((option.state & QStyle::State_Selected) != 0) {
            painter->fillRect(option.rect, option.palette.highlight());
        }
        painter->fillRect(option.rect.adjusted(3, 3, -3, -3), editData.value<QColor>());
        painter->restore();
    } else {
        QStyledItemDelegate::paint(painter, option, index);
    }
}

void CommandItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    const QVariant &editData = index.data(Qt::EditRole);

    if (editData.type() == QVariant::Color) {
        ColorEdit *edit = qobject_cast<ColorEdit *>(editor);
        if (edit != nullptr) {
            edit->setCurrentColor(editData.value<QColor>());
        } else {
            throw std::logic_error("bad editor widget");
        }
    } else {
        QStyledItemDelegate::setEditorData(editor, index);
    }
}

void CommandItemDelegate::setModelData(QWidget *editor,
                                       QAbstractItemModel *model,
                                       const QModelIndex &index) const
{
    QVariant editData = index.data(Qt::EditRole);

    if (editData.type() == QVariant::Color) {
        ColorEdit *edit = qobject_cast<ColorEdit *>(editor);
        if (edit != nullptr) {
            QVariant data = edit->currentColor();
            mUndoStack->push(std::make_unique<ModifyCommand>(index, data, model));
        }
    } else {
        FakeTableModel fake(model);
        QStyledItemDelegate::setModelData(editor, &fake, index);
        QVariant data = fake.getData();

        mUndoStack->push(std::make_unique<ModifyCommand>(index, data, model));
    }
}

} // namespace gmtool
