#include "paletteitemdelegate.h"

#include <QDebug>
#include <QPainter>

#include "palettepreview.h"
#include "paletterecord.h"
#include "tablemodel.h"

namespace gmtool {

PaletteItemDelegate::PaletteItemDelegate(TableModel *model, QWidget *parent)
    : QStyledItemDelegate(parent)
    , mModel(model)
{
    PaletteRecord dummy;
    mPreview = new PalettePreview(dummy);
}

PaletteItemDelegate::~PaletteItemDelegate()
{
    mPreview->deleteLater();
}

void PaletteItemDelegate::paint(QPainter *painter,
                                const QStyleOptionViewItem &option,
                                const QModelIndex &index) const
{
    if (!index.isValid())
        return;

    PaletteRecord record;
    record.assign(*mModel->getRecord(mModel->getIdOf(index.row())));

    mPreview->reset(record);
    mPreview->setFixedSize(option.rect.size());
    painter->save();

    if ((option.state & QStyle::State_Selected) != 0) {
        painter->fillRect(option.rect, option.palette.highlight());
    }

    painter->setRenderHint(QPainter::Antialiasing, false);

    auto mappedOrigin = painter->deviceTransform().map(option.rect.topLeft());

    mPreview->render(painter,
                     mappedOrigin,
                     QRegion(0, 0, option.rect.width(), option.rect.height()),
                     QWidget::RenderFlag::DrawChildren);
    painter->restore();
}

QSize PaletteItemDelegate::sizeHint(const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const
{
    if (!index.isValid())
        return QStyledItemDelegate::sizeHint(option, index);

    //! \note at the moment of writing there is not need to reset the record

    return mPreview->sizeHint();
}

} // namespace gmtool
