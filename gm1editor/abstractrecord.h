#pragma once

#include <QObject>

#include <memory>

namespace gmtool {

class AbstractRecord : public QObject
{
    Q_OBJECT
public:
    explicit AbstractRecord(QObject *parent = nullptr)
        : QObject(parent)
    {}
    virtual ~AbstractRecord() {}
    virtual std::unique_ptr<AbstractRecord> clone() const = 0;
    virtual void assign(const AbstractRecord &that) = 0;
};

} // namespace gmtool
