#pragma once

#include <vector>

#include "animationframegroup.h"
#include "animationnode.h"
#include "recordid.h"

namespace gmtool {
class AnimationEditor;

class AnimationPreviewItem : public AnimationNode
{
    Q_OBJECT
public:
    explicit AnimationPreviewItem(AnimationEditor *editor,
                                  const RecordId &animationFrameGroupId,
                                  QGraphicsItem *parent = nullptr);

    void reloadModelData() override;

    void setPaletteId(const RecordId &paletteId) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void advance(float dtime) override;

    float totalDuration() override;
    int totalFrames() override;

private:
    std::vector<QPixmap> getFrames() const;

    void setupFrames();

    void entriesInserted(const QModelIndex &parent, int first, int last);
    void entriesAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void entriesReset();
    void entriesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void entriesMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

    void spritesInserted(const QModelIndex &parent, int first, int last);
    void spritesAboutToBeRemoved(const QModelIndex &parent, int first, int last);
    void spritesReset();
    void spritesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void spritesMoved(
        const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row);

private:
    AnimationEditor *mEditor;
    RecordId mFrameGroupId;
    QSize mSize;
    float mAccumulatedTime;
    int mFrameIndex;
    QPoint mPivot;
    RecordId mPaletteId;
    bool mFramesDirty;
};

} // namespace gmtool
