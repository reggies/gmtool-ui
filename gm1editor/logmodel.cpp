#include "logmodel.h"

#include <algorithm>
#include <cstdarg>

#include <QDebug>
#include <QIcon>

namespace gmtool {

LogModel::LogModel(QObject *parent)
    : QAbstractListModel(parent)
{
    connect(this, &QAbstractItemModel::dataChanged, this, &LogModel::messagesChanged);
    connect(this, &QAbstractItemModel::modelReset, this, &LogModel::messagesChanged);
    connect(this, &QAbstractItemModel::rowsInserted, this, &LogModel::messagesChanged);
    connect(this, &QAbstractItemModel::rowsRemoved, this, &LogModel::messagesChanged);
    connect(this, &QAbstractItemModel::rowsMoved, this, &LogModel::messagesChanged);
}

LogModel::~LogModel() = default;

void LogModel::clear()
{
    beginResetModel();
    mMessages.clear();
    endResetModel();
}

void LogModel::merge(const LogModel *other)
{
    //
    // BUG: beginInsertRows() after endResetModel() causes an additional empty
    // row to appear in the view.
    // calling beginResetModel() after endResetModel() or
    // beginInsertRows() after endInsertRows() doesn't cause any troubles.
    //
    if (!other->mMessages.empty()) {
        beginResetModel();
        mMessages.insert(mMessages.end(), other->mMessages.begin(), other->mMessages.end());
        endResetModel();
    }
}

void LogModel::addError(const QString &text)
{
    return addMessage(text, Severity::Error);
}

void LogModel::addWarning(const QString &text)
{
    return addMessage(text, Severity::Warning);
}

void LogModel::addMessage(const QString &text, Severity severity)
{
    LogMessage message;
    message.text = text;
    message.severity = severity;
    message.timestamp = QDateTime::currentDateTime();
    message.viewed = false;
    beginInsertRows(QModelIndex(), mMessages.size(), mMessages.size());
    mMessages.push_back(message);
    endInsertRows();
}

int LogModel::errorCount() const
{
    return std::count_if(mMessages.begin(), mMessages.end(), [](const LogMessage &message) {
        return message.severity == Severity::Error && !message.viewed;
    });
}

int LogModel::warningCount() const
{
    return std::count_if(mMessages.begin(), mMessages.end(), [](const LogMessage &message) {
        return message.severity == Severity::Warning && !message.viewed;
    });
}

QVariant LogModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() < 0 || index.row() >= static_cast<int>(mMessages.size())) {
        return QVariant();
    }
    switch (role) {
    case LogModel::SeverityRole:
        return QVariant::fromValue(mMessages.at(index.row()).severity);
    case Qt::DisplayRole:
        return mMessages.at(index.row()).text;
    case Qt::DecorationRole: {
        auto severity = mMessages.at(index.row()).severity;
        auto viewed = mMessages.at(index.row()).viewed;
        switch (severity) {
        case Severity::Error:
            if (viewed) {
                return QIcon::fromTheme("dialog-error-gray");
            } else {
                return QIcon::fromTheme("dialog-error");
            }
        case Severity::Warning:
            if (viewed) {
                return QIcon::fromTheme("dialog-warning-gray");
            } else {
                return QIcon::fromTheme("dialog-warning");
            }
        case Severity::Information:
            return QIcon();
        }
        return QIcon();
    }
    default:
        return QVariant();
    }
}

Qt::ItemFlags LogModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

int LogModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return static_cast<int>(mMessages.size());
}

QVariant LogModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal || section != 0 || role != Qt::DisplayRole) {
        return QVariant();
    }

    return tr("Description");
}

void LogModel::error(const char *format, ...)
{
    va_list va;
    va_start(va, format);
    addMessage(QString::vasprintf(format, va));
    va_end(va);
}

void LogModel::warning(const char *format, ...)
{
    va_list va;
    va_start(va, format);
    addMessage(QString::vasprintf(format, va), Severity::Warning);
    va_end(va);
}

void LogModel::info(const char *format, ...)
{
    va_list va;
    va_start(va, format);
    addMessage(QString::vasprintf(format, va), Severity::Information);
    va_end(va);
}

void LogModel::markAsViewed()
{
    for (size_t i = 0; i < mMessages.size(); ++i) {
        if (!mMessages.at(i).viewed) {
            mMessages.at(i).viewed = true;
            emit dataChanged(index(i), index(i));
        }
    }
}

} // namespace gmtool
