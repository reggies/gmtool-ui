#include "documenttypesmodel.h"

namespace gmtool {

const std::vector<DocumentType> kDocumentTypes = {
    DocumentType::Font,
    DocumentType::Anim,
    DocumentType::Static,
    DocumentType::Tile,
    DocumentType::Bitmap,
};

DocumentTypesModel::DocumentTypesModel(QObject *parent)
    : QAbstractListModel(parent)
    , mTypes(kDocumentTypes.begin(), kDocumentTypes.end())
{}

QVariant DocumentTypesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() >= rowCount())
        return QVariant();

    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    const QString &name = getDocumentTypeName(mTypes.at(index.row()));

    if (index.row() == mRecentIndex) {
        return tr("%1 (Recent)").arg(name);
    } else {
        return name;
    }
}

int DocumentTypesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return static_cast<int>(mTypes.size());
}

DocumentType DocumentTypesModel::getValue(int index) const
{
    return mTypes.at(index);
}

int DocumentTypesModel::indexOf(const DocumentType &type) const
{
    auto it = std::find(mTypes.begin(), mTypes.end(), type);
    if (it == mTypes.end())
        return -1;
    return it - mTypes.begin();
}

int DocumentTypesModel::defaultIndex() const
{
    return indexOf(DocumentType::Anim);
}

QString DocumentTypesModel::getDocumentTypeName(const DocumentType &type) const
{
    switch (type) {
    case DocumentType::Font:
        return tr("Font");
    case DocumentType::Anim:
        return tr("Animation");
    case DocumentType::Static:
        return tr("Transparent");
    case DocumentType::Tile:
        return tr("Tile set");
    case DocumentType::Bitmap:
        return tr("Bitmap");
    default:
        return QString();
    }
}

void DocumentTypesModel::setRecentIndex(int index)
{
    mRecentIndex = index;
}

int DocumentTypesModel::recentIndex() const
{
    return mRecentIndex;
}

} // namespace gmtool
