#pragma once

#include <functional>

#include <QDebug>
#include <QString>
#include <QVariant>

namespace gmtool {

template<class RecordT>
class TableColumn
{
public:
    explicit TableColumn(const QString &title)
        : mHeaderData(title)
    {}

    virtual ~TableColumn() {}

    virtual QVariant displayData(const RecordT &row) const = 0;

    virtual QVariant editData(const RecordT &row) const { return QVariant(); }

    virtual bool setData(RecordT & /* row */, const QVariant & /* data */) const { return false; }

    virtual QVariant headerData() const { return mHeaderData; }

    virtual bool isEditable() const { return false; }

    virtual Qt::ItemFlags flags() const
    {
        Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
        if (isEditable()) {
            flags |= Qt::ItemIsEditable;
        }
        return flags;
    }

private:
    QVariant mHeaderData;
};

template<class T, class RecordT>
class MyColumn : public TableColumn<RecordT>
{
    using get_t = std::function<T(const RecordT *)>;
    using set_t = std::function<void(RecordT *, const T &)>;

public:
    explicit MyColumn(const QString &name, get_t get, set_t set = set_t())
        : TableColumn<RecordT>(name)
        , mGet(get)
        , mSet(set)
    {}

    virtual bool check(const RecordT & /* record */, const T & /* value */) const { return true; }

    QVariant displayData(const RecordT &record) const override
    {
        return QVariant::fromValue<T>(mGet(&record));
    }

    QVariant editData(const RecordT &record) const override
    {
        return QVariant::fromValue<T>(mGet(&record));
    }

    bool setData(RecordT &record, const QVariant &data) const override
    {
        if (!data.canConvert<T>()) {
            return false;
        }
        if (!check(record, data.value<T>())) {
            return false;
        }
        mSet(&record, data.value<T>());
        return true;
    }

    bool isEditable() const override { return mSet.operator bool(); }

private:
    get_t mGet;
    set_t mSet;
};

} // namespace gmtool
