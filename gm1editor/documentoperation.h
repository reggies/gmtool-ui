#pragma once

#include <QObject>

#include "operation.h"

namespace gmtool {

class DocumentOperation : public QObject
{
    Q_OBJECT

public:
    explicit DocumentOperation(QObject *parent = 0);

    bool isRunning() const;

    int currentProgress() const;

    int totalProgress() const;

    QString statusString() const;

    //! Ownership is not transferred
    bool pushOperation(Operation *operation);

    void setStatusString(const QString &statusString);

signals:
    void stateChanged();

private:
    void setProgress(int current, int total);
    void setFinished();
    void setStarted();

private:
    bool mIsRunning = false;
    int mCurrentProgress = 0;
    int mTotalProgress = 0;
    QString mStatusString;
};

} // namespace gmtool
