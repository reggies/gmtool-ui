#pragma once

#include <memory>
#include <set>

#include <QImage>

#include "nodefactory.h"
#include "spritesheet.h"
#include "tableid.h"

namespace gmtool {
class TableModel;
class AbstractRecord;
class Gm1FileData;
class SceneNode;
class SpriteLoader;
class PropertiesModel;
class RecordId;
class LogModel;

struct ILayoutContainer
{
    virtual TableModel *getEntries() = 0;
};

struct IPropertiesContainer
{
    virtual PropertiesModel *getPropertiesModel() = 0;
};

struct IModelContainer
{
    virtual TableModel *getModel(TableId table) = 0;
};

struct IEntryFactory
{
    virtual std::unique_ptr<AbstractRecord> createBlankEntry() = 0;
};

struct ISpriteLoaderFactory
{
    virtual std::unique_ptr<SpriteLoader> createSpriteLoader() = 0;
};

struct IAbstractLayoutFactory
{
    virtual std::unique_ptr<NodeFactory> createLayoutFactory() = 0;
};

struct IAbstractSpriteFactory
{
    virtual std::unique_ptr<NodeFactory> createSpriteFactory() = 0;
};

struct IGm1Loader
{
    virtual bool load(const Gm1FileData &fileData, LogModel *log) = 0;
};

struct IGm1Writer
{
    virtual bool save(Gm1FileData *fileData, LogModel *log) = 0;
};

struct ISpriteRenderer
{
    virtual bool renderSprites(const std::set<RecordId> &records, LogModel *log, QImage *result) = 0;
};

struct IAbstractAnimationFactory
{
    virtual std::unique_ptr<AnimationFactory> createAnimationFactory() = 0;
};

class DocumentEditor : public ISpriteSheet,              // view
                       public IPropertiesContainer,      // view
                       public ILayoutContainer,          // view
                       public IModelContainer,           // view
                       public IEntryFactory,             // plot
                       public IAbstractLayoutFactory,    // plot
                       public IAbstractSpriteFactory,    // plot
                       public IAbstractAnimationFactory, // preview
                       public IGm1Loader,
                       public IGm1Writer,
                       //! I need a strong reason to keep this function in this interface
                       public ISpriteRenderer,
                       public ISpriteLoaderFactory
{
public:
    virtual ~DocumentEditor() = default;

    virtual TableModel *getSpritesheet() final override { return getModel(TableId::Spritesheet); }
    virtual TableModel *getEntries() final override { return getModel(TableId::Layout); }
    virtual TableModel *getPalette() final override { return getModel(TableId::Palette); }
};

} // namespace gmtool
