#include "removecommand.h"

#include "abstractrecord.h"

#include <stdexcept>

namespace gmtool {

RemoveCommand::RemoveCommand(int position, TableModel *model, const QString &name)
    : mName(name)
    , mPos(position)
    , mModel(model)
{}

RemoveCommand::~RemoveCommand() = default;

bool RemoveCommand::redo()
{
    mRecord = mModel->getRecord(mModel->getIdOf(mPos))->clone();
    return mModel->removeRows(mPos, 1);
}

bool RemoveCommand::undo()
{
    if (!mModel->insertRows(mPos, 1)) {
        return false;
    }
    if (!mModel->assignRow(mPos, *mRecord)) {
        if (!mModel->removeRows(mPos, 1)) {
            qFatal("This is embarassing.");
        }
        return false;
    }
    return true;
}

QString RemoveCommand::name() const
{
    if (mName.isEmpty())
        return QObject::tr("remove");
    return mName;
}

} // namespace gmtool
