#include "documentloader.h"

#include "documenttype.h"
#include "editorfactory.h"
#include "gm1loader.h"
#include "logmodel.h"

#include <gm1.h>

namespace gmtool {

namespace fs = boost::filesystem;

using namespace std::placeholders;

static DocumentType Gm1ClassToDocumentType(gm1_entry_class_t entry_class)
{
    switch (entry_class) {
    case GM1_ENTRY_CLASS_TGX8:
        return DocumentType::Anim;

    case GM1_ENTRY_CLASS_TGX16:
    case GM1_ENTRY_CLASS_TGX16CONSTSIZE:
        return DocumentType::Static;

    case GM1_ENTRY_CLASS_GLYPH:
        return DocumentType::Font;

    case GM1_ENTRY_CLASS_BITMAP:
    case GM1_ENTRY_CLASS_ANOTHERBITMAP:
        return DocumentType::Bitmap;

    case GM1_ENTRY_CLASS_TILE:
        return DocumentType::Tile;
    }
    return DocumentType::Invalid;
}

DocumentLoader::DocumentLoader(QObject *parent)
    : Operation(parent)
    , mLogModel(new LogModel(this))
{}

void DocumentLoader::start()
{
    Gm1Loader *loader = new Gm1Loader(this);
    connect(loader, &Gm1Loader::progress, this, &DocumentLoader::progress);
    connect(loader, &Gm1Loader::diagnosticsError, mLogModel, &LogModel::addError);
    connect(loader, &Gm1Loader::diagnosticsWarning, mLogModel, &LogModel::addWarning);
    if (loader->loadFromFile(mFilePath)) {
        mGm1 = loader->fileData();
        mSuccess = true;
    } else {
        mSuccess = false;
    }

    loader->deleteLater();

    emit finished();
}

fs::path DocumentLoader::filePath() const
{
    return mFilePath;
}

void DocumentLoader::setFilePath(const fs::path &filepath)
{
    mFilePath = filepath;
}

std::unique_ptr<EditorFactory> DocumentLoader::createEditorFactory() const
{
    std::unique_ptr<EditorFactory> factory(new EditorFactory);
    factory->setDocumentType(Gm1ClassToDocumentType(mGm1.entryClass()));
    //! \todo hide this detail in additional method
    if (mGm1.entryClass() == GM1_ENTRY_CLASS_TGX16CONSTSIZE) {
        factory->setConstSize(true);
    } else if (mGm1.entryClass() == GM1_ENTRY_CLASS_ANOTHERBITMAP) {
        factory->setMagicBitmap(true);
    }
    factory->setFileData(mGm1);
    return factory;
}

QString DocumentLoader::status() const
{
    return tr("Loading");
}

LogModel *DocumentLoader::logs() const
{
    return mLogModel;
}

bool DocumentLoader::success() const
{
    return mSuccess;
}

} // namespace gmtool
