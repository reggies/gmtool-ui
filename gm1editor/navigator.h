#pragma once

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPaintEvent>
#include <QWidget>

namespace gmtool {
class NavigatorPopup;

class Navigator : public QWidget
{
    Q_OBJECT

public:
    explicit Navigator(QGraphicsScene *scene, QWidget *parent = nullptr);
    ~Navigator();

    QSize minimumSizeHint() const override;

signals:
    void navigated(const QPoint &pos, const QSize &size);

protected:
    void paintEvent(QPaintEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    void onPopupReleased();

private:
    QGraphicsScene *mScene;
    NavigatorPopup *mPopup;
};

} // namespace gmtool
