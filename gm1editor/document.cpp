#include "document.h"

#include <vector>

#include <boost/range/adaptor/reversed.hpp>

#include "documenteditor.h"
#include "documentloader.h"
#include "documentoperation.h"
#include "documentwriter.h"
#include "editorfactory.h"
#include "insertcommand.h"
#include "logmodel.h"
#include "movecommand.h"
#include "palettedialog.h"
#include "paletterecord.h"
#include "spriteloader.h"
#include "spriteplotter.h"
#include "spriterecord.h"
#include "tableid.h"
#include "tablemodel.h"
#include "undo.h"
#include "documentobject.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QString>
#include <QStringList>
#include <QThread>

namespace gmtool {

Document::Document(QObject *parent)
    : QObject(parent)
{
    mOperation = new DocumentOperation(this);

    mLogModel = new LogModel(this);

    connect(mOperation, &DocumentOperation::stateChanged, this, &Document::operationChanged);
}

Document::~Document()
{}

python::DocumentObject* Document::pythonObject()
{
    if (mEditor == nullptr) {
        return nullptr;
    }
    if (mDocumentObject == nullptr) {
        mDocumentObject = std::make_unique<python::DocumentObject>(&mUndoStack, mEditor.get());
    }
    return mDocumentObject.get();
}

LogModel *Document::logModel()
{
    return mLogModel;
}

undo::Stack *Document::undoStack()
{
    return &mUndoStack;
}

DocumentEditor *Document::editor()
{
    return mEditor.get();
}

boost::filesystem::path Document::filePath() const
{
    return mPath;
}

void Document::setFilePath(const boost::filesystem::path &path)
{
    mPath = path;
    emit filePathChanged();
}

bool Document::isEmpty() const
{
    return !mEditor;
}

void Document::setEditorFactory(const EditorFactory &factory)
{
    auto editor = factory.createEditor(mLogModel);
    if (editor) {
        setEditor(std::move(editor));
        mLogModel->clear();
    }
}

void Document::loadFromFile(const boost::filesystem::path &path)
{
    DocumentLoader *operation = new DocumentLoader;
    operation->setFilePath(path);

    connect(operation,
            &DocumentLoader::finished,
            std::bind(&Document::loaderFinished, this, operation));

    if (!mOperation->pushOperation(operation)) {
        operation->deleteLater();
    }
}

void Document::loaderFinished(DocumentLoader *loader)
{
    if (loader->success()) {
        mLogModel->clear();
        mLogModel->merge(loader->logs());
        auto editor = loader->createEditorFactory()->createEditor(mLogModel);
        if (editor) {
            mOperation->setStatusString(tr("Done"));
            setFilePath(loader->filePath());
            setEditor(std::move(editor));
        } else {
            mOperation->setStatusString(tr("Unsupported file type"));
        }
        mLogModel->addMessage(QString("Loaded file from %1")
                                  .arg(QString::fromStdWString(loader->filePath().wstring())),
                              Severity::Information);
    } else {
        mOperation->setStatusString(tr("Could not open file"));
        mLogModel->clear();
        mLogModel->merge(loader->logs());
    }

    loader->deleteLater();
}

void Document::saveToFile(const boost::filesystem::path &path)
{
    if (!mEditor)
        return;

    Gm1FileData gm1;
    if (!mEditor->save(&gm1, mLogModel)) {
        return;
    }

    DocumentWriter *operation = new DocumentWriter;
    operation->setFilePath(path);
    operation->setFileData(gm1);

    connect(operation,
            &DocumentWriter::finished,
            std::bind(&Document::writerFinished, this, operation));

    if (!mOperation->pushOperation(operation)) {
        operation->deleteLater();
    }
}

void Document::writerFinished(DocumentWriter *writer)
{
    mLogModel->merge(writer->logs());
    if (writer->success()) {
        mOperation->setStatusString(tr("Could not save file: %1"));
    } else {
        setFilePath(writer->filePath());
        mLogModel->addMessage(QString("File saved to %1")
                                  .arg(QString::fromStdWString(writer->filePath().wstring())),
                              Severity::Information);
    }
    writer->deleteLater();
}

DocumentOperation *Document::operation()
{
    return mOperation;
}

QString Document::name() const
{
    return mDocumentName;
}

void Document::setName(const QString &name)
{
    mDocumentName = name;
}

void Document::setEditor(std::unique_ptr<DocumentEditor> editor)
{
    if (editor.get() != mEditor.get()) {
        mEditor = std::move(editor);
        mDocumentObject = nullptr;
        mUndoStack.clear();
        emit editorChanged();
    }
}

void Document::moveForward(const std::set<int> &spriteIndices)
{
    TableModel *model = mEditor->getModel(TableId::Spritesheet);
    if (spriteIndices.empty() || spriteIndices.size() == static_cast<size_t>(model->rowCount())) {
        return;
    }
    auto macro = std::make_unique<undo::Macro>(tr("Move forward"));
    for (int row : spriteIndices | boost::adaptors::reversed) {
        macro->add(std::make_unique<MoveCommand>(model, row, 1, row + 2));
    }
    mUndoStack.push(std::move(macro));
}

void Document::moveBackward(const std::set<int> &spriteIndices)
{
    if (spriteIndices.find(0) != spriteIndices.end()) {
        return;
    }
    TableModel *model = mEditor->getModel(TableId::Spritesheet);
    if (spriteIndices.empty() || spriteIndices.size() == static_cast<size_t>(model->rowCount())) {
        return;
    }
    auto macro = std::make_unique<undo::Macro>(tr("Move backward"));
    for (int row : spriteIndices) {
        macro->add(std::make_unique<MoveCommand>(model, row, 1, row - 1));
    }
    mUndoStack.push(std::move(macro));
}

void Document::bringForward(const std::set<int> &spriteIndices)
{
    TableModel *model = mEditor->getModel(TableId::Spritesheet);
    if (spriteIndices.empty() || spriteIndices.size() == static_cast<size_t>(model->rowCount())) {
        return;
    }
    auto macro = std::make_unique<undo::Macro>(tr("Bring forward"));
    int rowsInserted = 0;
    for (int row : spriteIndices | boost::adaptors::reversed) {
        macro->add(std::make_unique<MoveCommand>(model, row, 1, model->rowCount() - rowsInserted));
        ++rowsInserted;
    }
    mUndoStack.push(std::move(macro));
}

void Document::sendBack(const std::set<int> &spriteIndices)
{
    TableModel *model = mEditor->getModel(TableId::Spritesheet);
    if (spriteIndices.empty() || spriteIndices.size() == static_cast<size_t>(model->rowCount())) {
        return;
    }
    auto macro = std::make_unique<undo::Macro>(tr("Send back"));
    int nextRowToInsert = 0;
    for (int row : spriteIndices) {
        macro->add(std::make_unique<MoveCommand>(model, row, 1, nextRowToInsert));
        ++nextRowToInsert;
    }
    mUndoStack.push(std::move(macro));
}

void Document::importSprites(QWidget *parent, const QPoint &advisedPosition)
{
    const QStringList &fileNames = QFileDialog::getOpenFileNames(parent, tr("Select file"));
    if (fileNames.empty()) {
        return;
    }
    std::vector<boost::filesystem::path> loadPaths;
    for (const QString &filePath : fileNames) {
        loadPaths.push_back(boost::filesystem::path(filePath.toUtf8().constData()));
    }
    auto loader = mEditor->createSpriteLoader();
    loader->setLogModel(mLogModel);
    if (!advisedPosition.isNull()) {
        loader->setPosition(advisedPosition);
    }
    auto sprites = loader->loadSprites(loadPaths, parent);
    if (sprites.empty()) {
        return;
    }
    TableModel *model = mEditor->getModel(TableId::Spritesheet);
    auto macro = std::make_unique<undo::Macro>(tr("Add %1 sprite(s)").arg(sprites.size()));
    for (auto &sprite : sprites) {
        macro->add(std::make_unique<InsertCommand>(std::move(sprite), model->rowCount(), model));
    }
    if (!mUndoStack.push(std::move(macro))) {
        QMessageBox::critical(parent, tr("Error"), tr("Sprites could not be loaded"));
    }
}

void Document::exportSprites(QWidget *parent, const std::set<RecordId> &ids)
{
    if (ids.empty()) {
        return;
    }
    const QString &savePath = QFileDialog::getSaveFileName(parent, tr("Save sprite"));
    if (savePath.isEmpty()) {
        return;
    }
    QImage image;
    if (!mEditor->renderSprites(ids, mLogModel, &image)) {
        return;
    }
    auto paletteModel = mEditor->getModel(TableId::Palette);
    if (image.format() == QImage::Format_Indexed8 && paletteModel != nullptr) {
        const RecordId &paletteId = PaletteDialog::getPaletteId(mEditor.get(), parent);
        if (!paletteId.isValid()) {
            return;
        }
        PaletteRecord palette;
        palette.assign(*mEditor->getModel(TableId::Palette)->getRecord(paletteId));
        QVector<QRgb> colorTable;
        for (const QColor &color : palette.colors()) {
            colorTable.push_back(color.rgb());
        }
        image.setColorTable(colorTable);
    }
    image.save(savePath);
}

void Document::addEntry(QWidget *parent, const QPoint &advisedPosition, int beforeRow)
{
    TableModel *model = mEditor->getModel(TableId::Layout);
    int row = (beforeRow == -1 ? model->rowCount() : beforeRow);
    auto entry = mEditor->createBlankEntry();
    auto cmd = std::make_unique<InsertCommand>(std::move(entry), row, model);
    if (!mUndoStack.push(std::move(cmd))) {
        QMessageBox::critical(parent, tr("Error"), tr("Entry could not be created"));
    }
}

} // namespace gmtool
