#include "gridgraphicsview.h"

#include <QWheelEvent>

#include <cmath>
#include <limits>

#include <QDebug>
#include <QPainter>
#include <QScrollBar>

namespace gmtool {

GridGraphicsView::GridGraphicsView(QWidget *parent)
    : QGraphicsView(parent)
{
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void GridGraphicsView::setShowGrid(bool on)
{
    if (mShowGrid != on) {
        mShowGrid = on;
        if (scene() != nullptr) {
            scene()->invalidate(QRectF(), QGraphicsScene::ForegroundLayer);
        }
        emit showGridChanged(on);
    }
}

void GridGraphicsView::drawForeground(QPainter *painter, const QRectF &rect)
{
    if (mShowGrid) {
        painter->save();
        painter->setCompositionMode(QPainter::CompositionMode_Exclusion);

        auto xform = viewportTransform().inverted();

        QPen major;
        major.setCosmetic(true);
        major.setColor(QColor(128, 128, 128, 128));
        major.setWidth(2.0);
        drawGrid(painter, rect, xform, 64, major);

        QPen minor;
        minor.setCosmetic(true);
        minor.setColor(QColor(128, 128, 128, 128));
        drawGrid(painter, rect, xform, 8, minor);

        painter->restore();
    }
}

void GridGraphicsView::drawGrid(QPainter *painter,
                                const QRectF &rect,
                                const QTransform &xform,
                                float strokeLength,
                                const QPen &pen) const
{
    QVarLengthArray<QLineF, 256> lines;

    painter->setPen(pen);

    auto yunit = xform.map(QLineF(0, 0, 0, strokeLength));
    auto xunit = xform.map(QLineF(0, 0, strokeLength, 0));

    auto stride = QPointF(qAbs(xunit.x2() - xunit.x1()), qAbs(yunit.y2() - yunit.y1()));

    auto left = rect.left() - fmod(rect.left(), stride.x());
    auto top = rect.top() - fmod(rect.top(), stride.y());

    for (auto x = left; x < rect.right(); x += stride.x()) {
        lines.append(QLineF(x, rect.top(), x, rect.bottom()));
    }
    for (auto y = top; y < rect.bottom(); y += stride.y()) {
        lines.append(QLineF(rect.left(), y, rect.right(), y));
    }

    painter->drawLines(lines.data(), lines.size());
}

void GridGraphicsView::wheelEvent(QWheelEvent *event)
{
    /*!
      zooming while dragging doesn't work as it should so
      disable it
     */
    if (!mScrolling && !(event->buttons() & Qt::AllButtons)) {
        mScrollDelta += event->angleDelta().y();
        auto scaleFactor = 1.0L;
        while (mScrollDelta >= 120) {
            scaleFactor *= 2;
            mScrollDelta -= 120;
        }
        while (mScrollDelta <= -120) {
            scaleFactor /= 2;
            mScrollDelta += 120;
        }

        if (std::abs(scaleFactor - 1.0L) > std::numeric_limits<double>::epsilon()) {
            scale(scaleFactor, scaleFactor);
            recalculateSceneRect();
            emit scaleChanged(scaleFactor);
        }
    }
    event->accept();
}

void GridGraphicsView::scrollContentsBy(int dx, int dy)
{
    QGraphicsView::scrollContentsBy(dx, dy);

    /*!
      \todo on first occassion it seems like I need to recalculate the scene
      rect right in here.

      this is because wheelEvent() and probably some more functions
      will eventually end up calling this method.

      however, doing this causes an infinite loop on windows, at least,
      and double scrolling on linux, at best.
    */
}

void GridGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if (event->buttons() == Qt::RightButton) {
        QGraphicsView::mousePressEvent(event);
        return;
    }
    if (
        /*! always drag on the middle button press */
        event->buttons() == Qt::MidButton ||

        /*! also, hook into default hand dragging of the QGraphicsView */
        ((dragMode() == QGraphicsView::ScrollHandDrag) && event->buttons() == Qt::LeftButton)) {
        mScrolling = true;
        mLastPos = event->pos();
        event->accept();
        return;
    }
    QGraphicsView::mousePressEvent(event);
}

void GridGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if (mScrolling) {
        mScrolling = false;
        event->accept();
        return;
    }
    QGraphicsView::mouseReleaseEvent(event);
}

void GridGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    if (mScrolling) {
        auto delta = event->pos() - mLastPos;
        mLastPos = event->pos();

        auto itemsRect = scene()->itemsBoundingRect().toRect();

        {
            auto hBar = horizontalScrollBar();
            auto hValue = hBar->value() - (int) (isRightToLeft() ? -delta.x() : delta.x());
            auto hValueMaximum = qMax(hValue, qMin(itemsRect.right(), hBar->maximum()));
            auto hValueMinimum = qMin(hValue, qMax(itemsRect.left(), hBar->minimum()));
            hBar->setRange(hValueMinimum, hValueMaximum);
            hBar->setValue(hValue);
        }

        {
            auto vBar = verticalScrollBar();
            auto vValue = vBar->value() - (int) delta.y();
            auto vValueMaximum = qMax(vValue, qMin(itemsRect.bottom(), vBar->maximum()));
            auto vValueMinimum = qMin(vValue, qMax(itemsRect.top(), vBar->minimum()));
            vBar->setRange(vValueMinimum, vValueMaximum);
            vBar->setValue(vValue);
        }

        event->accept();
        return;
    }
    QGraphicsView::mouseMoveEvent(event);
}

void GridGraphicsView::recalculateSceneRect()
{
    /*! \todo scrollbars scroll beyond viewport and items rect

      1. frameWidth() must be substracted or else the scrollbars will be
      incorrectly positioned by the QGraphicsView::recalculateContentsSize()

      2. QRectF is a must, not some pity QRect

      3. viewport()->size() is better way to do this
     */

    auto xform = viewportTransform().inverted();

    auto cameraRect =
        /*!
          I don't use viewport()->geometry() search a bug description below for
          more details.

            xform.mapRect(QRectF(viewport()->geometry()))
        */
        xform.mapRect(QRectF(rect()));

    /*! \todo on Windows this causes stack overflow:

      PlotWindow::onSceneRectChanged
      GridGraphicsView::recalculateSceneRect
      GridGraphicsView::scrollContentsBy
      PlotWindow::onSceneRectChanged
      ...

     */

    /* \bug
       fresh problem with the items movement on the scene:

       when an item is moved the camera rect is computed improperly -
       it is slighly shifted to the bottom-right.

       the value of shift depends on the current zoom.
     */

    /*!
      note that viewportTransform() will be changed by the call
      to QGraphicsView::setSceneRect().
     */
    setSceneRect(scene()->itemsBoundingRect() | cameraRect);
}

void GridGraphicsView::center(qreal sceneX, qreal sceneY)
{
    /*! the only reason to duplicate this function in my own subclass
      is to put some todos here. the function is essentially a duplicate
      of QGraphicsScene::centerOn(qreal, qreal)
    */

    auto xform = transform();

    auto viewPos = xform.map(QPointF(sceneX, sceneY));

    horizontalScrollBar()->setValue(viewPos.x());
    verticalScrollBar()->setValue(viewPos.y());

    /*! \todo scene rect of the navigator's view and plot windows's
      are not synchronized
     */

    /*! \todo most likely, transformation anchor gets broken by this code */
}

void GridGraphicsView::resizeEvent(QResizeEvent *event)
{
    QGraphicsView::resizeEvent(event);

    recalculateSceneRect();
}

bool GridGraphicsView::showGrid() const
{
    return mShowGrid;
}

} // namespace gmtool
