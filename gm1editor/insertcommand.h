#pragma once

#include "undo.h"

#include <memory>

namespace gmtool {
class AbstractRecord;
class TableModel;

class InsertCommand : public undo::Command
{
public:
    InsertCommand(std::unique_ptr<AbstractRecord> record,
                  int position,
                  TableModel *model,
                  const QString &name = QString());
    ~InsertCommand() override;

    bool undo() override;
    bool redo() override;
    QString name() const override;

private:
    QString mName;
    int mPos;
    TableModel *mModel;
    std::unique_ptr<AbstractRecord> mRecord;
};

} // namespace gmtool
