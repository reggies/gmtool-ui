#include "propertieswindow.h"

#include "commanditemdelegate.h"
#include "document.h"
#include "propertiesmodel.h"

#include <QVBoxLayout>

namespace gmtool {

PropertiesWindow::PropertiesWindow(Document *doc, QWidget *parent)
    : Window(parent)
{
    setWindowTitle(tr("Properties"));

    mView = new QTreeView(this);
    mView->setModel(doc->editor()->getPropertiesModel());
    mView->setSelectionMode(QAbstractItemView::SingleSelection);
    mView->setSelectionBehavior(QAbstractItemView::SelectRows);
    mView->setItemDelegate(
        new CommandItemDelegate(doc->editor()->getPropertiesModel(), doc->undoStack(), this));

    auto layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->addWidget(mView);
}

} // namespace gmtool
