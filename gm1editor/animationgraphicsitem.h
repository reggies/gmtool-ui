#pragma once

#include "animationrecord.h"
#include "recordid.h"
#include "scenenode.h"

namespace gmtool {
class DocumentEditor;
class AnimationEditor;
class AnimationPivotGraphicsItem;

class AnimationGraphicsItem : public SceneNode
{
public:
    explicit AnimationGraphicsItem(AnimationEditor *editor,
                                   const RecordId &id,
                                   SceneNode *parent = nullptr);

    void setViewState(const ViewState &state) override;

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

    void reloadModelData() override;

    std::unique_ptr<AbstractRecord> movedRecord(const QPoint &toPoint) override;

private:
    void positionChanged();

private:
    RecordId mRecordId;
    AnimationEditor *mEditor;
    AnimationPivotGraphicsItem *mPivot;
    const AnimationRecord *mRecord;
};

} // namespace gmtool
