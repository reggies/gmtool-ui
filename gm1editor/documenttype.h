#pragma once

namespace gmtool {

enum class DocumentType { Font, Anim, Static, Tile, Bitmap, Invalid };
}
