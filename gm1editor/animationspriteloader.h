#pragma once

#include <QObject>
#include <QPoint>

#include "spriteloader.h"

namespace gmtool {
class AnimationEditor;

class AnimationSpriteLoader : public SpriteLoader
{
public:
    explicit AnimationSpriteLoader(AnimationEditor *editor);

    void setPosition(const QPoint &point) override;

    std::vector<std::unique_ptr<AbstractRecord>> loadSprites(
        const std::vector<boost::filesystem::path> &loadPaths, QWidget *parent) override;

private:
    AnimationEditor *mEditor;
    QPoint mPos;
};

} // namespace gmtool
