#pragma once

#include <QColor>
#include <QImage>

#include <gm1.h>

#include <array>
#include <vector>

#include "tilealignment.h"

namespace gmtool {

struct Gm1FileEntry
{
    gm1_entry_t Gm1 = {};
    QRect Rect;

    struct TileBox
    {
        int left = 0;
        int height = 0;
        int width = 0;
    };

    void setEntryRect(const QRect &rect);
    void setPartId(int partId);
    void setPartSize(int partSize);
    void setBox(const Gm1FileEntry::TileBox &box);
    void setAlignment(TileAlignment alignment);
    void setFontBearing(int bearing);

    QRect entryRect() const;
    int partId() const;
    int partSize() const;
    TileAlignment alignment() const;
    int fontBearing() const;
};

struct Gm1FileData
{
    constexpr static size_t MaxPaletteCount = GM1_NUM_PALETTES;
    constexpr static size_t MaxPaletteSize = GM1_NUM_COLORS;

    typedef std::array<QColor, MaxPaletteSize> palette_t;

    gm1_header_t Header;
    gm1_image_props_t Properties;
    std::array<palette_t, MaxPaletteCount> Palettes;
    std::vector<Gm1FileEntry> Entries;
    QImage Plot;
    QPoint Origin;

    void setPaletteAt(size_t index, const palette_t &palette);
    void setPivot(const QPoint &point);
    void setPlotImage(const QImage &image);
    void setPlotOrigin(const QPoint &plotOrigin);
    void setEntryCount(size_t num_entries);
    void setEntryClass(gm1_entry_class_t entry_class);

    bool hasPivot() const;
    bool hasPalette() const;
    size_t paletteCount() const;
    bool hasTransparency() const;
    gm1_u16_t transparentPixel() const;
    palette_t paletteAt(size_t index) const;
    QPoint pivot() const;
    QImage plotImage() const;
    QPoint plotOrigin() const;
    size_t entryCount() const;
    gm1_entry_class_t entryClass() const;
};

} // namespace gmtool
