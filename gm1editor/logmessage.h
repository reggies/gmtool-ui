#pragma once

#include <QDateTime>

#include "severity.h"

namespace gmtool {

class LogMessage
{
public:
    QDateTime timestamp;
    QString text;
    Severity severity = Severity::Error;
    bool viewed = false;
};

inline static bool operator<(const LogMessage &lhs, const LogMessage &rhs)
{
    return lhs.timestamp < rhs.timestamp;
}

} // namespace gmtool
