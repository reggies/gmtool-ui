#pragma once

#include <memory>

#include "recordid.h"
#include "undo.h"

namespace gmtool {
class TableModel;
class AbstractRecord;

class AssignCommand : public undo::Command
{
public:
    AssignCommand(int row, TableModel *model, const AbstractRecord &record);

    ~AssignCommand() override;

    bool undo() override;
    bool redo() override;

    QString name() const override;

private:
    int mRow;
    TableModel *mModel;
    std::unique_ptr<AbstractRecord> mRecord;
    std::unique_ptr<AbstractRecord> mOldRecord;
};

} // namespace gmtool
