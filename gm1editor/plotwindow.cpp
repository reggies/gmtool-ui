#include "plotwindow.h"

#include <map>
#include <set>

#include <QDebug>
#include <QGraphicsScene>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QMenu>
#include <QScrollBar>
#include <QToolBar>
#include <QToolButton>
#include <QVBoxLayout>

#include <QApplication>

#include "assigncommand.h"
#include "document.h"
#include "gridgraphicsview.h"
#include "gridscene.h"
#include "mathutils.inl"
#include "navigator.h"
#include "nodeattribute.h"
#include "palettecombobox.h"
#include "propertiesmodel.h"
#include "repeaternode.h"
#include "scenenode.h"
#include "tableid.h"
#include "tablemodel.h"
#include "viewstate.h"
#include "zoomcombobox.h"

namespace gmtool {

PlotWindow::PlotWindow(Document *document, QWidget *parent)
    : Window(parent)
    , mDoc(document)
{
    mScene = new GridScene(this);

    prepareScene();

    connect(mScene, &GridScene::itemsMovementFinished, this, &PlotWindow::itemsMovementFinished);
    connect(mScene, &GridScene::itemsMovementReset, this, &PlotWindow::itemsMovementReset);
    connect(mScene, &QGraphicsScene::sceneRectChanged, this, &PlotWindow::onSceneRectChanged);
    connect(mScene, &QGraphicsScene::changed, this, &PlotWindow::onSceneChanged);
    connect(mScene, &GridScene::cursorMoved, this, &PlotWindow::cursorScenePosChanged);

    connect(mScene, &QGraphicsScene::selectionChanged, this, &PlotWindow::selectedNodesChanged);

    mPlotView = new GridGraphicsView(this);
    mPlotView->setRenderHint(QPainter::SmoothPixmapTransform, false);

    /*! all our graphic items will handle painter state by themselves
     */
    mPlotView->setOptimizationFlag(QGraphicsView::DontSavePainterState, true);

    mPlotView->setScene(mScene);

    /*! to track mouse pointer outside of the window.
      drag undo commands use this.
    */
    mPlotView->setMouseTracking(true);

    /*! allow the window to resize properly without loosing scroll position */
    mPlotView->setResizeAnchor(QGraphicsView::AnchorViewCenter);

    mPlotView->setContextMenuPolicy(Qt::ActionsContextMenu);

    createActions();

    auto layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->addWidget(mPlotView);

    /*!
      add our own zoom combo box widget to the plot view.
      QScrollArea::addScrollWidget() is does not suffice because
      it changes the height of the QComboBox.
     */
    auto viewScrollLayout = new QGridLayout;

    viewScrollLayout->setMargin(0);
    viewScrollLayout->setSpacing(0);

    auto hScroll = new QScrollBar(Qt::Horizontal, this);
    auto vScroll = new QScrollBar(Qt::Vertical, this);

    mPlotView->setHorizontalScrollBar(hScroll);
    mPlotView->setVerticalScrollBar(vScroll);

    auto corner = new Navigator(mScene, this);

    connect(corner, &Navigator::navigated, this, [this](const QPoint &pos, const QSize &size) {
        auto sceneRect = mPlotView->sceneRect();

        mPlotView->center((qreal) pos.x() * sceneRect.width() / size.width(),
                          (qreal) pos.y() * sceneRect.height() / size.height());
    });

    // +-----------------------------------------+---+
    // |                                         |   |
    // |                PlotView                 | V |
    // |                                         |   |
    // +--------+-----------+---+----------------+---+
    // |  Zoom  | Palette   |   |       H        | C |
    // +--------+-----------+---+----------------+---+

    hScroll->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);

    enum {
        ROW_0,
        ROW_1,
    };

    enum {
        ROW_SPAN_1 = 1,
        ROW_SPAN_2,
    };

    enum {
        COL_SPAN_1 = 1,
        COL_SPAN_2,
        COL_SPAN_3,
        COL_SPAN_4,
    };

    enum { COL_0, COL_1, COL_2, COL_3, COL_4 };

    auto zoomBox = new ZoomComboBox(this);
    zoomBox->setGraphicsView(mPlotView);
    viewScrollLayout->addWidget(zoomBox, ROW_1, COL_0, ROW_1, COL_1);
    if (mPaletteBox) {
        viewScrollLayout->addWidget(mPaletteBox, ROW_1, COL_1, ROW_1, COL_1);
    }

    viewScrollLayout->addWidget(mPlotView, ROW_0, COL_0, ROW_SPAN_1, COL_SPAN_4);
    viewScrollLayout->addWidget(hScroll, ROW_1, COL_3, ROW_SPAN_1, COL_SPAN_1, Qt::AlignTop);
    viewScrollLayout->addWidget(vScroll, ROW_0, COL_4, ROW_SPAN_1, COL_SPAN_1, Qt::AlignLeft);
    viewScrollLayout->addWidget(corner, ROW_1, COL_4);
    viewScrollLayout->setColumnMinimumWidth(COL_2, 5);

    layout->addLayout(viewScrollLayout);

    prepareView();

    setFocusProxy(mPlotView);

    mPlotView->setShowGrid(false);
    mScene->setSnapToGrid(true);
    // mScrollMode->trigger();

    setWindowTitle(tr("Plot"));
}

PlotWindow::~PlotWindow() = default;

void PlotWindow::prepareScene()
{
    // Properties
    connect(mDoc->editor()->getPropertiesModel(),
            &QAbstractItemModel::dataChanged,
            this,
            &PlotWindow::propertiesChanged);
    connect(mDoc->editor()->getPropertiesModel(),
            &QAbstractItemModel::modelReset,
            this,
            &PlotWindow::propertiesReset);

    // Palette
    if (auto palette = mDoc->editor()->getPalette()) {
        connect(palette, &QAbstractItemModel::dataChanged, this, &PlotWindow::paletteChanged);
        connect(palette, &QAbstractItemModel::modelReset, this, &PlotWindow::paletteReset);
    }

    rebuildScene();
}

void PlotWindow::prepareView()
{
    for (auto item : mLayoutLayer->childItems()) {
        mPlotView->centerOn(item);
        return;
    }
    for (auto item : mBgLayer->childItems()) {
        mPlotView->centerOn(item);
        return;
    }
}

void PlotWindow::paletteReset()
{
    mBgLayer->invalidatePalette();
    mLayoutLayer->invalidatePalette();
}

void PlotWindow::paletteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    mBgLayer->invalidatePalette();
    mLayoutLayer->invalidatePalette();
}

void PlotWindow::propertiesReset()
{
    rebuildScene();
}

void PlotWindow::propertiesChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    rebuildScene();
}

void PlotWindow::rebuildScene()
{
    mScene->clear();
    mBgLayer = new RepeaterNode(mDoc->editor()->getSpritesheet(),
                                mDoc->editor()->createSpriteFactory(),
                                nullptr);
    mBgLayer->setPropogatePaletteChanges(true);
    mScene->addItem(mBgLayer);
    mLayoutLayer = new RepeaterNode(mDoc->editor()->getEntries(),
                                    mDoc->editor()->createLayoutFactory(),
                                    nullptr);
    mScene->addItem(mLayoutLayer);

    if (mViewState) {
        setViewState(*mViewState);
    }
}

std::vector<SceneNode *> PlotWindow::selectedNodes() const
{
    auto bg = mBgLayer->selectedNodes();
    auto entries = mLayoutLayer->selectedNodes();

    bg.insert(bg.end(), entries.begin(), entries.end());
    return bg;
}

void PlotWindow::setViewState(const ViewState &state)
{
    mViewState = std::make_unique<ViewState>(state);
    mBgLayer->setViewState(*mViewState);
    mLayoutLayer->setViewState(*mViewState);
}

void PlotWindow::updateScene()
{
    ViewState state;
    state.showOverlay = mShowOverlay;
    state.paletteId = mPaletteId;
    setViewState(state);
}

void PlotWindow::paletteIndexChanged(int index)
{
    if (auto model = mDoc->editor()->getPalette()) {
        if (index < 0 || index >= model->rowCount()) {
            return;
        }
        mPaletteId = model->getIdOf(index);
        updateScene();
    }
}

void PlotWindow::addImage()
{
    mDoc->importSprites(this, mScene->mouseScenePos());
}

void PlotWindow::saveImage()
{
    auto ids = mBgLayer->selectedIds();
    if (!ids.empty()) {
        mDoc->exportSprites(this, std::set<RecordId>(ids.begin(), ids.end()));
    }
}

void PlotWindow::addEntry()
{
    mDoc->addEntry(this, mScene->mouseScenePos());
}

void PlotWindow::remove()
{
    auto macro = std::make_unique<undo::Macro>(tr("Remove items"));
    mBgLayer->removeSelectedNodes(macro.get());
    mLayoutLayer->removeSelectedNodes(macro.get());
    if (!macro->empty()) {
        if (!mDoc->undoStack()->push(std::move(macro))) {
            emit statusTextChanged(tr("Could not remove items"));
        } else {
            emit statusTextChanged(tr("Removed items"));
        }
    }
}

void PlotWindow::findSelected()
{
    auto nodes = selectedNodes();
    if (!nodes.empty()) {
        SceneNode *node = nodes.front();
        for (auto item : selectedNodes()) {
            if (item != node) {
                item->setSelected(false);
            }
        }
        RecordId id = node->data(NodeAttribute::RecordId).value<RecordId>();
        TableId table = node->data(NodeAttribute::TableId).value<TableId>();
        if (id.isValid() && (table != TableId::Invalid)) {
            emit findRequested(table, id);
        }
    }
}

void PlotWindow::createActions()
{
    if (mDoc->editor()->getPalette() != nullptr) {
        mPaletteId = mDoc->editor()->getPalette()->getIdOf(0);
        auto paletteBox = new PaletteComboBox(mDoc->editor(), this);
        connect(paletteBox,
                QOverload<int>::of(&QComboBox::currentIndexChanged),
                this,
                &PlotWindow::paletteIndexChanged);
        paletteBox->setCurrentIndex(0);
        mPaletteBox = paletteBox;
    }
}

void PlotWindow::onSceneRectChanged(const QRectF &rect)
{
    Q_UNUSED(rect);

    mPlotView->recalculateSceneRect();
}

void PlotWindow::onSceneChanged(const QList<QRectF> &region)
{
    Q_UNUSED(region);

    mPlotView->recalculateSceneRect();
}

void PlotWindow::updateInteractionMode()
{
    if (mScene->interactionMode() == InteractionMode::Transform) {
        if (!mAllowEntriesSelection) {
            mLayoutLayer->unselectAll();
            mLayoutLayer->allowSelection(false);
        } else {
            mLayoutLayer->allowSelection(true);
        }
        if (!mAllowSpritesSelection) {
            mBgLayer->unselectAll();
            mBgLayer->allowSelection(false);
        } else {
            mBgLayer->allowSelection(true);
        }
    } else {
        mLayoutLayer->unselectAll();
        mBgLayer->unselectAll();
        mLayoutLayer->allowSelection(false);
        mBgLayer->allowSelection(false);
    }
}

void PlotWindow::setAllowSelectSprites(bool allow)
{
    if (mAllowSpritesSelection != allow) {
        mAllowSpritesSelection = allow;
        updateInteractionMode();
    }
}

void PlotWindow::setAllowSelectEntries(bool allow)
{
    if (mAllowEntriesSelection != allow) {
        mAllowEntriesSelection = allow;
        updateInteractionMode();
    }
}

void PlotWindow::itemsMovementFinished()
{
    std::vector<std::unique_ptr<undo::Command>> commands;
    commands.reserve(mScene->transformingItems().size());
    for (auto item : mScene->transformingItems()) {
        auto node = qgraphicsitem_cast<SceneNode *>(item);
        if (node == nullptr) {
            continue;
        }
        RecordId recordId = node->data(NodeAttribute::RecordId).value<RecordId>();
        TableId tableId = node->data(NodeAttribute::TableId).value<TableId>();
        auto newRecord = node->movedRecord(floorPoint(node->pos()));
        if (newRecord != nullptr) {
            auto table = mDoc->editor()->getModel(tableId);
            auto row = table->getPositionOf(recordId);
            commands.emplace_back(new AssignCommand(row, table, *newRecord));
        }
    }
    auto macro = std::make_unique<undo::Macro>(tr("Move %1 items").arg(commands.size()));
    for (auto &command : commands) {
        macro->add(std::move(command));
    }
    mDoc->undoStack()->push(std::move(macro));
}

void PlotWindow::itemsMovementReset()
{
    for (auto item : mScene->transformingItems()) {
        if (auto node = qgraphicsitem_cast<SceneNode *>(item)) {
            node->reloadModelData();
        }
    }
}

void PlotWindow::moveForward()
{
    auto indices = mBgLayer->selectedIndices();
    mDoc->moveForward(std::set<int>(indices.begin(), indices.end()));
}

void PlotWindow::moveBackward()
{
    auto indices = mBgLayer->selectedIndices();
    mDoc->moveBackward(std::set<int>(indices.begin(), indices.end()));
}

void PlotWindow::bringForward()
{
    auto indices = mBgLayer->selectedIndices();
    mDoc->bringForward(std::set<int>(indices.begin(), indices.end()));
}

void PlotWindow::sendBack()
{
    auto indices = mBgLayer->selectedIndices();
    mDoc->sendBack(std::set<int>(indices.begin(), indices.end()));
}

void PlotWindow::selectAll()
{
    mBgLayer->selectAll();
    mLayoutLayer->selectAll();
}

void PlotWindow::selectClear()
{
    mBgLayer->unselectAll();
    mLayoutLayer->unselectAll();
}

void PlotWindow::selectedNodesChanged()
{
    auto spritesFound = mBgLayer->selectedCount();
    auto entriesFound = mLayoutLayer->selectedCount();
    emit gotSingleItemSelected(spritesFound + entriesFound == 1);
    emit gotSpriteOrEntrySelected(spritesFound + entriesFound != 0);
    emit gotSpriteButNoEntrySelected(spritesFound != 0 && entriesFound == 0);
}

void PlotWindow::setShowOverlay(bool showOverlay)
{
    if (mShowOverlay != showOverlay) {
        mShowOverlay = showOverlay;
        updateScene();
    }
}

void PlotWindow::setScrollMode()
{
    if (mScene->interactionMode() != InteractionMode::Scroll) {
        mScene->setInteractionMode(InteractionMode::Scroll);
        mPlotView->setDragMode(QGraphicsView::ScrollHandDrag);
        updateInteractionMode();
    }
}

void PlotWindow::setTransformMode()
{
    if (mScene->interactionMode() != InteractionMode::Transform) {
        mScene->setInteractionMode(InteractionMode::Transform);
        mPlotView->setDragMode(QGraphicsView::RubberBandDrag);
        updateInteractionMode();
    }
}

void PlotWindow::route(ImportSprites *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::addImage);
    action->setEnabled(true);
    mPlotView->addAction(action);
}

void PlotWindow::unroute(ImportSprites *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(ExportSprites *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::saveImage);
    connect(this, &PlotWindow::gotSpriteButNoEntrySelected, action, &QAction::setEnabled);
    selectedNodesChanged();
    mPlotView->addAction(action);
}

void PlotWindow::unroute(ExportSprites *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(FindSpriteOrEntry *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::findSelected);
    connect(this, &PlotWindow::gotSingleItemSelected, action, &QAction::setEnabled);
    selectedNodesChanged();
    mPlotView->addAction(action);
}

void PlotWindow::unroute(FindSpriteOrEntry *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(RemoveSpriteOrEntry *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::remove);
    connect(this, &PlotWindow::gotSpriteOrEntrySelected, action, &QAction::setEnabled);
    selectedNodesChanged();
    mPlotView->addAction(action);
}

void PlotWindow::unroute(RemoveSpriteOrEntry *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(AddEntry *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::addEntry);
    action->setEnabled(true);
    mPlotView->addAction(action);
}

void PlotWindow::unroute(AddEntry *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(MoveForward *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::moveForward);
    connect(this, &PlotWindow::gotSpriteButNoEntrySelected, action, &QAction::setEnabled);
    mPlotView->addAction(action);
    selectedNodesChanged();
}

void PlotWindow::unroute(MoveForward *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(MoveBackward *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::moveBackward);
    connect(this, &PlotWindow::gotSpriteButNoEntrySelected, action, &QAction::setEnabled);
    selectedNodesChanged();
    mPlotView->addAction(action);
}

void PlotWindow::unroute(MoveBackward *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(BringForward *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::bringForward);
    connect(this, &PlotWindow::gotSpriteButNoEntrySelected, action, &QAction::setEnabled);
    selectedNodesChanged();
    mPlotView->addAction(action);
}

void PlotWindow::unroute(BringForward *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(SendBack *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::sendBack);
    connect(this, &PlotWindow::gotSpriteButNoEntrySelected, action, &QAction::setEnabled);
    selectedNodesChanged();
    mPlotView->addAction(action);
}

void PlotWindow::unroute(SendBack *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(SelectAll *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::selectAll);
    action->setEnabled(true);
    mPlotView->addAction(action);
}

void PlotWindow::unroute(SelectAll *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(SelectClear *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::selectClear);
    connect(this, &PlotWindow::gotSpriteOrEntrySelected, action, &QAction::setEnabled);
    selectedNodesChanged();
    mPlotView->addAction(action);
}

void PlotWindow::unroute(SelectClear *action)
{
    disconnect(this, nullptr, action, nullptr);
    disconnect(action, nullptr, this, nullptr);
    mPlotView->removeAction(action);
    action->setDisabled(true);
}

void PlotWindow::route(ScrollMode *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::setScrollMode);
    if (action->isChecked())
        setScrollMode();
    action->setEnabled(true);
}

void PlotWindow::route(TransformMode *action)
{
    connect(action, &QAction::triggered, this, &PlotWindow::setTransformMode);
    if (action->isChecked())
        setTransformMode();
    action->setEnabled(true);
}

void PlotWindow::route(AllowSelectSprites *action)
{
    connect(action, &QAction::toggled, this, &PlotWindow::setAllowSelectSprites);
    setAllowSelectSprites(action->isChecked());
    action->setEnabled(true);
}

void PlotWindow::route(AllowSelectEntries *action)
{
    connect(action, &QAction::toggled, this, &PlotWindow::setAllowSelectEntries);
    setAllowSelectEntries(action->isChecked());
    action->setEnabled(true);
}

void PlotWindow::route(SnapToGrid *action)
{
    connect(action, &QAction::toggled, mScene, &GridScene::setSnapToGrid);
    mScene->setSnapToGrid(action->isChecked());
    action->setEnabled(true);
}

void PlotWindow::route(ShowGrid *action)
{
    connect(action, &QAction::toggled, mPlotView, &GridGraphicsView::setShowGrid);
    connect(action, &QAction::toggled, mScene, &GridScene::setSolidBackground);
    mPlotView->setShowGrid(action->isChecked());
    mScene->setSolidBackground(action->isChecked());
    action->setEnabled(true);
}

void PlotWindow::route(ShowOverlay *action)
{
    connect(action, &QAction::toggled, this, &PlotWindow::setShowOverlay);
    mShowOverlay = action->isChecked();
    updateScene();
    action->setEnabled(true);
}

} // namespace gmtool
