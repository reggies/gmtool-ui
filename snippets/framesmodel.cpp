#include "framesmodel.h"

#include "spriteplotter.h"
#include "logmodel.h"

#include <set>

namespace gmtool {


FramesModel::FramesModel(QObject *parent)
    : QAbstractItemModel(parent)
{}

void FramesModel::setLayoutModel(AnimationModel *layoutModel)
{
    beginResetModel();
    
    if (mLayoutModel) {
        disconnect(this, nullptr, mLayoutModel, nullptr);
        disconnect(mLayoutModel, nullptr, this, nullptr);
    }

    mLayoutModel = layoutModel;

    connect(mLayoutModel, &QAbstractItemModel::rowsInserted,
            this, &FramesModel::entryInserted);
    connect(mLayoutModel, &QAbstractItemModel::rowsAboutToBeRemoved,
            this, &FramesModel::entryAboutToBeRemoved);
    connect(mLayoutModel, &QAbstractItemModel::modelReset,
            this, &FramesModel::entryReset);
    connect(mLayoutModel, &QAbstractItemModel::dataChanged,
            this, &FramesModel::entryChanged);
    connect(mLayoutModel, &QAbstractItemModel::rowsMoved,
            this, &FramesModel::entryMoved);

    rebuildRecordList();

    endResetModel();
}

void FramesModel::setSpriteModel(SpriteModel *spriteModel)
{
    beginResetModel();
    
    if (mSpriteModel) {
        disconnect(this, nullptr, mSpriteModel, nullptr);
        disconnect(mSpriteModel, nullptr, this, nullptr);
    }

    mSpriteModel = spriteModel;

    connect(mSpriteModel, &QAbstractItemModel::rowsInserted,
            this, &FramesModel::spriteInserted);
    connect(mSpriteModel, &QAbstractItemModel::rowsAboutToBeRemoved,
            this, &FramesModel::spriteAboutToBeRemoved);
    connect(mSpriteModel, &QAbstractItemModel::modelReset,
            this, &FramesModel::spriteReset);
    connect(mSpriteModel, &QAbstractItemModel::dataChanged,
            this, &FramesModel::spriteChanged);
    connect(mSpriteModel, &QAbstractItemModel::rowsMoved,
            this, &FramesModel::spriteMoved);

    endResetModel();
}

void FramesModel::setFrameGroupModel(AnimationGroupModel *groupModel)
{
    beginResetModel();

    if (mFrameGroupModel) {
        disconnect(this, nullptr, mFrameGroupModel, nullptr);
        disconnect(mFrameGroupModel, nullptr, this, nullptr);
    }

    mFrameGroupModel = groupModel;

    connect(mFrameGroupModel, &QAbstractItemModel::rowsInserted,
            this, &FramesModel::groupInserted);
    connect(mFrameGroupModel, &QAbstractItemModel::rowsAboutToBeRemoved,
            this, &FramesModel::groupAboutToBeRemoved);
    connect(mFrameGroupModel, &QAbstractItemModel::modelReset,
            this, &FramesModel::groupReset);
    connect(mFrameGroupModel, &QAbstractItemModel::dataChanged,
            this, &FramesModel::groupChanged);
    connect(mFrameGroupModel, &QAbstractItemModel::rowsMoved,
            this, &FramesModel::groupMoved);

    rebuildRecordList();

    endResetModel();
}

void FramesModel::invalidateAllRecords()
{
    beginResetModel();
    for (FramesRecord *record : mRecords) {
        markRecordAsDirty(record);
    }
    endResetModel();
}

int FramesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    qDebug() << Q_FUNC_INFO << parent << mRecords.size();
    return mRecords.size();
}

QVariant FramesModel::data(const QModelIndex &index, int role) const
{
    if ((index.row() >= rowCount()) || (index.column() >= columnCount())) {
        return QVariant();
    }

    if (role == Qt::DisplayRole) {
        return getRecordByIndex(index.row())->toString();
    }

    return QVariant();
}

QModelIndex FramesModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid() || row < 0 || row >= rowCount() ||
        column < 0 || column >= columnCount()) {
        return QModelIndex();
    }
    return QAbstractItemModel::createIndex(row, column);
}

QModelIndex FramesModel::parent(const QModelIndex & /* index */) const
{
    return QModelIndex();
}

int FramesModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return 1;
}

QVariant FramesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    return QVariant();
}

Qt::ItemFlags FramesModel::flags(const QModelIndex &index) const
{
    if (index.column() >= columnCount()) {
        return 0;
    }

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

FramesRecord* FramesModel::getRecordByIndex(int row)
{
    return const_cast<FramesRecord*>(const_cast<const FramesModel*>(this)->getRecordByIndex(row));
}

const FramesRecord* FramesModel::getRecordByIndex(int row) const
{
    return mRecords.at(row);
}

void FramesModel::markRecordAsDirty(FramesRecord *record)
{
    record->markDirty();
}





void FramesModel::spriteInserted(const QModelIndex &parent, int first, int last)
{
    invalidateAllRecords();
}

void FramesModel::spriteMoved(
    const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row)
{
    invalidateAllRecords();
}

void FramesModel::spriteAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    invalidateAllRecords();
}

void FramesModel::spriteReset()
{
    invalidateAllRecords();
}

void FramesModel::spriteChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    invalidateAllRecords();
}





void FramesModel::entryInserted(const QModelIndex &parent, int first, int last)
{
    invalidateAllRecords();
}

void FramesModel::entryMoved(
    const QModelIndex &parent, int start, int end, const QModelIndex &destination, int row)
{
    invalidateAllRecords();
}

void FramesModel::entryAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    invalidateAllRecords();
}

void FramesModel::entryReset()
{
    invalidateAllRecords();
}

void FramesModel::entryChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    std::set<int> affectedRows;

    for(int i = topLeft.row(); i <= bottomRight.row(); ++i) {
        int index = 0;
        for(FramesRecord *record : mRecords) {
            if (record->frameGroup().indexInGroup(i)) {
                markRecordAsDirty(record);
                affectedRows.insert(index); // index
            }
            ++index;
        }
    }

    for(int n : affectedRows) {
        emit dataChanged(index(n, 0), index(n, 0));
    }
}





void FramesModel::groupInserted(const QModelIndex &parent, int first, int last)
{
    Q_ASSERT(!parent.isValid());

    beginInsertRows(QModelIndex(), first, last);
    for(int i = first; i <= last; ++i) {
        FramesRecord *record = new FramesRecord(this, mFrameGroupModel->getIdOf(i), this);
        mRecords.insert(mRecords.begin() + i, record);
    }
    endInsertRows();
}

void FramesModel::groupMoved(
    const QModelIndex &parent, int start, int end, const QModelIndex &destination, int dest)
{
    Q_ASSERT(!parent.isValid());
    Q_ASSERT(!destination.isValid());

    beginMoveRows(QModelIndex(), start, end, QModelIndex(), dest);
    if (start < dest)
    {
        std::rotate(mRecords.begin() + start,
                    mRecords.begin() + end + 1,
                    mRecords.begin() + dest);
    }
    else
    {
        std::rotate(mRecords.begin() + dest,
                    mRecords.begin() + start,
                    mRecords.begin() + end + 1);
    }
    endMoveRows();
}

void FramesModel::groupAboutToBeRemoved(const QModelIndex &parent, int first, int last)
{
    Q_ASSERT(!parent.isValid());

    beginRemoveRows(QModelIndex(), first, last);
    for(int i = first; i <= last; ++i) {
        mRecords.at(i)->deleteLater();
    }
    mRecords.erase(mRecords.begin() + first, mRecords.begin() + last);
    endRemoveRows();
}

void FramesModel::groupReset()
{
    beginResetModel();
    rebuildRecordList();
    endResetModel();
}

void FramesModel::groupChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight)
{
    for(int i = topLeft.row(); i <= bottomRight.row(); ++i) {
        markRecordAsDirty(mRecords.at(i));
    }
    emit dataChanged(index(topLeft.row(), 0), index(bottomRight.row(), 0));
}

int FramesModel::findIndexByFrameGroupId(const RecordId &id) const
{
    int index = 0;
    for(const FramesRecord *record : mRecords) {
        if (record->frameGroupId() == id) {
            return index;
        }
        ++index;
    }
    return -1;
}


void FramesModel::rebuildRecordList()
{
    for(FramesRecord *record : mRecords) {
        record->deleteLater();
    }
    mRecords.clear();

    if (mLayoutModel && mFrameGroupModel) {
        for(int i = 0; i < mFrameGroupModel->rowCount(); ++i) {
            FramesRecord *record = new FramesRecord(this, mFrameGroupModel->getIdOf(i), this);
            mRecords.insert(mRecords.begin() + i, record);
        }
    }
}

}
