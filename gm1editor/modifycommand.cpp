#include "modifycommand.h"

#include <stdexcept>

namespace gmtool {

ModifyCommand::ModifyCommand(const QModelIndex &index,
                             const QVariant &newValue,
                             QAbstractItemModel *model,
                             const QString &name)
    : mIndex(index)
    , mName(name)
    , mNewValue(newValue)
    , mModel(model)
{}

ModifyCommand::~ModifyCommand() = default;

bool ModifyCommand::redo()
{
    if (!mIndex.isValid()) {
        return false;
    }
    mOld = mIndex.data(Qt::EditRole);
    return mModel->setData(mIndex, mNewValue);
}

QString ModifyCommand::name() const
{
    return mName;
}

bool ModifyCommand::undo()
{
    if (!mIndex.isValid()) {
        return false;
    }
    if (!mModel->setData(mIndex, mOld)) {
        return false;
    }
    return true;
}

} // namespace gmtool
