#include "fontgraphicsitem.h"

#include "fonteditor.h"
#include "nodeattribute.h"
#include "tableid.h"
#include "tablemodel.h"
#include "viewstate.h"

#include <QDebug>
#include <QPainter>

namespace gmtool {

static QPen normalPen()
{
    QPen pen;
    pen.setCosmetic(true);
    return pen;
}

static QPen selectedPen()
{
    QPen pen;
    pen.setCosmetic(true);
    pen.setColor(QColor(255, 0, 0));
    return pen;
}

static QPen normalBaseLinePen()
{
    QPen pen(Qt::DotLine);
    pen.setCosmetic(true);
    return pen;
}

static QPen selectedBaseLinePen()
{
    QPen pen(Qt::DotLine);
    pen.setCosmetic(true);
    pen.setColor(QColor(255, 0, 0));
    return pen;
}

FontGraphicsItem::FontGraphicsItem(FontEditor *editor, const RecordId &id, SceneNode *parent)
    : SceneNode(parent)
    , mRecordId(id)
    , mEditor(editor)
{
    setData(NodeAttribute::RecordId, QVariant::fromValue(id));
    setData(NodeAttribute::TableId, QVariant::fromValue(TableId::Layout));

    mRecord = mEditor->getModel(TableId::Layout)->getRecordAs<FontRecord>(mRecordId);

    setPos(mRecord->left(), mRecord->top());
}

QRectF FontGraphicsItem::boundingRect() const
{
    return QRectF(0, 0, mRecord->width(), mRecord->height());
}

void FontGraphicsItem::setViewState(const ViewState &state)
{
    mShowOverlay = state.showOverlay;
}

void FontGraphicsItem::paint(QPainter *painter,
                             const QStyleOptionGraphicsItem *option,
                             QWidget *widget)
{
    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, false);
    if (isSelected()) {
        painter->setPen(selectedPen());
    } else {
        painter->setPen(normalPen());
    }
    painter->drawRect(boundingRect());
    if (mShowOverlay) {
        if (isSelected()) {
            painter->setPen(selectedBaseLinePen());
        } else {
            painter->setPen(normalBaseLinePen());
        }
        painter->drawLine(mRecord->baseLine());
    }
    painter->restore();
}

std::unique_ptr<AbstractRecord> FontGraphicsItem::movedRecord(const QPoint &toPoint)
{
    return mRecord->movedRecord(toPoint.x(), toPoint.y());
}

void FontGraphicsItem::reloadModelData()
{
    prepareGeometryChange();
    setPos(mRecord->rect().topLeft());
}

} // namespace gmtool
