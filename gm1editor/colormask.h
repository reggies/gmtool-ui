#pragma once

#include <QBitmap>
#include <QColor>
#include <QImage>

namespace gmtool {

QBitmap createColorMask(QRgb color, const QImage &image);

} // namespace gmtool
