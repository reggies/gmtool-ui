#include "animationpivotgraphicsitem.h"

#include <QPainter>

#include "animationeditor.h"
#include "tableid.h"
#include "tablemodel.h"

namespace gmtool {

AnimationPivotGraphicsItem::AnimationPivotGraphicsItem(AnimationEditor *editor,
                                                       const RecordId &id,
                                                       SceneNode *parent)
    : SceneNode(parent)
    , mEditor(editor)
    , mRecordId(id)
{
    AnimationRecord record;
    record.assign(*mEditor->getModel(TableId::Layout)->getRecord(mRecordId));
    mSize = record.rect().size();
    mPivot = mEditor->pivot();
}

QRectF AnimationPivotGraphicsItem::boundingRect() const
{
    return QRectF(QPointF(), mSize);
}

void AnimationPivotGraphicsItem::paint(QPainter *painter,
                                       const QStyleOptionGraphicsItem *option,
                                       QWidget *widget)
{
    if (parentItem() == nullptr)
        return;

    const QSizeF &size = parentItem()->boundingRect().size();

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, false);

    QPen pen;
    pen.setCosmetic(true);
    painter->setPen(pen);

    QVarLengthArray<QLine, 2> lines;

    lines.append(QLine(mPivot.x(), 0, mPivot.x(), size.height()));
    lines.append(QLine(0, mPivot.y(), size.width(), mPivot.y()));

    painter->drawLines(lines.data(), lines.size());
    painter->restore();
}

void AnimationPivotGraphicsItem::reloadModelData()
{
    AnimationRecord record;
    record.assign(*mEditor->getModel(TableId::Layout)->getRecord(mRecordId));

    if (record.rect().size() != mSize) {
        prepareGeometryChange();
        mSize = record.rect().size();
    }

    mPivot = mEditor->pivot();
}

} // namespace gmtool
