#include "operation.h"

namespace gmtool {

Operation::Operation(QObject *parent)
    : QObject(parent)
{}

void Operation::setProgress(int current, int total)
{
    emit progress(current, total);
}

QString Operation::status() const
{
    return QString();
}

} // namespace gmtool
