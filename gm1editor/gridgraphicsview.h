#pragma once

#include <QGraphicsView>
#include <QMouseEvent>
#include <QWidget>

namespace gmtool {

class GridGraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    explicit GridGraphicsView(QWidget *parent = nullptr);

    void setShowGrid(bool on);
    bool showGrid() const;

    void recalculateSceneRect();

    void center(qreal sceneX, qreal sceneY);

signals:
    void showGridChanged(bool value);
    void scaleChanged(float delta);

protected:
    void drawForeground(QPainter *painter, const QRectF &rect) override;
    void wheelEvent(QWheelEvent *event) override;

    void scrollContentsBy(int dx, int dy) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

    void resizeEvent(QResizeEvent *event) override;

private:
    void drawGrid(QPainter *painter,
                  const QRectF &rect,
                  const QTransform &xform,
                  float strokeLength,
                  const QPen &pen) const;

private:
    int mScrollDelta = 0;
    bool mShowGrid = true;

    bool mScrolling = false;

    QPointF mLastPos;
};

} // namespace gmtool
