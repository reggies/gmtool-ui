#include "logviewer.h"

#include "document.h"
#include "logmodel.h"
#include "severity.h"
#include "severityfiltermodel.h"

#include <QHeaderView>
#include <QToolBar>
#include <QVBoxLayout>

namespace gmtool {

LogViewer::LogViewer(Document *document, QWidget *parent)
    : Window(parent)
    , mDoc(document)
{
    auto clearAction = new QAction(tr("Clear"), this);
    clearAction->setIcon(QIcon::fromTheme("trash-can-outline"));
    connect(clearAction, &QAction::triggered, this, &LogViewer::clearAll);

    auto toolbar = new QToolBar(this);
    toolbar->addAction(clearAction);

    mTabWidget = new QTabWidget(this);
    createGeneralTab();
    createErrorsTab();
    createWarningsTab();

    auto layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->addWidget(toolbar);
    layout->addWidget(mTabWidget);

    setWindowTitle(tr("Errors and warnings"));
}

LogViewer::~LogViewer() {}

void LogViewer::createGeneralTab()
{
    mGeneralTab = new QWidget(this);
    mGeneralView = new QTableView(mGeneralTab);

    auto model = new SeverityFilterModel(Severity::Error | Severity::Warning | Severity::Information,
                                         this);
    model->setSourceModel(mDoc->logModel());
    mGeneralView->setModel(model);
    if (mGeneralView->horizontalHeader() != nullptr) {
        mGeneralView->horizontalHeader()->setVisible(false);
        mGeneralView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    }
    if (mGeneralView->verticalHeader() != nullptr) {
        mGeneralView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    }
    mGeneralView->setWordWrap(true);
    mGeneralView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    auto layout = new QVBoxLayout(mGeneralTab);
    layout->setContentsMargins(2, 2, 2, 2);
    layout->addWidget(mGeneralView);

    mTabWidget->addTab(mGeneralTab, tr("All Messages"));
}

void LogViewer::createErrorsTab()
{
    mErrors = new QWidget(this);
    mErrorsView = new QTableView(mErrors);

    auto model = new SeverityFilterModel(Severity::Error, this);
    model->setSourceModel(mDoc->logModel());
    mErrorsView->setModel(model);
    if (mErrorsView->horizontalHeader() != nullptr) {
        mErrorsView->horizontalHeader()->setVisible(false);
        mErrorsView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    }
    if (mErrorsView->verticalHeader() != nullptr) {
        mErrorsView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    }
    mErrorsView->setWordWrap(true);
    mErrorsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    auto layout = new QVBoxLayout(mErrors);
    layout->setContentsMargins(2, 2, 2, 2);
    layout->addWidget(mErrorsView);

    mTabWidget->addTab(mErrors, QIcon::fromTheme("dialog-error"), tr("Errors"));
}

void LogViewer::createWarningsTab()
{
    mWarnings = new QWidget(this);
    mWarningsView = new QTableView(mWarnings);

    auto model = new SeverityFilterModel(Severity::Warning, this);
    model->setSourceModel(mDoc->logModel());
    mWarningsView->setModel(model);
    if (mWarningsView->horizontalHeader() != nullptr) {
        mWarningsView->horizontalHeader()->setVisible(false);
        mWarningsView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    }
    if (mWarningsView->verticalHeader() != nullptr) {
        mWarningsView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    }
    mWarningsView->setWordWrap(true);
    mWarningsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    auto layout = new QVBoxLayout(mWarnings);
    layout->setContentsMargins(2, 2, 2, 2);
    layout->addWidget(mWarningsView);

    mTabWidget->addTab(mWarnings, QIcon::fromTheme("dialog-warning"), tr("Warnings"));
}

void LogViewer::showErrors()
{
    mTabWidget->setCurrentIndex(mTabWidget->indexOf(mErrors));
    mErrorsView->scrollToBottom();
}

void LogViewer::showWarnings()
{
    mTabWidget->setCurrentIndex(mTabWidget->indexOf(mWarnings));
    mWarningsView->scrollToBottom();
}

void LogViewer::clearAll()
{
    mDoc->logModel()->clear();
}

} // namespace gmtool
