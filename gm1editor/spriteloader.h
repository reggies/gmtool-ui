#pragma once

#include <boost/filesystem/path.hpp>

#include <memory>
#include <vector>

#include <QStringList>
#include <QWidget>

#include "abstractrecord.h"

namespace gmtool {
class LogModel;

class SpriteLoader
{
public:
    virtual ~SpriteLoader() {}

    virtual void setPosition(const QPoint &point){};

    virtual std::vector<std::unique_ptr<AbstractRecord>>
    loadSprites(const std::vector<boost::filesystem::path> &loadPaths, QWidget *parent) = 0;

    virtual void setLogModel(LogModel *model) {}
};

} // namespace gmtool
