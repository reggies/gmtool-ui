#include "zoommodel.h"

#include <algorithm>

namespace gmtool {

const int kDefaultZoomLevels[] = {25, 50, 100, 200, 400, 800, 1600};

ZoomModel::ZoomModel(QObject *parent)
    : QAbstractListModel(parent)
{
    mZoomValues.assign(std::begin(kDefaultZoomLevels), std::end(kDefaultZoomLevels));
}

QVariant ZoomModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    //
    // Handling EditRole is a must for editing combobox as a view.
    // Without it QComboBox behaves strange: it clears its line edit
    // when its satisfies the validator. Checks are made when opening
    // popup, activating item in popup menu or pressing enter.
    //
    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    auto row = index.row();

    if (row >= rowCount())
        return QVariant();

    return QString("%1%").arg(mZoomValues.at(row));
}

int ZoomModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return static_cast<int>(mZoomValues.size());
}

int ZoomModel::getValue(int index) const
{
    return mZoomValues.at(index);
}

int ZoomModel::indexOf(int value) const
{
    return std::find(mZoomValues.begin(), mZoomValues.end(), value) - mZoomValues.begin();
}

int ZoomModel::defaultIndex() const
{
    return indexOf(100);
}

} // namespace gmtool
