#include "gm1filedata.h"

namespace gmtool {

bool Gm1FileData::hasTransparency() const
{
    return Properties.bHasColorKey == 1;
}

gm1_u16_t Gm1FileData::transparentPixel() const
{
    return Properties.ColorKey;
}

bool Gm1FileData::hasPivot() const
{
    return true;
}

QPoint Gm1FileData::pivot() const
{
    return QPoint(Header.AnchorX, Header.AnchorY);
}

size_t Gm1FileData::paletteCount() const
{
    return Palettes.size();
}

Gm1FileData::palette_t Gm1FileData::paletteAt(size_t index) const
{
    return Palettes.at(index);
}

bool Gm1FileData::hasPalette() const
{
    //! \todo dry
    //! \todo dry? what?
    return Header.EntryClass == GM1_ENTRY_CLASS_TGX8;
}

size_t Gm1FileData::entryCount() const
{
    return Entries.size();
}

QImage Gm1FileData::plotImage() const
{
    return Plot;
}

gm1_entry_class_t Gm1FileData::entryClass() const
{
    return static_cast<gm1_entry_class_t>(Header.EntryClass);
}

QPoint Gm1FileData::plotOrigin() const
{
    return Origin;
}

void Gm1FileData::setPaletteAt(size_t index, const Gm1FileData::palette_t &palette)
{
    Palettes.at(index) = palette;
}

void Gm1FileData::setEntryCount(size_t num_entries)
{
    Entries.resize(num_entries);
}

void Gm1FileData::setPlotImage(const QImage &image)
{
    Plot = image;
}

void Gm1FileData::setPivot(const QPoint &point)
{
    Header.AnchorX = point.x();
    Header.AnchorY = point.y();
}

void Gm1FileData::setEntryClass(gm1_entry_class_t entry_class)
{
    Header.EntryClass = entry_class;
}

void Gm1FileData::setPlotOrigin(const QPoint &plotOrigin)
{
    Origin = plotOrigin;
}

QRect Gm1FileEntry::entryRect() const
{
    return Rect;
}

int Gm1FileEntry::partId() const
{
    return Gm1.Header.PartId;
}

int Gm1FileEntry::partSize() const
{
    return Gm1.Header.PartSize;
}

static inline TileAlignment convertToTileAlignment(gm1_u8_t alignment)
{
    switch (static_cast<gm1_alignment_t>(alignment)) {
    case GM1_ALIGN_LEFT:
        return TileAlignment::Left;
    case GM1_ALIGN_RIGHT:
        return TileAlignment::Right;
    case GM1_ALIGN_CENTER:
        return TileAlignment::Center;
    case GM1_ALIGN_NONE:
        return TileAlignment::None;
    }
    return TileAlignment::Left;
}

TileAlignment Gm1FileEntry::alignment() const
{
    return convertToTileAlignment(Gm1.Header.Alignment);
}

int Gm1FileEntry::fontBearing() const
{
    return Gm1.Header.TileOffset;
}

void Gm1FileEntry::setEntryRect(const QRect &rect)
{
    Rect = rect;
}

void Gm1FileEntry::setPartId(int partId)
{
    Gm1.Header.PartId = partId;
}

void Gm1FileEntry::setPartSize(int partSize)
{
    Gm1.Header.PartSize = partSize;
}

void Gm1FileEntry::setBox(const Gm1FileEntry::TileBox &box)
{
    Gm1.Header.TileOffset = box.height;
    Gm1.Header.BoxWidth = box.width;
    Gm1.Header.BoxOffset = box.left;
}

static inline gm1_u8_t convertToUint8(TileAlignment alignment)
{
    switch (alignment) {
    case TileAlignment::Left:
        return GM1_ALIGN_LEFT;
    case TileAlignment::Right:
        return GM1_ALIGN_RIGHT;
    case TileAlignment::Center:
        return GM1_ALIGN_CENTER;
    case TileAlignment::None:
        return GM1_ALIGN_NONE;
    }
    return GM1_ALIGN_LEFT;
}

void Gm1FileEntry::setAlignment(TileAlignment alignment)
{
    Gm1.Header.Alignment = convertToUint8(alignment);
}

void Gm1FileEntry::setFontBearing(int bearing)
{
    Gm1.Header.TileOffset = bearing;
}

} // namespace gmtool
