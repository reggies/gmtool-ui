#pragma once

namespace gmtool {

class TableModel;

class ISpriteSheet
{
public:
    virtual ~ISpriteSheet(){};
    virtual TableModel *getSpritesheet() = 0;
    virtual TableModel *getPalette() = 0;
};

} // namespace gmtool
