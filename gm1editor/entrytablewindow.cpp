#include "entrytablewindow.h"

#include <boost/range/adaptor/reversed.hpp>

#include <set>

#include <QDebug>
#include <QMenu>
#include <QVBoxLayout>

#include "commanditemdelegate.h"
#include "document.h"
#include "insertcommand.h"
#include "removecommand.h"

namespace gmtool {

EntryTableWindow::EntryTableWindow(Document *document, QWidget *parent)
    : Window(parent)
    , mDoc(document)
{
    setWindowTitle(tr("Entry table"));

    mTableView = new QTableView(this);
    mTableView->setModel(mDoc->editor()->getModel(TableId::Layout));
    mTableView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    mTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    mTableView->setContextMenuPolicy(Qt::CustomContextMenu);
    mTableView->setItemDelegate(
        new CommandItemDelegate(mDoc->editor()->getModel(TableId::Layout), mDoc->undoStack(), this));
    connect(mTableView->selectionModel(),
            &QItemSelectionModel::selectionChanged,
            this,
            &EntryTableWindow::selectionChanged);

    mTableView->setContextMenuPolicy(Qt::ActionsContextMenu);

    auto layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->addWidget(mTableView);
}

bool EntryTableWindow::focusEntry(TableId table, const RecordId &id)
{
    if (table == TableId::Layout) {
        int row = mDoc->editor()->getModel(table)->getPositionOf(id);
        if (row != -1) {
            mTableView->selectRow(row);
            return true;
        }
    }
    return false;
}

void EntryTableWindow::add()
{
    const QModelIndexList &selection = mTableView->selectionModel()->selectedIndexes();
    int position = mTableView->model()->rowCount();
    if (!selection.empty()) {
        position = selection.front().row();
    }
    mDoc->addEntry(this, QPoint(), position);
}

void EntryTableWindow::remove()
{
    TableModel *model = mDoc->editor()->getModel(TableId::Layout);
    std::set<int> rows;
    for (const QModelIndex &index : mTableView->selectionModel()->selectedIndexes()) {
        rows.insert(index.row());
    }
    auto macro = std::make_unique<undo::Macro>(tr("Remove %1 row(s)").arg(rows.size()));
    for (int row : rows | boost::adaptors::reversed) {
        macro->add(
            std::make_unique<RemoveCommand>(model->getPositionOf(model->getIdOf(row)), model));
    }
    if (!mDoc->undoStack()->push(std::move(macro))) {
        emit statusTextChanged(tr("Could not remove %1 items").arg(rows.size()));
    } else {
        emit statusTextChanged(tr("Removed %1 items").arg(rows.size()));
    }
}

void EntryTableWindow::selectionChanged(const QItemSelection &selected,
                                        const QItemSelection &deselected)
{
    emit gotAnyRowSelected(mTableView->selectionModel()->hasSelection());
}

void EntryTableWindow::route(RemoveSpriteOrEntry *action)
{
    connect(action, &QAction::triggered, this, &EntryTableWindow::remove);
    connect(this, &EntryTableWindow::gotAnyRowSelected, action, &QAction::setEnabled);
    mTableView->addAction(action);
    selectionChanged(QItemSelection(), QItemSelection());
}

void EntryTableWindow::unroute(RemoveSpriteOrEntry *action)
{
    disconnect(action, nullptr, this, nullptr);
    disconnect(this, nullptr, action, nullptr);
    mTableView->removeAction(action);
    action->setDisabled(true);
}

void EntryTableWindow::route(AddEntry *action)
{
    connect(action, &QAction::triggered, this, &EntryTableWindow::add);
    mTableView->addAction(action);
}

void EntryTableWindow::unroute(AddEntry *action)
{
    disconnect(action, nullptr, this, nullptr);
    disconnect(this, nullptr, action, nullptr);
    mTableView->removeAction(action);
    action->setDisabled(true);
}

} // namespace gmtool
