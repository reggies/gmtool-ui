#include "spritegraphicsitem.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QPainter>

#include "assigncommand.h"
#include "nodeattribute.h"
#include "paletterecord.h"
#include "spriterecord.h"
#include "spritesheet.h"
#include "tableid.h"
#include "tablemodel.h"
#include "viewstate.h"

namespace gmtool {

SpriteGraphicsItem::SpriteGraphicsItem(ISpriteSheet *spritesheet,
                                       const RecordId &id,
                                       SceneNode *parent)
    : SceneNode(parent)
    , mSpriteSheet(spritesheet)
    , mRecordId(id)
{
    setData(NodeAttribute::RecordId, QVariant::fromValue(id));
    setData(NodeAttribute::TableId, QVariant::fromValue(TableId::Spritesheet));
    auto model = mSpriteSheet->getPalette();
    if (model != nullptr) {
        mPaletteId = model->getIdOf(0);
    }

    mRecord = mSpriteSheet->getSpritesheet()->getRecordAs<SpriteRecord>(id);

    connect(mRecord, &SpriteRecord::imageChanged, this, &SpriteGraphicsItem::spriteChanged);

    connect(mRecord, &SpriteRecord::positionChanged, this, &SpriteGraphicsItem::positionChanged);

    mRecordSize = mRecord->rect().size();
    setPos(mRecord->rect().topLeft());
}

void SpriteGraphicsItem::setViewState(const ViewState &state)
{
    if (mPaletteId != state.paletteId) {
        mPaletteId = state.paletteId;
        mCachedPixmap = QPixmap();
        update();
    }
}

QRectF SpriteGraphicsItem::boundingRect() const
{
    return QRectF(QPointF(), mRecordSize);
}

void SpriteGraphicsItem::paint(QPainter *painter,
                               const QStyleOptionGraphicsItem *option,
                               QWidget *widget)
{
    painter->save();
    createPixmap();
    QPen pen;
    pen.setStyle(Qt::DashLine);
    pen.setCosmetic(true);
    painter->setPen(pen);
    painter->drawRect(QRect(QPoint(), mRecordSize));
    painter->drawPixmap(QRect(QPoint(), mRecordSize), mCachedPixmap);
    if (isSelected()) {
        QBrush brush;
        brush.setStyle(Qt::Dense6Pattern);
        const QTransform &xform = painter->transform();
        const QRectF &screenRect = xform.mapRect(QRect(QPoint(), mRecordSize));
        painter->resetTransform();
        painter->fillRect(screenRect, brush);
    }
    painter->restore();
}

void SpriteGraphicsItem::spriteChanged()
{
    mCachedPixmap = QPixmap();
    update();
}

void SpriteGraphicsItem::positionChanged()
{
    prepareGeometryChange();
    mRecordSize = mRecord->rect().size();
    setPos(mRecord->rect().topLeft());
}

std::unique_ptr<AbstractRecord> SpriteGraphicsItem::movedRecord(const QPoint &toPoint)
{
    return mRecord->movedRecord(toPoint.x(), toPoint.y());
}

void SpriteGraphicsItem::createPixmap(bool force)
{
    if (mCachedPixmap.isNull() || force) {
        QBitmap mask = mRecord->mask();
        QImage image = mRecord->image();
        auto model = mSpriteSheet->getPalette();
        if (model != nullptr) {
            PaletteRecord palette;
            palette.assign(*model->getRecord(mPaletteId));
            const auto &colors = palette.colors();
            QVector<QRgb> colorTable;
            colorTable.reserve(static_cast<int>(colors.size()));
            std::transform(colors.begin(),
                           colors.end(),
                           std::back_inserter(colorTable),
                           [](const QColor &color) { return color.rgb(); });
            image.setColorTable(colorTable);
        }
        QPixmap pix = QPixmap::fromImage(image);
        if (!mask.isNull()) {
            pix.setMask(mask);
        }
        mCachedPixmap = pix;
    }
}

QColor SpriteGraphicsItem::pixelAt(const QPoint &point)
{
    createPixmap();
    auto image = mCachedPixmap.copy(point.x(), point.y(), 1, 1).toImage();

    Q_ASSERT(image.valid(0, 0));

    return image.pixelColor(0, 0);
}

int SpriteGraphicsItem::type() const
{
    return SpriteGraphicsItem::Type;
}

void SpriteGraphicsItem::invalidatePalette()
{
    mCachedPixmap = QPixmap();
    update();
}

void SpriteGraphicsItem::reloadModelData()
{
    spriteChanged();
    positionChanged();
}

} // namespace gmtool
