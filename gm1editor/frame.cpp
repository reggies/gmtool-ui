#include "frame.h"

#include <QApplication>
#include <QDebug>
#include <QDockWidget>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>

#include "colortablewindow.h"
#include "document.h"
#include "documentoperation.h"
#include "entrytablewindow.h"
#include "logmodel.h"
#include "logviewer.h"
#include "plotwindow.h"
#include "previewwindow.h"
#include "propertieswindow.h"
#include "spriteswindow.h"
#include "window.h"
#include "replwindow.h"

namespace gmtool {

Frame::Frame(Document *document, QWidget *parent)
    : QMainWindow(parent)
    , mDocument(document)
{
    setMinimumSize(640, 480);
    setDockOptions(QMainWindow::AnimatedDocks | QMainWindow::AllowNestedDocks
                   | QMainWindow::AllowTabbedDocks);

    connect(qApp, &QApplication::focusChanged, this, &Frame::applicationFocusChanged);

    mOperationProgress = new QProgressBar;
    mOperationStatus = new QLabel;
    mCursorPosSection = new QLabel;

    connect(document->logModel(), &LogModel::messagesChanged, this, &Frame::updateMessages);

    auto warningsIcon = new QLabel(this);
    warningsIcon->setPixmap(QIcon::fromTheme("dialog-warning").pixmap(16, 16));
    mWarnings = new QLabel(tr("<a href=warnings:>Warnings: 0</a>"), this);
    auto warningsLayout = new QHBoxLayout;
    warningsLayout->setMargin(0);
    warningsLayout->addWidget(warningsIcon);
    warningsLayout->addWidget(mWarnings);
    mWarningsPanel = new QWidget(this);
    mWarningsPanel->setLayout(warningsLayout);
    connect(mWarnings, &QLabel::linkActivated, this, &Frame::linkActivated);

    auto errorsIcon = new QLabel(this);
    errorsIcon->setPixmap(QIcon::fromTheme("dialog-error").pixmap(16, 16));
    mErrors = new QLabel(tr("<a href=errors:>Errors: 0</a>"), this);
    auto errorsLayout = new QHBoxLayout;
    errorsLayout->setMargin(0);
    errorsLayout->addWidget(errorsIcon);
    errorsLayout->addWidget(mErrors);
    mErrorsPanel = new QWidget(this);
    mErrorsPanel->setLayout(errorsLayout);
    connect(mErrors, &QLabel::linkActivated, this, &Frame::linkActivated);

    mFileBar = addToolBar(tr("&File"));
    mEditBar = addToolBar(tr("&Edit"));
    mWindowBar = addToolBar(tr("&Window"));
    addToolBarBreak();
    mToolsBar = addToolBar(tr("Tools"));
    mViewBar = addToolBar(tr("&View"));
    mColorTableBar = addToolBar(tr("&Color table"));
    mSpritesBar = addToolBar(tr("&Sprites"));

    statusBar()->setSizeGripEnabled(true);
    statusBar()->addWidget(mOperationStatus, 1);
    statusBar()->addWidget(mOperationProgress);
    statusBar()->addPermanentWidget(mWarningsPanel);
    statusBar()->addPermanentWidget(mErrorsPanel);
    statusBar()->addPermanentWidget(mCursorPosSection);

    Q_ASSERT(document != nullptr);
    connect(document, &Document::filePathChanged, this, &Frame::updateTitle);

    connect(document, &Document::operationChanged, this, &Frame::updateOperation);

    connect(document, &Document::editorChanged, this, &Frame::updateEditor);

    updateOperation();
    updateTitle();
    updateMenus();
    updateMessages();
}

Frame::~Frame()
{
    std::set<Window *> trash;
    trash.swap(mWindows);

    // disconnect explicitly because otherwise ~QWidget would call
    // QWidget::setWidgetFocus
    disconnect(qApp, &QApplication::focusChanged, this, &Frame::applicationFocusChanged);

    for (auto window : trash) {
        // disconnect lambda slot because it drives valgrind crazy
        disconnect(window, nullptr, this, nullptr);
        // destroy windows explicitly because otherwise they will be
        // destroyed from ~QWidget when mWindows will be destroyed as well.
        window->deleteLater();
    }
}

const Document *Frame::document() const
{
    return mDocument;
}

Document *Frame::document()
{
    return mDocument;
}

Window *Frame::focusWindow()
{
    return const_cast<Window *>(const_cast<const Frame *>(this)->focusWindow());
}

const Window *Frame::focusWindow() const
{
    Window *focus = nullptr;
    for (Window *window : mWindows) {
        if (window->hasFocus())
            focus = window;
    }
    return focus;
}

bool Frame::isWindowFocused(Window *window) const
{
    return window->hasFocus();
}

void Frame::closeAllWindows()
{
    for (Window *window : mWindows) {
        auto dock = std::find_if(mDocks.begin(), mDocks.end(), [window](const QDockWidget *dock) {
            return dock->widget() == window;
        });
        if (dock != mDocks.end()) {
            removeDockWidget(*dock);
            (*dock)->setWidget(nullptr);
            (*dock)->deleteLater();
            mDocks.erase(dock);
        }
        window->deleteLater();
    }
    mWindows.clear();
}

void Frame::closeEvent(QCloseEvent *event)
{
    if (!mCloseAllowed) {
        event->ignore();
        emit closeFrameRequested(this);
    }
}

void Frame::newDocument()
{
    emit newDocumentRequested(this);
}

QString Frame::documentContainingDir() const
{
    if (!mDocument->filePath().empty()) {
        auto dirname = mDocument->filePath();
        return QString::fromStdWString(dirname.remove_filename().wstring());
    }
    return QString();
}

void Frame::openDocument()
{
    QStringList filters;
    filters << tr("GM1 Files (*.gm1)") << tr("Any files (*)");

    const QString &path = QFileDialog::getOpenFileName(this,
                                                       tr("Open document"),
                                                       documentContainingDir(),
                                                       filters.join(";;"));
    if (!path.isNull()) {
        emit openDocumentRequested(this, path.toStdWString());
    }
}

void Frame::saveDocument()
{
    if (mDocument->filePath().empty()) {
        saveAsDocument();
    } else {
        mDocument->saveToFile(mDocument->filePath());
    }
}

void Frame::saveAsDocument()
{
    QStringList filters;
    filters << tr("GM1 Files (*.gm1)") << tr("Any files (*)");

    QString path = QFileDialog::getSaveFileName(this,
                                                tr("Save document"),
                                                documentContainingDir(),
                                                filters.join(";;"));
    if (!path.isNull()) {
        mDocument->saveToFile(path.toStdWString());
    }
}

void Frame::revertDocument()
{
    if (!mDocument->filePath().empty()) {
        mDocument->loadFromFile(mDocument->filePath());
    }
}

void Frame::closeDocument()
{
    /// \todo ask for confirmation
    emit closeDocumentRequested(mDocument);
}

void Frame::cloneFrame()
{
    emit cloneFrameRequested(this);
}

QDockWidget *Frame::addWindow(Window *window)
{
    if (window == nullptr)
        return nullptr;

    mWindows.insert(window);

    auto dock = new QDockWidget(this);
    dock->setAllowedAreas(Qt::AllDockWidgetAreas);
    dock->setWindowTitle(window->windowTitle());
    dock->setWidget(window);
    dock->setAttribute(Qt::WA_DeleteOnClose);
    dock->installEventFilter(this);

    connect(window, &QObject::destroyed, this, [this, window]() { windowClosed(window); });
    connect(window, &Window::closed, dock, &QWidget::close);

    connect(window, &Window::statusTextChanged, statusBar(), [this](const QString &text) {
        statusBar()->showMessage(text, 5000);
    });

    addDockWidget(Qt::TopDockWidgetArea, dock);

    window->setFocus();

    mDocks.insert(dock);
    return dock;
}

void Frame::showEntryTable()
{
    auto window = new EntryTableWindow(mDocument, this);
    addWindow(window);
}

void Frame::showRepl()
{
    auto window = new ReplWindow(mDocument, this);
    addWindow(window);
}

void Frame::showPlot()
{
    auto window = new PlotWindow(mDocument, this);

    connect(window, &PlotWindow::findRequested, this, &Frame::findRequested);
    connect(window, &PlotWindow::cursorScenePosChanged, this, &Frame::updateCursorInfo);

    addWindow(window);
}

void Frame::showColorTable()
{
    auto window = new ColorTableWindow(mDocument, this);
    addWindow(window);
}

void Frame::showSprites()
{
    auto window = new SpritesWindow(mDocument, this);
    addWindow(window);
}

void Frame::showProperties()
{
    auto window = new PropertiesWindow(mDocument, this);
    addWindow(window);
}

void Frame::showPreview()
{
    auto window = new PreviewWindow(mDocument, this);
    addWindow(window);
}

void Frame::updateOperation()
{
    DocumentOperation *op = mDocument->operation();
    mOperationStatus->setText(op->statusString());
    if (op->isRunning()) {
        mOperationProgress->setVisible(op->currentProgress() != op->totalProgress());
    } else {
        mOperationProgress->setVisible(false);
    }
    mOperationProgress->setMaximum(op->totalProgress());
    mOperationProgress->setValue(op->currentProgress());
}

void Frame::updateTitle()
{
    QString asterisk;
    QString title;
    if (mDocument->filePath().empty()) {
        title = mDocument->name() + asterisk;
    } else {
        title = QString::fromStdWString(mDocument->filePath().wstring()) + asterisk;
    }

    setWindowTitle(title);
}

void Frame::setupFileMenu()
{
    auto menu = menuBar()->addMenu(tr("&File"));

    auto newAct = menu->addAction(tr("&New"));
    newAct->setShortcut(QKeySequence::New);
    newAct->setIcon(QIcon::fromTheme("document-new"));
    mFileBar->addAction(newAct);
    connect(newAct, &QAction::triggered, this, &Frame::newDocument);

    auto openAct = menu->addAction(tr("&Open"));
    openAct->setShortcut(QKeySequence::Open);
    openAct->setIcon(QIcon::fromTheme("document-open"));
    mFileBar->addAction(openAct);
    connect(openAct, &QAction::triggered, this, &Frame::openDocument);

    auto saveAct = menu->addAction(tr("&Save"));
    saveAct->setShortcut(QKeySequence::Save);
    saveAct->setIcon(QIcon::fromTheme("document-save"));
    mFileBar->addAction(saveAct);
    connect(saveAct, &QAction::triggered, this, &Frame::saveDocument);

    auto saveAsAct = menu->addAction(tr("Save &As"));
    saveAsAct->setShortcut(QKeySequence::SaveAs);
    saveAsAct->setIcon(QIcon::fromTheme("document-save-as"));
    mFileBar->addAction(saveAsAct);
    connect(saveAsAct, &QAction::triggered, this, &Frame::saveAsDocument);

    auto revertAct = menu->addAction(tr("&Revert"));
    revertAct->setIcon(QIcon::fromTheme("document-revert"));
    mFileBar->addAction(revertAct);
    connect(revertAct, &QAction::triggered, this, &Frame::revertDocument);

    auto closeAct = menu->addAction(tr("&Close"));
    closeAct->setShortcut(QKeySequence::Close);
    closeAct->setIcon(QIcon::fromTheme("document-close"));
    mFileBar->addAction(closeAct);
    connect(closeAct, &QAction::triggered, this, &Frame::closeDocument);
}

void Frame::setupEditMenu()
{
    auto menu = menuBar()->addMenu(tr("&Edit"));
    auto undo = mDocument->undoStack()->createUndoAction(menu, tr("&Undo"));
    undo->setShortcut(QKeySequence::Undo);
    undo->setIcon(QIcon::fromTheme("edit-undo"));
    menu->addAction(undo);
    mEditBar->addAction(undo);

    auto redo = mDocument->undoStack()->createRedoAction(menu, tr("&Redo"));
    redo->setShortcut(QKeySequence::Redo);
    redo->setIcon(QIcon::fromTheme("edit-redo"));
    menu->addAction(redo);
    mEditBar->addAction(redo);

    mEditBar->addSeparator();

    mMoveForward = new MoveForward(tr("Move forward"), this);
    mMoveForward->setIcon(QIcon::fromTheme("arrange-bring-forward"));
    mMoveForward->setEnabled(false);
    mEditBar->addAction(mMoveForward);

    mMoveBackward = new MoveBackward(tr("Move backward"), this);
    mMoveBackward->setIcon(QIcon::fromTheme("arrange-send-backward"));
    mMoveBackward->setEnabled(false);
    mEditBar->addAction(mMoveBackward);

    mBringForward = new BringForward(tr("Bring forward"), this);
    mBringForward->setIcon(QIcon::fromTheme("arrange-bring-to-front"));
    mBringForward->setEnabled(false);
    mEditBar->addAction(mBringForward);

    mSendBack = new SendBack(tr("Send back"), this);
    mSendBack->setIcon(QIcon::fromTheme("arrange-send-to-back"));
    mSendBack->setEnabled(false);
    mEditBar->addAction(mSendBack);

    mEditBar->addSeparator();

    mAddEntry = new AddEntry(tr("Add entry"), this);
    mAddEntry->setIcon(QIcon::fromTheme("list-add"));
    mAddEntry->setEnabled(false);
    mEditBar->addAction(mAddEntry);

    mRemoveSpriteOrEntry = new RemoveSpriteOrEntry(tr("Remove"), this);
    mRemoveSpriteOrEntry->setShortcuts(QKeySequence::Delete);
    mRemoveSpriteOrEntry->setIcon(QIcon::fromTheme("list-remove"));
    mRemoveSpriteOrEntry->setEnabled(false);
    mEditBar->addAction(mRemoveSpriteOrEntry);

    mFindSpriteOrEntry = new FindSpriteOrEntry(tr("Find"), this);
    mFindSpriteOrEntry->setIcon(QIcon::fromTheme("file-find"));
    mFindSpriteOrEntry->setShortcuts(QKeySequence::Find);
    mFindSpriteOrEntry->setEnabled(false);
    mEditBar->addAction(mFindSpriteOrEntry);

    mSelectAll = new SelectAll(tr("Select all"), this);
    mSelectAll->setIcon(QIcon::fromTheme("select-all"));
    mSelectAll->setShortcuts(QKeySequence::SelectAll);
    mSelectAll->setEnabled(false);
    mEditBar->addAction(mSelectAll);

    mSelectClear = new SelectClear(tr("Clear select"), this);
    mSelectClear->setIcon(QIcon::fromTheme("select-clear"));
    mSelectClear->setShortcut(QKeySequence(tr("Escape")));
    mSelectClear->setEnabled(false);
    mEditBar->addAction(mSelectClear);
}

void Frame::setupViewMenu()
{
    auto menu = menuBar()->addMenu(tr("&View"));

    if (mDocument->editor() != nullptr) {
        auto colortable = menu->addAction(tr("&Color table"));
        colortable->setIcon(QIcon::fromTheme("letter_c"));
        connect(colortable, &QAction::triggered, this, &Frame::showColorTable);
        mViewBar->addAction(colortable);

        auto preview = menu->addAction(tr("&Animation preview"));
        connect(preview, &QAction::triggered, this, &Frame::showPreview);
        preview->setIcon(QIcon::fromTheme("letter_a"));
        mViewBar->addAction(preview);

        auto images = menu->addAction(tr("&Sprite table"));
        connect(images, &QAction::triggered, this, &Frame::showSprites);
        images->setIcon(QIcon::fromTheme("letter_s"));
        mViewBar->addAction(images);

        auto properties = menu->addAction(tr("A&ttribute list"));
        properties->setIcon(QIcon::fromTheme("letter_t"));
        connect(properties, &QAction::triggered, this, &Frame::showProperties);
        mViewBar->addAction(properties);

        auto plot = menu->addAction(tr("&Layers view"));
        connect(plot, &QAction::triggered, this, &Frame::showPlot);
        plot->setIcon(QIcon::fromTheme("letter_l"));
        mViewBar->addAction(plot);

        auto entries = menu->addAction(tr("&Entry table"));
        entries->setIcon(QIcon::fromTheme("letter_e"));
        connect(entries, &QAction::triggered, this, &Frame::showEntryTable);
        mViewBar->addAction(entries);

        auto repl = menu->addAction(tr("&Repl"));
        repl->setIcon(QIcon::fromTheme("python"));
        connect(repl, &QAction::triggered, this, &Frame::showRepl);
        mViewBar->addAction(repl);

        menu->addSeparator();
        mViewBar->addSeparator();
    }

    auto messages = menu->addAction(tr("&Message log"));
    connect(messages, &QAction::triggered, this, &Frame::showMessageLog);
    messages->setIcon(QIcon::fromTheme("letter_m"));
    mViewBar->addAction(messages);

    menu->addSeparator();
    mViewBar->addSeparator();

    auto closeAll = menu->addAction(tr("Close All"));
    closeAll->setShortcut(tr("Ctrl-Shift-W"));
    closeAll->setIcon(QIcon::fromTheme("letter_x"));
    connect(closeAll, &QAction::triggered, this, &Frame::closeAllWindows);
    mViewBar->addAction(closeAll);

    mViewBar->addSeparator();

    mShowOverlay = new ShowOverlay(tr("Show overlay"), this);
    mShowOverlay->setCheckable(true);
    mShowOverlay->setIcon(QIcon::fromTheme("layers-outline"));
    mViewBar->addAction(mShowOverlay);

    mSnapToGrid = new SnapToGrid(tr("Snap to grid"), this);
    mSnapToGrid->setCheckable(true);
    mSnapToGrid->setIcon(QIcon::fromTheme("snap-to-grid"));
    mViewBar->addAction(mSnapToGrid);

    mShowGrid = new ShowGrid(tr("Show grid"), this);
    mShowGrid->setCheckable(true);
    mShowGrid->setIcon(QIcon::fromTheme("grid"));
    mViewBar->addAction(mShowGrid);
}

void Frame::setupWindowMenu()
{
    auto menu = menuBar()->addMenu(tr("&Window"));
    auto newWindow = menu->addAction(tr("&New window"));
    newWindow->setIcon(QIcon::fromTheme("window-new"));
    connect(newWindow, &QAction::triggered, this, &Frame::cloneFrame);
    mWindowBar->addAction(newWindow);

    auto closeWindow = menu->addAction(tr("&Close window"));
    closeWindow->setIcon(QIcon::fromTheme("window-close"));
    connect(closeWindow, &QAction::triggered, this, &Frame::close);
    mWindowBar->addAction(closeWindow);
}

void Frame::setupToolsMenu()
{
    mAllowSelectSprites = new AllowSelectSprites(tr("Sprites"), this);
    mAllowSelectSprites->setCheckable(true);
    mAllowSelectSprites->setChecked(true);

    mAllowSelectEntries = new AllowSelectEntries(tr("Entities"), this);
    mAllowSelectEntries->setCheckable(true);
    mAllowSelectEntries->setChecked(true);

    auto layersMenu = new QMenu(this);
    layersMenu->addAction(mAllowSelectSprites);
    layersMenu->addAction(mAllowSelectEntries);

    auto modeActionGroup = new QActionGroup(this);

    mScrollMode = new ScrollMode(tr("Scroll"), this);
    mScrollMode->setCheckable(true);
    mScrollMode->setIcon(QIcon::fromTheme("cursor-default-outline"));
    mScrollMode->setActionGroup(modeActionGroup);
    mToolsBar->addAction(mScrollMode);

    mTransformMode = new TransformMode(tr("Transform"), this);
    mTransformMode->setMenu(layersMenu);
    mTransformMode->setCheckable(true);
    mTransformMode->setIcon(QIcon::fromTheme("cursor-move"));
    mTransformMode->setActionGroup(modeActionGroup);
    mToolsBar->addAction(mTransformMode);
}

void Frame::setupSpritesMenu()
{
    mImportSprites = new ImportSprites(tr("Load image"), this);
    mImportSprites->setIcon(QIcon::fromTheme("file-import"));
    mImportSprites->setEnabled(false);
    mSpritesBar->addAction(mImportSprites);

    mExportSprites = new ExportSprites(tr("Save image"), this);
    mExportSprites->setIcon(QIcon::fromTheme("file-export"));
    mExportSprites->setEnabled(false);
    mSpritesBar->addAction(mExportSprites);
}

void Frame::setupColorTableMenu()
{
    mLoadPalette = new LoadPalette(tr("Load palette"), this);
    mLoadPalette->setIcon(QIcon::fromTheme("file-import"));
    mLoadPalette->setEnabled(false);
    mColorTableBar->addAction(mLoadPalette);

    mSavePalette = new SavePalette(tr("Save palette"), this);
    mSavePalette->setIcon(QIcon::fromTheme("file-export"));
    mSavePalette->setEnabled(false);
    mColorTableBar->addAction(mSavePalette);

    mChangeHue = new ChangeHue(tr("Change Hue"), this);
    mChangeHue->setIcon(QIcon::fromTheme("palette"));
    mChangeHue->setEnabled(false);
    mColorTableBar->addAction(mChangeHue);
}

void Frame::updateMenus()
{
    menuBar()->clear();
    mFileBar->clear();
    mEditBar->clear();
    mViewBar->clear();
    mWindowBar->clear();
    mSpritesBar->clear();
    mColorTableBar->clear();
    mToolsBar->clear();
    setupFileMenu();
    setupEditMenu();
    setupViewMenu();
    setupWindowMenu();
    setupSpritesMenu();
    setupColorTableMenu();
    setupToolsMenu();
}

void Frame::updateEditor()
{
    closeAllWindows();
    updateMenus();

    if (mDocument->editor() != nullptr) {
        showPlot();
    }
}

void Frame::setCloseAllowed(bool allowed)
{
    mCloseAllowed = allowed;
}

void Frame::findRequested(TableId table, const RecordId &id)
{
    bool focused = false;
    if (!id.isValid() || table == TableId::Invalid)
        return;
    for (Window *window : mWindows) {
        if (!isWindowFocused(window))
            focused |= window->focusEntry(table, id);
    }
    if (!focused) {
        Window *window = nullptr;
        if (table == TableId::Layout) {
            window = new EntryTableWindow(mDocument, this);
        } else if (table == TableId::Spritesheet) {
            window = new SpritesWindow(mDocument, this);
        }
        if (window != nullptr) {
            if (window->focusEntry(table, id)) {
                window->setWindowTitle(
                    tr("Show %1 (%2)").arg(getTableElementName(table).toLower()).arg(id.toString()));
                addWindow(window);
            } else {
                window->close();
                window->deleteLater();
            }
        }
    }
}

void Frame::updateCursorInfo(const QPoint &pos)
{
    mCursorPosSection->setText(tr("(%1,%2)").arg(pos.x()).arg(pos.y()));
}

void Frame::linkActivated(const QString &link)
{
    if (mCurrentViewer == nullptr) {
        mCurrentViewer = new LogViewer(mDocument, this);
        mViewerDock = addWindow(mCurrentViewer);
    } else {
        mDocument->logModel()->markAsViewed();
        mViewerDock->show();
    }

    if (link == "errors:") {
        mCurrentViewer->showErrors();
    } else if (link == "warnings:") {
        mCurrentViewer->showWarnings();
    }
}

void Frame::updateMessages()
{
    if (mDocument->logModel()->warningCount() != 0) {
        mWarnings->setText(
            tr("<a href=warnings:>Warnings: %1</a>").arg(mDocument->logModel()->warningCount()));
        mWarningsPanel->setVisible(true);
        mWarnings->setVisible(true);
    } else {
        mWarningsPanel->setVisible(false);
        mWarnings->setVisible(false);
    }

    if (mDocument->logModel()->errorCount() != 0) {
        mErrors->setText(
            tr("<a href=errors:>Errors: %1</a>").arg(mDocument->logModel()->errorCount()));
        mErrorsPanel->setVisible(true);
        mErrors->setVisible(true);
    } else {
        mErrorsPanel->setVisible(false);
        mErrors->setVisible(false);
    }
}

void Frame::showMessageLog()
{
    linkActivated("errors:");
}

void Frame::applicationFocusChanged(QWidget *old, QWidget *now)
{
    for (auto window : mWindows) {
        if (mLastFocusedWindow == window) {
            if (window->isAncestorOf(old) && !window->isAncestorOf(now)) {
                unrouteAllActions(window);
                break;
            }
        }
    }
    bool foundAnyFocused = false;
    for (auto window : mWindows) {
        if (mLastFocusedWindow != window) {
            if (!window->isAncestorOf(old) && window->isAncestorOf(now)) {
                foundAnyFocused = true;
                mLastFocusedWindow = window;
                routeAllActions(window);
                break;
            }
        }
    }
    if (!foundAnyFocused) {
        mLastFocusedWindow = nullptr;
    }
}

void Frame::windowClosed(Window *window)
{
    auto it = std::find(mWindows.begin(), mWindows.end(), window);
    if (it != mWindows.end()) {
        mWindows.erase(it);
        unrouteAllActions(window);
    }
}

void Frame::routeAllActions(Window *window)
{
    window->route(mImportSprites);
    window->route(mExportSprites);
    window->route(mLoadPalette);
    window->route(mSavePalette);
    window->route(mChangeHue);
    window->route(mFindSpriteOrEntry);
    window->route(mRemoveSpriteOrEntry);
    window->route(mAddEntry);
    window->route(mMoveForward);
    window->route(mMoveBackward);
    window->route(mBringForward);
    window->route(mSendBack);
    window->route(mSelectAll);
    window->route(mSelectClear);
    window->route(mScrollMode);
    window->route(mTransformMode);
    window->route(mAllowSelectSprites);
    window->route(mAllowSelectEntries);
    window->route(mSnapToGrid);
    window->route(mShowGrid);
    window->route(mShowOverlay);
}

void Frame::unrouteAllActions(Window *window)
{
    window->unroute(mImportSprites);
    window->unroute(mExportSprites);
    window->unroute(mLoadPalette);
    window->unroute(mSavePalette);
    window->unroute(mChangeHue);
    window->unroute(mFindSpriteOrEntry);
    window->unroute(mRemoveSpriteOrEntry);
    window->unroute(mAddEntry);
    window->unroute(mMoveForward);
    window->unroute(mMoveBackward);
    window->unroute(mBringForward);
    window->unroute(mSendBack);
    window->unroute(mSelectAll);
    window->unroute(mSelectClear);
    window->unroute(mScrollMode);
    window->unroute(mTransformMode);
    window->unroute(mAllowSelectSprites);
    window->unroute(mAllowSelectEntries);
    window->unroute(mSnapToGrid);
    window->unroute(mShowGrid);
    window->unroute(mShowOverlay);
}

bool Frame::eventFilter(QObject *receiver, QEvent *event)
{
    if (event->type() == QEvent::Close) {
        for (auto dock : mDocks) {
            if (dock == receiver) {
                mDocks.erase(std::find(mDocks.begin(), mDocks.end(), dock));
                auto tempWindows = mWindows;
                for (auto window : tempWindows) {
                    if (dock->isAncestorOf(window)) {
                        windowClosed(window);
                    }
                }
                return QMainWindow::eventFilter(receiver, event);
            }
        }
    }
    return QMainWindow::eventFilter(receiver, event);
}

} // namespace gmtool
