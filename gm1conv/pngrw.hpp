#ifndef PNGRW_HPP_
#define PNGRW_HPP_

#include <string>
#include <iosfwd>
#include <vector>

#include <SDL.h>

class PngRwPrivate;

class PngRw
{
public:
    PngRw();
    ~PngRw();
    
    PngRw(const PngRw&) = delete;
    PngRw& operator=(const PngRw&) = delete;

    SDL_Surface* read(std::istream* is);
    bool write(SDL_Surface* surface, std::ostream* out);

    std::vector<std::string> errors() const;
    std::vector<std::string> warnings() const;

private:
    PngRwPrivate* p_;
};

#endif
