#pragma once

#include <QObject>

#include <memory>

#include <boost/filesystem/path.hpp>

#include "gm1filedata.h"
#include "logmessage.h"
#include "operation.h"

namespace gmtool {
class EditorFactory;
class LogModel;

class DocumentLoader : public Operation
{
    Q_OBJECT

public:
    explicit DocumentLoader(QObject *parent = 0);

    void setFilePath(const boost::filesystem::path &filepath);

    bool success() const;

    LogModel *logs() const;

    boost::filesystem::path filePath() const;

    std::unique_ptr<EditorFactory> createEditorFactory() const;

    QString status() const override;

    void start() override;

private:
    Gm1FileData mGm1;
    LogModel *mLogModel;
    boost::filesystem::path mFilePath;
    bool mSuccess = false;

    DocumentLoader(const DocumentLoader &) = delete;
    DocumentLoader &operator=(const DocumentLoader &) = delete;
};

} // namespace gmtool
