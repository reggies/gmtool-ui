#include "repl.h"

#include <sstream>

#include <Python.h>

#include <QFileInfo>
#include <QDebug>

#include "document.h"
#include "documentobject.h"

#include <boost/python/object.hpp>
#include <boost/python/proxy.hpp>

namespace bp = boost::python;

namespace gmtool {

Repl::Repl(Document *document, QObject *parent)
    : QObject(parent)
    , mDoc(document)
{
    createWorld();
}

Repl::~Repl()
{
}

bool Repl::redirectStdHandles(bp::dict &globals)
{
    std::string classDefinition =
        "class MyStringIO:\n\
    def __init__(self):\n\
        self.value = ''\n\
    def clear(self):\n\
        self.value = ''\n\
    def write(self, txt):\n\
        self.value += txt\n\
"
        ;
    try
    {
        bp::exec(classDefinition.c_str(), globals, mLocals);
        bp::object sysModule = bp::import("sys");
        mLocals["MyStringIO"] = bp::eval("MyStringIO", globals, mLocals);
        mLocals["sys"] = sysModule;
        /* TBD: why implicit conversion of proxy<> to bp::object does not work as intended? */
        sysModule.attr("stdout") = mLocals["MyStringIO"]();
        sysModule.attr("stderr") = mLocals["MyStringIO"]();
        return true;
    }
    catch (bp::error_already_set &e)
    {
        qCritical("could not redirect stderr/stdout:");
        PyErr_Print();
        bp::handle_exception();
    }
    return false;
}

bool Repl::createWorld()
{
    try
    {
        bp::object main = bp::import("__main__");
        bp::dict globals = bp::extract<bp::dict>(main.attr("__dict__"));
        if (!redirectStdHandles(globals))
            return false;
        bp::object gmtool = bp::import("gmtool");
        mLocals["data"] = bp::ptr(mDoc->pythonObject());
        // This has no effect with boost 1.59, but it is
        // required for 1.65
        mLocals["__name__"] = "__main__";
        return true;
    }
    catch (bp::error_already_set& e)
    {
        PyErr_Print();
        bp::handle_exception();
    }
    return false;
}

bool Repl::loadScript(const std::string &scriptName)
{
    QFileInfo info(scriptName.c_str());
    const std::string& baseName = info.baseName().toStdString();
    try
    {
        bp::object main = bp::import("__main__");
        bp::object script = bp::import(baseName.c_str());
        bp::dict globals = bp::extract<bp::dict>(main.attr("__dict__"));
        if (!redirectStdHandles(globals))
            return false;
        bp::object gmtool = bp::import("gmtool");
        mLocals[baseName.c_str()] = script;
        bp::dict locals = bp::extract<bp::dict>(script.attr("__dict__"));
        locals["data"] = bp::ptr(mDoc->pythonObject());
        return true;
    }
    catch (bp::error_already_set &e)
    {
        PyErr_Print();
        bp::handle_exception();
        std::string stderr;
        if (grabStderr(stderr)) {
            emit outputStderr(stderr);
        }
    }
    return true;
}

bool Repl::getSystemExitExceptionObject(bp::object &systemExit)
{
    try {
        bp::object main = bp::import("__main__");
        bp::dict globals = bp::extract<bp::dict>(main.attr("__dict__"));
        systemExit = bp::eval("SystemExit", globals, globals);
        return true;
    }
    catch (bp::error_already_set &e) {
        PyErr_Clear();
        // bp::handle_exception();
        return false;
    }
}

bool Repl::evaluate(const std::string &expr)
{
    bp::object systemExit;
    if (!getSystemExitExceptionObject(systemExit))
        return false;
    bool wasSystemExitRaised = false;
    try
    {
        bp::object main = bp::import("__main__");
        bp::dict globals = bp::extract<bp::dict>(main.attr("__dict__"));
        if (!redirectStdHandles(globals))
            return false;
        bp::object module = bp::import("gmtool");
        bp::object result = bp::eval(expr.c_str(), globals, mLocals);
        if (!result.is_none()) {
            auto repr = bp::ptr(PyObject_Repr(result.ptr()));
            emit outputStdout(bp::extract<std::string>(repr));
        }
        std::string stdout;
        if (grabStdout(stdout)) {
            emit outputStdout(stdout);
        }
        std::string stderr;
        if (grabStderr(stderr)) {
            emit outputStderr(stderr);
        }
        return true;
    }
    catch (bp::error_already_set &e)
    {
        if (PyErr_ExceptionMatches(systemExit.ptr())) {
            PyErr_Clear();
            // bp::handle_exception();
            wasSystemExitRaised = true;
        } else {
            PyErr_Clear();
            // bp::handle_exception();
        }
    }
    if (wasSystemExitRaised) {
        emit systemExitRaised();
        return true;
    }
    return false;
}

bool Repl::execute(const std::string &commands)
{
    if (evaluate(commands))
        return true;
    bp::object systemExit;
    if (!getSystemExitExceptionObject(systemExit))
        return false;
    bool wasSystemExitRaised = false;
    try
    {
        bp::object main = bp::import("__main__");
        bp::dict globals = bp::extract<bp::dict>(main.attr("__dict__"));
        if (!redirectStdHandles(globals))
            return false;
        bp::object gmtool = bp::import("gmtool");
        bp::exec(commands.c_str(), globals, mLocals);
        std::string stdout;
        if (grabStdout(stdout)) {
            emit outputStdout(stdout);
        }
        std::string stderr;
        if (grabStderr(stderr)) {
            emit outputStderr(stderr);
        }
        return true;
    }
    catch (bp::error_already_set &e)
    {
        if (PyErr_ExceptionMatches(systemExit.ptr())) {
            PyErr_Clear();
            // bp::handle_exception();
            wasSystemExitRaised = true;
        }
        else {
            PyErr_Print();
            bp::handle_exception();
            std::string stderr;
            if (grabStderr(stderr)) {
                emit outputStderr(stderr);
            }
        }
    }
    if (wasSystemExitRaised) {
        emit systemExitRaised();
        return true;
    }
    return false;
}

static void removeTrailingNewline(std::string &line)
{
    if (line.size() == 0)
        return;
    if (!line.empty())
    {
        char eol = line [ line.size() - 1 ];
        if (eol == '\n' ||
            eol == '\r')
        {
            line.pop_back();
            if (!line.empty())
            {
                if (line.back() == '\n' || line.back() == '\r')
                {
                    if (line.back() != eol) {
                        line.pop_back();
                    }
                }
            }
        }
    }
}

bool Repl::grabStdout(std::string &output)
{
    try
    {
        bp::object sysModule = bp::import("sys");
        bp::object myout = sysModule.attr("stdout");
        bp::object value = myout.attr("value");
        bp::object clear = myout.attr("clear");
        output = bp::extract<std::string>(value);
        removeTrailingNewline(output);
        clear();
        return true;
    }
    catch (bp::error_already_set& e)
    {
        qCritical("stdout is not available");
        PyErr_Print();
        bp::handle_exception();
        std::string stderr;
        if (grabStderr(stderr)) {
            emit outputStderr(stderr);
        }
        return false;
    }
}

bool Repl::grabStderr(std::string &error)
{
    try
    {
        bp::object sysModule = bp::import("sys");
        bp::object myerr = sysModule.attr("stderr");
        bp::object value = myerr.attr("value");
        bp::object clear = myerr.attr("clear");
        error = bp::extract<std::string>(value);
        removeTrailingNewline(error);
        clear();
        return true;
    }
    catch (bp::error_already_set& e)
    {
        qCritical("stderr is not available");
        PyErr_Print();
        bp::handle_exception();
        return false;
    }
}

}
