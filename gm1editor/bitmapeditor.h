#pragma once

#include <set>

#include "bitmaprecord.h"
#include "documenteditor.h"
#include "gm1filedata.h"
#include "recordid.h"
#include "recordset.h"
#include "spriterecord.h"

namespace gmtool {

class BitmapEditor : public DocumentEditor
{
public:
    enum Flags {
        NoFlags = 0,
        ///< for GM1_ENTRY_CLASS_BITMAP

        UseTransparency = 1,
        ///< for GM1_ENTRY_CLASS_TGX16

        LayoutIsConstSize = 2,
        ///< for GM1_ENTRY_CLASS_TGX16CONSTSIZE

        MagicBitmap = 4
        ///< for GM1_ENTRY_CLASS_ANOTHERBITMAP
    };

public:
    explicit BitmapEditor(int flags = BitmapEditor::NoFlags);
    ~BitmapEditor() override;

    PropertiesModel *getPropertiesModel() override;
    TableModel *getModel(TableId table) override;
    std::unique_ptr<AbstractRecord> createBlankEntry() override;
    std::unique_ptr<NodeFactory> createLayoutFactory() override;
    std::unique_ptr<NodeFactory> createSpriteFactory() override;
    std::unique_ptr<SpriteLoader> createSpriteLoader() override;
    bool load(const Gm1FileData &fileData, LogModel *log) override;
    bool save(Gm1FileData *fileData, LogModel *log) override;

    bool renderSprites(const std::set<RecordId> &ids, LogModel *log, QImage *result) override;
    std::unique_ptr<AnimationFactory> createAnimationFactory() override;

private:
    void setupEntriesTableModel();
    void setupSpritesTableModel();

    auto getGm1EntryClass() const;

    QRgb getDefaultTransparentPixel() const;

private:
    RecordSet<BitmapRecord> mEntries;
    RecordSet<SpriteRecord> mSprites;

    std::unique_ptr<TableModel> mEntriesTableModel;
    std::unique_ptr<TableModel> mSpritesTableModel;
    std::unique_ptr<PropertiesModel> mPropertiesModel;

    Gm1FileData mGm1;

    int mFlags;

private:
    BitmapEditor(const BitmapEditor &) = delete;
    BitmapEditor &operator=(const BitmapEditor &) = delete;
};

} // namespace gmtool
