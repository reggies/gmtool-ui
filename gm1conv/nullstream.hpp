#ifndef NULLSTREAM_HPP_
#define NULLSTREAM_HPP_

#include <iostream>
#include <iomanip>

class nullbuf : public std::streambuf
{
public:
    virtual std::streamsize xsputn(const char* s, std::streamsize n) override
    {
        (void)s;
        return n;
    }

    virtual int overflow(int c) override
    {
        (void)c;
        return 1;
    }
};

class nullostream : public std::ostream
{
public:
    nullostream()
        : std::ostream(&buf)
    {}

private:
    nullbuf buf;
};

#endif
