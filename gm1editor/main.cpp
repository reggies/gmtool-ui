#include "engine.h"
#include <QApplication>
#include <QtPlugin>

// #include "DarkStyle.h"

Q_IMPORT_PLUGIN(TgxPlugin)

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // // Ownership of the style object is transferred to QApplication
    // QApplication::setStyle(new DarkStyle);

    gmtool::Engine engine;
    return engine.exec();
}
