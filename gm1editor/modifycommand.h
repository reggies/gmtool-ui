#pragma once

#include "undo.h"
#include <QAbstractItemModel>

namespace gmtool {

class ModifyCommand : public undo::Command
{
public:
    ModifyCommand(const QModelIndex &index,
                  const QVariant &newValue,
                  QAbstractItemModel *model,
                  const QString &name = QString());

    ~ModifyCommand() override;

    bool undo() override;
    bool redo() override;

    QString name() const override;

private:
    QModelIndex mIndex;
    QString mName;
    QVariant mNewValue;
    QVariant mOld;
    QAbstractItemModel *mModel;
};

} // namespace gmtool
