#include "documentobject.h"

#include <boost/python.hpp>
#include <boost/python/list.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/numpy.hpp>
#include <boost/tuple/tuple.hpp>

#include <Python.h>

#include <vector>

#include "spriterecord.h"
#include "animationrecord.h"
#include "tablemodel.h"
#include "removecommand.h"

namespace bp = boost::python;
namespace np = boost::python::numpy;

template <class VectorT>
struct VectorToPythonList
{
    using T = typename VectorT::value_type;

    static PyObject* convert(const VectorT &vec)
    {
        bp::list list;
        for (const T& item : vec) {
            list.append(item);
        }
        return bp::incref(list.ptr());
    }
};

template <class VectorT>
struct VectorToPythonListRef
{
    using T = typename VectorT::value_type;
    using Ref = typename bp::reference_existing_object::apply<T*>::type;

    static PyObject* convert(const VectorT &vec)
    {
        bp::list list;
        Ref ref;
        for (const T &item : vec) {
            list.append(bp::handle<>(ref(item)));
        }
        return bp::incref(list.ptr());
    }
};


template <class VectorT>
struct VectorFromPythonTuple
{
    using T = typename VectorT::value_type;

    VectorFromPythonTuple()
    {
        bp::converter::registry::push_back(
            &convertible,
            &construct,
            bp::type_id<VectorT>());
    }

    static void* convertible(PyObject *object)
    {
        if (!PyTuple_Check(object))
            return nullptr;

        // Check the tuple elements and convert to a
        // boost::tuple object
        bp::tuple t(bp::handle<>(bp::borrowed(object)));
        auto elementCount = PyTuple_Size(object);
        // Check each element individually with bp::extract
        // and then check
        for (decltype(elementCount) i = 0; i < elementCount; ++i)
        {
            if (!bp::extract<T>(t[i]).check())
                return nullptr;
        }
        return object;
    }

    static void construct(PyObject *object, bp::converter::rvalue_from_python_stage1_data *data)
    {
        bp::tuple tuple(bp::handle<>(bp::borrowed(object)));
        auto rvalue = reinterpret_cast<bp::converter::rvalue_from_python_storage<VectorT>*>(data);
        auto elementCount = PyTuple_Size(object);
        auto container = new (rvalue->storage.bytes) VectorT();
        container->reserve(elementCount);
        for (decltype(elementCount) i = 0; i < elementCount; ++i)
        {
            container->push_back(bp::extract<T>(tuple[i])());
        }
        data->convertible = container;
    }
};


template <class VectorT>
struct VectorFromPythonList
{
    using T = typename VectorT::value_type;

    VectorFromPythonList()
    {
        bp::converter::registry::push_back(
            &convertible,
            &construct,
            bp::type_id<VectorT>());
    }

    static void* convertible(PyObject *object)
    {
        if (!PyList_Check(object))
            return nullptr;

        // Check that all of the list elements can be
        // converted to the right type
        bp::list list(bp::handle<>(bp::borrowed(object)));
        auto elementCount = PyList_Size(object);
        // Can all of the elements be converted to type 'T'?
        for (decltype(elementCount) i = 0; i < elementCount; ++i)
        {
            if (!bp::extract<T>(list[i]).check())
                return nullptr;
        }
        return object;
    }

    static void construct(PyObject *object, bp::converter::rvalue_from_python_stage1_data *data)
    {
        // Object is a borrowed reference, so create a handle indicting it is
        // borrowed for proper reference counting.
        bp::list list(bp::handle<>(bp::borrowed(object)));
        // Obtain a handle to the memory block that the converter has allocated
        // for the C++ type.
        auto rvalue = reinterpret_cast<bp::converter::rvalue_from_python_storage<VectorT>*>(data);
        auto elementCount = PyList_Size(object);
        // Allocate the C++ type into the converter's memory block, and assign
        // its handle to the converter's convertible variable.
        auto container = new (rvalue->storage.bytes) VectorT();
        container->reserve(elementCount);
        for (decltype(elementCount) i = 0; i < elementCount; ++i)
        {
            container->push_back(bp::extract<T>(list[i])());
        }
        data->convertible = container;
    }
};

template<class T>
void defineVectorType(const char *name)
{
    bp::to_python_converter<std::vector<T>, VectorToPythonList<std::vector<T>>>();

    VectorFromPythonList<std::vector<T>>();
    VectorFromPythonTuple<std::vector<T>>();
}

template<class T>
void defineVectorTypeRef(const char *name)
{
    bp::to_python_converter<std::vector<T *>, VectorToPythonListRef<std::vector<T *>>>();

    VectorFromPythonList<std::vector<T *>>();
    VectorFromPythonTuple<std::vector<T *>>();
}

namespace gmtool {

namespace python {

DocumentObject::DocumentObject(undo::Stack *stack, DocumentEditor *editor)
    : mStack(stack), mEditor(editor)
{
    mImageList = new ImageList(stack, editor);
}

DocumentObject::~DocumentObject()
{
    delete mImageList;
}

SpriteDelegate* DocumentObject::addImage()
{
    qDebug() << Q_FUNC_INFO;
    auto model = mEditor->getModel(TableId::Spritesheet);
    model->insertRows(model->rowCount(), 1);
    auto id = model->getIdOf(model->rowCount() - 1);
    return new SpriteDelegate(mStack, mEditor, id);
}


//



template<>
QImage Image::toQImage() const
{
    return QImage((const uchar *)mPixels.data(), width(), height(), QImage::Format_RGB555);
}

template<>
QImage IndexedImage::toQImage() const
{
    return QImage((const uchar *)mPixels.data(), width(), height(), QImage::Format_Indexed8);
}



//



Point2D SpriteData::pos() const
{
    return mPos;
}

void SpriteData::setPos(const Point2D& pos)
{
    mPos = pos;
}

Image SpriteData::image() const
{
    return mImage;
}

void SpriteData::setImage(const Image& image)
{
    mImage = image;
}

std::string SpriteData::toString() const
{
    std::ostringstream oss;
    oss << std::showpos << "Sprite(" << Point2D(mPos).toString() << ")";
    return oss.str();
}


//


class SetImage : public undo::Command {
    TableModel *model;
    int index;
    QImage oldImage;
    QImage newImage;
public:
    SetImage(TableModel *model, int index, const QImage &image)
        : model(model), index(index), newImage(image)
    {}
    ~SetImage() override {}
    bool undo() override {
        auto record = (SpriteRecord *)model->getRecordAs<SpriteRecord>(model->getIdOf(index));
        record->setImage(oldImage);
        return true;
    }
    bool redo() override {
        auto record = (SpriteRecord *)model->getRecordAs<SpriteRecord>(model->getIdOf(index));
        oldImage = record->image();
        record->setImage(newImage);
        return true;
    }
    QString name() const override {
        return "set image";
    }
};

class SetPos : public undo::Command {
    TableModel *model;
    int index;
    QPoint newPos;
    QPoint oldPos;
public:
    SetPos(TableModel *model, int index, const QPoint &pos)
        : model(model), index(index), newPos(pos)
    {}
    ~SetPos() override {}
    bool undo() override {
        auto record = (SpriteRecord *)model->getRecordAs<SpriteRecord>(model->getIdOf(index));
        record->setPosition(oldPos);
        return true;
    }
    bool redo() override {
        auto record = (SpriteRecord *)model->getRecordAs<SpriteRecord>(model->getIdOf(index));
        oldPos = record->position();
        record->setPosition(newPos);
        return true;
    }
    QString name() const override {
        return "set position";
    }
};

Point2D SpriteDelegate::pos() const
{
    qDebug() << Q_FUNC_INFO;
    auto model = mEditor->getModel(TableId::Spritesheet);
    auto record = (SpriteRecord *)model->getRecordAs<SpriteRecord>(mRecordId);
    return Point2D(record->position());
}

void SpriteDelegate::setPos(const Point2D& pos)
{
    qDebug() << Q_FUNC_INFO;
    auto model = mEditor->getModel(TableId::Spritesheet);
    auto index = model->getPositionOf(mRecordId);
    auto record = (SpriteRecord *)model->getRecordAs<SpriteRecord>(mRecordId);
    mStack->push(std::make_unique<SetPos>(model, index, pos.toQPoint()));
}

Image SpriteDelegate::image() const
{
    qDebug() << Q_FUNC_INFO;
    auto model = mEditor->getModel(TableId::Spritesheet);
    auto record = (SpriteRecord *)model->getRecordAs<SpriteRecord>(mRecordId);
    auto image = record->image();
    return Image((const uchar *)image.constBits(), image.width(), image.height(), image.bytesPerLine());
}

void SpriteDelegate::setImage(const Image& img)
{
    qDebug() << Q_FUNC_INFO;
    auto model = mEditor->getModel(TableId::Spritesheet);
    auto index = model->getPositionOf(mRecordId);
    auto record = (SpriteRecord *)model->getRecordAs<SpriteRecord>(mRecordId);
    mStack->push(std::make_unique<SetImage>(model, index, img.toQImage()));
}




//




ImageList::ImageList(undo::Stack *stack, DocumentEditor *editor)
    : mStack(stack), mEditor(editor)
{}

size_t ImageList::len() const
{
    qDebug() << Q_FUNC_INFO;
    return mEditor->getModel(TableId::Spritesheet)->rowCount();
}

SpriteDelegate* ImageList::getItem(int index)
{
    qDebug() << Q_FUNC_INFO << index;
    if (index >= len()) {
        PyErr_SetObject(PyExc_ValueError, bp::object("index out of range").ptr());
        bp::throw_error_already_set();
    }
    auto model = mEditor->getModel(TableId::Spritesheet);
    auto id = model->getIdOf(index);
    auto record = model->getRecordAs<SpriteRecord>(id);
    return new SpriteDelegate(mStack, mEditor, id);
}

void ImageList::setItem(int index, const ISprite &elem)
{
    qDebug() << Q_FUNC_INFO << index;
    if (index >= len()) {
        PyErr_SetObject(PyExc_ValueError, bp::object("index out of range").ptr());
        bp::throw_error_already_set();
    }
    // PyObject* p = boost::python::expect_non_null(
    //     PyObject_GetAttrString(obj.ptr(), "__name__"));
    auto model = mEditor->getModel(TableId::Spritesheet);
    auto macro = std::make_unique<undo::Macro>(QObject::tr("set item"));
    macro->add(std::make_unique<SetPos>(model, index, elem.pos().toQPoint()));
    macro->add(std::make_unique<SetImage>(model, index, elem.image().toQImage()));
    mStack->push(std::move(macro));
}

void ImageList::delItem(int index)
{
    qDebug() << Q_FUNC_INFO << index;
    if (index >= len()) {
        PyErr_SetObject(PyExc_ValueError, bp::object("index out of range").ptr());
        bp::throw_error_already_set();
    }
    auto model = mEditor->getModel(TableId::Spritesheet);
    mStack->push(std::make_unique<RemoveCommand>(index, model));
}

SpriteDelegate* ImageList::insert(int index, const ISprite &elem)
{
    qDebug() << Q_FUNC_INFO << index;
    if (index >= len()) {
        PyErr_SetObject(PyExc_ValueError, bp::object("index out of range").ptr());
        bp::throw_error_already_set();
    }
    auto model = mEditor->getModel(TableId::Spritesheet);
    model->insertRows(index, 1);
    auto id = model->getIdOf(index);
    setItem(index, elem);
    return getItem(index);
}

// class MyStringIO {
// public:
//     MyStringIO() {
//         qDebug() << Q_FUNC_INFO;
//     }
//     void clear() {
//         mBuffer.clear();
//     }
//     void write(const std::string& str) {
//         mBuffer.insert(mBuffer.end(), str.begin(), str.end());
//     }
//     std::string value() const {
//         return std::string(mBuffer.begin(), mBuffer.end());
//     }
// private:
//     std::vector<char> mBuffer;
// };

// void setupStdio()
// {
//     try {
//         bp::object sys;
//         bp::object main;
//         bp::dict globals;
//         bp::dict locals;
//         bp::object my;

//         main = bp::import("__main__");
//         sys = bp::import("sys");
//         my = bp::import("gmtool");

//         std::string classDefinition =
//             "class MyCStringIO:\n\
//     def __init__(self):\n\
//         self.value = ''\n\
//     def clear(self):\n\
//         self.value = ''\n\
//     def write(self, txt):\n\
//         self.value += txt\n\
// "
//             ;

//         globals = bp::extract<bp::dict>(main.attr("__dict__"));

//         bp::exec(classDefinition.c_str(), globals, locals);
//         locals["MyStringIO"] = bp::eval("MyCStringIO", globals, locals);

//         sys.attr("stdout") = locals["MyStringIO"]();
//         sys.attr("stderr") = locals["MyStringIO"]();

//     } catch (bp::error_already_set &e)
//     {
//         qDebug() << Q_FUNC_INFO << "exception";
//         PyErr_Print();
//         bp::handle_exception();
//     }
// }

} // namespace python

} // namespace gmtool

BOOST_PYTHON_MODULE(gmtool)
{
    try { bp::import("gmtool"); } catch (...) {}

    defineVectorType<gmtool::python::ColorRGBA>("ColorList");
    defineVectorType<uint16_t>("U16Vector");
    defineVectorType<uint8_t>("U8Vector");

    // bp::class_<gmtool::python::MyStringIO, boost::noncopyable>("MyCStringIO", bp::no_init)
    //     .def("clear", &gmtool::python::MyStringIO::clear)
    //     .def("write", &gmtool::python::MyStringIO::write)
    //     .add_property("value", &gmtool::python::MyStringIO::value)
    //     ;

    // gmtool::python::setupStdio();

    bp::class_<gmtool::python::Point2D>("Point2D", bp::no_init)
        .def(bp::init<>())
        .def(bp::init<int, int>())
        .def("__repr__", &gmtool::python::Point2D::toString)
        .add_property("x", &gmtool::python::Point2D::x, &gmtool::python::Point2D::setX)
        .add_property("y", &gmtool::python::Point2D::y, &gmtool::python::Point2D::setY)
        ;

    bp::class_<gmtool::python::ColorRGBA>("ColorRGBA", bp::no_init)
        .def(bp::init<>())
        .def(bp::init<int, int, int, int>())
        .def("__repr__", &gmtool::python::ColorRGBA::toString)
        .add_property("red", &gmtool::python::ColorRGBA::red, &gmtool::python::ColorRGBA::setRed)
        .add_property("green", &gmtool::python::ColorRGBA::green, &gmtool::python::ColorRGBA::setGreen)
        .add_property("blue", &gmtool::python::ColorRGBA::blue, &gmtool::python::ColorRGBA::setBlue)
        .add_property("alpha", &gmtool::python::ColorRGBA::alpha, &gmtool::python::ColorRGBA::setAlpha)
        ;

    bp::class_<gmtool::python::Rect2D>("Rect2D", bp::no_init)
        .def(bp::init<>())
        .def(bp::init<int, int, int, int>())
        .def("__repr__", &gmtool::python::Rect2D::toString)
        .add_property("x", &gmtool::python::Rect2D::x, &gmtool::python::Rect2D::setX)
        .add_property("y", &gmtool::python::Rect2D::y, &gmtool::python::Rect2D::setY)
        .add_property("width", &gmtool::python::Rect2D::width, &gmtool::python::Rect2D::setWidth)
        .add_property("height", &gmtool::python::Rect2D::height, &gmtool::python::Rect2D::setHeight)
        ;

    bp::class_<gmtool::python::ISprite, boost::noncopyable>("ISprite", bp::no_init)
        .add_property("pos", &gmtool::python::ISprite::pos, &gmtool::python::ISprite::setPos)
        .add_property("image", &gmtool::python::ISprite::image, &gmtool::python::ISprite::setImage)
        .def("clone",
             bp::make_function(&gmtool::python::ISprite::clone, bp::return_value_policy<bp::manage_new_object>()))
        ;

    bp::class_<gmtool::python::SpriteDelegate,
               bp::bases<gmtool::python::ISprite>, boost::noncopyable>("SpriteDelegate", bp::no_init)
        // .add_property("data", &gmtool::python::SpriteDelegate::data, &gmtool::python::SpriteDelegate::setData)
        ;

    bp::class_<gmtool::python::SpriteData,
               bp::bases<gmtool::python::ISprite>>("SpriteData", bp::no_init)
        .def(bp::init<gmtool::python::Point2D>())
        .def("__repr__", &gmtool::python::SpriteData::toString)
        // .add_property("indexed_image", &gmtool::python::SpriteData::indexedImage, &gmtool::python::SpriteData::setIndexedImage)
        ;

    bp::class_<gmtool::python::ImageList, boost::noncopyable>("ImageList", bp::no_init)
        .def("__len__", &gmtool::python::ImageList::len)
        // .def("__length_hint__", &gmtool::python::ImageList::len)
        .def("__getitem__",
             bp::make_function(&gmtool::python::ImageList::getItem, bp::return_value_policy<bp::manage_new_object>()))
        .def("__setitem__", &gmtool::python::ImageList::setItem)
        .def("__delitem__", &gmtool::python::ImageList::delItem)
        // .def("__missing__", &gmtool::python::ImageList::missing)
        // .def("__iter__", &gmtool::python::ImageList::iter)
        // .def("__reversed__", &gmtool::python::ImageList::reversed)
        // .def("__contains__", &gmtool::python::ImageList::contains)
        .def("insert",
            bp::make_function(&gmtool::python::ImageList::insert, bp::return_value_policy<bp::manage_new_object>()))
        ;

    bp::class_<gmtool::python::Image// , boost::noncopyable
               >("Image", bp::no_init)
        .def(bp::init<int, int>())
        .add_property("width", &gmtool::python::Image::width, &gmtool::python::Image::setWidth)
        .add_property("height", &gmtool::python::Image::height, &gmtool::python::Image::setHeight)
        .add_property("pixels", &gmtool::python::Image::pixels, &gmtool::python::Image::setPixels)
        ;

    // bp::class_<gmtool::python::IndexedImage, boost::noncopyable>("IndexedImage", bp::no_init)
    //     .add_property("width", &gmtool::python::ImageIndexed::width, &gmtool::python::ImageIndexed::setWidth)
    //     .add_property("height", &gmtool::python::ImageIndexed::height, &gmtool::python::ImageIndexed::setHeight)
    //     .add_property("pixels", &gmtool::python::IndexedImage::pixels, &gmtool::python::IndexedImage::setPixels)
    //     ;

    // bp::class_<gmtool::python::Image, boost::noncopyable>("Image", bp::no_init)
    //     .def(bp::init<gmtool::python::Rect2D>())
    //     .add_property("pos", &gmtool::python::Image::pos, &gmtool::python::Image::setPos)
    //     .add_property("rect", &gmtool::python::Image::rect, &gmtool::python::Image::setRect)
    //     .add_property("pixels", &gmtool::python::Image::pixels, &gmtool::python::Image::setPixels)
    //     ;

    // bp::class_<gmtool::python::IndexedImage>("IndexedImage", bp::no_init)
    //     .add_property("pos", &gmtool::python::IndexedImage::pos, &gmtool::python::IndexedImage::setPos)
    //     .add_property("rect", &gmtool::python::IndexedImage::rect, &gmtool::python::IndexedImage::setRect)
    //     .add_property("pixels", &gmtool::python::IndexedImage::pixels, &gmtool::python::IndexedImage::setPixels)
    //     ;

    bp::class_<gmtool::python::DocumentObject, boost::noncopyable>("DocumentObject", bp::no_init)

        .add_property("image_list",
                      bp::make_function(&gmtool::python::DocumentObject::imageList, bp::return_value_policy<bp::reference_existing_object>()))

        .def("add_image",
             bp::make_function(&gmtool::python::DocumentObject::addImage, bp::return_value_policy<bp::manage_new_object>()))

        // .def("add_image",
        //      bp::make_function(&gmtool::python::DocumentObject::addImage, bp::return_value_policy<bp::reference_existing_object>()))
        // .def("add_indexed_image",
        //      bp::make_function(&gmtool::python::DocumentObject::addIndexedImage, bp::return_value_policy<bp::reference_existing_object>()))
        ;
}
