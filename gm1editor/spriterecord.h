#pragma once

#include <boost/filesystem/path.hpp>

#include <limits>

#include <QBitmap>
#include <QImage>
#include <QPoint>
#include <QRegion>

#include "abstractrecord.h"
#include "colormask.h"
#include "transparencymode.h"

namespace gmtool {

class SpriteRecordData
{
public:
    QImage mImg;
    mutable QBitmap mMask;
    QPoint mPos;
    QRgb mTransparentPixel = 0;
    TransparencyMode mTransparencyMode = TransparencyMode::None;
    boost::filesystem::path mPath;
};

class SpriteRecord : public AbstractRecord, public SpriteRecordData
{
    Q_OBJECT

public:
    explicit SpriteRecord(const SpriteRecordData &data = {}, QObject *parent = nullptr)
        : AbstractRecord(parent)
        , SpriteRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new SpriteRecord(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        *(static_cast<SpriteRecordData *>(this)) = dynamic_cast<const SpriteRecordData &>(other);
        emit imageChanged();
        emit positionChanged();
    }

    std::unique_ptr<AbstractRecord> movedRecord(int x, int y) const
    {
        SpriteRecordData data(*this);
        data.mPos.setY(y);
        data.mPos.setX(x);
        return SpriteRecord(data, parent()).clone();
    }

    auto isNull() const { return mImg.isNull() && mPos.isNull(); }

    QBitmap mask() const
    {
        if (mMask.isNull() && (mTransparencyMode == TransparencyMode::Color)) {
            mMask = createColorMask(mTransparentPixel, mImg);
        }
        return mMask;
    }

    auto image() const { return mImg; }
    void setImage(const QImage &image)
    {
        mImg = image;
        emit imageChanged();
    }
    auto position() const { return mPos; }
    void setPosition(const QPoint &position)
    {
        mPos = position;
        emit positionChanged();
    }
    auto left() const { return mPos.x(); }
    void setLeft(int left)
    {
        mPos.setX(left);
        emit positionChanged();
    }
    auto top() const { return mPos.y(); }
    void setTop(int top)
    {
        mPos.setY(top);
        emit positionChanged();
    }
    auto width() const { return mImg.width(); }
    auto height() const { return mImg.height(); }
    auto rect() const { return QRect(mPos, mImg.size()); }

    void setTransparentPixel(const QRgb &color)
    {
        mTransparentPixel = color;
        mMask = QBitmap();
        emit imageChanged();
    }

    void setMask(const QBitmap &mask)
    {
        mMask = mask;
        emit imageChanged();
    }

    void setTransparencyMode(TransparencyMode mode)
    {
        mTransparencyMode = mode;
        emit imageChanged();
    }

    auto filePath() const { return mPath; }
    void setFilePath(const boost::filesystem::path &path) { mPath = path; }

signals:
    void positionChanged();
    void imageChanged();
};

} // namespace gmtool
