#include "bitmapgraphicsitem.h"

#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>

#include "assigncommand.h"
#include "bitmapeditor.h"
#include "nodeattribute.h"
#include "tableid.h"
#include "tablemodel.h"

namespace gmtool {

BitmapGraphicsItem::BitmapGraphicsItem(BitmapEditor *editor, const RecordId &id, SceneNode *parent)
    : SceneNode(parent)
    , mRecordId(id)
    , mEditor(editor)
{
    setData(NodeAttribute::RecordId, QVariant::fromValue(id));
    setData(NodeAttribute::TableId, QVariant::fromValue(TableId::Layout));

    mRecord = mEditor->getModel(TableId::Layout)->getRecordAs<BitmapRecord>(mRecordId);
    setPos(mRecord->rect().topLeft());
}

QRectF BitmapGraphicsItem::boundingRect() const
{
    return QRectF(0, 0, mRecord->width(), mRecord->height());
}

void BitmapGraphicsItem::setViewState(const ViewState &state) {}

void BitmapGraphicsItem::paint(QPainter *painter,
                               const QStyleOptionGraphicsItem *option,
                               QWidget *widget)
{
    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, false);
    QPen pen;
    pen.setCosmetic(true);
    if (isSelected()) {
        pen.setColor(QColor(255, 0, 0));
    }
    painter->setPen(pen);
    painter->drawRect(boundingRect());
    painter->restore();
}

std::unique_ptr<AbstractRecord> BitmapGraphicsItem::movedRecord(const QPoint &toPoint)
{
    return mRecord->movedRecord(toPoint.x(), toPoint.y());
}

void BitmapGraphicsItem::reloadModelData()
{
    prepareGeometryChange();
    setPos(mRecord->rect().topLeft());
}

} // namespace gmtool
