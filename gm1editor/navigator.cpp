#include "navigator.h"

#include <QApplication>
#include <QDebug>
#include <QDesktopWidget>
#include <QIcon>
#include <QPainter>

#include "navigatorpopup.h"

namespace gmtool {

Navigator::Navigator(QGraphicsScene *scene, QWidget *parent)
    : QWidget(parent)
{
    setCursor(Qt::SizeAllCursor);

    mPopup = new NavigatorPopup;
    mPopup->hide();
    mPopup->setInteractive(false);
    mPopup->setScene(scene);
    mPopup->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    mPopup->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    mPopup->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    connect(mPopup, &NavigatorPopup::mouseMoved, this, &Navigator::navigated);
}

Navigator::~Navigator()
{
    delete mPopup;
}

QSize Navigator::minimumSizeHint() const
{
    return QSize(16, 16);
}

void Navigator::paintEvent(QPaintEvent *event)
{
    QSize size(width(), height());
    QPixmap pm = QIcon::fromTheme("cursor-move").pixmap(size);

    Q_ASSERT(!pm.isNull());

    QPainter painter(this);
    painter.drawPixmap(0, 0, pm);
}

void Navigator::mouseMoveEvent(QMouseEvent *event)
{
    qDebug() << Q_FUNC_INFO;
    event->accept();
}

void Navigator::mousePressEvent(QMouseEvent *event)
{
    mPopup->show();

    QSize hint = mPopup->sceneRect().size().toSize();

    auto f = 256.f / qMax(hint.width(), hint.height());

    hint.setWidth(qMax(128.f, hint.width() * f));
    hint.setHeight(qMax(128.f, hint.height() * f));

    QDesktopWidget *desktop = QApplication::desktop();

    auto screenRect = QRect(event->screenPos().toPoint()
                                - QPoint(hint.width() / 2, hint.height() / 2),
                            event->screenPos().toPoint()
                                + QPoint((hint.width() + 1) / 2, (hint.height() + 1) / 2));

    if (screenRect.bottom() > desktop->rect().bottom()) {
        screenRect.translate(0, -(screenRect.bottom() - desktop->rect().bottom()));
    }

    if (screenRect.right() > desktop->rect().right()) {
        screenRect.translate(-(screenRect.right() - desktop->rect().right()), 0);
    }

    mPopup->setGeometry(screenRect);
    mPopup->fitInView(mPopup->sceneRect(), Qt::KeepAspectRatio);

    event->accept();
}

void Navigator::mouseReleaseEvent(QMouseEvent *event)
{
    mPopup->hide();
    event->accept();
}

} // namespace gmtool
