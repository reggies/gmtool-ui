#pragma once

#include <functional>

#include "tilealignment.h"
#include "tilerecord.h"

namespace gmtool {

struct SubTile
{
    int order;
    int xSkewed;
    int ySkewed;
    int xAligned;
    int yAligned;
    TileAlignment alignment;
    QRect tileBbox;
    QRect entryBbox;
    int boxHeight;
};

void walkSubTiles(const TileRecord &record, const std::function<void(SubTile)> &visitor);

} // namespace gmtool
