#pragma once

#include <memory>

#include <QDialog>
#include <QListView>

#include "documenttype.h"

namespace gmtool {
class DocumentTypesModel;
class EditorFactory;

class CreateDocumentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CreateDocumentDialog(QWidget *parent = nullptr);

    DocumentType selectedDocumentType() const;

    static std::unique_ptr<EditorFactory> createEditorFactory(QWidget *parent);

private:
    DocumentTypesModel *mModel;
    QListView *mView;
};

} // namespace gmtool
