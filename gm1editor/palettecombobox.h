#pragma once

#include <QComboBox>

namespace gmtool {

class DocumentEditor;

class PaletteComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit PaletteComboBox(DocumentEditor *editor, QWidget *parent = nullptr);

private:
    DocumentEditor *mEditor;
};

} // namespace gmtool
