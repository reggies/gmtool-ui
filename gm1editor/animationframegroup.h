#pragma once

#include "abstractrecord.h"

#include <memory>

#include <QString>

namespace gmtool {

struct AnimationFrameGroupRecordData
{
    AnimationFrameGroupRecordData() = default;
    AnimationFrameGroupRecordData(
        const QString &name, int frameStart, int frameStep, int frameCount, bool backAndForth)
        : name(name)
        , frameStart(frameStart)
        , frameStep(frameStep)
        , frameCount(frameCount)
        , backAndForth(backAndForth)
    {}

    AnimationFrameGroupRecordData(const AnimationFrameGroupRecordData &) = default;
    AnimationFrameGroupRecordData &operator=(const AnimationFrameGroupRecordData &) = default;

    QString name;
    int frameStart = 0;
    int frameStep = 0;
    int frameCount = 1;
    bool backAndForth = false;
};

class AnimationFrameGroup : public AbstractRecord, public AnimationFrameGroupRecordData
{
    Q_OBJECT

public:
    explicit AnimationFrameGroup(const AnimationFrameGroupRecordData &data = {},
                                 QObject *parent = nullptr)
        : AbstractRecord(parent)
        , AnimationFrameGroupRecordData(data)
    {}

    virtual std::unique_ptr<AbstractRecord> clone() const override
    {
        return std::unique_ptr<AbstractRecord>(new AnimationFrameGroup(*this, parent()));
    }

    virtual void assign(const AbstractRecord &other) override
    {
        *((AnimationFrameGroupRecordData *) this)
            = dynamic_cast<const AnimationFrameGroupRecordData &>(other);
    }
};

} // namespace gmtool
