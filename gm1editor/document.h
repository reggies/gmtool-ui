#pragma once

#include <QWidget>

#include <memory>
#include <set>

#include <boost/filesystem/path.hpp>

#include "documenteditor.h"
#include "undo.h"

namespace gmtool {
class DocumentWriter;
class DocumentLoader;
class DocumentOperation;
class EditorFactory;
class LogModel;

namespace python {
class DocumentObject;
}

class Document : public QObject
{
    Q_OBJECT

public:
    explicit Document(QObject *parent = 0);
    virtual ~Document();

    python::DocumentObject* pythonObject();

    undo::Stack *undoStack();

    DocumentEditor *editor();

    LogModel *logModel();

    DocumentOperation *operation();

    boost::filesystem::path filePath() const;
    void setFilePath(const boost::filesystem::path &path);

    QString name() const;
    void setName(const QString &name);

    bool isEmpty() const;

    void setEditorFactory(const EditorFactory &factory);

    void loadFromFile(const boost::filesystem::path &path);

    void saveToFile(const boost::filesystem::path &path);

    //! This is probably not the best class for this method.
    void moveForward(const std::set<int> &spriteIndices);

    //! This is probably not the best class for this method.
    void moveBackward(const std::set<int> &spriteIndices);

    //! This is probably not the best class for this method.
    void bringForward(const std::set<int> &spriteIndices);

    //! This is probably not the best class for this method.
    void sendBack(const std::set<int> &spriteIndices);

    //! This is probably not the best class for this method.
    void importSprites(QWidget *parent, const QPoint &advisedPosition = QPoint());

    //! This is probably not the best class for this method.
    void exportSprites(QWidget *parent, const std::set<RecordId> &ids);

    //! This is probably not the best class for this method.
    void addEntry(QWidget *parent, const QPoint &advisedPosition = QPoint(), int beforeRow = -1);

signals:
    void filePathChanged();
    void editorChanged();
    void operationChanged();

private slots:
    //! the loader ownership is transferred to this function
    void loaderFinished(DocumentLoader *loader);

    //! the loader ownership is transferred to this function
    void writerFinished(DocumentWriter *writer);

private:
    void setEditor(std::unique_ptr<DocumentEditor> editor);

private:
    undo::Stack mUndoStack;
    std::unique_ptr<DocumentEditor> mEditor;
    DocumentOperation *mOperation;

    boost::filesystem::path mPath;

    QString mDocumentName;

    LogModel *mLogModel;

    std::unique_ptr<python::DocumentObject> mDocumentObject;

private:
    Document &operator=(const Document &) = delete;
    Document(const Document &) = delete;
};

} // namespace gmtool
