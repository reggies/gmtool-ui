#pragma once

#include <QPoint>

#include "recordid.h"
#include "scenenode.h"

namespace gmtool {
class AnimationEditor;

class AnimationPivotGraphicsItem : public SceneNode
{
public:
    explicit AnimationPivotGraphicsItem(AnimationEditor *editor,
                                        const RecordId &id,
                                        SceneNode *parent = nullptr);

    QRectF boundingRect() const override;

    void reloadModelData() override;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget = nullptr) override;

private:
    AnimationEditor *mEditor;
    QPoint mPivot;
    QSize mSize;
    RecordId mRecordId;
};

} // namespace gmtool
