/*
  Copyright (c) 2016 Alexey Natalin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef GM1_ENDIAN_H_
#define GM1_ENDIAN_H_

#include "gm1_conf.h"

/* TODO use gcc intrinsics */
#define gm1_bswap32(val) ((((val) & 0xff000000) >> 24) |    \
                          (((val) & 0x00ff0000) >>  8) |    \
                          (((val) & 0x0000ff00) <<  8) |    \
                          (((val) & 0x000000ff) << 24))

#define gm1_bswap16(val) (((((val) >> 8) & 0xff) | (((val) & 0xff) << 8)))

#if GM1_BIG_ENDIAN
#  define gm1_swap32le(val) ((val) = (gm1_bswap32((val))))
#  define gm1_swap16le(val) ((val) = (gm1_bswap16((val))))
#else
#  define gm1_swap32le(val)
#  define gm1_swap16le(val)
#endif

#endif // GM1_ENDIAN_H_
