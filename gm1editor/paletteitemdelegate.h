#pragma once

#include <QStyledItemDelegate>

namespace gmtool {

class PalettePreview;
class TableModel;

class PaletteItemDelegate : public QStyledItemDelegate
{
public:
    explicit PaletteItemDelegate(TableModel *model, QWidget *parent = nullptr);

    ~PaletteItemDelegate() override;

    virtual void paint(QPainter *painter,
                       const QStyleOptionViewItem &option,
                       const QModelIndex &index) const override;

    virtual QSize sizeHint(const QStyleOptionViewItem &option,
                           const QModelIndex &index) const override;

private:
    PalettePreview *mPreview;
    TableModel *mModel;
};

} // namespace gmtool
