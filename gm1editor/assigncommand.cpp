#include "assigncommand.h"

#include "abstractrecord.h"
#include "tablemodel.h"

#include <stdexcept>

namespace gmtool {

AssignCommand::AssignCommand(int row, TableModel *model, const AbstractRecord &record)
    : mRow(row)
    , mModel(model)
    , mRecord(record.clone())
{}

AssignCommand::~AssignCommand() = default;

bool AssignCommand::redo()
{
    mOldRecord = mModel->getRecord(mModel->getIdOf(mRow))->clone();
    return mModel->assignRow(mRow, *mRecord);
}

QString AssignCommand::name() const
{
    return QString();
}

bool AssignCommand::undo()
{
    return mModel->assignRow(mRow, *mOldRecord);
}

} // namespace gmtool
