#pragma once

#include <QCloseEvent>

#include <boost/filesystem/path.hpp>

#include <set>

namespace gmtool {

class Document;
class Frame;

class FrameManager : public QObject
{
    Q_OBJECT

public:
    explicit FrameManager(QObject *parent = 0);
    virtual ~FrameManager();

    Frame *createFrame(Document *document);
    void removeFrame(Frame *frame);

    int getFramesCount(const Document *document) const;
    void removeFrames(const Document *document);

signals:
    void newDocumentRequested(Frame *frame);
    void openDocumentRequested(Frame *frame, const boost::filesystem::path &path);
    void closeDocumentRequested(Document *document);

private slots:
    void cloneFrameRequested(Frame *frame);
    void closeFrameRequested(Frame *frame);

private:
    std::set<Frame *> mFrames;
    ///< owner by this object exclusively

    FrameManager(const FrameManager &) = delete;
    FrameManager &operator=(const FrameManager &) = delete;
};

} // namespace gmtool
