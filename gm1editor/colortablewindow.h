#pragma once

#include "window.h"

#include <QAction>
#include <QTableView>

namespace gmtool {
class Document;

class ColorTableWindow : public Window
{
    Q_OBJECT

public:
    explicit ColorTableWindow(Document *document, QWidget *parent = nullptr);

    void route(ChangeHue *action) override;
    void unroute(ChangeHue *action) override;

    void route(LoadPalette *action) override;
    void unroute(LoadPalette *action) override;

    void route(SavePalette *action) override;
    void unroute(SavePalette *action) override;

signals:
    void gotAnySelection(bool any);
    void gotSingleSelectedRow(bool single);

private slots:
    void loadPalette();
    void savePalette();
    void changeHue();

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    Document *mDoc;
    QTableView *mTableView;
    QAbstractItemDelegate *mDefaultDelegate;
    QAbstractItemDelegate *mTextDelegate;
};

} // namespace gmtool
