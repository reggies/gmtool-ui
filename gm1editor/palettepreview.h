#pragma once

#include <QList>

#include <QLabel>
#include <QWidget>

namespace gmtool {

class ColorsArrayWidget;
class PaletteRecord;

class PalettePreview : public QWidget
{
    Q_OBJECT

public:
    explicit PalettePreview(const PaletteRecord &record, QWidget *parent = nullptr);

    void reset(const PaletteRecord &record);

private:
    QLabel *mLabel;
    ColorsArrayWidget *mColorsArray;
};

} // namespace gmtool
