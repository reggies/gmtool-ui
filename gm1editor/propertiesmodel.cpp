#include "propertiesmodel.h"

namespace gmtool {

namespace {

enum { NameColumn, ValueColumn, LastColumn };
}

PropertiesModel::PropertiesModel(QObject *parent)
    : QAbstractTableModel(parent)
{}

void PropertiesModel::reset()
{
    beginResetModel();
    endResetModel();
}

PropertiesModel::~PropertiesModel()
{
    for (PropertyItem *item : mItems)
        delete item;
}

int PropertiesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return mItems.size();
}

QVariant PropertiesModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::EditRole) {
        if (index.column() == ValueColumn) {
            return mItems.at(index.row())->value();
        }
    } else if (role == Qt::DisplayRole) {
        if (index.column() == NameColumn) {
            return mItems.at(index.row())->name();
        } else if (index.column() == ValueColumn) {
            return mItems.at(index.row())->value();
        }
    }

    return QVariant();
}

QModelIndex PropertiesModel::index(int row, int column, const QModelIndex &parent) const
{
    if (parent.isValid())
        return QModelIndex();
    return QAbstractItemModel::createIndex(row, column);
}

int PropertiesModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return LastColumn;
}

QVariant PropertiesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation != Qt::Horizontal)
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    if (section == NameColumn)
        return tr("Property");
    else if (section == ValueColumn)
        return tr("Value");

    return QVariant();
}

Qt::ItemFlags PropertiesModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    if (index.column() == NameColumn) {
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    } else if (index.column() == ValueColumn) {
        Qt::ItemFlags f = Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        if (mItems.at(index.row())->isEditable())
            f |= Qt::ItemIsEditable;
        return f;
    }

    return Qt::NoItemFlags;
}

bool PropertiesModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    if (role != Qt::EditRole)
        return false;

    if (index.column() == ValueColumn) {
        if (mItems.at(index.row())->setValue(value)) {
            emit dataChanged(index, index);
            return true;
        }
    }

    return false;
}

} // namespace gmtool
