#pragma once

#include "window.h"

#include <QAction>
#include <QTableView>
#include <QToolBar>

namespace gmtool {
class Document;

class SpritesWindow : public Window
{
    Q_OBJECT

public:
    explicit SpritesWindow(Document *doc, QWidget *parent = nullptr);
    ~SpritesWindow() override;

    bool focusEntry(TableId table, const RecordId &id) override;

    void route(ImportSprites *action) override;
    void unroute(ImportSprites *action) override;

    void route(ExportSprites *action) override;
    void unroute(ExportSprites *action) override;

    void route(RemoveSpriteOrEntry *action) override;
    void unroute(RemoveSpriteOrEntry *action) override;

    void route(MoveForward *action) override;
    void unroute(MoveForward *action) override;

    void route(MoveBackward *action) override;
    void unroute(MoveBackward *action) override;

    void route(BringForward *action) override;
    void unroute(BringForward *action) override;

    void route(SendBack *action) override;
    void unroute(SendBack *action) override;

signals:
    void gotAnyRowSelected(bool any);

private slots:
    void add();
    void save();
    void remove();
    void bringForward();
    void sendBack();
    void moveForward();
    void moveBackward();

    void selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);

private:
    Document *mDoc;
    QTableView *mView;
};

} // namespace gmtool
