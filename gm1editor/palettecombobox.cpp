#include "palettecombobox.h"

#include "documenteditor.h"
#include "paletteitemdelegate.h"
#include "tableid.h"
#include "tablemodel.h"

#include <QListView>

namespace gmtool {

PaletteComboBox::PaletteComboBox(DocumentEditor *editor, QWidget *parent)
    : QComboBox(parent)
    , mEditor(editor)
{
    auto popupView = new QListView;
    popupView->setModelColumn(0);
    popupView->setModel(mEditor->getModel(TableId::Palette));
    popupView->setItemDelegate(new PaletteItemDelegate(editor->getModel(TableId::Palette), this));
    setModel(mEditor->getModel(TableId::Palette));
    setModelColumn(0);

    //! \note some kind of hack. the combobox wants its own size for popup
    setView(popupView);
    popupView->setMinimumWidth(popupView->sizeHintForColumn(0));
}

} // namespace gmtool
