#include "coloredit.h"

#include <QColorDialog>
#include <QHBoxLayout>
#include <QPointer>
#include <QToolButton>

namespace gmtool {

ColorEdit::ColorEdit(QWidget *parent)
    : QWidget(parent)
{
    QSizePolicy lineEditSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    QSizePolicy buttonSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    mLineEdit = new QLineEdit(this);
    mLineEdit->setSizePolicy(lineEditSizePolicy);
    mLineEdit->setFrame(false);
    mLineEdit->setInputMask("\\#>HHHHHH");
    mLineEdit->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    connect(mLineEdit, &QLineEdit::editingFinished, this, &ColorEdit::editingFinished);

    auto button = new QToolButton(this);
    button->setSizePolicy(buttonSizePolicy);
    button->setArrowType(Qt::DownArrow);
    connect(button, &QToolButton::clicked, this, &ColorEdit::buttonPressed);

    auto layout = new QHBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(mLineEdit);
    layout->addWidget(button);
}

void ColorEdit::buttonPressed()
{
    QPointer<QColorDialog> dialog = new QColorDialog(this);
    dialog->setCurrentColor(QVariant(mLineEdit->text()).value<QColor>());
    dialog->exec();
    if (dialog == nullptr)
        return;
    if (dialog->result() == QDialog::Accepted) {
        setCurrentColor(dialog->selectedColor());
    }
}

void ColorEdit::setCurrentColor(const QColor &color)
{
    mCurrentColor = color;
    mLineEdit->setText(QVariant(mCurrentColor).toString());
    emit currentColorChanged();
}

void ColorEdit::editingFinished()
{
    mCurrentColor = QColor(QVariant(mLineEdit->text()).value<QColor>());
}

QColor ColorEdit::currentColor() const
{
    return mCurrentColor;
}

} // namespace gmtool
