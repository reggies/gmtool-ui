for FILENAME in $(find . -iname '*.h') ; do git mv -f ${FILENAME} ${FILENAME%.h}.h ; done
find . -iname '*.*' -exec sed -i 's/\.h/.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/filesystem\/path\.h/#include\ <boost\/filesystem\/path.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/optional\.h/#include\ <boost\/optional.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/range\/adaptor\/map\.h/#include\ <boost\/range\/adaptor\/map.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/range\/algorithm\/copy\.h/#include\ <boost\/range\/algorithm\/copy.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/range\/algorithm\/reversed\.h/#include\ <boost\/range\/algorithm\/reversed.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/range\/adaptor\/reversed\.h/#include\ <boost\/range\/adaptor\/reversed.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/filesystem\.h/#include\ <boost\/filesystem.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/program_options\.h/#include\ <boost\/program_options.h/g' {} \;
find . -iname '*.*' -exec sed -i 's/#include\ <boost\/filesystem\/fstream\.h/#include\ <boost\/filesystem\/fstream.h/g' {} \;
git add -u
